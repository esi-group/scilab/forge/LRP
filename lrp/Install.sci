//
// This script and function is used to read paths to the LRP toolkit
//   To execute it, issue
//   exec('Install.sci');
//

clear lrpReadPaths LRP_OPTIONS
clearglobal LRP_OPTIONS

// set up correct file name
fileNameDirectory=fullfile("etc","config");


function lrpReadPaths(fileNameDirectory)

//open config file for rading pathes
 [__lrp.fd, __lrp.err]=mopen(fileNameDirectory,"r");
 if (__lrp.err ~= 0) then
		printf("****************************************************************\n");
		printf("No file: ''%s''\nin current directory: ''%s''\n",fileNameDirectory, pwd());
		printf("You should execute Install.sci from LRP home directory.\n");
		printf("****************************************************************\n");
    error("No file.");
 end

__lrp.line_num=0;

__lrp.names=[];
__lrp.paths=[];
__lrp.i=0;

while meof(__lrp.fd)==0
   __lrp.line_1=mgetl(__lrp.fd,1);
   __lrp.line_num=__lrp.line_num+1;

   if ~isempty(__lrp.line_1) then
      //find a space and remove it from a string
      __lrp.str_tmp=strsubst(__lrp.line_1, " ","");
      //find first occurence '//'
      __lrp.str_poz=strindex(__lrp.str_tmp,"//");

      //ignore comments if first occurence '//'  is equal 1
      if (__lrp.str_poz==1) then
         continue;
      else
         __lrp.str_poz_i=strindex(__lrp.line_1,"=");
         if (isempty(__lrp.str_poz_i)) then
						mclose(__lrp.fd);
						printf("******************************************************************\n");
            printf("No sign ''='' in file ''%s''  - line# %d\n",fileNameDirectory,__lrp.line_num);
						printf("******************************************************************\n");
						error("No sign ''='' in file.");
         end
         __lrp.str_name=part(__lrp.line_1,[1: __lrp.str_poz_i-1]);
         __lrp.str_value=part(__lrp.line_1,[__lrp.str_poz_i+1: length(__lrp.line_1)]);

         //name
         //find character different from space (32) and tab (9)
         __lrp.ind=find(ascii(__lrp.str_name)~=32 & ascii(__lrp.str_name)~=9);
         //check if the difference is equal to 1
         __lrp.ind1=find((__lrp.ind($:-1:2)-__lrp.ind($-1:-1:1))>1);
         if ~isempty(__lrp.ind1) then
						mclose(__lrp.fd);
						printf("******************************************************************\n");
            printf("Wrong ''name'' in file ''%s'' - line# %d\n",fileNameDirectory,__lrp.line_num);
						printf("******************************************************************\n");
            error("Wrong ""name"".");
         end
         //space to empty string
         __lrp.str_name=strsubst(__lrp.str_name, ascii(32),"");
         //tab to empty string
         __lrp.str_name=strsubst(__lrp.str_name, ascii(9),"")
         //if someone writes `           = '\home\scilab\lrp'` without name before the `=` sign
         if length(__lrp.str_name)==0 then
						mclose(__lrp.fd);
						printf("**********************************************************\n");
						printf("The ''name value'' must have a name in file:\n''%s''  - line# %d\n",fileNameDirectory,__lrp.line_num);
						printf("**********************************************************\n");
						error("Set a ""name"".");
         end

         //value
         //find ' or "
         __lrp.inda=find(ascii(__lrp.str_value)==ascii(""""));
         __lrp.indb=find(ascii(__lrp.str_value)==ascii(''''));
         if ((length(__lrp.inda)<=1) & (length(__lrp.indb)<=1) ...
            | (length(__lrp.inda)>=3) & (length(__lrp.indb)>=3)) then
	  					mclose(__lrp.fd);
							printf("******************************************************************\n");
							printf("The path must be enclosed in (double or single quotes)\n""path"" or ''path'' in file ''%s'' - line# %d.\n",fileNameDirectory,__lrp.line_num);
							printf("******************************************************************\n");
							error("Set the quotes.");
         end

         if (length(__lrp.inda)==2) then
            __lrp.str_value=part(__lrp.str_value,[__lrp.inda(1)+1 : __lrp.inda(2)-1]);
         elseif (length(__lrp.indb)==2) then
            __lrp.str_value=part(__lrp.str_value,[__lrp.indb(1)+1 : __lrp.indb(2)-1]);
         end

         __lrp.name=convstr(__lrp.str_name,"l");  //convert names to lower case
         __lrp.value=__lrp.str_value;
         __lrp.i=__lrp.i+1;
         __lrp.info=0;

         select __lrp.name
            //lrp home dir
            case "lrphomedir"  then
               __lrp.info=1;
            // latex dir
            case "latexdir" then
               __lrp.info=1;
            case "resultsdir" then
               __lrp.info=1;
            else
               break
         end //select
         if (__lrp.info==1) then
            __lrp.names(__lrp.i)=__lrp.name;
            __lrp.value = fullfile(strsubst(__lrp.value,'*',SCI),'');
            __lrp.paths(__lrp.i)=__lrp.value;
         end
      end
   end
end //while
mclose(__lrp.fd);

//define global variable
global LRP_OPTIONS

//define structure LRP_OPTIONS
LRP_OPTIONS=tlist(['lrp_options';...
    "menu";...  //menu lrp
    "path";...  //path to toolkit 'lrp'
    "latex_path"; ... //path to latex
    "results_path"],[],[],[],[]...//path to results in the LaTeX format
);


//path to LRP toolkit
__lrp.ind=find(__lrp.names == "lrphomedir");
LRP_OPTIONS.path=__lrp.paths(__lrp.ind);
if length(LRP_OPTIONS.path)==0 then
	printf("**************************************************************\n");
	printf("The path to LRP home cannot be empty.\nSet a proper path in file:\n''%s''\n",fullfile(pwd(),fileNameDirectory));
	printf("**************************************************************\n");
	error("The path to LRP home cannot be empty.");
end

//path to latex
__lrp.ind=find(__lrp.names == "latexdir");
LRP_OPTIONS.latex_path=__lrp.paths(__lrp.ind);
if length(LRP_OPTIONS.latex_path)==0 then
	LRP_OPTIONS.latex_path=[];
end
__lrp.ind=find(__lrp.names == "resultsdir");
LRP_OPTIONS.results_path=__lrp.paths(__lrp.ind);
if length(LRP_OPTIONS.results_path)==0 then
   // If no patch is defined, assume LRP_OPTIONS.path\results
  LRP_OPTIONS.results_path=fullfile(LRP_OPTIONS.path,"results");
end

//menu LRP
LRP_OPTIONS.menu=[...
    "exec(fullfile(LRP_OPTIONS.path, ''macros'',''gui'',''LRP_Gui.sce''),-1);"; ...
    "exec(fullfile(LRP_OPTIONS.path, ''macros'',''library'',''createLibrary.sci''),-1);"; ...
    "exec(fullfile(LRP_OPTIONS.path, ''macros'',''library'',''loadLibrary.sci''),-1); printf(''Libraries loaded.\nPress ENTER.'')" ...
];


// Setup the Scilab menu
try
    delmenu("LRP");
    addmenu("LRP", [...
        "Start GUI";...
        "Create libraries";...
        "Load libraries"],...
        list(0,'LRP_OPTIONS.menu')...
    );
catch
    printf("You are in console mode - no GUI is available for the ''LRP Toolkit''.\n");
end;    

endfunction

global LRP_OPTIONS

try
	//read paths
	lrpReadPaths(fileNameDirectory);
catch
	clear lrpReadPaths LRP_OPTIONS
	clearglobal LRP_OPTIONS
	error(lasterror(%t));
end


// if (isempty(fileinfo(fullfile(LRP_OPTIONS.path,'/macros/library/createHelpFiles.sci'))))
// 	printf("***************************************************************\n");
//   printf("Unable to find file createHelpFiles.sci in %s\n",fullfile(LRP_OPTIONS.path,'/macros/library/'));
//   printf("Probably your path to LRP home is incorrectly set.\nCheck file:\n''%s''\n",fullfile(pwd(),fileNameDirectory));
// 	printf("***************************************************************\n");
// 	clear lrpReadPaths LRP_OPTIONS
// 	clearglobal LRP_OPTIONS
// 	error("Probably your path to LRP home is incorrectly set.");
// end
// try
// 	//load library
// 	exec(fullfile(LRP_OPTIONS.path, "/macros/library/createHelpFiles.sci"),-1);
// catch
// 	printf("**********************************************************************\n");
//   printf(" WARNING! LRP Toolkit core file\n");
//   printf("%s\n",fullfile(LRP_OPTIONS.path, "/macros/library/createHelpFiles.sci"));
//   printf(" cannot be successfully executed. Consider reinstalling the Toolkit\n");
// 	printf("**********************************************************************\n");
// 	clear lrpReadPaths LRP_OPTIONS
// 	clearglobal LRP_OPTIONS
// 	error("Toolkit core file damaged.");
// end

// Initialise the help files
//disp("Initialising the help files")
// First, we add all the pathes - IMPORTANT - do it BEFORE any AddHelpChapter!
//%helps=[%helps; [LRP_OPTIONS.path+'\help\misc','LRP misc']];
//%helps=[%helps; [LRP_OPTIONS.path+'\help\classic','LRP classic']];
//AddHelpChapter("misc");
//AddHelpChapter("classic");


if (isempty(fileinfo(fullfile(LRP_OPTIONS.path,'/macros/library/loadLibrary.sci'))))
	printf("***************************************************************\n");
  printf("Unable to find file loadLibrary.sci in %s\n",fullfile(LRP_OPTIONS.path,'/macros/library/'));
  printf("Probably the ''lrpHomeDir'' variable is incorrectly set in:\n''%s''\n",fullfile(pwd(),fileNameDirectory));
	printf("***************************************************************\n");
	clear lrpReadPaths LRP_OPTIONS
	clearglobal LRP_OPTIONS
	error("Probably your path to LRP home is incorrectly set.");
end
try
	//load library
  exec(fullfile(LRP_OPTIONS.path,"macros","library", "loadLibrary.sci"),-1);
catch
	printf("**********************************************************************\n");
  printf(" WARNING! LRP Toolkit core file\n");
  printf("%s\n",fullfile(LRP_OPTIONS.path, "/macros/library/loadLibrary.sci"));
  printf(" cannot be successfully executed. Consider reinstalling the Toolkit\n");
	printf("**********************************************************************\n");
	clear lrpReadPaths LRP_OPTIONS
	clearglobal LRP_OPTIONS
	error("Toolkit core file damaged.");
end

printf("Version:  0.31\n");
cd(LRP_OPTIONS.path);
printf("Current directory:\n  ''%s''\n", pwd());


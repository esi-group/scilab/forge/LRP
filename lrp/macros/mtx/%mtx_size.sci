function result=%mtx_size(a_mtx, dimension)
//size(m,dimension)
//size(m)

// Check the rhs size
 numOfRHS=argn(2);
 if numOfRHS==2 then
		result=size(a_mtx.mat,dimension);
 	else
 		if numOfRHS==1 then
		 	result=size(a_mtx.mat);
		else
			error(sprintf('Wrong number of args to %mtx_size. Required: 1 or 2, got: %d.',numOfRHS));
		end
 	end
endfunction 

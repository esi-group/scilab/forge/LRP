function result=%mtx_s_mtx(mtxLeft,mtxRight)
	// mtxLeft-mtxRight
	if or(mtxLeft.idxMin~=mtxRight.idxMin) then
		error(sprintf('Both indices must be equal. Got - left: %s, right: %s.', ...
			sci2exp(mtxLeft.idxMin),sci2exp(mtxRight.idxMin) ...
		)); 
	end 
	if or(size(mtxLeft.mat)~=size(mtxRight.mat)) then
		error(sprintf('Both sizes must be equal. Got - left: %s, right: %s.', ...
			sci2exp(size(mtxLeft.mat)),sci2exp(size(mtxRight.mat)) ...
		)); 
	end 
	result=mtxLeft;
	result.mat=mtxLeft.mat-mtxRight.mat;
endfunction 

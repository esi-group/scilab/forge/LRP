function result=%mtx_floor(a_mtx)
 // floor(m)
 result=a_mtx;
 result.mat=floor(a_mtx.mat);
endfunction

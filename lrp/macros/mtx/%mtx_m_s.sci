function result=%mtx_m_s(a_mtx,a_matrix)
 // m*rand(m.size)

 mtxSize = size(a_mtx.mat);
 matrixSize = size(a_matrix);
 
 //Check the sizes first
	// As a special exception, we allow multiplication by scalar value 
 if or(matrixSize~=[1,1]) then
	 if or(mtxSize(2)~=matrixSize(1)) | or(mtxSize(3:$)~=matrixSize(3:$))
		// We need to display the error neatly, First, check the matrix dimension
		if length(size(a_matrix))>2 then
		 	error(sprintf('Inconsistent multiplication. Required size: [%d,*,%s], got: [%d,*,%s].', ...
			 	mtxSize(2), ...
				sci2exp(mtxSize(3:$)), ...
				matrixSize(1), ...
			 	sci2exp(matrixSize(3:$)) ...
			 ));
		else
		 	error(sprintf('Wrong size of the right-hand matrix. Required: [%d,*], got: %s.', ...
			 	mtxSize(2), ...
				sci2exp(matrixSize) ...
			 )); // This error is number 10 in Scilab			  	
		end
	 end
 end
 result=a_mtx;
 result.mat=result.mat*a_matrix;
endfunction

function result=%mtx_f_s(leftMtx, a_matrix)
// [mtx; a_matrix]  

// size(leftMtx,2) must be equal to size(a_matrix,2)
//  Furthermore the size(leftMtx,3:$) must be equal 
//  to size(a_matrix,3:$)  
	leftSize=size(leftMtx);
	rightSize=size(a_matrix);
	if leftSize(2)~=rightSize(2) | or(leftSize(3:$)~=rightSize(3:$)) then
		if length(leftSize)>2
			strLeftMtxSize = sci2exp(leftSize(3:$));
			if length(strLeftMtxSize)>1 then
				strLeftMtxSize = part(strLeftMtxSize,[2:length(strLeftMtxSize)-1]);
			end 
			error(sprintf('Wrong right-hand size. Need: [#,%d,%s], got: %s.', ...
				leftSize(2),strLeftMtxSize, sci2exp(rightSize) ... 	
			));
		else
			error(sprintf('Wrong right-hand size. Need: [#,%d], got: %s.', ...
				leftSize(2), sci2exp(rightSize) ... 	
			));
		end
	end		
	result=leftMtx;
	result.mat=[leftMtx.mat; a_matrix];

endfunction

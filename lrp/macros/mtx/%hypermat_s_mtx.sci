function result = %hypermat_s_mtx(a_hypermat, a_mtx)
// hypermat - mtx => hypermat
  result = a_hypermat - a_mtx.mat;
endfunction

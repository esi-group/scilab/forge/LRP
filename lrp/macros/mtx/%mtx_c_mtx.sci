function result=%mtx_c_mtx(leftMtx, rightMtx)
// [m m]  

// Second index must form a continuous region -
//    when the last element of leftMtx has index 'a' the 
//    first of rightMtx must have index a+1.
//  TO OVERRIDE THIS CHECK USE:
//    [leftMtx rightMtx.mat];
//  CAUTION: THIS WILL NOT PERFORM *ANY* INDICES CHECKS. 
//    Make sure that *ALL* the indices are right before 
//    using this trick.

// First indices must be equal
	if leftMtx.idxMin(1)~=rightMtx.idxMin(1) then
		error(sprintf('Wrong right-hand index. Need: [%d,*], got: [%d,*].', ...
			leftMtx.idxMin(1), rightMtx.idxMin(1) ... 	
		));
	end
// Second ones must form a continuous region -
//    when the last element of leftMtx has index 'a' the 
//    first of rightMtx must have index a+1.
	if leftMtx.idxMax(2)+1~=rightMtx.idxMin(2) then
		error(sprintf('Discontinuous indices. Right idx needed: [#,%d,*], got: [#,%d,*].', ...
			leftMtx.idxMax(2)+1, rightMtx.idxMin(2) ... 	
		));		
	end
// Third ones and so on must be equal
	if or(leftMtx.idxMin(3:$)~=rightMtx.idxMin(3:$)) then
		strLeftMtxSize = sci2exp(leftMtx.idxMin(3:$));
		if length(strLeftMtxSize)>1 then
			strLeftMtxSize = part(strLeftMtxSize,[2:length(strLeftMtxSize)-1]);
		end 
		strRightMtxSize = sci2exp(rightMtx.idxMin(3:$));
		if length(strRightMtxSize)>1 then
			strRightMtxSize = part(strRightMtxSize,[2:length(strRightMtxSize)-1]);
		end
		error(sprintf('Wrong right-hand index. Need: [#,#,%s], got: [#,#,%s].', ...
			strLeftMtxSize, strRightMtxSize ... 	
		));
	end		
// size(leftMtx,1) must be equal to size(rightMtx,1)
//  Furthermore the size(leftMtx,3:$) must be equal 
//  to size(rightMtx,3:$)  
	leftSize=size(leftMtx);
	rightSize=size(rightMtx);
	if leftSize(1)~=rightSize(1) | or(leftSize(3:$)~=rightSize(3:$)) then
		strLeftMtxSize = sci2exp(leftSize(3:$));
		if length(strLeftMtxSize)>1 then
			strLeftMtxSize = part(strLeftMtxSize,[2:length(strLeftMtxSize)-1]);
		end 
		error(sprintf('Wrong right-hand size. Need: [%d,#,%s], got: %s.', ...
			leftSize(1),strLeftMtxSize, sci2exp(rightSize) ... 	
		));
	end		
	result=leftMtx;
	result.mat=[leftMtx.mat rightMtx.mat];
endfunction

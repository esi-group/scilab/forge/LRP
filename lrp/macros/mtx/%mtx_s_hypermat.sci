function result = %mtx_s_hypermat(a_mtx, a_hypermat)
// mtx - hypermat => mtx
//   result = mlist(["mtx","mat","idxMin"],a_mtx.mat - a_hypermat,a_mtx.idxMin);
  result = %mtx_s_s(a_mtx,a_hypermat);
endfunction

function [mtx]=mtx_pack(mtx)
// Removes as many single dimensions as possible, starting from the rightmost one
//  Example: M=new_mtx([1,2,3],rand(3,2,1));
//  mtx_pack(M) creates a new CoolMatrix of dimension 3x2.
requireType("mtx",mtx,"CoolMatrix");
s = size(mtx);
idx=length(s);
// We must leave at least two indices
while ((s(idx)== 1) & (idx>2)), idx=idx-1; end;
if idx==length(s)
   // Nothing to pack
   return
end
mtx.idxMin = mtx.idxMin(1:idx);
mtx.mat = matrix(mtx.mat,s(1:idx));
endfunction

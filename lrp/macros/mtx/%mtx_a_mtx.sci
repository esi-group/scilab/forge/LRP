function result=%mtx_a_mtx(mtxLeft,mtxRight)
	// mtxLeft+mtxRight
	// Do not check for indices - a(1,1)+a(1,3) is legal (if both values exist,
  // of course). The indices are taken from the left-hand side operand.
// 	if or(mtxLeft.idxMin~=mtxRight.idxMin) then
// 		error(sprintf('Both indices must be equal. Got - left: %s, right: %s.', ...
// 			sci2exp(mtxLeft.idxMin),sci2exp(mtxRight.idxMin) ...
// 		));
// 	end
// 	if or(size(mtxLeft.mat)~=size(mtxRight.mat)) then
// 		error(sprintf('Both sizes must be equal. Got - left: %s, right: %s.', ...
// 			sci2exp(size(mtxLeft.mat)),sci2exp(size(mtxRight.mat)) ...
// 		));
// 	end
	result=mtxLeft;
	result.mat=mtxLeft.mat+mtxRight.mat;
endfunction 

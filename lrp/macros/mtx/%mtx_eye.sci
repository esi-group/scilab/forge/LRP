function result=%mtx_eye(a_mtx)
 // eye(m)
 result=a_mtx;
 result.mat=eye(a_mtx.mat);
endfunction

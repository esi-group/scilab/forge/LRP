function m=new_mtx(indices, a_matrix);
	// Creates a new CoolMatrix. 
	// Arguments:
	//   Indices - vector of start indices; ndims(a_matrix) must 
  //                be equal to the length of this vector. 
  //                indices(k) denotes the start index of k-th dimension of 
  //                the created matrix.
	//  a_matrix - any matrix
	// Note that ndims(a_matrix) must be equal to 
	//		the lenght of the 'indices' argument
  // Example 
  //  M=new_mtx([-1,1],[2,3,4,5; 6,7,8,9]) denotes a 2x4 matrix. 
  //   Valid indices are [-1..0, 1..4] 
  //   Note that index "0" is allowed, M(0,2)=7.  

	// WARNINGS: 
	// The CoolMatrix is stored as a TList:
	// The 2nd field must hold the matrix, (i.e, 'mat'), otherwise 
	//    everything will break.

	// The 3rd field must hold the minimal index, the idxMin, otherwise 
	//    everything will break.
	
	// Caution! The Scilab's notation of dynamically increasing matrices is NOT supported
	//  You cannot write (see in the above example) M(1,2)=0. The dimension of the CoolMatrix 
	//  can only be changed by assigning a larger object, e.g. type M=[M [2;3]] to change the
	//  dimension of M to 2x5.
	
  if (exists('indices')==0) then
     error('Call this as ''new_mtx(indices, a_matrix)''. The ''indices'' argument is missing.');
  end
  if (exists('a_matrix')==0) then
     error('Call this as ''new_mtx(indices, a_matrix)''. The ''a_matrix'' argument is missing.');
  end
	// Always check the boundaries
	if ~isvector(indices) & length(indices)>1 then
		error(sprintf('The argument ''indices'' must be a vector. Current size: %s',sci2exp(size(indices))));
	end
	if or(ndims(a_matrix) >length(indices)) 
		error(sprintf('ndims(a_matrix)=%d must be <= length(indices)=%d.',ndims(a_matrix),length(indices)));
  else
  	if or(ndims(a_matrix) <length(indices)) 
  		//error(sprintf('ndims(a_matrix)=%d must be >= length(indices)=%d.',ndims(a_matrix),length(indices)));
      // We want to 
      a_matrix = hypermat([size(a_matrix),ones(1,length(indices)-ndims(a_matrix))],a_matrix);
  	end
	end
// 	if length(indices)<ndims(a_matrix) then
// 		indices=[indices, ones(ndims(a_matrix)-length(indices),1)];
// 	end
  m=mlist(["mtx","mat","idxMin"],a_matrix, indices);
endfunction

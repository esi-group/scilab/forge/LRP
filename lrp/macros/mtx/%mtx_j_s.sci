function result=%mtx_j_s(a_mtx,a_scalar)
 // m.^1
 result=a_mtx;
 result.mat=result.mat.^a_scalar;
endfunction

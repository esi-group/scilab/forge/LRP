function result=%mtx_t(a_mtx)
	// a_mtx' - transpose operation	
	if ndims(a_mtx.mat)>2 then
		error(sprintf('Transpose supported only for 1 or 2 dimensions. This has %d.',length(a_mtx.idxMin)));
	end
	result=a_mtx;
	result.mat=result.mat';
	// As the matrix is transposed,
	//   so must be the indices
	idx=result.idxMin;
	result.idxMin=[idx(2),idx(1),idx(3:$)];
endfunction 

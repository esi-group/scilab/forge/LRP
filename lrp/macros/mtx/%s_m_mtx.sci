function result=%s_m_mtx(a_matrix,a_mtx)
 // rand(m.size)*m
 mtxSize = size(a_mtx.mat);
 matrixSize = size(a_matrix);
 
 //Check the sizes first
	// As a special exception, we allow multiplication by scalar value 
 if or(matrixSize~=[1,1]) then
		if or(matrixSize(2) ~=mtxSize(1)) then
			if length(matrixSize)>2 | length(mtxSize)>2 then
				error(sprintf('Wrong sizes. Need: L: [#,%d,*], R: [%d,#,*]. Got: L: %s, R: %s.', ...
					matrixSize(2), matrixSize(2), ...
					sci2exp(matrixSize),sci2exp(mtxSize) ...
				));
			else
				error(sprintf('Wrong sizes. Need: L: [#,%d], R: [%d,#]. Got: L: %s, R: %s.', ...
					matrixSize(2), matrixSize(2), ...
					sci2exp(matrixSize),sci2exp(mtxSize) ...
				));
			end	
		end
		if (length(matrixSize)~=length(mtxSize)) then
			error(sprintf('Number of dimensions must be equal. Got: L: %d, R: %d.', ...
				length(matrixSize), length(mtxSize) ...
			));
		end
		
 end
 result=a_mtx;
 result.mat=a_matrix*result.mat;
endfunction

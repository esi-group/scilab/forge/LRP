function result=%mtx_d_s(a_mtx, a_matrix)
// a_mtx ./ a_matrix
// As a special exception, we always allow to use a scalar.

	if or(size(a_matrix) ~=ones(size(a_matrix))) then
		if or(size(a_mtx.mat) ~=size(a_matrix)) then
			error(sprintf('Wrong right-hand matrix size. Needed: %s, got: %s.', ...
				sci2exp(size(a_mtx.mat)), sci2exp(size(a_matrix)) ...
			));
		end
	end
	// No need to check, if we are not dividing by zero. 
	//   We will not get any more information, so let's leave
	//   this to the default error handler
	result=a_mtx;
	result.mat=result.mat ./ a_matrix;
endfunction

function result=%mtx_sin(a_mtx)
// sin(m)
 result=a_mtx;
 result.mat=sin(a_mtx.mat);
endfunction 

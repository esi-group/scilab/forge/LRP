function result = %l_a_l(listA, listB)
 fun = "%"+typeof(listA) + "_a_"+typeof(listB);
 if exists(fun) then
    execstr("result = "+ fun + "(listA, listB);");
 else
     result=%nan;
     error(sprintf("Undefined operation. Check or define function %s",fun));
 end
endfunction

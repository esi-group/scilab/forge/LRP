function result=%mtx_a_s(a_mtx,a_matrix)
 // m+1 or m+[1 2; 3 4]
 // As a special exception, we always allow to use a scalar.
 
 if or(size(a_matrix) ~=ones(size(a_matrix))) then
		if or(size(a_mtx.mat)~=size(a_matrix)) then
			error(sprintf('Both sizes must be equal. Got - left: %s, right: %s.', ...
				sci2exp(size(a_mtx.mat)),sci2exp(size(a_matrix)) ...
			)); 
		end  
 end 
 result=a_mtx;
 
 result.mat=result.mat+a_matrix
endfunction

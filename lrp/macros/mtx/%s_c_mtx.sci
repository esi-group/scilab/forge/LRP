function result=%s_c_mtx(a_matrix, rightMtx)
// [a_matrix m]  

// size(a_matrix,1) must be equal to size(rightMtx,1)
//  Furthermore the size(a_matrix,3:$) must be equal 
//  to size(rightMtx,3:$)  

// CAUTION: After calling this function, the idxMin(2) is reduced by 
//   the size of the matrix. If you had a CoolMatrix 'mtx' from 2 and 
//   the matrix X of size(X,2)=2 after
//   RESULT=[X mtx] 
// 	 RESULT will start from zero, not 2. If you make
//   RESULT=[mtx X] 
//   RESULT will start from 2. Note that the resulting matrices are 
//    obviously different
//

	leftSize=size(a_matrix);
	rightSize=size(rightMtx);
	if leftSize(1)~=rightSize(1) | or(leftSize(3:$)~=rightSize(3:$)) then
		if length(leftSize)>2 then
			strLeftMatrixSize = sci2exp(leftSize(3:$));
			if length(strLeftMatrixSize)>1 then
				strLeftMatrixSize = part(strLeftMatrixSize,[2:length(strLeftMatrixSize)-1]);
			end
			error(sprintf('Wrong right-hand size. Need: [%d,#,%s], got: %s.', ...
				leftSize(1),strLeftMatrixSize, sci2exp(rightSize) ... 	
			));
		else
			error(sprintf('Wrong right-hand size. Need: [%d,#], got: %s.', ...
				leftSize(1), sci2exp(rightSize) ... 	
			));
		end		
	end		
	result=rightMtx;
	result.mat=[a_matrix, rightMtx.mat];
	// IF YOU DO NOT LIKE THE REDUCTION, REMOVE THIS LINE
	result.idxMin(2)=result.idxMin(2)-size(a_matrix,2);
	// END OF LINE TO REMOVE IF YOU DO NOT LIKE THE REDUCTION
endfunction

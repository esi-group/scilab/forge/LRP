function result=%s_i_mtx(varargin)
// m.FIELD=2;
// OR 
// m(1,:)=[3 2 1];

//Input values:
//  varargin(1) - field name (string)
//  varargin(2) - required value for field, which name is stored in varargin(1)
//  varargin($) - the mtx structured variable to have its field value set
//
// OR
//
//  varargin(1..length(varargin)-2) -  set of indices
//  varargin(length(varargin)-1)    -  new value to insert under those indices
//  varargin(length(varargin))      -  mtx type variable to modify 
// 

if type(varargin(1))==10 then
	// It is a string, so it must be a field name
		select varargin(1)
			case 'dim' then
				error(sprintf('Field %s is read-only.',varargin(1)));
			case 'idxMax' then
				error(sprintf('Field %s is read-only.',varargin(1)));
			else
				error(sprintf("Wrong field name ''%s'' in the CoolMatrix type",varargin(1)));
		end
else
	// This is a second form - the index.
	// Make the indices first
	result=varargin($);
	if length(varargin)>3 then
		if length(varargin)-2~=length(result.idxMin) then
			error(sprintf('Too many indices. Got: %d, Need 1 or %d.', ...
				length(varargin)-2,length(result.idxMin) ...
			));
		end
	else
		// We allow one index for vectors only. Vector is a 
		//	2D matrix that has size equal to [1,x] or [x,1] 
		if (result.idxMin(1)~=1 & result.idxMin(2)~=1) then
			error(sprintf('One index is allowed only for 2D vector. Call ans(*) with %d indices.', ...
				length(result.idxMin) ...
			));
		end
	end
	for idx=1:length(varargin)-2
		// Check if this is x(sth, :, sth) 
		if size(varargin(idx),1)~=-1 then
			// Nope - modify the indices to the actual - from 1 to length(idxMin)
			varargin(idx)=varargin(idx)-result.idxMin(idx)+1;
		end
	end
	// Check if user supplied us with an allowed index...
	for dim=1:length(varargin)-2
		// As I do not know of any way of checking for the "eye *" directly, 
		//  therefore I have to use the "size" trick - this stuff has always 
		//  size of MINUS ONE (-1) 
		if and(size(varargin(dim))~=[-1,-1]) then  
			if varargin(dim)<1 | max(varargin(dim))>size(result.mat,dim) then
				error(sprintf('Wrong index of dimension %d. Is: %s, Allowed range: <%d, %d>, inclusive.', ... 
											dim,sci2exp(varargin(dim)+result.idxMin(dim)-1), ...
											result.idxMin(dim),result.idxMin(dim)+size(result.mat,dim)-1 ...
										));
				result=%nan;
				return;
			end
		end
	end

  resultMat=result.mat;
  //(varargin(1:$-2))
  resultMat(varargin(1:$-2))=varargin($-1);
  result.mat=resultMat;
end
endfunction


//*****************************************************************************
// ustawienie opcji domyslnych solvera
//*****************************************************************************
function [options]=lmiSolverOptions()
Mbound = 1e3;
abstol = 1e-10;
nu = 20;
maxiters = 250;
reltol = 1e-10;
options=[Mbound,abstol,nu,maxiters,reltol];
endfunction

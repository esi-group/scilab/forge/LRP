// LRP Toolkit for Scilab.
//  
//  [ind,last]=findLRPIndex(lrp,whichField,functionName)
// 
// This function returns a given list index.
//   Internally it checks all >>whichField<< elements for field named
//  "functionName" equal to the argument of the same name (e.g. functionName=='MYFUN'). 
//  If such field is found, its index (from 1 upto 'last') is stored 
//   in 'ind'. if there are no such "functionName" fields, 'ind' is set to -1.
//  The return value of 'last' is equal to the length of checked structure.
//
//  If there is no field whose name matches the one given in 'whichField' is present, 
//  'ind' = -1
//  If there is a struct that does not have a field named "functionName" (literally), 
//   a nice-formatted error is generated and ind=-1, last=-2;  
//
// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2009-12-30 23:36:00 by L. Hladowski

function [ind,last]=findLRPIndex(lrp,whichField,functionName)
//check:  Is this entry in 'lrp'
if isThisEntry(lrp(1), whichField) == %F then
   ind=-1;  //No data found
   last=-2; //No entry found:'whichField'
   return
end

last=length(lrp(whichField));
for i=1:last
   try
     if lrp(whichField)(i).functionName==functionName then
        ind=i;
        return
     end
   catch
         // As we have caught the error, no sense to look for any useful data
         //    return the 'not found' info.
         ind=-1;
         last=-2;
         [error_message,error_number]=lasterror(%t);
         found=%F;
         if (error_number==144) then
              // Check, if we have the field "functionName"
              for x=1:size( lrp(whichField)(i)(1),1)
                 if lrp(whichField)(i)(1)(x)=="functionName" then
                    found=%T; //Yes, the error is caused by sth. else
                    break;
                 end
              end
              if found==%F then
                  warning('**********************************');
                  if whichField=="controller" then
                       warning(sprintf('*  Controller: >>%s<<',lrp(whichField)(i)(1)(1)));
                       warning('* Check a proper file in directory');
                       warning(fullfile(LRP_OPTIONS.path,'control'));
                  else
                       warning(sprintf('*  Type: >>%s<<',lrp(whichField)(i)(1)(1)));
                  end
                  warning('* does not have a required field: ""functionName""');
                  warning('* This should not have happened - consult the help.');
                  warning('**********************************');
                  error(sprintf('Field: lrp(""%s"")(%d).functionName does not exist',whichField,i));
              else
                  // Field exists - we do not know, what caused the error -
                  //    let's display the original message
                  error(error_number);
              end
         else
              // Some other error - let's display it.
              error(error_number);
         end
   end
end

//No data found
ind = -1;
endfunction

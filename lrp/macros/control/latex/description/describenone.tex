\section{Discrete Linear Repetitive process}

Discrete linear repetitive process whose state space model is of the form
\begin{align}
    \begin{split}
        x_{k+1}(p + 1) &= A x_{k+1}(p) + B u_{k+1}(p) + B_{0}y_{k}(p)\\
        y_{k+1}(p) &= C x_{k+1}(p) + D u_{k+1}(p) + D_{0} y_{k}(p)
    \end{split}
    \label{model:DLRP}
\end{align}
Here $\alpha< \infty$ denotes the pass length on pass $k,$
$x_{k}(p)$ $\in {\mathbb{R}}^{n}$ is the state vector, $y_{k}(p) \in
{\mathbb{R}}^{m}$ the pass profile vector, and $u_{k}(p)\in
{\mathbb{R}}^{l}$ is the vector of control inputs.

To complete the process description it is necessary to specify the
initial, or boundary, conditions, i.e. the pass state initial
vector sequence and the initial pass profile. Here these are taken
to be of the form
\begin{align}
    \nonumber
    x_{k+1}(0) & =  d_{k + 1},\qquad k \geq 0  \\
    y_{0}(p)   & =  f(p), \qquad 0 \leq p \leq \alpha-1
    \label{b:condition}
\end{align}
where $d_{k + 1}$ is an $n \times 1$ vector with constant entries
and $f(p)$ is an $m \times 1$ vector whose entries are known
functions of $p$.

In this paper we will be use the following Lemmas and Theorems
\begin{lemma}
\label{lem:schur}
Given constant matrices $W$, $L$ and $V$ of appropriate dimensions where $W=W^T$
and $V=V^T>0$, then
\begin{align}
    W+L^TVL < 0
    \label{schur:0}
\end{align}
if and only if
\begin{align}
    \begin{bmatrix}
        W & L^T\\
        L & -V^{-1}
    \end{bmatrix} < 0
    \label{schur:1}
\end{align}
or, equivalently,
\begin{align}
    \begin{bmatrix}
        -V^{-1} & L\\
        L^T & W
    \end{bmatrix} < 0.
    \label{schur:2}
\end{align}
The matrix $W+L^TVL$ is known as the Schur complement of the matrix
in~\eqref{schur:1} or~\eqref{schur:2}.
\end{lemma}


\begin{theorem}
\label{th:stab:none:0}
Discrete linear repetitive described by~\eqref{model:DLRP} and
\eqref{b:condition} are stable along the pas if, and only if, the 2D
characteristic polynomial
\begin{align}
 \label{th:eq:1}
 C(z_1,z_2):=\det
 \begin{bmatrix}
    I_n-z_1A & -z_1B_0\\
    -z_2C & I_m - z_2D_0
 \end{bmatrix} \ne 0
 \qquad \forall (z_1, z_2) \in \bar{U}^2
\end{align}
where $\bar{U}^2 = \diag{(z_1, z_2):|z_1| \leq 1, |z_2| \leq 1}$.
\end{theorem}

\begin{theorem}
\label{th:stab:none:1}
Discrete linear repetitive processes described by~\eqref{model:DLRP} are stable
along the pass if $\exists$ matrices $P=P^T>0$ and $Q=Q^T>0$ satisfying the
following LMI
\begin{align}
    \begin{bmatrix}
        \widehat{A}^T_1 P \widehat{A}_1 + Q -P & \widehat{A}^T_1 P \widehat{A}_2\\
        \widehat{A}^T_2 P \widehat{A}_1 & \widehat{A}^T_2 P \widehat{A}_2 - Q
    \end{bmatrix}<0
    \label{th:stab:1}
\end{align}
\end{theorem}
\begin{proof}
Using $*$ to denote the complex conjugate transpose operation, first
pre-multiply \eqref{th:stab:1} by
$
\begin{bmatrix}z_1^{*}I_n & z_2^{*}I_m \end{bmatrix}
$
and post-multiply it by the complex conjugate transpose of this last matrix to
yield
\[
(z_1\widehat{A}_1+z_2\widehat{A}_2)^{*} P (z_1\widehat{A}_1+z_2\widehat{A}_2)+
z_1^{*}Qz_1-z_1^{*}Pz_1-z_2^{*}Qz_2<0.
\]
Also for \eqref{th:stab:1} to hold we must have that $Q-P<0$ and hence
$(\forall (z_1, z_2) \in \bar{U}^2)$
\begin{align*}
z_1^{*}Qz_1-z_1^{*}Pz_1-z_2^{*}Qz_2&=z_1^{*}(Q-P)z_1-z_2^{*}Qz_2\\
&=-(z_1^{*}(P-Q)z_1+z_2^{*}Qz_2)\\
&\geq -(P-Q+Q)=-P
\end{align*}
Consequently for \eqref{th:stab:1} to hold
$r(z_1\widehat{A}_1+z_2\widehat{A}_2)<1$, $\forall (z_1, z_2) \in \bar{U}^2$.
This in turn implies that
\[
  \det{I_{n+m}-z_1\widehat{A}_1 - z_2\widehat{A}_2} \ne 0, \qquad
  \forall (z_1, z_2) \in \bar{U}^2
\]
which is equivalent to (\ref{th:eq:1}). This completes the proof.
\end{proof}

Introduce the following matrices
\begin{align}
    \widehat{A}&=\begin{bmatrix} A & B_0\\ C & D_0 \end{bmatrix},
    &
    \widehat{A}_1&=\begin{bmatrix} A & B_0\\ 0 & 0 \end{bmatrix},
    &
    \widehat{A}_2&=\begin{bmatrix} 0 & 0\\ C & D_0 \end{bmatrix},
    &
    \widetilde{A}_1&=\begin{bmatrix} A & 0\\ C & 0 \end{bmatrix},
    &
    \widetilde{A}_2&=\begin{bmatrix} 0 & B_0\\ 0 & D_0 \end{bmatrix},
    \label{mat:A}
    \\
    \widehat{B}&=\begin{bmatrix} B & 0\\ 0 & D \end{bmatrix},
    &
    \widehat{B}_1&=\begin{bmatrix} B\\ 0\end{bmatrix},
    &
    \widehat{B}_1&=\begin{bmatrix} 0\\ D\end{bmatrix}.
    \label{mat:B}
\end{align}

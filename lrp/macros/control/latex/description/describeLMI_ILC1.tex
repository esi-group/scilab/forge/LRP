\section{Controler ILC 1}
Iterative learning control (ILC) is a technique for controlling
systems operating in a repetitive (or pass-to-pass) mode with the
requirement that a reference trajectory $r_{ref}(p)$ defined over a finite
interval $0 \leq p \leq \alpha-1$ is followed to a high precision. Examples
of such systems include robotic manipulators that are required to
repeat a given task to high precision, chemical batch processes or,
more generally, the class of tracking systems.

Since the original work~\cite{Arimoto84} in the mid 1980's, the
general area of ILC has been the subject of intense research effort.
One possible initial source for the literature here is the survey
paper~\cite{Hyo-Sung2007}. One approach in ILC is to construct the input to the
plant or process from the input used on the last trial plus an
additive increment which is typically a function of the past values
of the measured output error, i.e. the difference between the
achieved output on the current pass and the desired plant output. As
such, it places the analysis of ILC schemes firmly outside standard
(or 1D) control theory --- although it is still has a significant
role to play in certain cases of practical interest.

In essence, ILC has the structure of a repetitive process and to illustrate some
of the main features of the Toolkit (as it currently stands) we now detail the
design of an ILC control law based on an LMI setting and then give the results
of performance assessment via simulation.  The starting point is a process described
by a 1D discrete linear time invariant state-space model of the form
\begin{equation}
\label{ILCintro}
\begin{array}{rcl}
  x(p+1)&=&Ax(p)+Bu(p)\\
  y(p)&=&Cx(p)\\
  p&=&0,1,\ldots,\alpha-1
\end{array}
\end{equation}
where $x\in \mathbb{R}^{n}$ ($u\in \mathbb{R}^{r}$, $y\in
\mathbb{R}^{m}$) denotes the state, input and output vectors
respectively. The initial state state vector is taken as $x(0)=d_0$.
In many practical cases, a physical process is required to repeat the
same task over a finite duration. One example here is a gantry robot
whose task is to place items on a moving conveyer under synchronization,
i.e. collect an object (or workpiece) from a specified location and
place it on the conveyer and then return to pick up the next one and
so on. Once the operation has been completed for, say, the $kth$ item
then (in principle at least) all input, output and state dynamics
generated during this trial is available for use to update the control
input vector to be applied for item $k+1$ and so on.  This is the core
ILC problem, i.e. use information from the previous trial (or trials)
to update the control input vector applied from trial to trial in order
to sequentially improve performance and, in particular, force the system
to produce a desired trajectory, say $y_{ref}(p),$ with `acceptable'
dynamics along each trial.

To introduce the ILC setting, we use the integer subscript $k\geq0$
to denote the current trial and rewrite the model
of~(\ref{ILCintro}) as
\begin{equation}
\label{ILCEq1}
\begin{array}{rcl}
  x_{k}(p+1)&=&Ax_k(p)+Bu_k(p)\\
  y_k(p)&=&Cx_k(p)
\end{array}
\end{equation}
and take the control objective to force the tracking error on trial $k$ as
\begin{equation}
  \label{error}
  e_k(p)=y_{ref}(p)-y_k(p)
\end{equation}
$p = 0, 1, \cdots, p-1, k \geq 1$, where $y_{ref}(p)$ denotes the reference signal to be learnt.
Then a known result is that, in its
strongest form, convergence of the ILC scheme is equivalent to the property of linear
constant pass length repetitive processes known as stability along the pass. In the repetitive
process case, this is equivalent to uniform bounded input bounded output stability (defined in
terms of the norm on the underlying function space), i.e. independent of the pass length.

Introduce now the following additional variables, (i.e. the state increment from trial-to-trial
$\eta_{k+1}(p)$ and the input update $\Deltau{k+1}(p)$)
\begin{equation}
\label{lrpVariables}
\begin{array}{rcl}
  \eta_{k+1}(p+1)&=&x_{k+1}(p)-x_k(p)\\
  \Delta u_{k+1}(p)&=&u_{k+1}(p)-u_k(p)
\end{array}
\end{equation}
Then
\begin{equation}\label {eta}
  \eta_{k+1}(p+1)=A\eta_{k+1}(p)+B\Deltau{k+1}(p-1)
\end{equation}
and consider also a control law of the form
\begin{equation}
 \label{controller}
 \Deltau{k+1}(p)=K_1\eta_{k+1}(p+1)+K_2e_{k}(p+1)
\end{equation}
Then we can write the following state-space model for the error dynamics of the considered ILC scheme
\begin{equation}
\label{ILC_closedLoop}
\begin{array}{rcl}
  \eta_{k+1}(p+1)&=&\hat A\eta_{k+1}(p) +\hat B_0 e_{k}(p)\\
  e_{k+1}(p)     &=&\hat C\eta_{k+1}(p) +\hat D_0 e_{k}(p)
\end{array}
\end{equation}
where
\begin{equation*}
\begin{array}{rcl}
  \hat A &=& A+BK_1\\
  \hat B_0 &=& BK_2\\
  \hat C &=& -C(A+BK_1)\\
  \hat D_0 &=& (I-CBK_2) \\
\end{array}
\end{equation*}

Following the analysis in \cite{Hladowski2007}, the solution of the ILC problem here is
equivalent to control law design for stability along the pass in the repetitive process
interpretation of the system dynamics.  The following result is an LMI based solution
to this problem

\begin{theorem}{}
The ILC problem considered here is stable along the pass
if there exist matrices $X_1=X_1^T\succ 0$ and $X_2=X_2^T\succ 0$, $R_1$ and $R_2$ for which
the following LMI is feasible
\begin{equation*}
\begin{array}{c}
\begin{array}{cc}
    M=
    \left[
    \begin{array}{cccc}
        -X_1                                    &   0          \\
        0                                       & -X_2         \\
        AX_1+BR_1 & BR_2\\
        -CAX_1-CBR_1 & X_2- CBR_2\\
    \end{array}
    \right.
\end{array}
\\
\begin{array}{cccc}
\label{matrixM}
\left.
\begin{array}{cc}
   X_1A^T+R_1^TB^T  & -X_1A^TC^T-R_1^TB^TC^T\\
   R_2^TB^T                          & X_2-R_2^TB^TC^T\\
    -X_1                                          & 0 \\
     0                                            & -X_2
\end{array}
\right]\prec 0
\end{array}
\end{array}
\end{equation*}
If this condition holds, the  control matrices $K_1$ and $K_2$ in (\ref{controller}) are given by
\begin{equation*}
\begin{array}{rcl}
 K_1&=&  R_1X_1^{-1}\\
 K_2&=&  R_2X_2^{-1}
\end{array}
\end{equation*}
\label{pppp}
\end{theorem}

function writeLMI_ILC1(f,lrp)
//------------------------------------------------------------------------------
name="lrp_Sys_Cla_C_I1";
[ind]=findLRPIndex(lrp,"controller",name);
if ind==-1 then
    error("No such conntroller: "+ name);
end
//------------------------------------------------------------------------------
c=lrp.controller(ind);
c1=lrp.controller(1);
//------------------------------------------------------------------------------
matricesLst=tlist(["MatricesLst";...
     "nameOfMat";...
     "mat"],...
     list(),list()...
);
//------------------------------------------------------------------------------
mfprintf(f,"Matrices of the stable system are\n");
matricesLst.nameOfMat=list("A", "B", "B_0", "C", "D", "D_0");
matricesLst.mat=list(c.Ad, c1.B, c.B0d, c.Cd, c1.D, c.D0d);
saveMat(f,matricesLst);
//------------------------------------------------------------------------------
mfprintf(f,"Matrices from LMI controller are\n");
matricesLst.nameOfMat=list("X_1", "X_2", "R_1", "R_2");
matricesLst.mat=list(c.X1, c.X2, c.R1, c.R2);
saveMat(f,matricesLst);
//------------------------------------------------------------------------------
mfprintf(f,"Matrices of controller are\n");
matricesLst.nameOfMat=list("K_1", "K_2");
matricesLst.mat=list(c.K1, c.K2);
saveMat(f,matricesLst);
//------------------------------------------------------------------------------
endfunction

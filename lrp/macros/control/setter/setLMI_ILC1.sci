// LRP Toolkit for Scilab. This function sets the
//   values calculated by setLMI_ILC1
//
// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-12-07 12:46:00

function [lrp]=setLMI_ILC1(lrp)
  requireType("lrp",lrp,"tlist","lrp_Sys_Cla");
[ind]=findLRPIndex(lrp,"controller","LMI_ILC1");
if ind<1 then
	error(printf("Invalid index=%d",ind));
	return
end
if lrp.controller(ind).solutionExists then
  lrp.mat.A=lrp.controller(ind).Ad;
  lrp.mat.B0=lrp.controller(ind).B0d;
  lrp.mat.C=lrp.controller(ind).Cd;
  lrp.mat.D0=lrp.controller(ind).D0d;
  //no change in these matrices
  lrp.mat.D=lrp.controller(1).D;
  lrp.mat.B=lrp.controller(1).B;
  lrp.indController = ind;
else
  lrp.mat.A=[];
  lrp.mat.B0=[];
  lrp.mat.C=[];
  lrp.mat.D0=[];
  //no change in these matrices then we rewrite it from none at index 1
  lrp.mat.D=[];
  lrp.mat.B=[];
  lrp.indController = ind;
end

endfunction

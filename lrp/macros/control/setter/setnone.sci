function [lrp]=setnone(lrp)
  requireType("lrp",lrp,"tlist","lrp_Sys_Cla");
// This controller always has solutions --- this is an open loop system after all...
  lrp.mat.A=lrp.controller(1).A;
  lrp.mat.B=lrp.controller(1).B;
  lrp.mat.B0=lrp.controller(1).B0;
  lrp.mat.C=lrp.controller(1).C;
  lrp.mat.D=lrp.controller(1).D;
  lrp.mat.D0=lrp.controller(1).D0;
  lrp.indController = 1; // This controller is ALWAYS one, so no need to look for it...
  //disp('SETNONE!');
endfunction

// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-12-07 11:45:00
//
//*****************************************************************************
// ILC 1
//*****************************************************************************
// Article
// (International Journal of Applied Mathematics and Computer Science (AMCS)):
//
// On the development of scilab compatible software for
// analysis and control of repetitive processes
//
// LUKASZ HLADOWSKI, BLAZEJ CICHY, KRZYSZTOF GALKOWSKI, ERIC ROGERS
//
// pp.8 in Theorem 4
//*****************************************************************************
//
// X1=X1' > 0
// X2=X2' > 0
//
// R1, R2
//
// [-X1,               0,           (A*X1+B*R1)'   (-C*A*X1-C*B*R1)' ]
// [0,                -X2,          (R2'*B')       (X2-C*B*R2)'      ] < 0
// [(A*X1+B*R1),      (B*R2),       -X1            0                 ]
// [(-C*A*X1-C*B*R1)  (X2-C*B*R2)   0              -X2               ]
//
// K1=R1*inv(X1)
// K2=R2*inv(X2)


function [lrp]=LMI_ILC1(lrp)
if TCL_ExistVar("LRP_Help_Status")
   TCL_SetVar("LRP_Help_Status","**** WORKING ****");
end
ncl=lines();  // Save the current pager options
lines(0);     // Remove the [more] prompt

lrp=setnone(lrp); //Start from the original system, not the current, (possibly closed-loop) one.


// lrp.mat.D must be zero for this method to work, otherwise it will not produce useable result.
if (or(lrp.mat.D ~= 0))

    //save results to lrp.controller(indController)
    lrp.controller(lrp.indController)=tlist(["lrp_Sys_Cla_C_I1"; ...
    "functionName"; ...
    "solutionExists"],...
    "LMI_ILC1", ...        // Always the current function name
    %f);                   //no solution - false

    lrp.indController=1; // Set to NONE as no controller could be found

    warning("lrp.mat.D must be zero for this method to work.");
    return;
end


[ind,last]=findLRPIndex(lrp,"controller","lrp_Sys_Cla_C_I1");
if ind==-1 then
    lrp.indController=last+1;
else
    lrp.indController=ind;
end

X1=zeros(lrp.mat.A);
X2=zeros(lrp.mat.D0);

R1=zeros(lrp.mat.B');
R2=zeros(lrp.mat.D');

XListIn=list(X1,X2,R1,R2);

options=lmiSolverOptions();
errcatch(-1,"continue");
[XListOut]=lmisolver(XListIn,LMI_ILC1_eval,options);
if (iserror()) then

    //save results to lrp.controller(indController)
    lrp.controller(lrp.indController)=tlist(["lrp_Sys_Cla_C_I1"; ...
    "functionName"; ...
    "solutionExists"],...
    "LMI_ILC1", ...        // Always the current function name
    %f);                   //no solution - false

    lrp.indController=1; // Set to NONE as no controller could be found
    //lrp=setnone(lrp)   // lrp=setnone(lrp) has been called in the beginning of
                         // this function; if not then uncomment this line (IMPORTANT).

    errclear();
else
    [X1,X2,R1,R2]=XListOut(:);

    [m]=size(lrp.mat.C,1);

    K1=R1*inv(X1);
    K2=R2*inv(X2);

    //closed loop
    Ad  = A+B*K1;
    B0d = B*K2;
    Cd  = -C*Ad;
    D0d = eye(m,m)-C*B*K2;

    //save results to lrp.controller(indController)
    lrp.controller(lrp.indController)=tlist(["lrp_Sys_Cla_C_L3";...
    "functionName"; ...
    "displayName"; ...
    "solutionExists"; ...
    "X1";"X2";"R1";"R2";"K1";"K2";...
    "Ad";"B0d";"Cd";"D0d";...
    ],...
      "LMI_ILC1", ...                     //function name
      "ILC stabilising controller 1", ... //DisplayName
      %t, ...                             //solution exists - true
      X1,X2,R1,R2,K1,K2,...
      Ad,B0d,Cd,D0d,...
    );

    [lrp]=setLMI_ILC1(lrp);
end
lines(ncl(2),ncl(1)); // Load the previous pager options
if TCL_ExistVar("LRP_Help_Status")
   TCL_SetVar("LRP_Help_Status","Ready");
end
endfunction



function [LME,LMI,OBJ]=LMI_ILC1_eval(XListIn)
[X1,X2,R1,R2]=XListIn(:);

OBJ=[];
LME=list(X1-X1', X2-X2');

// X1 [n,n]
// X2 [m,m]
// R1 [r,n]
// R2 [r,m]
// [-X1,               0,           (A*X1+B*R1)'   (-C*A*X1-C*B*R1)' ]
// [0,                -X2,          (R2'*B')       (X2-C*B*R2)'      ] < 0
// [(A*X1+B*R1),      (B*R2),       -X1            0                 ]
// [(-C*A*X1-C*B*R1)  (X2-C*B*R2)   0              -X2               ]

ZEROS_m_n=zeros(size(lrp.mat.C));
ZEROS_n_m=ZEROS_m_n';

LMI=list( -[-X1,              ZEROS_n_m,    (A*X1+B*R1)'   (-C*A*X1-C*B*R1)'; ...
            ZEROS_m_n,        -X2,          (R2'*B')       (X2-C*B*R2)'     ; ...
            (A*X1+B*R1),      (B*R2),       -X1            ZEROS_n_m        ; ...
            (-C*A*X1-C*B*R1)  (X2-C*B*R2)   ZEROS_m_n      -X2              ]-eye(),...
            X1-eye(),...
            X2-eye());
endfunction

// LRP Toolkit for Scilab. This function is used to display the matrices in the open loop LRP
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-05 15:12:00
function []=%lrp_Sys_Cla_C_non_p(var)
//printf('  Open loop matrices:\n')
printf('  %s:\n',var.displayName);
printf('  file: %s.sci:\n',var.functionName);
//constant
STUB="STUB";

   printf('A= ');
if var.A<>STUB then
   disp(var.A);
else
   printf(' *** ??? *** \n');
end
   printf('B= ');
if var.B<>STUB then
   disp(var.B);
else
   printf(' *** ??? *** \n');
end
   printf('B0=');
if var.B0<>STUB then
   disp(var.B0);
else
   printf(' *** ??? *** \n');
end
   printf('C= ');
if var.C<>STUB then
   disp(var.C);
else
   printf(' *** ??? *** \n');
end
   printf('D= ');
if var.D<>STUB then
   disp(var.D);
else
   printf(' *** ??? *** \n');
end
   printf('D0=');
if var.D0<>STUB then
   disp(var.D0);
else
   printf(' *** ??? *** \n');
end
endfunction

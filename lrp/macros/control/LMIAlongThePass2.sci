// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-18 01:45:00 by L.Hladowski
//
// ======================================================================
// Stabilization 2
// ======================================================================
// Author  : K. Galkowski, E. Rogers, S. Xu, J. Lam, D. H. Ownes,
//           B. Sulikowski, W. Paszke
// Article: LMI based stability analisis and robsyt controller design
//          for discrete linear repetetive processes
//
// pp. 6 eq. (24)
//
// P > 0
//
// [-P             Ad*P+Bd*N]
// [                        ] < 0
// [P*Ad'+N'*Bd'  -P        ]
// ======================================================================
function [lrp]=LMIAlongThePass2(lrp)
if TCL_ExistVar("LRP_Help_Status")
  TCL_SetVar("LRP_Help_Status","**** WORKING ****");
end
ncl=lines(); // Save the current pager options
lines(0); // Remove the [more] prompt

lrp=setnone(lrp); //Start from the original system, not the current, (possibly closed-loop) one.

[ind,last]=findLRPIndex(lrp,"controller","lrp_Sys_Cla_C_L2");
if ind==-1 then
    lrp.indController=last+1;
else
    lrp.indController=ind;
end

Ad=[lrp.mat.A lrp.mat.B0; lrp.mat.C lrp.mat.D0];
Bd=blkdiag(lrp.mat.B, lrp.mat.D);

P1i=zeros(lrp.mat.A);
P2i=zeros(lrp.mat.D0);
N1i=zeros(lrp.mat.B');
N2i=zeros(lrp.mat.D');
XListIn=list(P1i,P2i,N1i,N2i);

options=lmiSolverOptions();
errcatch(-1,"continue");
[XListOut]=lmisolver(XListIn,LMIAlongThePass2_eval,options);
if (iserror()) then

    //save results to lrp.controller(indController)
    lrp.controller(lrp.indController)=tlist(["lrp_Sys_Cla_C_L2";...
      "functionName"; ...
      "displayName"; ...
      "solutionExists"], ...
     "LMIAlongThePass2", ... // Always the current function name
     "Along the pass stabilising controller 2", ... //DisplayName
     %f);    //no solution - false

    lrp.indController=1; // Set to NONE as no controller could be found
    //lrp=setnone(lrp)   // lrp=setnone(lrp) has been called in the beginning of
                         // this function; if not then uncomment this line (IMPORTANT).
    errclear();
else
    [P1,P2,N1,N2]=XListOut(:);
    N=[N1 N2; N1 N2];
    P=blkdiag(P1,P2);
    K=N*inv(P);

    [n,r]=size(lrp.mat.B);
    K1=K(1:r,1:n);
    K2=K(1:r,n+1:$);

    //closed loop
    nAd=Ad+Bd*K;
    nA=nAd(1:n,1:n);
    nB0=nAd(1:n,n+1:$);
    nC=nAd(n+1:$,1:n);
    nD0=nAd(n+1:$,n+1:$);

    //save results to lrp.controller(indController)
    lrp.controller(lrp.indController)=tlist(["lrp_Sys_Cla_C_L2";...
    "functionName"; ...
    "displayName"; ...
    "solutionExists"; ...
    "P";"P1";"P2";"N";"N1";"N2";"K";"K1";"K2";...
    "Ad";"Bd";...
    "newAd";...
    "newA";"newB0";"newC";"newD0"],...
    "LMIAlongThePass2", ...
    "Along the pass stabilising controller 2", ... //DisplayName
    %t, ... //solution exists - true
    P,P1,P2,N,N1,N2,K,K1,K2,...
    Ad,Bd,...
    nAd,...
    nA,nB0,nC,nD0);

    [lrp]=setLMIAlongThePass2(lrp);
end
lines(ncl(2),ncl(1)); // Load the previous pager options
if TCL_ExistVar("LRP_Help_Status")
  TCL_SetVar("LRP_Help_Status","Ready");
end
endfunction


function [LME,LMI,OBJ]=LMIAlongThePass2_eval(XListIn)
[P1,P2,N1,N2]=XListIn(:);

N=[N1 N2; N1 N2];
P=blkdiag(P1,P2);

OBJ=[];

LME=list(...
    P1-P1',...
    P2-P2'...
);

LMI=list(...
   -[-P,           Ad*P+Bd*N;...
     P*Ad'+N'*Bd', -P        ]-eye(),...
   P1-eye(),...
   P2-eye()...
);
endfunction

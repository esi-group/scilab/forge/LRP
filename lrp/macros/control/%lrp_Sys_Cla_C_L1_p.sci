// LRP Toolkit for Scilab. This function is used to display the results of the LMIAlongThePass1
//   controller
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-18 00:52:00
function []=%lrp_Sys_Cla_C_L1_p(var)
//printf('  LMI Along the pass 1:\n')
printf('  %s:\n',var.displayName);
printf('  file: %s.sci:\n',var.functionName);
printf('Result:\n')
if var.solutionExists==%t then
    printf("Z\n");
		disp(var.Z);    
		printf("Y\n");
		disp(var.Y);
		printf("N\n");
		disp(var.N);
		printf("N1\n");
		disp(var.N1);
		printf("N2\n");
		disp(var.N2);
		printf("K\n");
		disp(var.K);
		printf("K1\n");
		disp(var.K1);
		printf("K2\n");
		disp(var.K2);
    printf("Ad\n");
    disp(var.Ad);
		printf("Ad1\n");
		disp(var.Ad1);
		printf("Ad2\n");
		disp(var.Ad2);
		printf("Bd\n");
		disp(var.Bd);
		printf("Bd1\n");
		disp(var.Bd1);
		printf("Bd2\n");
		disp(var.Bd2);
    printf("newAd1\n");
		disp(var.newAd1);
		printf("newAd2\n");
		disp(var.newAd2);
printf('\nClosed loop matrices:\n')
    printf("newA\n");
    disp(var.newA);
		printf("newB0\n");
disp(var.newB0);		
		printf("newC\n");
disp(var.newC);		
		printf("newD0\n");
disp(var.newD0);		
else
	printf('UNABLE TO CALCULATE CONTROLLER USING %s\n',var.functionName);
end
endfunction

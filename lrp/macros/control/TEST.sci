
function [lrp]=TEST(lrp)
// Inform the GUI that we have started
if TCL_ExistVar("LRP_Help_Status")
  TCL_SetVar("LRP_Help_Status","**** WORKING ****");
end

lrp=setnone(lrp); //Start from the original system, not the current, (possibly closed-loop) one.

[ind,last]=findLRPIndex(lrp,'controller','TEST');
if ind==-1 then
    lrp.indController=last+1;
else
    lrp.indController=ind;
end
nA=lrp.mat.A+0.1;
nB0=lrp.mat.B0;
nC=lrp.mat.C;
nD0=lrp.mat.D0;

//save results to lrp.controller(indController)
lrp.controller(lrp.indController)=tlist(['TEST';...
  'functionName'; ... // REQUIRED
  "displayName"; ... // REQUIRED
  'solutionExists'; ... // REQUIRED
  'newA';'newB0';'newC';'newD0'],...
  "TEST", ... // Always the current function name
  "Test controller", ... // The name of the controller
  %t, ... //solution exists - true
  nA,nB0,nC,nD0);

[lrp]=setTEST(lrp);

// Inform the GUI that we have finished
if TCL_ExistVar("LRP_Help_Status")
  TCL_SetVar("LRP_Help_Status","Ready");
end    

endfunction

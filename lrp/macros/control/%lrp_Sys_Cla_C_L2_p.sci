// LRP Toolkit for Scilab. This function is used to display the results of the LMIAlongThePass2
//   controller
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-18 00:52:00
function []=%lrp_Sys_Cla_C_L2_p(var)
printf('  %s:\n',var.displayName);
printf('  file: %s.sci:\n',var.functionName);
//printf('  LMI Along the pass 2:\n')
printf('Result:\n')
if var.solutionExists==%t then
		printf("P\n");
		disp(var.P);
		printf("P1\n");
		disp(var.P1);
		printf("P2\n");
		disp(var.P2);
		printf("N\n");
		disp(var.N);
		printf("N1\n");
		disp(var.N1);
		printf("N2\n");
		disp(var.N2);
		printf("K\n");
		disp(var.K);
		printf("K1\n");
		disp(var.K1);
		printf("K2\n");
		disp(var.K2);
    printf("Ad\n");
    disp(var.Ad);
		printf("Bd\n");
		disp(var.Bd);
    printf("newAd\n");
		disp(var.newAd);
printf('\nClosed loop matrices:\n')
    printf("newA\n");
    disp(var.newA);
		printf("newB0\n");
disp(var.newB0);		
		printf("newC\n");
disp(var.newC);		
		printf("newD0\n");
disp(var.newD0);		
else
	printf('UNABLE TO CALCULATE CONTROLLER USING %s\n',var.functionName);
end
endfunction

// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-03-14 00:59:00 by L.Hladowski
//
// ======================================================================
// Stabilization 1
// ======================================================================
// Author:  K. Galkowski, E. Rogers, S. Xu, J. Lam, D. H. Ownes,
//          B. Sulikowski, W. Paszke
// Article: LMI based stability analisis and robust controller design
//          for discrete linear repetetive processes
//
// pp. 7, eq. (19)
//
// Y > 0
// Z > 0
// [Z-Y,         zeros(Z),    Y*Ad1'+N'*Bd1']
// [zeros(Z),    -Z,          Y*Ad2'+N'*Bd2'] < 0
// [Ad1*Y+Bd1*N, Ad2*Y+Bd2*N, -Y            ]
//
// controller is given by
// K=N*inv(Y);
// ======================================================================
function [lrp]=LMIAlongThePass1(lrp)
if TCL_ExistVar("LRP_Help_Status")
  TCL_SetVar("LRP_Help_Status","**** WORKING ****");
end
ncl=lines(); // Save the current pager options
lines(0); // Remove the [more] prompt

lrp=setnone(lrp); //Start from the original system, not the current, (possibly closed-loop) one.

[ind,last]=findLRPIndex(lrp,"controller","lrp_Sys_Cla_C_L1");
if ind==-1 then
    lrp.indController=last+1;
else
    lrp.indController=ind;
end
//prepare data
Ad1=[lrp.mat.A lrp.mat.B0; zeros(lrp.mat.C) zeros(lrp.mat.D0)];
Ad2=[zeros(lrp.mat.A) zeros(lrp.mat.B0); lrp.mat.C lrp.mat.D0];
Bd1=[lrp.mat.B; zeros(lrp.mat.D)];
Bd2=[zeros(lrp.mat.B); lrp.mat.D];

//initial values of variables
Zi=zeros(Ad1);
Yi=zeros(Ad1);
Ni=zeros(Bd1');
XListIn=list(Zi,Yi,Ni);

options=lmiSolverOptions();
errcatch(-1,"continue");
[XListOut]=lmisolver(XListIn,LMIAlongThePass1_eval,options);
if (iserror()) then

    //save results to lrp.controller(indController)
    lrp.controller(lrp.indController)=tlist(["lrp_Sys_Cla_C_L1"; ...
    "functionName"; ...
    "displayName"; ...
    "solutionExists"], ...
    "LMIAlongThePass1", ... // Always the current function name
    "Along the pass stabilising controller 3", ... //DisplayName
    %f);    //no solution - false
    lrp.indController=1; // Set to NONE as no controller could be found
    //lrp=setnone(lrp)   // lrp=setnone(lrp) has been called in the beginning of
                         // this function; if not then uncomment this line (IMPORTANT).
    errclear();
else
    [Z,Y,N]=XListOut(:);
    K=N*inv(Y);
    [n,r]=size(lrp.mat.B);

    K1=K(1:r,1:n);
    K2=K(1:r,n+1:$);

    N1=N(1:r,1:n);
    N2=N(1:r,n+1:$);


    //closed loop
    nA1d=Ad1+Bd1*K;
    nA2d=Ad2+Bd2*K;
    nAd=nA1d+nA2d;

    //new stable matrices
    nA=nAd(1:n,1:n);
    nB0=nAd(1:n,n+1:$);
    nC=nAd(n+1:$,1:n);
    nD0=nAd(n+1:$,n+1:$);
    //save results to lrp.controller(indController)
    lrp.controller(lrp.indController)=tlist(["lrp_Sys_Cla_C_L1";...
    "functionName"; ...
    "displayName"; ...
    "solutionExists"; ...
    "Z";"Y";"N";"N1";"N2";"K";"K1";"K2";...
    "Ad";"Ad1";"Ad2";"Bd";"Bd1";"Bd2";...
    "newAd1";"newAd2";"newAd";...
    "newA";"newB0";"newC";"newD0"],...
    "LMIAlongThePass1", ...
    "Along the pass stabilising controller 1", ... //DisplayName
    %t, ... //solution exists - true
    Z,Y,N,N1,N2,K,K1,K2,...
    Ad1+Ad2,Ad1,Ad2,Bd1+Bd2,Bd1,Bd2,...
    nA1d,nA2d,nAd,...
    nA,nB0,nC,nD0);
    [lrp]=setLMIAlongThePass1(lrp);
end
lines(ncl(2),ncl(1)); // Load the previous pager options
if TCL_ExistVar("LRP_Help_Status")
  TCL_SetVar("LRP_Help_Status","Ready");
end
endfunction


//function for LMI
function [LME,LMI,OBJ]=LMIAlongThePass1_eval(XListIn)
[Z,Y,N]=XListIn(:);

OBJ=[];

LME=list(...
    Z-Z',...
    Y-Y'...
);

LMI=list(...
    -[Z-Y,         zeros(Z),       Y*Ad1'+N'*Bd1'; ...
    zeros(Z),      -Z,             Y*Ad2'+N'*Bd2'; ...
    Ad1*Y+Bd1*N,   Ad2*Y+Bd2*N,    -Y]-eye(),...
    Y-eye(),...
    Z-eye()...
);
endfunction

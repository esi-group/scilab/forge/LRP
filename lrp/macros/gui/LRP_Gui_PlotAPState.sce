// LRP GUI utility script.
// This is used to make a 2D along the pass plot of state
//
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
// 
//  Institute of Control & Computation Engineering,
//  Faculty of Electrical Engineering, Computer Science and Telecommunications,
//  University of Zielona Gora,
//  POLAND
// 
//  Last revised: 2009-07-27 15:46:00

TCL_EvalStr("catch {DeactivateAll .rc}");
lrpMergedDatToPlots=createPlotDataStub();
lrpMergedDatToPlots=lrpAddPlot(lrp,"","alongthepass", "state", eval(TCL_GetVar("LRP_ProvideStateNumberToPlot_Result")), list([eval(TCL_GetVar("result"))]), list("all"), lrpMergedDatToPlots);
showInfoAboutPlot=%F;
plotBlackOrColor3DPlot="color";
lrpPlot(lrp, lrpMergedDatToPlots, plotBlackOrColor3DPlot, showInfoAboutPlot);
TCL_EvalStr("ActivateAll .rc;");
TCL_EvalStr("DeactivateMainWindow;");

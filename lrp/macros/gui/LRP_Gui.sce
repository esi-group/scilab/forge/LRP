if exists('lrp') then
   if typeof(lrp)~='lrp_Sys_Cla' then
      warning('*************************************');
      printf("lrp/macros/gui/LRP_Gui.sce:\n");
      warning('The variable named');
      warning('');
      warning('      lrp');
      warning('');               
      warning('must be of type');
      warning('');      
      warning('   lrp_Sys_Cla')
      warning('');      
      warning('The LRP Toolkit''s GUI cannot run.');
      warning('To remove this error, remove this variable by typing:');
      warning('');
      warning('   clear(''lrp'');');
      warning('');
      warning('*************************************');
      error('The GUI cannot continue. See above.');
   end
end
if ~exists('LRP_OPTIONS') then
   printf("\n**************************************************\n");
   printf("lrp/macros/gui/LRP_Gui.sce:\n");
   printf('''LRP_OPTIONS''' variable does NOT exist!\n');
   printf('Try issuing:\n');
   printf('global LRP_OPTIONS\n');
   printf('and retry. If the problem persists, restart Scilab\n');
   printf("**************************************************\n");
   error('''LRP_OPTIONS'' does not exist!');
   return;
end
if isempty(fileinfo(fullfile(LRP_OPTIONS.path,'macros','/gui/SetValues.sci'))) then
   printf("\n**************************************************\n");
   printf("lrp/macros/gui/LRP_Gui.sce:\n");
   printf("File ''%s'' does not exist!\n",fullfile(LRP_OPTIONS.path,'macros','/gui/SetValues.sci'));
   printf("Check contents of file ''/lrp/etc/config''\n");
   printf("**************************************************\n");
   error('''LRP_OPTIONS'' does not exist!');
   return;
end
exec(fullfile(LRP_OPTIONS.path,'macros','gui','GUI_SetPaths.sce'));
try
    TCL_EvalFile(fullfile(LRP_OPTIONS.path, '/macros/gui/LRP_Gui.tcl'));
catch
    printf("***************************************************************\n");
    printf("lrp/macros/gui/LRP_Gui.sce:\n");
    printf("Error calling \n    TCL_EvalFile(""%s"")\n",fullfile(LRP_OPTIONS.path, '/macros/gui/LRP_Gui.tcl'));
    printf("Check contents of file ''/lrp/etc/config''\n");    
    printf("Scilab reports the following error:\n");
    printf("    ''%s''\n",lasterror(%f));    
    printf("***************************************************************\n");
    error(lasterror());
    return;    
end

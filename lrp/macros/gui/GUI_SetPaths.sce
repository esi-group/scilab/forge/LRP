// LRP Toolkit for SciLab. This file sets the paths for lrp_gui.tcl to run
//
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2009-07-19 11:10:00

TCL_SetVar('LRP_PATH',pathconvert(fullfile(string(LRP_OPTIONS.path),'macros'),%f));
TCL_SetVar('TMP_DIR',TMPDIR);


// AUTOMATICALLY CREATED FILE - DO NOT MODIFY, CHANGES WILL BE LOST

// In the grid, assume the zero system (external) input
// Caution: assumes that lrp.r is correct number of inputs
[lrpInputMatrixU]=zeros(lrp.dim.alpha,lrp.dim.beta+1,lrp.dim.r);

//Clear the helper variable
lrpGeneratedFileNumbersForSavingToLaTeX=[];
//Create the plots
lrpPlotDataResult=createPlotDataStub();
lrpPlotDataResult=lrpAddPlot( ...
    lrp, ...            // The name of the variable
    'TEST' , ... // Name of plot
    'alongthepass', ... // Type of plot
    'state', ...        // What to plot
    [1 ], ...  // Which state
    list( ...
       [1 2] ...
    ), ...              // Which points
    list( ...
        "all" ...
    ), ...              // Which passes (range only)
    lrpPlotDataResult ... // The previous plots' options; almost
    ...                   //    always the same as the output variable name
);
lrpPlotDataResult=lrpAddPlot( ...
    lrp, ...            // The name of the variable
    'TEST2' , ... // Name of plot
    'alongthepass', ... // Type of plot
    'output', ...       // What to plot
    [1 ], ...  // Which output
    list( ...
       [2 3 4 5] ...
    ), ...              // Which points
    list( ...
        "all" ...
    ), ...              // Which passes (range only)
    lrpPlotDataResult ... // The previous plots' options; almost
    ...                   //    always the same as the output variable name
);
lrpPlotDataResult=lrpAddPlot( ...
    lrp, ...            // The name of the variable
    'TEST3' , ... // Name of plot
    'passtopass', ...   // Type of plot
    'output', ...       // What to plot
    [1 ], ...  // Which output
    list( ...
       [2 3 4 5] ...
    ), ...              // Which passes
    list( ...
        "all" ...
    ), ...              // Which points (range only)
    lrpPlotDataResult ... // The previous plots' options; almost
    ...                   //    always the same as the output variable name
);
lrpPlotDataResult=lrpAddPlot( ...
    lrp, ...            // The name of the variable
    'TEST4' , ... // Name of plot
    '3DPlot', ...       // Type of plot
    'output', ...       // What to plot
    [1 ], ...  // Which output
    list( ...
        "all" ...
    ), ...              // Which points (range only)
    list( ...
        "all" ...
    ), ...              // Which passes (range only)
    lrpPlotDataResult ... // The previous plots' options; almost
    ...                   //    always the same as the output variable name
);


showInfoAboutPlot=%T;
plotBlackOrColor3DPlot="color";
rootDir=createDirForResult();
lrpExportPlotsToLaTeX(rootDir, lrp, lrpPlotDataResult, lrpInputMatrixU, plotBlackOrColor3DPlot, showInfoAboutPlot);


parseLaTeX(lrp,lrpPlotDataResult,'D:\Teksty\Praca\TclTk\blazej_27.06.2006\scilab\lrp/latex/templates/simple.tex',fullfile(rootDir,'result.tex'));
//Create the PDF
createPDF(fullfile(rootDir,'result.tex'));
createPDF(fullfile(rootDir,'result.tex'));
showPDF(fullfile(rootDir,'result.pdf')); // Note that the file must have the extension in small letters

// LRP Toolkit for SciLab. This file is used to read the LRP model into the Tcl/Tk toolkit.
//
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-06 15:40:00


//Check, if we have the model
  if ~exists('lrp') then
    //No, so let's create one.

    //lrp=createLRPModel(tlist(['matrices_LRP';'A';'B';'B0';'C';'D';'D0'],[0],[0],[0],[0],[0],[0]), 10, 20);
    lrp=createLRPModel(0,0,0,0,0,0,10,20);
//     lrp=createStubLRP();
//     lrp.mat.A=0;
//     lrp.mat.B=0;
//     lrp.mat.B0=0;
//     lrp.mat.C=0;
//     lrp.mat.D=0;
//     lrp.mat.D0=0;
//     lrp.dim.n=1;
//     lrp.dim.m=1;
//     lrp.dim.r=1;
//     lrp.dim.alpha=10;
// 		lrp.dim.beta=20;
//     lrp.dim.kmin=0;
//     lrp.dim.kmax=lrp.dim.beta;
//     lrp.dim.pmin=0;
//     lrp.dim.pmax=lrp.dim.alpha-1;
//     
// 		TCL_SetVar('LRPModel_n',string(size(lrp.mat.A,'c')));
//     TCL_SetVar('LRPModel_r',string(size(lrp.mat.B,'c')));
//     TCL_SetVar('LRPModel_m',string(size(lrp.mat.B0,'c')));
//     lrp.ini.x0 = zeros(size(lrp.mat.A,'c'), lrp.dim.beta+1); // There are beta+1 passes
//     lrp.ini.y0 = ones(size(lrp.mat.B,'c'),lrp.dim.alpha);
// 		// We need to make sure that we can recreate the matrices 
// 		// after closing the loop
// 		lrp.controller(1).A=lrp.mat.A;
// 		lrp.controller(1).B=lrp.mat.B;
// 		lrp.controller(1).B0=lrp.mat.B0;
// 		lrp.controller(1).C=lrp.mat.C;
// 		lrp.controller(1).D=lrp.mat.D;
// 		lrp.controller(1).D0=lrp.mat.D0;    
    TCL_EvalStr("tk_messageBox -message {No lrp variable found. Created a new one} -type ok -icon info -title {New model created};");
  end;
  //... and parse it into toolkit
  TCL_SetVar('LRPModel_Alpha',string(sci2exp(lrp.dim.alpha,0)));
  TCL_SetVar('LRPModel_Beta',string(sci2exp(lrp.dim.beta,0)));
  TCL_SetVar('LRPModel_A',string(sci2exp(lrp.mat.A,0)));
  TCL_SetVar('LRPModel_B',string(sci2exp(lrp.mat.B,0)));
  TCL_SetVar('LRPModel_B0',string(sci2exp(lrp.mat.B0,0)));
  TCL_SetVar('LRPModel_C',string(sci2exp(lrp.mat.C,0)));
  TCL_SetVar('LRPModel_D',string(sci2exp(lrp.mat.D,0)));
  TCL_SetVar('LRPModel_D0',string(sci2exp(lrp.mat.D0,0)));
  TCL_SetVar('LRPModel_X0',string(sci2exp(lrp.ini.x0,0)));
  TCL_SetVar('LRPModel_Y0',string(sci2exp(lrp.ini.y0,0)));
  TCL_SetVar('LRPModel_n',string(lrp.dim.n));
  TCL_SetVar('LRPModel_r',string(lrp.dim.r));
  TCL_SetVar('LRPModel_m',string(lrp.dim.m));
  TCL_SetVar('LRPModel_DISPLAY_NAME',string(lrp.controller(lrp.indController).displayName));
  TCL_EvalStr('UpdateMainFormTitle');
	__funcprot=funcprot();
	// Do not issue a warning that __fun is redefined.
	funcprot(0);
	__fun=eval(lrp.ext.uGenerator);__fun = __fun(); funcprot(__funcprot); 
	TCL_SetVar('LRPModel_INPUT',string(__fun(1)));  

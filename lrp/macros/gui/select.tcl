# Save to LaTeX handling
#Horizontal = along the pass
#Vertical = pass to pass
#############################################################
proc ExecInScilab {cmd {mode {}}} {
  global LRPModel_n LRPModel_m LRPModel_r;
  global LRPModel_A LRPModel_B LRPModel_B0;
  global LRPModel_C LRPModel_D LRPModel_D0;
  global LRPModel_Alpha LRPModel_Beta
  global LRPModel_X0 LRPModel_Y0
  global LRP_PATH
  global LRP_Help_Status
  #  set ErrorCode {};
    #ScilabEval "TCL_SetVar('ErrorCode',execstr($cmd,'errcatch'))";
    if "\$mode==\{\}" then " \
      puts \"ScilabEval >>\$cmd<<\"; \
    " else " \
      puts \"ScilabEval >>\$cmd<<, mode=>>\$mode<<)\";  \
    "
  global RUN_IN_SCILAB
  if {$RUN_IN_SCILAB==0} {return}

    #ActivateAll
    ScilabEval $cmd "sync" "seq"
}
# =======================================================================================
proc SetOptions {previous n Alpha Beta idxToSet elementNumber} {
  # idxToSet - INDEX of which output/state to select initially (usually = State-1)
  # elementNumber - '-1' for add, >-1 for edit - which element of a list is edited
  #Unbind the destruction handler, so the old form can be destroyed
  catch "bind .doublelist <Destroy> {set RESULT '1'; set RESULT {};}";
  catch {destroy .doublelist}
  #bind the destruction handler again
  catch "bind .doublelist <Destroy> {}"
  toplevel .doublelist
  wm title .doublelist {Select plot options}
  global RESULT
  global main
  global src
  global buttons
  global dest
  global confirmationbuttons
  global lstSource
  global lstDestination
  global StateOrOutput;
  global HorizontalOrVertical;
  global lstResult
  global lstResultINTERNAL
  global lstElementNumber;
	global plotName;
	
	set lstElementNumber $elementNumber;
  set lstDestination $previous
  set lstSource {}
  set plotName {}

  set main {.doublelist}
  set src "$main.fr.src"
  set buttons "$main.fr.buttons"
  set dest "$main.fr.dest"
  set confirmationbuttons "$main.confirmationbuttons"

  if {$lstElementNumber>-1} {
		set plotName [lrange [lindex $lstResultINTERNAL $lstElementNumber] 4 4];
	} else {
		set plotName {}
	}
	
  # along the pass plot is selected by default, so we must choose k
  label $main.lbl -text {};
  labelframe $main.fr -labelwidget $main.lbl;

	labelframe $main.frname -text {Plot name}
	entry $main.frname.enname -textvariable {plotName}
  labelframe $main.radioso -text {Select state or output}
  radiobutton $main.radioso.radiostate -text "State" -justify left -variable "StateOrOutput" -value "S" -underline 0 -command {
     Populate $main.number.list 1 $LRPModel_n;
     #make sure, there is no state 0
     if [string compare $HorizontalOrVertical "H"]==0 {
        if [$src.lst get 0]==0 {
           $src.lst delete 0 0;
        } else {
          if [$dest.lst get 0]==0 {
             $dest.lst delete 0 0;
             tk_messageBox -icon info -message "States do not have pass #0, they are counted from 1. Pass #0 deleted." -type ok -parent .doublelist;
          }
        }
     }
     $main.number.list selection set 0 0
  }
  radiobutton $main.radioso.radiooutput -text "Output" -justify left -variable "StateOrOutput" -value "O" -underline 1 -command {
     Populate $main.number.list 1 $LRPModel_m;
     if [string compare $HorizontalOrVertical "H"]==0 {
        if [$src.lst get 0]!=0 {
           $src.lst insert 0 "0";
        }
     }
     $main.number.list selection set 0 0
  }

  #Setup the listbox - state/output number
  label $main.lblnumber -text {Which one?} -underline 0;
  labelframe $main.number -labelwidget $main.lblnumber;
  listbox $main.number.list -listvariable number -yscrollcommand {$main.number.sb set} -exportselection 0
  $main.number.list selection set 0 0
  scrollbar $main.number.sb -command {$main.number.list yview}

  #Get number of states/outputs
  Populate $main.number.list 1 $n
  #Select the requested element
  .doublelist.number.list selection clear 0 end;
  .doublelist.number.list selection set $idxToSet $idxToSet;

  labelframe $main.radiohv -text {Plot type}
  radiobutton $main.radiohv.radiohorizontal -text "Along the pass" -justify left -underline 0 -variable "HorizontalOrVertical" -value "H" -command {
    $dest.lst delete 0 end;
     if [string compare $StateOrOutput "S"]==0 {
        Populate $src.lst 1 [expr $LRPModel_Beta - 1];
     } else {
        Populate $src.lst 0 [expr $LRPModel_Beta - 1];
     }
     $main.lbl configure -text {Select pass numbers >>k<<}
    AssessButtons;
  }
  radiobutton $main.radiohv.radiovertical -text "Pass to pass" -justify left -underline 0 -variable "HorizontalOrVertical" -value "V" -command {
    $dest.lst delete 0 end;
    Populate $src.lst 0 [expr $LRPModel_Alpha - 1];
     AssessButtons;
     $main.lbl configure -text {Select points >>p<< on pass};
  }
  radiobutton $main.radiohv.radio3d -text "3D plot" -underline 1 -justify left -variable "HorizontalOrVertical" -value "T" -command {
    $dest.lst delete 0 end;
    $src.lst delete 0 end;
     $main.lbl configure -text {No further options needed for 3D plot};
    AssessButtons;
  }

  pack $main.frname -side top -expand 0 -fill x
	pack $main.frname.enname -side top -expand 0 -fill x -padx 3 -pady 3  
  pack $main.radioso -side top -expand 0 -fill x
  pack $main.number -side top -expand 0 -fill both
  pack $main.radiohv -side top -expand 0 -fill x
  pack $main.radioso.radiostate -side left
  pack $main.radioso.radiooutput

  pack $main.radiohv.radiohorizontal -side left
	pack $main.radiohv.radio3d  -side right
  pack $main.radiohv.radiovertical -expand 1

  pack $main.number.list -side left -expand 1 -fill both
  pack $main.number.sb -side left -fill y -expand 0


  frame $src
  frame $buttons
  frame $dest
  frame $confirmationbuttons
  label $src.lbl -text {Available} -underline 1;
  listbox $src.lst -listvariable {lstSource} -selectmode extended -yscrollcommand {$src.scrbar set}
  label $dest.lbl -text {Selected} -underline 2;
  listbox $dest.lst -listvariable {lstDestination} -selectmode extended -yscrollcommand {$dest.scrbar set}
  #button $main.bttadd -text {Add} -command {puts [$main.lstsrc get ];}
  scrollbar $src.scrbar -orient vertical -command {$src.lst yview}
  scrollbar $dest.scrbar -orient vertical -command {$dest.lst yview}
  #move all selected elements into the destination
  button $buttons.add -text {>} -width 3 -underline 0 -command {
    foreach elem [$src.lst curselection] {
      $dest.lst insert end [$src.lst get $elem]
    }
    foreach elem [lsort -decreasing -integer [$src.lst curselection]] {
      $src.lst delete $elem;
    }
    $src.lst selection clear 0 end
    set lstDestination [lsort -increasing -integer $lstDestination]
    AssessButtons
  }
  #move all the elements into the destination
  button $buttons.addall -text {>>>} -width 3 -command {
    #move all elements
    for {set elem 0} {$elem<[$src.lst index end]} {incr elem} {
      $dest.lst insert end [$src.lst get $elem]}
    #and delete them from the source
      $src.lst delete 0 end;
    $src.lst selection clear 0 end
    set lstDestination [lsort -increasing -integer $lstDestination]
    $buttons.add configure -state disabled;
    AssessButtons
    # We have added everything - no sence in doing it again before adding anything, hence - button disabled
    $buttons.addall configure -state disabled
  }
  button $buttons.removeall -text {<<<} -width 3 -command {
    for {set elem 0} {$elem<[$dest.lst index end]} {incr elem} {
      $src.lst insert end [$dest.lst get $elem]
    }
      $dest.lst delete 0 end;
    $dest.lst selection clear 0 end
    set lstSource [lsort -increasing -integer $lstSource]
    # We have added everything - no sence in doing it again before adding anything, hence - button disabled
    AssessButtons
  }

  button $buttons.removefromdest -width 3 -text {<} -underline 0 -command {
    foreach elem [$dest.lst curselection] {
      $src.lst insert end [$dest.lst get $elem]
    }
    foreach elem [lsort -decreasing -integer [$dest.lst curselection]] {
      $dest.lst delete $elem;
    }
    $dest.lst selection clear 0 end
    set lstSource [lsort -increasing -integer $lstSource]
    $buttons.add configure -state disabled;
    AssessButtons
  }
  ########### ADD THE SELECTED PLOT TO THE SET ################################
  button $confirmationbuttons.ok -text {OK} -underline 0 -width 8 -command {
   #Get the state/output number
   if [string compare $HorizontalOrVertical "T"]!=0 {
     if [llength $lstDestination]==0 {
        tk_messageBox -icon info -message "At least one point must be selected.\nUse the > or >>> buttons to add points." -type ok -parent .doublelist;
        return;
     }
   }
    global lstElementNumber;
    set currSelection [.doublelist.number.list curselection];
     if $currSelection==-1 {
        tk_messageBox -icon info -message "Select a point first" -type ok -parent .doublelist;
        return;
     }

    set num [.doublelist.number.list get $currSelection]
    catch "bind $main <Destroy> {}";
    destroy $main;
     if ([string compare $StateOrOutput S]==0) {set res "State"; set resINTERNAL "S";} else {set res "Output"; set resINTERNAL "O"};
     if ([string compare $HorizontalOrVertical H]==0) {set res "$plotName: Along, $res"; set resINTERNAL "$resINTERNAL H";} else {
        if ([string compare $HorizontalOrVertical V]==0) {
            set res "$plotName: Pass to pass, $res"; set resINTERNAL "$resINTERNAL V";
        } else {
            set res "$plotName: 3D plot, $res"; set resINTERNAL "$resINTERNAL T";
        }
     };     
    if {$lstElementNumber>-1} {
    	# EDIT ACTION
      set lstResultINTERNAL [lreplace $lstResultINTERNAL $lstElementNumber $lstElementNumber "$resINTERNAL $currSelection \{$lstDestination\} \{$plotName\}"];
      if ([string compare $HorizontalOrVertical T]==0) {
         set lstResult         [lreplace $lstResult         $lstElementNumber $lstElementNumber "$res #$num"];
      } else {
         set lstResult         [lreplace $lstResult         $lstElementNumber $lstElementNumber "$res #$num \{$lstDestination\}"];
      }
      set RESULT $lstDestination;
      return {$lstDestination};
    } else {
      if ([string compare $HorizontalOrVertical T]==0) {
        lappend lstResult "$res #$num TESTNAME";
      } else {
        lappend lstResult "$res #$num \{$lstDestination\}";
      }
			
			lappend lstResultINTERNAL "$resINTERNAL $currSelection \{$lstDestination\} \{$plotName\}";
      
			set RESULT $lstDestination;
      return {$lstDestination};
    }
  }
  button $confirmationbuttons.cancel -text {Cancel} -underline 0 -width 8 -command {
         bind $main <Destroy> {};
         destroy $main;
         set RESULT {' '};
         set RESULT $lstDestination;
         #return $lstDestination;
  };

  #Prepare the source - use all possible inputs
  if {[string compare $HorizontalOrVertical "V"]==0} {
#     Populate $src.lst 0 [expr $Alpha - 1]
      $main.radiohv.radiovertical invoke;
  } else {
    if {[string compare $HorizontalOrVertical "H"]==0} {
      $main.radiohv.radiohorizontal invoke;
#       if [string compare $StateOrOutput "S"]==0 {
#          Populate $src.lst 1 $Beta;
#       } else {
#          Populate $src.lst 0 $Beta;
#       }
    } else {
#      $src.lst delete 0 end;
      $main.radiohv.radio3d invoke;
    }
  }
  set lstDestination $previous
  set lstSource [lsort -increasing -integer $lstSource]
  # remove all elements that are present in the Destination from the Source
  foreach elem $lstDestination {
      set idx [lsearch -sorted -exact $lstSource $elem];
      #if element exists,
      if $idx!=-1 {
         #remove it!
         set lstSource [lreplace $lstSource $idx $idx ];
      }
  }

  pack $confirmationbuttons -side bottom -expand 0 -anchor n -fill both
  pack $main.fr -side bottom -expand 1 -fill both
  pack $src.lbl -side top -expand 0 -anchor n;
  pack $src.lst -side left -expand 1 -anchor w -fill both
  pack $src.scrbar -side left -expand 0 -anchor w -fill y
  pack $src -side left -expand 1 -anchor w -fill both

  pack $buttons.add -fill none -expand 0 -padx 2 -pady 2
  pack $buttons.addall -fill none -expand 0 -padx 2 -pady {2 14}
  pack $buttons.removefromdest -fill none -expand 0 -padx 2 -pady 2
  pack $buttons.removeall -fill none -expand 0 -padx 2 -pady 2
  pack $buttons -expand 0 -side left

  pack $dest.lbl -side top -expand 0 -anchor n;
  pack $dest.lst -side left -expand 1 -anchor e -fill both
  pack $dest.scrbar -side left -expand 0 -anchor w -fill y
  pack $dest -side left -expand 1 -anchor w -fill both

  pack $confirmationbuttons.cancel -side right -anchor e -padx 3 -pady 3
  pack $confirmationbuttons.ok -side right -anchor e -padx 3 -pady 3
  $buttons.add configure -state disabled;
  $buttons.removefromdest configure -state disabled;

  # key bindings
  bind $src.lst <<ListboxSelect>> {AssessButtons}
  bind $dest.lst <<ListboxSelect>> {AssessButtons}

  bind .doublelist <Alt-c> {$confirmationbuttons.cancel invoke}
  bind .doublelist <Alt-o> {$confirmationbuttons.ok invoke}
  bind .doublelist <Alt-greater> {$buttons.add invoke}
  bind .doublelist <Alt-period> {$buttons.add invoke}
  bind .doublelist <Alt-less> {$buttons.removefromdest invoke}
  bind .doublelist <Alt-comma> {$buttons.removefromdest invoke}
  bind .doublelist <Alt-a> {$main.radiohv.radiohorizontal invoke}
  bind .doublelist <Alt-p> {$main.radiohv.radiovertical invoke}
  bind .doublelist <Alt-d> {$main.radiohv.radio3d invoke}
  bind .doublelist <Alt-s> {$main.radioso.radiostate invoke}
  bind .doublelist <Alt-u> {$main.radioso.radiooutput invoke}
  bind .doublelist <Alt-v> {focus $src.lst;}
  bind .doublelist <Alt-l> {focus $dest.lst;}
  bind .doublelist <Alt-w> {focus $main.number.list;}
    bind $main <Destroy> {
         $confirmationbuttons.cancel invoke
    }
    AssessButtons;
   focus -force .doublelist;
    grab set .doublelist;
   vwait RESULT
  # END PROC
}

#############################################################
proc AssessButtons {} {
  # Checks, which buttons to enable and which to disable
global main
global src
global buttons
global dest
global confirmationbuttons
global lstSource
global lstDestination

  # there is something selected in the source listbox
  if ([llength [$src.lst curselection]]!=0) then {
    $buttons.add configure -state normal;
  } else {
    $buttons.add configure -state disabled;
  }
  # there is something selected in the destination listbox
  if ([llength [$dest.lst curselection]]!=0) then {
    $buttons.removefromdest configure -state normal;
  } else {
    $buttons.removefromdest configure -state disabled;
  }
  if ([llength $lstSource]!=0) then {
    $buttons.addall configure -state normal;
  } else {
    $buttons.addall configure -state disabled;
  }
  if ([llength $lstDestination]!=0) then {
    $buttons.removeall configure -state normal;
  } else {
    $buttons.removeall configure -state disabled;
  }
}


global lstResult
global lstResultINTERNAL
global TEMPLATE_FILE_NAME
set lstResultINTERNAL {};
set lstResult {};
set TEMPLATE_FILE_NAME {};
# ===========================================
catch { destroy .win}
toplevel .win
focus -force .win;
wm title .win {Save to LaTeX options}
global number;
global n;
set n {10}
global StateOrOutput;
global HorizontalOrVertical;
global LRPModel_n;
global LRPModel_m;
global LRPModel_r;
global LRPModel_Alpha;
global LRPModel_Beta;
global LRP_PATH;

# set LRPModel_n {8}
# set LRPModel_m {5}
# set LRPModel_r {3}
# set LRPModel_Alpha {10}
# set LRPModel_Beta {15}
#Get the LRP Path
# HACK =============================================
global RUN_IN_SCILAB;
set RUN_IN_SCILAB {1};
 if 1==0 {
   puts "!!!!!! LRP_PATH FORCED !!!!!!!!!"
   set LRP_PATH {d:\\teksty\\praca\\tcltk\\lrp}
 } else {
   ExecInScilab {TCL_SetVar('LRP_PATH',string(LRP_OPTIONS.path));}
 }
# Read the values from Scilab
ExecInScilab "exec('$LRP_PATH/gui/SetValues.sci');"

#############################################################
proc AlongThePass {str} {
  global lstResult
  global lstResultINTERNAL
	global TEMPLATE_FILE_NAME
	set aName ""
	set aVariable ""
	set aVariableNumber ""
	set aPoints ""
	set aPassesRange ""
	
	regexp -nocase {<name> {0,}(.*?) {0,}</name>} $str -> aName;
	regexp -nocase {<variable> {0,}(.*?) {0,}</variable>} $str -> aVariable;
	regexp -nocase {<variablenumber> {0,}(.*?) {0,}</variablenumber>} $str -> aVariableNumber;
	regexp -nocase {<points> {0,}(.*?) {0,}</points>} $str -> aPoints;
	regexp -nocase {<passesrange> {0,}(.*?) {0,}</passesrange>} $str -> aPassesRange;
	
	regsub -nocase {^x$} $aVariable {S} aVariable 
	regsub -nocase {^state$} $aVariable {S} aVariable
	regsub -nocase {^y$} $aVariable {O} aVariable
	regsub -nocase {^output$} $aVariable {O} aVariable

	# PassesRange can be a set, e.g. [1,2,5]. We have to get rid of []
	regexp {^\[{0,1}(.*?)(\]){0,1}$} $aPassesRange -> aPassesRange
	# We also do not accept the commas; we simply change them to spaces them
	regsub -all {[,]} $aPassesRange " " aPassesRange
	# We might have 2 or more spaces between numbers - convert to a single one
	regsub -all {[ ]{2,}} $aPassesRange " " aPassesRange	
	
	# Points can also be a set, e.g. [1,2,5]. First, get rid of []
	regexp {^\[{0,1}(.*?)(\]){0,1}$} $aPoints -> aPoints
	# Then normalize commas. We need just a comma, no spaces
	regsub {[ ]{1,}} $aPoints "," aPoints
	# We can be left width multiple commas - change them to a single one
	regsub {[,]{2,}} $aPoints "," aPoints
	# then this time iterate through all the points
	#while {[regexp {^([0-9]{1,}) {0,}[,]{0,1}(.*)$} $aPoints -> aSinglePoint aPoints]} {

#		puts ""
#		puts "Name: $aName"
#		puts "Variable: $aVariable"
#		puts "VariableNumber: $aVariableNumber"
#		puts "Point: $aSinglePoint"
#		puts "PassesRange: $aPassesRange"
	
		
		if [string equal $aVariable "S"] {
			lappend lstResult "$aName: Along, State #$aVariableNumber \{$aPassesRange\}"
			#lappend lstResult "\{Along, State #$aSinglePoint \{$aPassesRange\}\}"
		} else {
			if [string equal $aVariable "O"] {
				lappend lstResult "$aName: Along, Output #$aVariableNumber \{$aPassesRange\}"
				#lappend lstResult "\{Along, Output #$aSinglePoint \{$aPassesRange\}\}"
			} else {
				lappend lstResult "Unknown value of aVariable: >>$aVariable<<"
			}
		}

		incr aVariableNumber -1

		lappend lstResultINTERNAL "$aVariable H $aVariableNumber \{$aPassesRange\} \{$aName\}"

	#}
}
#####################################################################
proc PassToPass {str} {
  global lstResult
  global lstResultINTERNAL
	global TEMPLATE_FILE_NAME
	set aName ""
	set aVariable ""
	set aVariableNumber ""
	set aPasses ""
	set aPassesRange ""

	regexp -nocase {<name> {0,}(.*?) {0,}</name>} $str -> aName;
	regexp -nocase {<variable> {0,}(.*?) {0,}</variable>} $str -> aVariable;
	regexp -nocase {<variablenumber> {0,}(.*?) {0,}</variablenumber>} $str -> aVariableNumber;
	regexp -nocase {<passes> {0,}(.*?) {0,}</passes>} $str -> aPasses;
	regexp -nocase {<pointsrange> {0,}(.*?) {0,}</pointsrange>} $str -> aPointsRange;

	regsub -nocase {^x$} $aVariable {S} aVariable
	regsub -nocase {^state$} $aVariable {S} aVariable
	regsub -nocase {^y$} $aVariable {O} aVariable
	regsub -nocase {^output$} $aVariable {O} aVariable

	# PointsRange can be a set, e.g. [1,2,5]. We have to get rid of []
	regexp {^\[{0,1}(.*?)(\]){0,1}$} $aPointsRange -> aPointsRange
	# We also do not accept the commas; we simply change them to spaces them
	regsub -all {[,]} $aPointsRange " " aPointsRange
	# We might have 2 or more spaces between numbers - convert to a single one
	regsub -all {[ ]{2,}} $aPointsRange " " aPointsRange

	# Passes can also be a set, e.g. [1,2,5]. First, get rid of []
	regexp {^\[{0,1}(.*?)(\]){0,1}$} $aPasses -> aPasses
	# Then normalize commas. We need just a comma, no spaces
	regsub {[ ]{1,}} $aPasses "," aPasses
	# We can be left width multiple commas - change them to a single one
	regsub {[,]{2,}} $aPasses "," aPasses
	# then this time iterate through all the Passes
#	while {[regexp {^([0-9]{1,}) {0,}[,]{0,1}(.*)$} $aPasses -> aSinglePass aPasses]} {

#		puts ""
#		puts "Name: $aName"
#		puts "Variable: $aVariable"
#		puts "VariableNumber: $aVariableNumber"
#		puts "Pass: $aSinglePass"
#		puts "PointsRange: $aPointsRange"


		if [string equal $aVariable "S"] {
			lappend lstResult "$aName: Pass to pass, State #$aVariableNumber \{$aPointsRange\}"
			#lappend lstResult "\{Pass to pass, State #$aSinglePass \{$aPointsRange\}\}"
		} else {
			if [string equal $aVariable "O"] {
				lappend lstResult "$aName: Pass to pass, Output #$aVariableNumber \{$aPointsRange\}"
				#lappend lstResult "\{Pass to pass, Output #$aSinglePass \{$aPointsRange\}\}"
			} else {
				lappend lstResult "Unknown value of aVariable: >>$aVariable<<"
			}
		}
		incr aVariableNumber -1
		lappend lstResultINTERNAL "$aVariable V $aVariableNumber \{$aPointsRange\} \{$aName\}"
#	}
}

#############################################################
proc ThreeDimensional {str} {
  global lstResult
  global lstResultINTERNAL
	global TEMPLATE_FILE_NAME
	set aName ""
	set aVariable ""
	set aVariableNumber ""
	set aPoints ""
	set aPassesRange ""

	regexp -nocase {<name> {0,}(.*?) {0,}</name>} $str -> aName;
	regexp -nocase {<variable> {0,}(.*?) {0,}</variable>} $str -> aVariable;
	regexp -nocase {<variablenumber> {0,}(.*?) {0,}</variablenumber>} $str -> aVariableNumber;
	regexp -nocase {<pointsrange> {0,}(.*?) {0,}</pointsrange>} $str -> aPointsRange;
	regexp -nocase {<passesrange> {0,}(.*?) {0,}</passesrange>} $str -> aPassesRange;

	regsub -nocase {^x$} $aVariable {S} aVariable
	regsub -nocase {^state$} $aVariable {S} aVariable
	regsub -nocase {^y$} $aVariable {O} aVariable
	regsub -nocase {^output$} $aVariable {O} aVariable
#
# 	# PassesRange can be a set, e.g. [1,2,5]. We have to get rid of []
# 	regexp {^\[{0,1}(.*?)(\]){0,1}$} $aPassesRange -> aPassesRange
# 	# We also do not accept the commas; we simply change them to spaces them
# 	regsub -all {[,]} $aPassesRange " " aPassesRange
# 	# We might have 2 or more spaces between numbers - convert to a single one
# 	regsub -all {[ ]{2,}} $aPassesRange " " aPassesRange
#
# 	# Points can also be a set, e.g. [1,2,5]. First, get rid of []
# 	regexp {^\[{0,1}(.*?)(\]){0,1}$} $aPointsRange -> aPointsRange
# 	# Then normalize commas. We need just a comma, no spaces
# 	regsub {[ ]{1,}} $aPointsRange "," aPointsRange
# 	# We can be left width multiple commas - change them to a single one
# 	regsub {[,]{2,}} $aPointsRange "," aPointsRange
# 	# then this time iterate through all the points
# 	while {[regexp {^([0-9]{1,}) {0,}[,]{0,1}(.*)$} $aPointsRange -> aSinglePoint aPoints]} {
#
# #		puts ""
# #		puts "Name: $aName"
# #		puts "Variable: $aVariable"
# #		puts "VariableNumber: $aVariableNumber"
# #		puts "Point: $aSinglePoint"
# #		puts "PassesRange: $aPassesRange"
#

		if [string equal $aVariable "S"] {
			lappend lstResult "$aName: 3D plot, State #$aVariableNumber"
			#lappend lstResult "\{Along, State #$aSinglePoint \{$aPassesRange\}\}"
		} else {
			if [string equal $aVariable "O"] {
				lappend lstResult "$aName: 3D plot, Output #$aVariableNumber"
				#lappend lstResult "\{Along, Output #$aSinglePoint \{$aPassesRange\}\}"
			} else {
				lappend lstResult "Unknown value of aVariable: >>$aVariable<<"
			}
# 		}
 		incr aVariableNumber -1
		lappend lstResultINTERNAL "$aVariable T $aVariableNumber \{\} \{$aName\}"
#	}
}



set lstResult ""
set lstResultINTERNAL ""
set TEMPLATE_FILE_NAME "$LRP_PATH/latex/templates/simple";
#Open file
  set myfile [open "$TEMPLATE_FILE_NAME.xml" r]
	set line ""
	set allFile ""
	while {1} {
    set line [gets $myfile]
    if {[eof $myfile]} {
        close $myfile
        break
    }
		append allFile [string trim $line]
	} 
regexp {<PlotSet>(.*)</PlotSet>} $allFile -> allFile
# Along the pass
while {[regexp {^(.*?)<AlongThePass>(.*?)</AlongThePass>(.*)$} $allFile -> startPart contents endPart]} {
	set allFile [concat $startPart $endPart]
	AlongThePass $contents 
}
# Pass to pass
while {[regexp {^(.*?)<PassToPass>(.*?)</PassToPass>(.*)$} $allFile -> startPart contents endPart]} {
	set allFile [concat $startPart $endPart]
	PassToPass $contents
}

# # ThreeDimensional
while {[regexp {^(.*?)<ThreeDimensional>(.*?)</ThreeDimensional>(.*)$} $allFile -> startPart contents endPart]} {
	set allFile [concat $startPart $endPart]
	ThreeDimensional $contents
}

puts "RES:$lstResult"
puts "INT:$lstResultINTERNAL"
#puts $allFile
#############################################################

#setup the radio button
set StateOrOutput "S";
set HorizontalOrVertical "H";

frame .win.result -relief groove -borderwidth 2
listbox .win.result.list -listvariable lstResult -yscrollcommand {.win.result.sb set} -exportselection 0
scrollbar .win.result.sb -command {.win.result.list yview}

frame .win.result.frmbuttons
button .win.result.frmbuttons.clear -text {Clear} -underline 0 -width 5 -command {
  set answer [tk_messageBox -default no -icon question -message "All plots will be deleted. Continue?" -type yesno -parent .win;];
  if [string compare $answer yes]==0 {
     .win.result.list delete 0 end;
     set lstResultINTERNAL {};
     set lstResult {};
  }
}
button .win.result.frmbuttons.add -text {Add} -underline 0 -width 10 -command {
   #if nothing is selected in the number - inform user and exit
   global HorizontalOrVertical;
   global StateOrOutput;
   set HorizontalOrVertical "H";
   set StateOrOutput "S";
        SetOptions {} $LRPModel_n $LRPModel_Alpha $LRPModel_Beta 0 -1;
  .win.result.list selection clear 0 end
  .win.result.list selection set end end
}
button .win.result.frmbuttons.edit -text {Edit} -underline 0 -command {
   #if nothing is selected in the number - inform user and exit
   if [string length [.win.result.list curselection]]==0 {
      tk_messageBox -icon info -message "Select the number first" -type ok -parent .win;
      return;
   }
  #sel - which element is selected
   set sel [.win.result.list curselection];
   set current [lindex $lstResultINTERNAL $sel];
   #Set the variables
   global HorizontalOrVertical;
   global StateOrOutput;
  set HorizontalOrVertical [lindex $current 1];
  set StateOrOutput [lindex $current 0];
  #select the needed value
   if [string compare $StateOrOutput S]==0 {
        SetOptions [lindex $current 3] $LRPModel_n $LRPModel_Alpha $LRPModel_Beta [lindex $current 2] $sel;
   } else {
        SetOptions [lindex $current 3] $LRPModel_m $LRPModel_Alpha $LRPModel_Beta [lindex $current 2] $sel;
   }
}
button .win.result.frmbuttons.delete -underline 0 -text {Delete} -command {
  #sel - which element is selected
   if [llength $lstResult]==0 {
      tk_messageBox -icon info -message "Use the 'Add' button to add data first" -type ok -parent .win;
      return;
   }
  set answer [tk_messageBox -default no -icon question -message "Selected plot will be deleted. Continue?" -type yesno -parent .win;];
  if [string compare $answer yes]==0 {
  set sel [.win.result.list curselection];
  set lstResultINTERNAL [lreplace $lstResultINTERNAL $sel $sel];
  set lstResult         [lreplace $lstResult         $sel $sel];
  }
}
frame .win.frmplot
button .win.frmplot.plot -text {Save to LaTeX} -underline 0 -width 15 -command {
   if [llength $lstResult]==0 {
      tk_messageBox -icon info -message "Use the 'Add' button to add data first" -type ok -parent .win;
      return;
   }
  #Select a file to save
  set types {
     {"LaTeX files" {.tex}}
     {"All files"    {*}  }
  }
#  set file_Name [tk_getSaveFile -filetypes $types -parent .win \
#      -initialfile "report.tex" -defaultextension ".tex" -title {Save LaTeX}]
#  if [string length $file_Name]==0 {
#     return;
#  }
   set file_Name {report.tex}
  #If file exists, check, whether we can save to it, otherwise assume that we can.
  if [file exists $file_Name]==1 {
    if [file writable $file_Name]==0 {
       tk_messageBox -icon error -message "Cannot write to\n$file_Name" -type ok -title {Error creating file} -parent .win;
       return;
    }
  }

  #  # file is selected, let's get to work...
      puts "Plotting..";

  puts $lstResultINTERNAL
  puts $lstResult

  #Delete the previous file
     catch "file delete \"$LRP_PATH/gui/lrpplotme.sci\";"
  # #Open file
      set myfile [open "$LRP_PATH/gui/lrpplotme.sci" w]
  # Add some headers
      puts $myfile {// AUTOMATICALLY CREATED FILE - DO NOT MODIFY, CHANGES WILL BE LOST}
      puts $myfile "";
  # and calculate the grid
      puts $myfile {// In the grid, assume the zero system (external) input};
      puts $myfile {// Caution: assumes that lrp.r is correct number of inputs};
      puts $myfile {[lrpInputMatrixU]=zeros(lrp.dim.alpha,lrp.dim.beta,lrp.dim.r);};
      puts $myfile "";
      puts $myfile {//Clear the helper variable};
      puts $myfile {lrpGeneratedFileNumbersForSavingToLaTeX=[];};
      puts $myfile {//Create the plots};
      puts $myfile {lrpPlotDataResult=createPlotDataStub();};
  #    puts $myfile {lrp, ...};
  #     "3DPlot", "state", ...
  #     [1 2 1], ...
  #     list("all", [1 2 3 8 9 ],[1 2 8 ]), ...
  #     list([ 1  3], "all", [0 10 ]), ...
  #     lrpMergedDatToPlots ...
  # );

  #Get the states first
  #   horizontal
  set resPlots {};
  set resPoints {};
  set resRange {};
  set resName {};
  foreach {x} $lstResultINTERNAL {
  if ([string compare [lindex $x 0] "S"]==0) {
     if ([string compare [lindex $x 1] "H"]==0) {
        append resPlots [expr [lindex $x 2] + 1] " ";
        append resPoints "       \[" [lindex $x 3] "\], ...\n";
        append resRange "        \"all\", ... \n";
        set resName "[lindex $x 4]";
     }
   }
  }
  set resPoints [string trimright $resPoints ",.\n "];
  set resRange [string trimright $resRange ",.\n "];
  #Create the plots
  if [string length $resPlots]!=0 {
      puts $myfile {lrpPlotDataResult=lrpAddPlot( ...};
      puts $myfile {    lrp, ...            // The name of the variable};
      puts -nonewline $myfile {    '};
  		puts -nonewline $myfile "$resName";
  		puts $myfile {' , ... // Name of plot};
      puts $myfile {    'alongthepass', ... // Type of plot};
      puts $myfile {    'state', ...        // What to plot};
      puts $myfile "    \[$resPlots\], ...  // Which state";
      puts $myfile {    list( ...};
      puts $myfile "$resPoints ...";
      puts $myfile {    ), ...              // Which points};
      puts $myfile {    list( ...};
      puts $myfile "$resRange ...";
      puts $myfile {    ), ...              // Which passes (range only)};
      puts $myfile {    lrpPlotDataResult ... // The previous plots' options; almost};
      puts $myfile {    ...                   //    always the same as the output variable name};
      puts $myfile {);}
  }

  #   vertical
  set resPlots {};
  set resPoints {};
  set resRange {};
  foreach {x} $lstResultINTERNAL {
  if ([string compare [lindex $x 0] "S"]==0) {
     if ([string compare [lindex $x 1] "V"]==0) {
        append resPlots [expr [lindex $x 2] + 1] " ";
        append resPoints "       \[" [lindex $x 3] "\], ...\n";
        append resRange "        \"all\", ... \n";
        set resName "[lindex $x 4]";
     }
   }
  }
  set resPoints [string trimright $resPoints ",.\n "];
  set resRange [string trimright $resRange ",.\n "];
  #Create the plots
  if [string length $resPlots]!=0 {
      puts $myfile {lrpPlotDataResult=lrpAddPlot( ...};
      puts $myfile {    lrp, ...            // The name of the variable};
      puts -nonewline $myfile {    '};
  		puts -nonewline $myfile "$resName";
  		puts $myfile {' , ... // Name of plot};
      puts $myfile {    'passtopass', ...   // Type of plot};
      puts $myfile {    'state', ...        // What to plot};
      puts $myfile "    \[$resPlots\], ...  // Which state";
      puts $myfile {    list( ...};
      puts $myfile "$resPoints ...";
      puts $myfile {    ), ...              // Which passes};
      puts $myfile {    list( ...};
      puts $myfile "$resRange ...";
      puts $myfile {    ), ...              // Which points (range only)};
      puts $myfile {    lrpPlotDataResult ... // The previous plots' options; almost};
      puts $myfile {    ...                   //    always the same as the output variable name};
      puts $myfile {);}
  }

  #Then the outputs
  #   horizontal
  set resPlots {};
  set resPoints {};
  set resRange {};
  foreach {x} $lstResultINTERNAL {
  if ([string compare [lindex $x 0] "O"]==0) {
     if ([string compare [lindex $x 1] "H"]==0) {
        append resPlots [expr [lindex $x 2] + 1] " ";
        append resPoints "       \[" [lindex $x 3] "\], ...\n";
        append resRange "        \"all\", ... \n";
        set resName "[lindex $x 4]";
     }
   }
  }
  set resPoints [string trimright $resPoints ",.\n "];
  set resRange [string trimright $resRange ",.\n "];
  #Create the plots
  if [string length $resPlots]!=0 {
      puts $myfile {lrpPlotDataResult=lrpAddPlot( ...};
      puts $myfile {    lrp, ...            // The name of the variable};
      puts -nonewline $myfile {    '};
  		puts -nonewline $myfile "$resName";
  		puts $myfile {' , ... // Name of plot};
      puts $myfile {    'alongthepass', ... // Type of plot};
      puts $myfile {    'output', ...       // What to plot};
      puts $myfile "    \[$resPlots\], ...  // Which output";
      puts $myfile {    list( ...};
      puts $myfile "$resPoints ...";
      puts $myfile {    ), ...              // Which points};
      puts $myfile {    list( ...};
      puts $myfile "$resRange ...";
      puts $myfile {    ), ...              // Which passes (range only)};
      puts $myfile {    lrpPlotDataResult ... // The previous plots' options; almost};
      puts $myfile {    ...                   //    always the same as the output variable name};
      puts $myfile {);}
  }

  #   vertical
  set resPlots {};
  set resPoints {};
  set resRange {};
  foreach {x} $lstResultINTERNAL {
  if ([string compare [lindex $x 0] "O"]==0) {
     if ([string compare [lindex $x 1] "V"]==0) {
        append resPlots [expr [lindex $x 2] + 1] " ";
        append resPoints "       \[" [lindex $x 3] "\], ...\n";
        append resRange "        \"all\", ... \n";
        set resName "[lindex $x 4]";
     }
   }
  }
  set resPoints [string trimright $resPoints ",.\n "];
  set resRange [string trimright $resRange ",.\n "];
  #Create the plots
  if [string length $resPlots]!=0 {
      puts $myfile {lrpPlotDataResult=lrpAddPlot( ...};
      puts $myfile {    lrp, ...            // The name of the variable};
      puts -nonewline $myfile {    '};
  		puts -nonewline $myfile "$resName";
  		puts $myfile {' , ... // Name of plot};
      puts $myfile {    'passtopass', ...   // Type of plot};
      puts $myfile {    'output', ...       // What to plot};
      puts $myfile "    \[$resPlots\], ...  // Which output";
      puts $myfile {    list( ...};
      puts $myfile "$resPoints ...";
      puts $myfile {    ), ...              // Which passes};
      puts $myfile {    list( ...};
      puts $myfile "$resRange ...";
      puts $myfile {    ), ...              // Which points (range only)};
      puts $myfile {    lrpPlotDataResult ... // The previous plots' options; almost};
      puts $myfile {    ...                   //    always the same as the output variable name};
      puts $myfile {);}
  }





  #Then the 3D plots
  #   states
  set resPlots {};
  set resPointsRange {};
  set resPassesRange {};
  foreach {x} $lstResultINTERNAL {
  if ([string compare [lindex $x 0] "S"]==0) {
     if ([string compare [lindex $x 1] "T"]==0) {
        append resPlots [expr [lindex $x 2] + 1] " ";
        append resPointsRange "        \"all\", ... \n";
        append resPassesRange "        \"all\", ... \n";
        set resName "[lindex $x 4]";
     }
   }
  }
  set resPointsRange [string trimright $resPointsRange ",.\n "];
  set resPassesRange [string trimright $resPassesRange ",.\n "];
  #Create the plots
  if [string length $resPlots]!=0 {
      puts $myfile {lrpPlotDataResult=lrpAddPlot( ...};
      puts $myfile {    lrp, ...            // The name of the variable};
      puts -nonewline $myfile {    '};
  		puts -nonewline $myfile "$resName";
  		puts $myfile {' , ... // Name of plot};
      puts $myfile {    '3DPlot', ...       // Type of plot};
      puts $myfile {    'state', ...        // What to plot};
      puts $myfile "    \[$resPlots\], ...  // Which state";
      puts $myfile {    list( ...};
      puts $myfile "$resPointsRange ...";
      puts $myfile {    ), ...              // Which points (range only)};
      puts $myfile {    list( ...};
      puts $myfile "$resPassesRange ...";
      puts $myfile {    ), ...              // Which passes (range only)};
      puts $myfile {    lrpPlotDataResult ... // The previous plots' options; almost};
      puts $myfile {    ...                   //    always the same as the output variable name};
      puts $myfile {);}
  }

  #   outputs
  set resPlots {};
  set resPointsRange {};
  set resPassesRange {};
  foreach {x} $lstResultINTERNAL {
  if ([string compare [lindex $x 0] "O"]==0) {
     if ([string compare [lindex $x 1] "T"]==0) {
        append resPlots [expr [lindex $x 2] + 1] " ";
        append resPointsRange "        \"all\", ... \n";
        append resPassesRange "        \"all\", ... \n";
        set resName "[lindex $x 4]";
     }
   }
  }
  set resPointsRange [string trimright $resPointsRange ",.\n "];
  set resPassesRange [string trimright $resPassesRange ",.\n "];

  #Create the plots
  if [string length $resPlots]!=0 {
      puts $myfile {lrpPlotDataResult=lrpAddPlot( ...};
      puts $myfile {    lrp, ...            // The name of the variable};
      puts -nonewline $myfile {    '};
  		puts -nonewline $myfile "$resName";
  		puts $myfile {' , ... // Name of plot};
      puts $myfile {    '3DPlot', ...       // Type of plot};
      puts $myfile {    'output', ...       // What to plot};
      puts $myfile "    \[$resPlots\], ...  // Which output";
      puts $myfile {    list( ...};
      puts $myfile "$resPointsRange ...";
      puts $myfile {    ), ...              // Which points (range only)};
      puts $myfile {    list( ...};
      puts $myfile "$resPassesRange ...";
      puts $myfile {    ), ...              // Which passes (range only)};
      puts $myfile {    lrpPlotDataResult ... // The previous plots' options; almost};
      puts $myfile {    ...                   //    always the same as the output variable name};
      puts $myfile {);}
  }
  global TEMPLATE_FILE_NAME;
  puts $myfile "\n";
  puts $myfile {showInfoAboutPlot=%T;}
  puts $myfile {plotBlackOrColor3DPlot="color";}
  puts $myfile {rootDir=createDirForResult();}
  puts $myfile {lrpExportPlotsToLaTeX(rootDir, lrp, lrpPlotDataResult, lrpInputMatrixU, plotBlackOrColor3DPlot, showInfoAboutPlot);}
  puts $myfile "\n";
  puts $myfile "parseLaTeX(lrp,lrpPlotDataResult,'$TEMPLATE_FILE_NAME.tex',fullfile(rootDir,'result.tex'));"
  puts $myfile {//Create the PDF};
  puts $myfile {createPDF(fullfile(rootDir,'result.tex'));};
  puts $myfile {createPDF(fullfile(rootDir,'result.tex'));};
  puts $myfile {showPDF(fullfile(rootDir,'result.pdf')); // Note that the file must have the extension in small letters};
#  puts $myfile {//exportToLaTeX(lrp,lrpGeneratedFileNumbersForSavingToLaTeX,"Result generated by the LRP Toolkit for Scilab","LRP Toolkit");};




  # close the file
  #
    close $myfile
  #Run the file
    ExecInScilab "exec('$LRP_PATH/gui/lrpplotme.sci',-1);"

}
 ### END OF BUTTON OPERATION
button .win.frmplot.close -text {Close} -width 5 -underline 1 -command {destroy .win}
pack .win.result.frmbuttons -expand 0 -fill x -side bottom -padx 1 -pady 1
pack .win.frmplot -side bottom -expand 0 -fill x -pady {5 0}
pack .win.frmplot.close -side right  -padx {3 0}
pack .win.frmplot.plot -side right  -padx {0 3}

pack .win.result.list -side left -expand 1 -fill both -padx 1 -pady 1
pack .win.result.sb -side left -fill y -expand 0 -padx 1 -pady 1
pack .win.result.frmbuttons.clear -side left -anchor w -fill x -padx {0 3}
pack .win.result.frmbuttons.add -side left -anchor w -fill x -padx 3
pack .win.result.frmbuttons.delete -side right  -anchor e -fill x -padx {3 0}
pack .win.result.frmbuttons.edit -fill x -anchor w -padx 3
pack .win.result -side bottom -expand 1 -fill both


puts "---------------"
global RESULT
bind .win <Alt-s> {.win.frmplot.plot invoke}
bind .win <Alt-c> {.win.result.frmbuttons.clear invoke}
bind .win <Alt-a> {.win.result.frmbuttons.add invoke}
bind .win <Alt-e> {.win.result.frmbuttons.edit invoke}
bind .win <Alt-d> {.win.result.frmbuttons.delete invoke}
bind .win <Alt-l> {.win.frmplot.close invoke}
# Fill the listbox with proper values
#############################################################
proc Populate {lstbox min size} {
     $lstbox delete 0 end
     for {set x $min} {$x<=$size} {incr x} {
          $lstbox insert end "$x"
     }
}

// AUTOMATICALLY GENERATED FILE - DO NOT MODIFY, CHANGES WILL BE LOST!

//   To change this file use ''Create a new system'' in 
//     the LRP Toolkit GUI, accessible from the LRP menu

clear lrp; //Get rid of the old variable

// First create an empty lrp structure that will be filled later
lrp=createStubLRP(); 

lrp.type=0; //0 means "classic" system

lrp.dim.alpha=10; //number of points

// The system will be simulated upto beta pass, from 0 to the ''beta''.
lrp.dim.beta=20;

// In the classic LRP setting, the points are numbered from pmin=0 to pmax=alpha-1
lrp.dim.pmin=0;
lrp.dim.pmax=lrp.dim.alpha-1;

// In the classic LRP setting, the passes are numbered from kmin=0 to kmax=beta
//   Note that there are beta+1 passes.
lrp.dim.kmin=0;
lrp.dim.kmax=lrp.dim.beta;

lrp.dim.n=2; //number of states x
lrp.dim.r=4; //number of inputs u
lrp.dim.m=3; //number of outputs y

// The system matrices
lrp.mat.A=[
0 0 
 0 0 
];
lrp.mat.B=[
0 0 0 0 
 0 0 0 0 
];
lrp.mat.B0=[
0 0 0 
 0 0 0 
];

lrp.mat.C=[
0 0 
 0 0 
 0 0 
];
lrp.mat.D=[
0 0 0 0 
 0 0 0 0 
 0 0 0 0 
];
lrp.mat.D0=[
0 0 0 
 0 0 0 
 0 0 0 
];

//Boundary conditions
lrp.ini.x0=[
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
];
lrp.ini.y0=[
1,1,1,1,1,1,1,1,1,1
1,1,1,1,1,1,1,1,1,1
1,1,1,1,1,1,1,1,1,1
];

// Required for the toolkit. The lrp.controller(0) should contain
//   copies of all the model matrices
lrp.controller(1).A=lrp.mat.A;
lrp.controller(1).B=lrp.mat.B;
lrp.controller(1).B0=lrp.mat.B0;
lrp.controller(1).C=lrp.mat.C;
lrp.controller(1).D=lrp.mat.D;
lrp.controller(1).D0=lrp.mat.D0;


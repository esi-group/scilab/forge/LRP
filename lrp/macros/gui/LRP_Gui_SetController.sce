// This script calculates and sets a controller requested by user. It is called by 
//   TCL/Tk's SelectControllerOption function.
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2009-12-02 09:18:00
//disp(sprintf('About to set a controller: %s',TCL_GetVar('optionValue')));
execstr(sprintf('lrp=%s(lrp);',TCL_GetVar('optionValue')));
TCL_SetVar('LRPModel_DISPLAY_NAME',string(lrp.controller(lrp.indController).displayName));
TCL_EvalStr('UpdateMainFormTitle');

ERROR="** ERROR **";
printf("============= int2str =================\n");
testCase("[2,2,1]","int2str",[2,2,1]);
testCase("""ala""","int2str","ala");
testCase(ERROR,"int2str"); // wrong number of arguments

ERROR_STRING="** ERROR **";    // Error is expected
NO_RESULT="ERR* 54290127598221040596024312840%nan *ERR"; // No result is expected
NO_ARGUMENTS="ERR* %nan22343994991579761355498794699 *ERR"; // Do NOT add parenthesis after function.

printf("============= requireValue =================\n");
//  0 - [lrp]=createLRPModel()
testCase(%T,"checkLRP(createLRPModel())",NO_ARGUMENTS);
//  3 - [lrp]=createLRPModel([n,m,r], numberOfPoints, numberOfPasses)
testCase(%T,"checkLRP(createLRPModel([1,2,3],5,8))",NO_ARGUMENTS);
//  4 - [lrp]=createLRPModel([n,m,r], rangeForValues, numberOfPoints, numberOfPasses)
testCase(%T,"checkLRP(createLRPModel([1,2,3],[1,12],5,8))",NO_ARGUMENTS);
//  5 - [lrp]=createLRPModel([n,m,r], rangeForValues, numberOfPoints, numberOfPasses,functionForBoundary)
//  6 - [lrp]=createLRPModel([n,m,r], rangeForValues, numberOfPoints, numberOfPasses,boundaryX0, boundaryY0)
testCase(%T,"checkLRP(createLRPModel([1,2,3],[1,12],5,8,10,20))",NO_ARGUMENTS);
//  2 - [lrp]=createLRPModel(numberOfPoints, numberOfPasses)
testCase(%T,"checkLRP(createLRPModel(5,8))",NO_ARGUMENTS);
//  3 - [lrp]=createLRPModel(rangeForValues, numberOfPoints, numberOfPasses)
testCase(%T,"checkLRP(createLRPModel([1,12],5,8))",NO_ARGUMENTS);
//  4 - [lrp]=createLRPModel(rangeForValues, numberOfPoints, numberOfPasses, functionForBoundary)
//  5 - [lrp]=createLRPModel(rangeForValues, numberOfPoints, numberOfPasses, boundaryX0, boundaryY0)
testCase(%T,"checkLRP(createLRPModel([1,12],5,8,10,20))",NO_ARGUMENTS);

//  6 - [lrp]=createLRPModel([n,m,r], rangeForValues, numberOfPoints, numberOfPasses,boundaryX0, boundaryY0)
// Both boundary conditions random
testCase(%T,"checkLRP(createLRPModel([1, 2, 3], [5,7], 10, 6, rand(1,6), rand(3,10)))",NO_ARGUMENTS);

// X0 random, Y0 given
testCase(%T,"checkLRP(createLRPModel([1, 2, 3], [5,7], 10, 6, rand(1,6), 11))",NO_ARGUMENTS);

n=3; m=2; r=4;
A=rand(n,n);
B=rand(n,r);
B0=rand(n,m);
C=rand(m,n);
D=rand(m,r);
D0=rand(m,m);

//  8 - [lrp]=createLRPModel(A,B,B0,C,D,D0, numberOfPoints, numberOfPasses)
testCase(%T,"checkLRP(createLRPModel(A,B,B0,C,D,D0,5,8))",NO_ARGUMENTS);
//  9 - [lrp]=createLRPModel(A,B,B0,C,D,D0, numberOfPoints, numberOfPasses, functionForBoundary)
//  10 - [lrp]=createLRPModel(A,B,B0,C,D,D0, numberOfPoints, numberOfPasses, boundaryX0, boundaryY0)
testCase(%T,"checkLRP(createLRPModel(A,B,B0,C,D,D0,5,8,10,20))",NO_ARGUMENTS);
matricesLRP=createLstMatrices(A,B,B0,C,D,D0);
//  3 - [lrp]=createLRPModel(matricesLRP, numberOfPoints, numberOfPasses)
testCase(%T,"checkLRP(createLRPModel(matricesLRP,5,8))",NO_ARGUMENTS);
//  4 - [lrp]=createLRPModel(matricesLRP, numberOfPoints, numberOfPasses, functionForBoundary)
//  5 - [lrp]=createLRPModel(matricesLRP, numberOfPoints, numberOfPasses, boundaryX0, boundaryY0)
testCase(%T,"checkLRP(createLRPModel(matricesLRP,5,8,10,20))",NO_ARGUMENTS);

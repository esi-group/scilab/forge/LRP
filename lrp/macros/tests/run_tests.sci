currentDir=pwd();
cd(LRP_OPTIONS.path+'/test');
files=listfiles('TEST*.sci',%t,%t);
cd(currentDir);
for index=1:size(files,1)
    exec(LRP_OPTIONS.path+'/test/'+files(index));
end
printf('\n+----------------------------------+\n');
printf('|          ALL TESTS PASSED!       |\n');
printf('+----------------------------------+\n');

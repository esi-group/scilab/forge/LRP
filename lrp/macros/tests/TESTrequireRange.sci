ERROR="** ERROR **";
printf("============= requireRange =================\n");

val=10;

//A simple range test
testCase(%t,"requireRange", "val",10,5,20);
// PASSED - val>=5 and val<=20

// The same as above, using the vector notation
testCase(%t,"requireRange", "val",10,[5,20]);
// PASSED - val>=5 and val<=20

// Misused vector notation
testCase(ERROR,"requireRange", "val",10,[5,20],30);
// ERROR - Wrong arguments

// Require that val>=5
testCase(%t,"requireRange", "val",10,5,%inf);
//PASSED, as val>=5. Note that the 2nd test (val<=%inf) is always true as no
//  variable can have value greater than infinity.

// Require that val<7
testCase(ERROR ,"requireRange", "val",10,[-%inf,7]);
//FAILED, as the requirement that val<=7 is NOT fulfilled. Note that the 1st test (val>=-%inf) is always true as no
//  variable can have value lesser than minus infinity.
// Obviously the same could be achieved by
testCase(ERROR,"requireRange", "val",val,-%inf,7);
// This is the same, only using the two-argument notation

// Passed this time
testCase(%t,"requireRange", "val",10,[-%inf,15]);

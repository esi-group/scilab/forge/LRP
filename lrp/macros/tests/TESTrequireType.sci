ERROR="** ERROR **";
testCase(ERROR,"requireType",1,"integer"); // wrong number of arguments
    str="this is a string";
		strEmpty="";

		testCase(%t,"requireType", "str",str,["string"]);
// CHECK PASSED, str IS a non-empty string

		testCase(ERROR,"requireType", "strEmpty",strEmpty,["string"]);
// CHECK FAILED, str IS NOT a non-empty string.

		testCase(%t,"requireType", "strEmpty",strEmpty,["string","empty string"]);
// CHECK PASSED, strEmpty must be either
//    a non-empty string
// OR
//    an empty string
// Since str is an empty string, the check is passed

		testCase(%t,"requireType", "str",str,["string","empty string"]);
// CHECK PASSED, str must be either
//    a non-empty string
// OR
//    an empty string
// Since str is a non-empty string, the check is passed

		testCase(ERROR ,"requireType", "str",str,["empty string"]);
// CHECK FAILED, str must be an empty string.
// Since str is a non-empty string, the check has failed.

		testCase(ERROR ,"requireType", "str",str,["integer"]);
// CHECK FAILED, str IS *NOT* an integer.
// Error will be generated

		testCase(%t ,"requireType", "str",str,["integer","string"]);
// CHECK PASSED, str must be either
//    an integer
// OR
//    a string
// Since str is a string, the check is passed

		myTlist = tlist(["MY_TYPE";"field"],"Test");
		testCase(%t ,"requireType", "myTlist",myTlist,["tlist"],["MY_TYPE"]);
//CHECK PASSED, myTlist IS a tlist of user type "MY_TYPE"

		testCase(ERROR ,"requireType", "myTlist",myTlist,["tlist"],["OtherType"]);
//CHECK FAILED, myTlist IS a tlist (which is OK), but it is
// *NOT* of user type "OtherType" (it is of "MY_TYPE" after all)

		testCase(%t ,"requireType", "myTlist",myTlist,["tlist"]);
//CHECK PASSED, myTlist IS a tlist (the user type is irrelevant)

// Pass only a row vector of integers
   rowVector=[1,2,3,4];
// First, we check if it is an integer vector (row or column one; does
//    not matter for now)...
// Then we enforce it to be a row vector. We are not concerned with it being
//   an INTEGER vector, as we have already checked it
   testCase(%t ,"requireType", "rowVector",rowVector,["integer vector"]);
   testCase(%t ,"requireType", "rowVector",rowVector,["row vector"]);
// This simple trick can be used for combining other requirements as well
// Caution: Do not confuse it with
   testCase(%t ,"requireType", "rowVector",rowVector,["integer vector", "row vector"]);
// This will pass anything that is EITHER an integer vector or ANY ROW
//   vector (e.g. of strings)

// =============================================================
// And finally a very common ****** MISTAKE ******:

	  testCase(ERROR ,"requireType", "myTlist",myTlist,["TLIST"]);

// ***WRONG TYPE*** - an error will be generated. Pay attention to
//  the type name - all of them must be given exactly as written in this help file.
//  The programmer probably intended something like:
    testCase(%t ,"requireType", "myTlist",myTlist,["tlist"]);
// (note the case in "tlist"). This a a proper way of calling
//  this function (and the result is, as you already know "CHECK PASSED").

ERROR="** ERROR **";
printf("============= replaceValue =================\n");
testCase(list("WW"),"replaceValue",list([1,2,3]),list("abc",[1,2,3]),"WW");
testCase("WW","replaceValue",["a","b","c"],list("abc",["A","B","C"]),"WW",%f);
testCase(["a","b","c"],"replaceValue",["a","b","c"],list("abc",["A","B","C"]),"WW",%t);
testCase(["ABC","DEF", "FOO"],"replaceValue",["ABC","DEF", "G"],list("G"),"FOO");
testCase(ERROR,"replaceValue",["ABC","DEF", "G"],list("G"),13); //cannot replace a single string with integer
testCase(13,"replaceValue",["ABC","DEF", "G"],list(["ABC","DEF", "G"]),13);

testCase(list([3,3,3]),"replaceValue",list(["all","all","all"]),list("all"),3)
//replaceValue(mlist(["a";"b"],list(["all","all","all"])),list("all"),3)
// replaceValue(["all","all","all"],list(1,"all"),[11,12])
testCase(list([-33,17,-33,17,-33,17],[-33,17],"FOO",[16,22]), "replaceValue", list(["all","all","all"],"all","FOO",[16,22]), list("all"),[-33,17]);
testCase([1,2,3,1,20], "replaceValue", [1,2,3,1,20], list(50, "FOO", [7,5,2]),199);
testCase(list([1,2,199],199), "replaceValue", list([1,2,3],"foo"), list(3, "foo"),199);
testCase([199,199,199,199,20],  "replaceValue", [1,2,3,1,20],list(1,2,3,4,5,6),199);
testCase([199,2,3,199], "replaceValue", [1,2,3,1,20],list(1),199);

testCase(list(list([9,8,67]),2,3),"replaceValue",list(1,2,3),list(1),list([9,8,67]))

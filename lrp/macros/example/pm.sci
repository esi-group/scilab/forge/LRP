//print matrix in scilab form

function pm(fd,M,digits)
[w,k]=size(M);

pole=6;

if ((k>1) & (w>1))  then
	mfprintf(fd,"[");
	for i=1:w-1 
		for j=1:k-1
			if (j==1) & (i~=1) then
				mfprintf(fd," ");
			end	
			if (M(i,j)>=0) then
				mfprintf(fd," ");
			end
			mfprintf(fd,"%*.*g ",pole,digits,M(i,j));
		end
		if (M(i,$)>=0) then
			mfprintf(fd," ");
		end
		mfprintf(fd,"%*.*g;...\n",pole,digits,M(i,$));
	end
	for j=1:k-1
		if (j==1) then
			mfprintf(fd," ");
		end	
		if (M($,j)>=0) then
			mfprintf(fd," ");
		end
		mfprintf(fd,"%*.*g ",pole,digits,M($,j));
	end
	if (M($,$)>=0) then
		mfprintf(fd," ");
	end
	mfprintf(fd,"%*.*g];\n",pole,digits,M($,$));				
end

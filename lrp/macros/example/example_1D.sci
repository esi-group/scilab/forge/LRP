// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2004-03-02 16:09:00

clc
clear

//load library
//******************************************************************************
global LRP_PATH
//uczelnia
//LRP_PATH="D:\uczelnia\blazej\doktorat\dzialanie\projekt scilab\nowy\projekt\LRP";
//dom
LRP_PATH='D:\uczelnia\blazej\doktorat\dzialanie\projekt scilab\LRP';
exec(LRP_PATH+'/library/createLibrary.sci');
exec(LRP_PATH+'/library/loadLibrary.sci');
//******************************************************************************

//debug checking
LRP_DEBUG=1;



n=2;
m=3;
r=4;
boundaryValue=[-1,1];
number_of_points=5;
number_of_passes=10;

//no debuging
isDebug(0);

//make matrices LRP ->  A,B,B0,C,D,D0
[matricesLRP]=genRandLRP(n, m, r, boundaryValue);

//make LRP
lrp=createLRPModel(matricesLRP, number_of_points, number_of_passes);

//make matrices LRP for 1D
[matricesLRP_1D]=genMatrLRP1D(matricesLRP,number_of_points);

//insert matrices 1D in LRP model
//*******************************

//insert substructure m1d (-> Model 1D) into 'lrp'
//=================================================
//lrp=insertM1DToLrp(lrp, matricesLRP_1D)

[lrp]=make1D(lrp)

//cenvert from 1D to 2D intwo ways:
//====================================
// 1.
[mLRP1]=genMatrLRP1DTo2D(lrp.m1d, number_of_points);
// 2.
[mLRP2]=genMatrLRP1DTo2D(lrp);


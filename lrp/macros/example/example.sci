// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2004-03-02 16:09:00



//cleaning, setting, closing
clear
clc
//******************************************************************************
//it must be  after command clear !!!
global LRP_OPTIONS
//load library
printf('Lading library to Scilab envirnoment.\n');
exec(LRP_OPTIONS.path+'/library/loadLibrary.sci');
//******************************************************************************






// We set up a initial value the generator of Scilab
rand('seed',getdate("x"));

//close all open graphics windows
//close_windows=winsid();
xdel(winsid());




//******************************************************************************
//working directory
//******************************************************************************
WORKING_DIRECTORY=LRP_OPTIONS.path+'\example';

//chnge current directory to: WORKING_DIRECTORY
printf('Changing current directory to WORKING_DIRECTORY:\n\t%s\n',WORKING_DIRECTORY);
chdir(WORKING_DIRECTORY);
//******************************************************************************


//******************************************************************************
// LaTeX
//******************************************************************************
title='Subject of some intresing topic';
author='Blazej Cichy';
nameOfLatexFile='new_2.tex';

//create directory for latex article
printf('Create directory for LaTeX article:\n');
[dirOfLaTeXArt,nameOfLatexFileFull]=createDirectoryForLaTeX(WORKING_DIRECTORY, nameOfLatexFile);
printf('\t%s\n',dirOfLaTeXArt);
//******************************************************************************


//******************************************************************************
// Parameters of our model
//******************************************************************************
printf('Setting parameters for our process:\n');
//nuber of states
n=7;
//nuber of outputs
m=4;
//nuber of inputs
r=5;

//nuber of points on the pass (alpha)
number_of_points=15;
//nuber of passes (beta)
number_of_passes=25;
//boundary for random values in LRP matrices
boundaryValue=[-0.8, 0.8];

printf('\tnumber of states: %d\n',n);
printf('\tnumber of inputs: %d\n',r);
printf('\tnumber of outputs: %d\n',m);
printf('\tnumber of points on a pass: %d\n', number_of_points);
printf('\tnumber of passes: %d\n', number_of_passes);
printf('Matrices: A, B, B0, C, D and D0 will be random from values: [%g, %g].\n',boundaryValue(:)');
//========================================


//make matrices LRP ->  A,B,B0,C,D,D0
printf('Creating matrices: A, B, B0, C, D and D0 for model LRP.\n');
[matricesLRP]=genRandLRP(n, m, r, boundaryValue);
//make LRP
printf('Creating structure for LRP model as variable: lrp.\n');
lrp=createLRPModel(matricesLRP, number_of_points, number_of_passes);


//******************************************************************************
//bonduary condition
//******************************************************************************
printf('Setting boundary conditions.\n');
// lrp.x0=zeros(n,lrp.beta);  //for example
//initial pass
// lrp.Y0=ones(m,lrp.alpha);    //for example

[lrp]=boundCond_x0_y1(lrp);
//[lrp]=boundCond_x1_y0(lrp);
//[lrp]=boundCond_x1_y1(lrp)


//matrix inputs
printf('Setting signal input matrix U as zero matrix.\n');
U=zeros(lrp.alpha,lrp.beta+1,r); //for example




[nameFigure, wFigPSO, wFigPVH]=createListStructure();


//*****************************************************************************
// I fill this above structure: wFigPVH
//*****************************************************************************
// which state
//===================================
wFigPVH.state.state=[3 5 3 5 2 1 2]; //here no unique states, some are repeated 3, 2 and 5
// state vertical
//===================================
//this is for wFigPVH.state.state(1) == 3
wFigPVH.state.vertical(1)=[3 8 10];
//this is for wFigPVH.state.state(2) == 5
wFigPVH.state.vertical(2)=[12 3 7 11 9 3 7]; //here no unique vertical states, some are repeated 3 and 7
//this is for wFigPVH.state.state(3) == 3
wFigPVH.state.vertical(3)=[1 2 3 4 5 6 7 8 9 10];
// state horizontal
//===================================
wFigPVH.state.horizontal(1)=[3 8 10];
wFigPVH.state.horizontal(2)=[3 8 10];

// which output
//===================================
wFigPVH.output.output=[2 1 3];
// output vertical
//===================================
wFigPVH.output.vertical(1)=[1 2 3 4 ];
wFigPVH.output.vertical(2)=[5 6 7 8 ];
// output horizontal
//===================================
wFigPVH.output.horizontal(1)=[3 8 2 12];
//*****************************************************************************










printf('Setting which states and outputs will be plot:\n');
//*************************************************************************************
// prepare empty structure in which number indicate what have to be plot;  State and Output ONLY !!!
//*************************************************************************************
//wFigPSO=tlist(['figure_nr_SO'; 'state'; 'output'], [], []);
//*************************************************************************************
wFigPSO.state=[1 1 2 1 2 1];
wFigPSO.output=[1 1 3 2 3 1 2 4 2];
printf('\tstates: ');
for i=1:length(wFigPSO.state),  printf('%d,',wFigPSO.state(i)); end
printf('\n\toutputs: ');
for i=1:length(wFigPSO.output), printf('%d,',wFigPSO.output(i)); end
printf('\n');

printf('Checking for corectness states and outputs that will be plot.\n')
[wFigPSO]=checkFigPSO(wFigPSO);


nameOfController='none';
printf('Create directory for controller: %s\n',nameOfController);
[dirOfLaTeXController]=createDirForFig(dirOfLaTeXArt, nameOfController);  //directory
printf('\t%s\n',dirOfLaTeXController);

printf('Setting the number of graphic window:\n\tnrGW=%d\n',0);
nrGW=0;

printf('Calculate mesh to plot (this may take some time)... ');
[gso]=createGSO(lrp, U);    //calculate (or recalculate) model after stabilization
printf('done.\n');

printf('Setting parameters to plots.\n');
parToPlot=tlist(['parToPlot';'wFigPlot';'nameOfController';'nameOfLatexFile';'dirOfLaTeXController'],...
    wFigPSO, ...
    nameOfController, nameOfLatexFile, dirOfLaTeXController);

printf('Plotting figures.\n');
[nrGW]=plotSeriesSO(parToPlot, lrp, gso, nrGW);

// //It can be done as
// parToPlot=tlist(['parToPlot';'wFigPlot';'nameOfController';'nameOfLatexFile';'dirOfLaTeXController'],...
//     wFigPVH,... // !!! here is diferent structure, than before !!!
//     nameOfController, nameOfLatexFile, dirOfLaTeXController);
//or simpler as
parToPlot.wFigPlot=wFigPVH;

soField='state';
[nrGW]=plotSeriesSOVH(soField, 'horizontal', parToPlot, lrp, gso, nrGW);
[nrGW]=plotSeriesSOVH(soField, 'vertical',   parToPlot, lrp, gso, nrGW);

soField='output';
[nrGW]=plotSeriesSOVH(soField, 'horizontal', parToPlot, lrp, gso, nrGW);
[nrGW]=plotSeriesSOVH(soField, 'vertical',   parToPlot, lrp, gso, nrGW);


//****************************************************************************************
nameOfController='controller1';

printf('Calculate stable process by controller: %s (this may take a while)...\n',nameOfController);
[lrp]=controller1(lrp);

printf('Chcking the solution:\n');
[ind]=findLRPIndex(lrp,'controller',nameOfController);
if (lrp.controller(ind).solutionExists == %f) then
    printf('\tno solution exists for that problem.\n');
else
    printf('\tsolution found.\n');
    //*****************************

    printf('Create directory for controller: %s\n',nameOfController);
    [dirOfLaTeXController]=createDirForFig(dirOfLaTeXArt, nameOfController);  //directory
    printf('\t%s\n',dirOfLaTeXController);

    printf('Calculate mesh to plot (this may take some time)... ');
    [gso]=createGSO(lrp, U);    //calculate (or recalculate) model after stabilization
    printf('done.\n');

    printf('Setting parameters to plots.\n');
    parToPlotSO=tlist(['parToPlotSO';'wFigPlot';'nameOfController';'nameOfLatexFile';'dirOfLaTeXController'],...
        wFigPSO, nameOfController, nameOfLatexFile, dirOfLaTeXController);

    printf('Plotting figures.\n');
    [nrGW]=plotSeriesSO(parToPlotSO, lrp, gso, nrGW);
end



//****************************************************************************************
//stability
//****************************************************************************************
printf('Setting the oryginal model values.\n')
[lrp]=setnone(lrp);

nameOfTestSyability="stAlongThePass1";

printf('Checking that the process is stable by: %s...\n',nameOfTestSyability);
[lrp]=stAlongThePass1(lrp);

[indStab]=findLRPIndex(lrp,'stability',nameOfTestSyability);
if lrp.stability(indStab).solutionExists==%T
   printf('The oryginal system is stable\n');
else
   printf('The oryginal system is unstable\n');
end

//closed loop process
printf('Setting the closed loop model values.\n')
lrp=setcontroller1(lrp);

printf('Checking that the closed loop process is stable by %s...\n',nameOfTestSyability);
[lrp]=stAlongThePass1(lrp);

[indStab]=findLRPIndex(lrp,'stability',nameOfTestSyability);
if lrp.stability(indStab).solutionExists==%T
   printf('The oryginal system is stable\n');
else
   printf('The oryginal system is unstable\n');
end

//setting the oryginal values
printf('Setting the oryginal model values.\n')
[lrp]=setnone(lrp);


// // //------------------------------------------------------------------------------
// // //check for unique values in 'wFigPVH' -- because some elements, by
// // //mistake can repated, then it will be remove
// // //------------------------------------------------------------------------------
// // //[wFigPVH]=checkWhichFigurePlot(wFigPVH);






// // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // //plot the i-th state
// // //printf("Ploting states\n");
// // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // // // // for i=1:length(wFigPVH.state.state)
// // // // // //     fvi=wFigPVH.state.state(i);
// // // // // //     if (fvi > n) | (fvi <= 0)
// // // // // //        error('No exist that state: '+ sci2exp(fvi, 0));
// // // // // //     end
// // // // // //     plotState(grid_X, grid_Y, X, fvi, nrGW, n, lrp);
// // // // // //     nameFigure.state.state(i)=getNamePicture(nameOfLatexFile,'state',fvi);
// // // // // //     nrGW=figToEps(nrGW,nameFigure.state.state(i));
// // // // // // end
// // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // //plot the i-th output
// // //printf("\nPloting outputs\n");
// // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // // // // for i=1:length(wFigPVH.output.output)
// // // // // //     fvi=wFigPVH.output.output(i);
// // // // // //     if (fvi > m) | (fvi < 0)
// // // // // //        error('No exist that output: '+ sci2exp(fvi,0));
// // // // // //     end
// // // // // //     plotOutput(grid_X, grid_Y, Y, fvi, nrGW, m, lrp);
// // // // // //     nameFigure.output.output(i)=getNamePicture(nameOfLatexFile,'output',fvi);
// // // // // //     nrGW=figToEps(nrGW,nameFigure.output.output(i));
// // // // // // end
// // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////[nrGW]=plotSeriesSO(plotOutput, wFigPVH, nameOfLatexFile, nrGW, 'output', gridXY, Y, lrp);
// // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // //--------------------------------------------
// // //plot horizontal profile (along the pass)
// // //---------------------------------------------
// // //printf("\nPloting horizontal profile (along the pass)\n");
// // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // soField='state';
// // // hvField='horizontal';
// // // [nrGW]=plotSeriesSOVH(plotHorProfile,  wFigPVH, nameOfLatexFile, nrGW, soField, hvField, X, lrp);
// // // hvField='vertical';
// // // [nrGW]=plotSeriesSOVH(plotVertProfile, wFigPVH, nameOfLatexFile, nrGW, soField, hvField, X, lrp);



// // //nuber graphics widow
// // nrGW=0;


// // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // //for controller: none
// // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // //state
// // name_of_controller='none';
// // nameFigureC=nameFigure; //copy oryginal structure
// // [gso]=createGSO(lrp, U); //calculate model before stabilization
// // createDirForFig(WORKING_DIRECTORY,directory,name_of_controller);
// // [nrGW, nameFigureC]=plotSeriesSO('state', wFigPVH, nameOfLatexFile, nrGW, gso, lrp, nameFigureC, name_of_controller);
// // [nrGW, nameFigureC]=plotSeriesSOVH('state', 'horizontal', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // [nrGW, nameFigureC]=plotSeriesSOVH('state', 'vertical', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // //output
// // [nrGW, nameFigureC]=plotSeriesSO('output',wFigPVH, nameOfLatexFile, nrGW, gso, lrp, nameFigureC, name_of_controller);
// // [nrGW, nameFigureC]=plotSeriesSOVH('output', 'horizontal', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // [nrGW, nameFigureC]=plotSeriesSOVH('output', 'vertical', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);

// // [ind]=findLRPIndex(lrp,'controller',name_of_controller);
// // lrp.controller(ind).nameOfFigures=nameFigureC;
// // chdir(WORKING_DIRECTORY);



// // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // //for controller: controller1
// // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // name_of_controller='controller1';
// // [lrp]=controller1(lrp);
// // //state
// // nameFigureC=nameFigure; //copy oryginal structure
// // [gso]=createGSO(lrp, U); //recalculate model after stabilization
// // createDirForFig(WORKING_DIRECTORY,directory,name_of_controller);
// // [nrGW, nameFigureC]=plotSeriesSO('state', wFigPVH, nameOfLatexFile, nrGW, gso, lrp, nameFigureC, name_of_controller);
// // [nrGW, nameFigureC]=plotSeriesSOVH('state', 'horizontal', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // [nrGW, nameFigureC]=plotSeriesSOVH('state', 'vertical', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // //output
// // [nrGW, nameFigureC]=plotSeriesSO('output',wFigPVH, nameOfLatexFile, nrGW, gso, lrp, nameFigureC, name_of_controller);
// // [nrGW, nameFigureC]=plotSeriesSOVH('output', 'horizontal', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // [nrGW, nameFigureC]=plotSeriesSOVH('output', 'vertical', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);

// // [ind]=findLRPIndex(lrp,'controller',name_of_controller);
// // lrp.controller(ind).nameOfFigures=nameFigureC;
// // chdir(WORKING_DIRECTORY);


// // // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // //for controller: controller2
// // // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // name_of_controller='controller2';
// // // [lrp]=controller2(lrp);
// // // //state
// // // nameFigureC=nameFigure; //copy oryginal structure
// // // [gso]=createGSO(lrp, U); //recalculate model after stabilization
// // // createDirForFig(WORKING_DIRECTORY,directory,name_of_controller);
// // // [nrGW, nameFigureC]=plotSeriesSO('state', wFigPVH, nameOfLatexFile, nrGW, gso, lrp, nameFigureC, name_of_controller);
// // // [nrGW, nameFigureC]=plotSeriesSOVH('state', 'horizontal', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // // [nrGW, nameFigureC]=plotSeriesSOVH('state', 'vertical', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // // //output
// // // [nrGW, nameFigureC]=plotSeriesSO('output',wFigPVH, nameOfLatexFile, nrGW, gso, lrp, nameFigureC, name_of_controller);
// // // [nrGW, nameFigureC]=plotSeriesSOVH('output', 'horizontal', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // // [nrGW, nameFigureC]=plotSeriesSOVH('output', 'vertical', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);

// // // [ind]=findLRPIndex(lrp,'controller',name_of_controller);
// // // lrp.controller(ind).nameOfFigures=nameFigureC;
// // // chdir(WORKING_DIRECTORY);

// // // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // //for controller: controller3
// // // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // name_of_controller='controller3';
// // // [lrp]=controller3(lrp);
// // // //state
// // // nameFigureC=nameFigure; //copy oryginal structure
// // // [gso]=createGSO(lrp, U); //recalculate model after stabilization
// // // createDirForFig(WORKING_DIRECTORY,directory,name_of_controller);
// // // [nrGW, nameFigureC]=plotSeriesSO('state', wFigPVH, nameOfLatexFile, nrGW, gso, lrp, nameFigureC, name_of_controller);
// // // [nrGW, nameFigureC]=plotSeriesSOVH('state', 'horizontal', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // // [nrGW, nameFigureC]=plotSeriesSOVH('state', 'vertical', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // // //output
// // // [nrGW, nameFigureC]=plotSeriesSO('output',wFigPVH, nameOfLatexFile, nrGW, gso, lrp, nameFigureC, name_of_controller);
// // // [nrGW, nameFigureC]=plotSeriesSOVH('output', 'horizontal', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);
// // // [nrGW, nameFigureC]=plotSeriesSOVH('output', 'vertical', wFigPVH, nameOfLatexFile, nrGW,  gso, lrp, nameFigureC, name_of_controller);

// // // [ind]=findLRPIndex(lrp,'controller',name_of_controller);
// // // lrp.controller(ind).nameOfFigures=nameFigureC;
// // // chdir(WORKING_DIRECTORY);




//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// LaTeX
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//[namesDirUsedFig]=getNamesDirUsedFig(lrp);
[f]=latexHeader(title,author,nameOfLatexFileFull);

    //description to none
    [path_to_file]=convertPathToUnix(LRP_OPTIONS.path+'/control/latex/description/describenone');
    mfprintf(f,"\\input{""%s""}\n",path_to_file);
    //matrices of none
    mfprintf(f,"\\section{Controller: none}\n");
    mfprintf(f,"The system matrices are:\n");
    writenone(f,lrp);

    //description to controller1
    [path_to_file]=convertPathToUnix(LRP_OPTIONS.path+'/control/latex/description/describecontroller1');
    mfprintf(f,"\\input{""%s""}\n",path_to_file);

    mfprintf(f,"\\section{Controller: controller1}\n");
    writecontroller1(f,lrp)

    //structure
    parToLaTeXSaveFig=tlist(['parToLaTeXSaveFig';...
        'nameOfLatexFile';'nameOfController';'wFigPlot'],...
         nameOfLatexFile,  'controller1',  wFigPSO);

    //(B)efore and (A)fter stabilization
    saveFigToTeXSO_BA(f,lrp,parToLaTeXSaveFig);

    mfprintf(f,'\\clearpage\n');
    //given controller (S)tate and (O)utput
    saveFigToTeX_SO(f,lrp,parToLaTeXSaveFig);

    mfprintf(f,'\\clearpage\n');
    //given controller (S)tate or (O)utput, (B)efore and (A)fter stabilization
    saveFigToTeX_SorO_BA(f,lrp,parToLaTeXSaveFig,'state');
    mfprintf(f,'\\clearpage\n');
    //given controller (S)tate or (O)utput, (B)efore and (A)fter stabilization
    saveFigToTeX_SorO_BA(f,lrp,parToLaTeXSaveFig,'output',3);

    mfprintf(f,'\\clearpage\n');
    //save only state
    saveFigToTeX_SorO(f,lrp,parToLaTeXSaveFig,'state');
    mfprintf(f,'\\clearpage\n');
    //save only output
    saveFigToTeX_SorO(f,lrp,parToLaTeXSaveFig,'output');

    mfprintf(f,"\\section{Sekcja}\n");
    mfprintf(f,"Some stiupid text.\n");

latexFooter(f);



// // // //==============================================================================
// // // //go to directory of picture
// // // //==============================================================================
// // // chdir(previous_directory);
// // // clear previous_directory
// // // //==============================================================================




// // // // //zapisuje w tej formie by sprawdzic czy sa dobre wykresy z toolboxem Gramackich
// // // // f=mopen("wyniki.txt","w");
// // // //  mfprintf(f,"a=%s\n", sci2exp(lrp.alpha,0));
// // // //  mfprintf(f,"a=%s\n", sci2exp(lrp.beta,0));
// // // //     mfprintf(f,"a=%s\n", sci2exp(lrp.A,0));
// // // //     mfprintf(f,"b=%s\n", sci2exp(lrp.B,0));
// // // //     mfprintf(f,"b0=%s\n", sci2exp(lrp.B0,0));
// // // //     mfprintf(f,"c=%s\n", sci2exp(lrp.C,0));
// // // //     mfprintf(f,"d=%s\n", sci2exp(lrp.D,0));
// // // //     mfprintf(f,"d0=%s\n", sci2exp(lrp.D0,0));
// // // // mclose(f);



// // cd(WORKING_DIRECTORY);

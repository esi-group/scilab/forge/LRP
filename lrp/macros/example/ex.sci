// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-04-03 16:09:00


//cleaning, setting, closing
clear
clc
//******************************************************************************
//it must be  after the command clear !!!
global LRP_OPTIONS
//load library
printf('Loading library to Scilab envirnoment.\n');
exec(LRP_OPTIONS.path+'/library/loadLibrary.sci');
//******************************************************************************

// We set up a initial value the generator of Scilab
rand('seed',getdate("x"));

//close all open graphics windows
//close_windows=winsid();
xdel(winsid());




//******************************************************************************
//working directory
//******************************************************************************
//pathExample="example";
WORKING_DIRECTORY=LRP_OPTIONS.path;

//chnge current directory to: WORKING_DIRECTORY
printf('Changing current directory to WORKING_DIRECTORY:\n\t%s\n', WORKING_DIRECTORY);
chdir(WORKING_DIRECTORY);
//******************************************************************************


//******************************************************************************
// LaTeX
//******************************************************************************
title='Subject of some intresing topic';
author='Blazej Cichy';
// nameOfLatexFile='new_2.tex';

//create directory for latex article
// printf('Creating directory for LaTeX article:\n');
// [dirOfLaTeXArt,nameOfLatexFileFull]=createDirectoryForLaTeX(WORKING_DIRECTORY, nameOfLatexFile);
// printf('\t%s\n',dirOfLaTeXArt);
//******************************************************************************


//******************************************************************************
// Parameters of our model
//******************************************************************************
printf('Setting parameters for the process:\n');
//nuber of states
n=2;
//nuber of outputs
m=2;
//nuber of inputs
r=1;

//nuber of points on the pass (alpha)
number_of_points=15;
//nuber of passes (beta)
number_of_passes=200;
//boundary for random values in LRP matrices
boundaryValue=[-0.8, 0.8];

printf('\tnumber of states n=%d\n',n);
printf('\tnumber of inputs r=%d\n',r);
printf('\tnumber of outputs m= %d\n',m);
printf('\tnumber of points alpha=%d\n', number_of_points);
printf('\tnumber of passes beta=%d\n', number_of_passes);
printf('Creating model matrices: A, B, B0, C, D, D0\n\twith randomly generated values (with normal distribution)\n\tfrom %g to %g\n',boundaryValue(:)');
//========================================


//make matrices LRP ->  A,B,B0,C,D,D0
printf('\tgenerating...\n');
[matricesLRP]=genRandLRP(n, m, r, boundaryValue);
//make LRP
printf('\tcreating structure for LRP model as ''lrp'' variable...\n');
lrp=createLRPModel(matricesLRP, number_of_points, number_of_passes);
printf('\tFinished creating matrices.\n');

//******************************************************************************
//bonduary condition
//******************************************************************************
printf('Setting boundary conditions.\n');
// lrp.x0=zeros(n,lrp.beta);  //for example
//initial pass
// lrp.Y0=ones(m,lrp.alpha);    //for example
[lrp]=boundCond_x0_y1(lrp);
//[lrp]=boundCond_x1_y0(lrp);
//[lrp]=boundCond_x1_y1(lrp)


// Caution: assumes that lrp.r is correct number of inputs
printf('Creating input signal ''U'' as a zero matrix \n\t(i.e. ''beta+1'' vectors, ''alpha'' points each -''r'' dimensional matrix).\n');
//for example
//[U]=zeros(lrp.dim.alpha, lrp.dim.beta+1, lrp.dim.r);
lrp=setGenerator(lrp,"u","uZero");



lrpMergedDatToPlots=createPlotDataStub();

// lrpMergedDatToPlots=lrpAddPlot(...
//    lrp, ...
//    "3DPlot", "state", ...
//    [1 2 1], ...
//    list("all", [1 2 3 8 9 10 11 12 13 14],[1 2 10 13]), ...
//    list([ 1  3], "all", [0 11 ]), ...
//    lrpMergedDatToPlots ...
// );
lrpMergedDatToPlots=lrpAddPlot(...
   lrp, ...
   "passtopass", "state", ...
   [1 2 1], ...
   list("all", [1 2 3 8 9 10 11 12 13 14],[1 2 10 13]), ...
   list([ 190  200], "all", 1), ...
   lrpMergedDatToPlots ...
);

// lrpMergedDatToPlots=lrpAddPlot(...
//    lrp, ...
//    "alongthepass", "state", ...
//    [1 2 1], ...
//    list("all", [1 2 3 8 9 10 11 12 13 14],[1 2 10 13]), ...
//    list([ 1  3], "all", [0 11 ]), ...
//    lrpMergedDatToPlots ...
// );

// disp("assssssssssssssssssss 00")
//
// lrpMergedDatToPlots=lrpAddPlot(...
//    lrp, ...
//    "alongthepass", "output", ...
//    [1 2 2], ...
//    list("all", [ 6 12],[ 2 10 11]), ...
//    list([ 1  3], ["all"], [6 11 ]), ...
//    lrpMergedDatToPlots ...
// );
//



// lrpMergedDatToPlots=lrpAddPlot(...
//    lrp, ...
//    "alongthepass", "state", ...
//    [1 2 2], ...
//    list("all", [ 6 12],[ 2 10 11]), ...
//    list([ 1  3], "all", [6 11 ]), ...
//    lrpMergedDatToPlots ...
// );
//

// lrpMergedDatToPlots=add2DPlot(...
//    lrp, ...
//    "alongthepass", "output", ...
//    [1 2 1], ...
//    list([1 4 2 3 9 ], [1 2 3 8 9 10 11 12 13 14],[1 2 10 13]), ...
//    list([ 1  3], ['all'], [0 11 ]), ...
//    lrpMergedDatToPlots ...
// );


// lrpMergedDatToPlots=add2DPlot(...
//    lrp, ...
//    "passtopass", "state", ...
//    [1 2 1], ...
//    list([1 4 2 3 9 ], [1 2 3 8 12 14 7 4],[1 2 10 13]), ...
//    list([1 3], ["all"], [0 11 ]), ...
//    lrpMergedDatToPlots ...
// );
//
// lrpMergedDatToPlots=add2DPlot(...
//    lrp, ...
//    "alongthepass", "output", ...
//    [1 2 1], ...
//    list([1 4 2 3 9 ], [1 2 3 8 12 14 7 4],[1 2 10 13]), ...
//    list([1 3], ["all"], [0 11 ]), ...
//    lrpMergedDatToPlots ...
// );
//


// lrpMergedDatToPlots=add2DPlot(...
//    lrp, ...
//    'passtopass', 'state', ...
//    [1 2 ], ...
//    list( ...
//      [1 2 3 ], ...
//      [1 2 3 8 9 10 ] ...
//    ),...
//    list( ...
//      [ 1  3], ...
//      ['all'] ...
//    ),...
//    lrpMergedDatToPlots...
// );
//
//
//
// lrpMergedDatToPlots=add2DPlot(...
//    lrp, ...
//    'passtopass', 'output', ...
//    [1 2], ...
//    list( ...
//      [1 2 3 ], ...
//      [1 2 3 8 9 10 ] ...
//    ),...
//    list( ...
//      [ 1  3], ...
//      ['all'] ...
//    ),...
//    lrpMergedDatToPlots...
// );
//
//
// lrpMergedDatToPlots=add2DPlot(...
//    lrp, ...
//    'alongthepass', 'output', ...
//    [2 2 2], ...
//    list( ...
//      [1 4 2 3 9 ], ...
//      [1 2 3 8 9 10 11 12 13], ...
//      [1 2 10 6] ...
//    ),...
//    list( ...
//      [ 1  3], ...
//      ['all'], ...
//      [0 4 ] ...
//    ),...
//    lrpMergedDatToPlots...
// );
//
// lrpMergedDatToPlots=add2DPlot(...
//    lrp, ...
//    'alongthepass', 'output', ...
//    [2], ...
//    list([1 2 3 8 9 10 11 12 13]), ...
//    list(['all']), ...
//    lrpMergedDatToPlots ...
// );
//
//
// //[gso]=createGSO(lrp, U);
//
// // lrpPlot3D(...
// //    'state', ...  //string: what to plot: 'state' ('x')or 'output' ('y')
// //    gso.grid.X, ...          //matrix grid on OX
// //    gso.grid.Y, ...          //matrix grid on OY
// //    gso.output, ...                  //what has to be painted:  matrix 3 dimensional
// //    1, ...  //scalar - which number shuld be plot
// //    [],...  //range axis on OX: vector 2 elements [10,30] - [from, to]
// //    [],...  //range axis on OX: vector 2 elements [10,30] - [from, to]
// //    1, ...  //scalar: number of grapchic window
// //    lrp ... //system lrp
// // );
// //
// //figToEps(1, 'ala121');
// //unix_s('evince ala121.pdf &');
//
//
// // lrpPlot2D(...
// //    'output', ...  //string: what to plot: 'state' ('x') or 'output' ('y')
// //    'alongthepass', ... //string: 'alongthepass' ('horizontal') or 'passtopass' ('vertical')
// //    gso.output,...                   //what has to be painted:  matrix 3 dimensional
// //    1, ...  //scalar - which number shuld be plot
// //    [2,3,4,5,10],...    //which profiles
// //    [], ...   //range axis on OX or OY: vector 2 elements [10,30] -> [from, to]
// //    2, ...  //scalar: number of grapchic window
// //    lrp ... //system lrp
// // );
// //
// // lrpPlot2D(...
// //    'output', ...  //string: what to plot: 'state' ('x') or 'output' ('y')
// //    'passtopass', ... //string: 'alongthepass' ('horizontal') or 'passtopass' ('vertical')
// //    gso.output,...                   //what has to be painted:  matrix 3 dimensional
// //    2, ...  //scalar - which number shuld be plot
// //    [2,3,4,5,10],...    //which profiles
// //    [], ...   //range axis on OX or OY: vector 2 elements [10,30] -> [from, to]
// //    3, ...  //scalar: number of grapchic window
// //    lrp ... //system lrp
// // );
// //
// //
// // lrpPlot2D(...
// //    'state', ...  //string: what to plot: 'state' ('x') or 'output' ('y')
// //    'alongthepass', ... //string: 'alongthepass' ('horizontal') or 'passtopass' ('vertical')
// //    gso.state,...                   //what has to be painted:  matrix 3 dimensional
// //    1, ...  //scalar - which number shuld be plot
// //    [2,3,4,5,10],...    //which profiles
// //    [], ...   //range axis on OX or OY: vector 2 elements [10,30] -> [from, to]
// //    4, ...  //scalar: number of grapchic window
// //    lrp ... //system lrp
// // );
// //
// // lrpPlot2D(...
// //    'state', ...  //string: what to plot: 'state' ('x') or 'output' ('y')
// //    'passtopass', ... //string: 'alongthepass' ('horizontal') or 'passtopass' ('vertical')
// //    gso.state,...                   //what has to be painted:  matrix 3 dimensional
// //    2, ...  //scalar - which number shuld be plot
// //    [2,3,4,5,10],...    //which profiles
// //    [], ...   //range axis on OX or OY: vector 2 elements [10,30] -> [from, to]
// //    5, ...  //scalar: number of grapchic window
// //    lrp ... //system lrp
// // );
// //
// //
// // lrpPlot2D(...
// //    'state', ...  //string: what to plot: 'state' ('x') or 'output' ('y')
// //    'alongthepass', ... //string: 'alongthepass' ('horizontal') or 'passtopass' ('vertical')
// //    gso.state,...                   //what has to be painted:  matrix 3 dimensional
// //    1, ...  //scalar - which number shuld be plot
// //    [2,3,4,5,8,12,10],...    //which profiles
// //    [], ...   //range axis on OX or OY: vector 2 elements [10,30] -> [from, to]
// //    6, ...  //scalar: number of grapchic window
// //    lrp ... //system lrp
// // );
// //
// // lrpPlot2D(...
// //    'state', ...  //string: what to plot: 'state' ('x') or 'output' ('y')
// //    'passtopass', ... //string: 'alongthepass' ('horizontal') or 'passtopass' ('vertical')
// //    gso.state,...                   //what has to be painted:  matrix 3 dimensional
// //    2, ...  //scalar - which number shuld be plot
// //    [2,3,4,5,8,12,10],...    //which profiles
// //    [], ...   //range axis on OX or OY: vector 2 elements [10,30] -> [from, to]
// //    7, ...  //scalar: number of grapchic window
// //    lrp ... //system lrp
// // );
// //
// // lrpPlot2D(...
// //    'output', ...  //string: what to plot: 'state' ('x') or 'output' ('y')
// //    'alongthepass', ... //string: 'alongthepass' ('horizontal') or 'passtopass' ('vertical')
// //    gso.output,...                   //what has to be painted:  matrix 3 dimensional
// //    1, ...  //scalar - which number shuld be plot
// //    [2,3,4,5,7,9,10],...    //which profiles
// //    [], ...   //range axis on OX or OY: vector 2 elements [10,30] -> [from, to]
// //    9, ...  //scalar: number of grapchic window
// //    lrp ... //system lrp
// // );
// //
// // lrpPlot2D(...
// //    'output', ...  //string: what to plot: 'state' ('x') or 'output' ('y')
// //    'passtopass', ... //string: 'alongthepass' ('horizontal') or 'passtopass' ('vertical')
// //    gso.output,...                   //what has to be painted:  matrix 3 dimensional
// //    2, ...  //scalar - which number shuld be plot
// //    [2,3,4,5,7,8,10],...    //which profiles
// //    [], ...   //range axis on OX or OY: vector 2 elements [10,30] -> [from, to]
// //    10, ...  //scalar: number of grapchic window
// //    lrp ... //system lrp
// // );
// //
// //
// //
// // // figToEps(3, 'ala3');
// // // unix_s('evince ala3.pdf &');
// //
// //
// //
// // // lrpPlot3D(...
// // //    'state', ...  //string: what to plot: 'state' ('x')or 'output' ('y')
// // //    gso.grid.X, ...          //matrix grid on OX
// // //    gso.grid.Y, ...          //matrix grid on OY
// // //    gso.state, ...                  //what has to be painted:  matrix 3 dimensional
// // //    1, ...  //scalar - which number shuld be plot
// // //    [],...  //range axis on OX: vector 2 elements [10,30] - [from, to]
// // //    [],...  //range axis on OX: vector 2 elements [10,30] - [from, to]
// // //    1, ...  //scalar: number of grapchic window
// // //    lrp,... //system lrp
// // // )
// //
// //
// //
// //
// //
// // // // // lrpMergedDatToPlots=plotSeriesSOVH(lrp, ...
// // // // //    U, "/home/prac/scilab/LRP/report.tex", ...
// // // // //    'alongthepass', 'output', ...
// // // // //    [2 4 3 1 3 2], ...
// // // // //    list( ...
// // // // //      [1 2 3 11 12 13 14 15 16 17 18 19 20], ...
// // // // //      [1 2 3 8 9 10 15 16 17 18 19 20], ...
// // // // //      [1 2 3 8 9 10 11 12 13 14  18 19 20], ...
// // // // //      [1 2 3 8 9 10 11 12 13 14 15 16 17 ], ...
// // // // //      [1 2 ], ...
// // // // //      [1 2 10 18] ...
// // // // //    ),...
// // // // //    lrpMergedDatToPlots ...
// // // // // );
// // // // //
// // // // // // lrpMergedDatToPlots=plotSeriesSOVH(lrp, ...
// // // // // //    U, "/home/prac/scilab/LRP/report.tex", ...
// // // // // //    '3D', 'state', ...
// // // // // //    [1 ], ...
// // // // // //    lrpMergedDatToPlots ...
// // // // // // );
// // // // // //
// // // // // // lrpMergedDatToPlots=plotSeriesSOVH(lrp, ...
// // // // // //    U, "/home/prac/scilab/LRP/report.tex", ...
// // // // // //    '3D', 'output', ...
// // // // // //    [1, 2, 3, 4 ], ...
// // // // // //    lrpMergedDatToPlots ...
// // // // // // );
// // // // // //
// //
// //
// //
// // // // //
// // // // //
// // // // // //Create the PDF
// // // // // //exportToLaTeX(lrp,lrpMergedDatToPlots,"Result generated by the LRP Toolkit for Scilab","LRP Toolkit");
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // //
// // // // // //
// // // // // //
// // // // // //
// // // // // // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // // // // // LaTeX
// // // // // // //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// // // // // // //[namesDirUsedFig]=getNamesDirUsedFig(lrp);
// // // // // // [f]=latexHeader(title,author,nameOfLatexFileFull);
// // // // // //
// // // // // //     //description to none
// // // // // //     [path_to_file]=convertPathToUnix(LRP_OPTIONS.path+'/control/latex/description/describenone');
// // // // // //     mfprintf(f,"\\input{""%s""}\n",path_to_file);
// // // // // //     //matrices of none
// // // // // //     mfprintf(f,"\\section{Controller: none}\n");
// // // // // //     mfprintf(f,"The system matrices are:\n");
// // // // // //     writenone(f,lrp);
// // // // // //
// // // // // // //     //description to controller1
// // // // // // //     [path_to_file]=convertPathToUnix(LRP_OPTIONS.path+'/control/latex/description/describecontroller1');
// // // // // // //     mfprintf(f,"\\input{""%s""}\n",path_to_file);
// // // // // // //
// // // // // // //     mfprintf(f,"\\section{Controller: controller1}\n");
// // // // // // //     writecontroller1(f,lrp)
// // // // // // //
// // // // // // //     //structure
// // // // // // //     parToLaTeXSaveFig=tlist(['parToLaTeXSaveFig';...
// // // // // // //         'nameOfLatexFile';'nameOfController';'wFigPlot'],...
// // // // // // //          nameOfLatexFile,  'controller1',  wFigPSO);
// // // // // // //
// // // // // // //     //(B)efore and (A)fter stabilization
// // // // // // //     saveFigToTeXSO_BA(f,lrp,parToLaTeXSaveFig);
// // // // // // //
// // // // // // //     mfprintf(f,'\\clearpage\n');
// // // // // // //     //given controller (S)tate and (O)utput
// // // // // // //     saveFigToTeX_SO(f,lrp,parToLaTeXSaveFig);
// // // // // // //
// // // // // // //     mfprintf(f,'\\clearpage\n');
// // // // // // //     //given controller (S)tate or (O)utput, (B)efore and (A)fter stabilization
// // // // // // //     saveFigToTeX_SorO_BA(f,lrp,parToLaTeXSaveFig,'state');
// // // // // // //     mfprintf(f,'\\clearpage\n');
// // // // // // //     //given controller (S)tate or (O)utput, (B)efore and (A)fter stabilization
// // // // // // //     saveFigToTeX_SorO_BA(f,lrp,parToLaTeXSaveFig,'output',3);
// // // // // // //
// // // // // // //     mfprintf(f,'\\clearpage\n');
// // // // // // //     //save only state
// // // // // // //     saveFigToTeX_SorO(f,lrp,parToLaTeXSaveFig,'state');
// // // // // // //     mfprintf(f,'\\clearpage\n');
// // // // // // //     //save only output
// // // // // // //     saveFigToTeX_SorO(f,lrp,parToLaTeXSaveFig,'output');
// // // // // //
// // // // // //     mfprintf(f,"\\section{Sekcja}\n");
// // // // // //     mfprintf(f,"Some stiupid text.\n");
// // // // // //
// // // // // // latexFooter(f);
// // // // // //


plotLRP(lrp, lrpMergedDatToPlots);

// //go to home LRP
cd(LRP_OPTIONS.path);


// rodzajRys - jakie rysunki maja byc wlaczane do pdf-a
//           rodzajRys == 1  <-- kolorowe rysunki,
//           rodzajRys == 2  <-- czarnobiale rysunki,

function save_includegraphics(f, ile_rysowac)


//wchodzimy do katalogu losowego
cd(katalogST.los_pelny);



if ktPole==1,
    stanCzyWyjscie='x';
else
    stanCzyWyjscie='y';
end


nazwyPol=fieldnames(rysNazwyST.prS);  //{'stan' 'wyjscie'}
// tak samo jest dla:
//      nazwyPol=fieldnames(rysNazwyST.poS);  %{'stan' 'wyjscie'}

licz=1;
pom=0;
for i=1:ile_rysowac
    fprintf(f,'\n\\begin{figure}[h]\n\\centering\n');

    fprintf(f,'\\begin{tabular}{cc}\n');
        // przed stabilizacja
        [PATHSTR,NAME_pr,EXT,VERSN] = fileparts(rysNazwyST.prS.(nazwyPol{ktPole}){rodzajRys,i});
        fprintf(f,'\t\\includegraphics[scale=0.39]{%s}\n&%%\n',NAME_pr); %bez rozszerzenia

        // po stabilizacji
        [PATHSTR,NAME_po,EXT,VERSN] = fileparts(rysNazwyST.poS.(nazwyPol{ktPole}){rodzajRys,i});
        fprintf(f,'\t\\includegraphics[scale=0.39]{%s}\n',NAME_po); %bez rozszerzenia
    fprintf(f,'\\end{tabular}\n');

   // Simulation for open and closed loop process $x_k^{1}(p)$
   // or
   // Simulation for open and closed loop process $y_k^{1}(p)$
    fprintf(f,'\\caption{Simulation for open and closed loop process $%c_k^{%d}$.}\n', stanCzyWyjscie, i);
    fprintf(f,'\\label{rys:%c:%d-%d}\n',stanCzyWyjscie,licz,licz+1);
    fprintf(f,'\\end{figure}\n');
    licz=licz+2;
end



pola=fieldnames(rysNazwyST);  //{'prS', 'poS'}

// konwersja na wasciwy format eps bb na poczatku a nie na koncu
for kp=1:length(pola)
    epsToEpsCorrect(rysNazwyST.(pola{kp}).(nazwyPol{ktPole})(1,:));  // kolorowe rysunki
    epsToEpsCorrect(rysNazwyST.(pola{kp}).(nazwyPol{ktPole})(2,:));  // czarnobiale rysunki
end

for kp=1:length(pola)
    //konwersja na pdf-y - kolorowych a potem czarnobialych - petla od 1 do 2
    for rodzajRys=1:2
        for i=1:ile_rysowac
            [s,m]=system(['epstopdf "' rysNazwyST.(pola{kp}).(nazwyPol{ktPole}){rodzajRys,i} '"']);
            if (s~=0),
                disp('Nie powiodlo sie EPSTOPDF.');
                fprintf(1,'%s -- %d',m,s);
                error('BLAD');
            end
        end
    end
end



// wracamy do katalogu domowego
cd(katalogST.domowy);

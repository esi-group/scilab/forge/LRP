//
// //lrp=createStubLRP()
// function [lrp]=createStubLRP()
//
// //constant
// STUB='STUB';
//
// //matrices
// A=STUB;
// B=STUB;
// B0=STUB;
// C=STUB;
// D=STUB;
// D0=STUB;
//
// //sizes
// n=STUB;
// r=STUB;
// m=STUB;
//
// //points and passes
// numberOfPoints=STUB;
// numberOfPasses=STUB;
//
//
// //bonduary condition
// x0=STUB;
// //initial pass
// Y0=STUB;
//
//
// //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// //setting typed list for controller "none"
// CONTROLLER_NONE=tlist(...
// ['none';...
//    'solutionExists'; ...       //solution exists (always true)
//    'A';'B';'B0';'C';'D';'D0'; ...  ///name of matrices;
// ],...
//     %T, ... //always true (a priori)
//     A, B, B0, C, D, D0 ...
// );
// //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//
//
// //range on axis
// //points 0 <= p <= alpha - 1
// pmin=STUB;
// pmax=STUB;;   // such long as number of points minus one:  alpha-1
//
// //passes 0 <= k <= beta
// kmin=STUB;
// kmax=STUB;   // such long as number of passes:  beta
//
//
// //dimensions
// tl_dim=tlist(...
// ['lrp_dim_classic'; ...  //name of this list - dimensions of classic lrp
//    'n'; ...  //number of states
//    'r'; ...  //number of inputs
//    'm'; ... //number of outputs
//    'alpha'; ... //number of points
//    'beta';...     //number of passes
//    'pmin'; ... //range on axis on points; each point starts from 'pmin' and ends at 'pmax', including 'pmin' and 'pmax'
//    'pmax'; ...//range on axis on points;     pmin <= p <= pmax
//    'kmin'; ... //range on axis on passes; each pass starts from 'kmin' and ends at 'kmax', including 'kmin' and 'kmax'
//    'kmax'; ...//range on axis on passes    kmin <= k <= kmax
// ],...
//    n,r,m,...
//    numberOfPoints,...
//    numberOfPasses,...
//    pmin, pmax, ...
//    kmin, kmax ...
// );
//
// tl_mat=tlist(...
// ['lrp_mat_classic'; ...  //matrices of classic LRP
//    'A';'B';'B0';...  //name of matrices
//    'C';'D';'D0' ...
// ],...
//    A, B, B0,...
//    C, D, D0 ...
// );
//
//
// //create lrp
// lrp=tlist(...
// ['lrp_classic'; ...
//    'type' ...
//  ... // 'dim'; ...
//  ... // 'mat'; ...
//   ... //'x0';'Y0'; ... //boundary conditions
//   ...// 'controller'; ...  //list of controllers
//   ...// 'indController' ...  //index which controller is current
// ], ...
//    0 ...            //type of LRP model; 0 -means classic LRP model
//    ...//tl_dim, ...
//    ...//tl_mat, ...
//   ...//  x0, Y0, ...
//   ...//  list(CONTROLLER_NONE),... //controller none - the first controller
//   ...//  1 ...                     //indController - which controller is used from above list. 1==no controller
// );
// endfunction

//exec ('/home/prac/scilab/LRP_drastyczneZmiany/example/ex.sci',-1)

//[lrp]=createStubLRP();

//lrp=tlist(['qwertyuiqqqwwwwer';'type'],0);
clear
lrp=mlist(['slurp','type'],0);



//function []=%qwertyuiqqqwwwwer_p(arg)
function []=%slurp_p(arg)
printf('type:'); disp(arg.type)
// printf('mat: [tlist]\n');
// printf('dim: [tlist]\n');
// printf('x0:'); disp(arg.x0);
// printf('Y0:'); disp(arg.Y0);
endfunction

lrp
// saveMat(f,matricesLst,saveLst)
//
// f=mopen('matrices.tex','w');
// A=rand(2,4);
// B=rand(6,2);
// matricesLst=tlist(['MatricesLst';...
//     'nameOfMat';...
//     'mat'],...
//     list(),list());

// matricesLst.nameOfMat=list('\widetilde{A}', '\widehat{B}');
// matricesLst.mat=list(A, B);
// saveMat(f,matricesLst);
// mclose(f);


function saveMat(f,matricesLst,saveLst);

[nargout,nargin]=argn();

numOfMatrices=length(matricesLst.mat);

if nargin==2 then
    saveLst=tlist(['SaveLst';...
      'fonts';...             //which kind of fonts must be set: \small, \footnotesize
      'sepBtCols';...         //sep beetwen cols in array envirnoment 'bmatrix'
      'frac';...              //precision of value data in matrix
      'cutAlign';...          //cut (sep) in 'align' - how many matrix in one row
      'cutBmatrix'],...       //how many columns in 'bmatrix'
      '\small','2pt',4,2,8 ...
    );
end


// fonts and sep beetwen cols
if (~isempty(saveLst.fonts) & ~isempty(saveLst.sepBtCols)) then
    nap='\setlength\arraycolsep{' + saveLst.sepBtCols + '}';
    mfprintf(f,'{%s%s',saveLst.fonts,nap);

// separate cols
elseif ~isempty(saveLst.sepBtCols) then
    nap='{\setlength\arraycolsep{' + saveLst.sepBtCols + '}';
    mfprintf(f,'%s',nap);

// insert fonts
elseif ~isempty(saveLst.fonts) then
    mfprintf(f,'{%s',saveLst.fonts);
end


begAL='\begin{align*}';
endAL='\end{align*}';

// save begin align
mfprintf(f,'%s\n',begAL);

countM=0;
for i=1:numOfMatrices
    countM=countM+1;
    zm(f,matricesLst.nameOfMat(i), matricesLst.mat(i), [], saveLst.frac);
    if (countM ~= numOfMatrices) & (modulo(i,saveLst.cutAlign)~=0),
        mfprintf(f,',&%%\n');
    elseif (modulo(i,saveLst.cutAlign) == 0) & (countM ~= numOfMatrices)
        mfprintf(f,',\\\\%%\n');
    end
end

// save end align
mfprintf(f,'%s',endAL);

if ~isempty(saveLst.fonts) & ~isempty(saveLst.sepBtCols) then
    mfprintf(f,'}%%');
elseif ~isempty(saveLst.fonts) then
    mfprintf(f,'}%%');
elseif ~isempty(saveLst.sepBtCols) then
     mfprintf(f,'}%%');
end

mfprintf(f,'\n');
endfunction

function [dirOfLaTeXControllerGraphicsFiles]=createDirForFig(pathToGraphicsFiles, nameOfController)
//create directory for pictures (based on the name of nameController)
[status,msg]=mkdir(nameOfController);
if (status == 0) then
   error(msg);
end
dirOfLaTeXControllerGraphicsFiles=fullfile(pathToGraphicsFiles,nameOfController);
endfunction
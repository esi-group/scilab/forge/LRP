// f=mopen('bigMatrix.tex','w');
// A=rand(7,100);
// zmp(f,'\widehat{A}',A,7,3);
// zmp(f,'\widehat{B}',A,5,8);
// mclose(f);
function zmp(f,nazwa,macierz,podzialCoIleKolumn,precyzja)

[nargout,nargin]=argn();

if nargin==4,
    precyzja=4;
end

[n_wierszy,m_kolumn]=size(macierz);

poczMac='\begin{matrix}';
konMac='\end{matrix}';

prec1='% 2.' + sci2exp(precyzja,0) + 'g & ';
prec2='% 2.' + sci2exp(precyzja,0) + 'g ';

[l_wierszy,l_kolumn]=size(macierz);

// zapis macierzy bez podzialu kolumn
if l_kolumn<=podzialCoIleKolumn,
    zm(f,nazwa,macierz,odstep,precyzja);
    return;
end

//ile pelnych macierzy
ileKolumnPelnych=floor(l_kolumn/podzialCoIleKolumn);
//ile w ostatniej macierzy bedzie kolumn
ileKolumnNiePelnych=l_kolumn-ileKolumnPelnych*podzialCoIleKolumn;



ind_start=1;
ind_koniec=podzialCoIleKolumn;

// poczatek macierzy
mfprintf(f,'%s&=\\left[%s\n',nazwa,poczMac);

// poczatk 1 macierzy
wnetrze(f,macierz,l_wierszy,l_kolumn,prec1,prec2,konMac,ind_start,ind_koniec)
mfprintf(f,'%s\\right.\n\\\\%%\n',konMac);

// jesli sa tylko pelne macierze
if ileKolumnNiePelnych==0,
    ileKolumnPelnych=ileKolumnPelnych-1;
end

// zapis pelnych macierzy
for i=1:ileKolumnPelnych-1
    ind_start=ind_koniec+1;
    ind_koniec=ind_start+podzialCoIleKolumn-1;
    mfprintf(f,'&%s\n',poczMac);
        wnetrze(f,macierz,l_wierszy,l_kolumn,prec1,prec2,konMac,ind_start,ind_koniec)
    mfprintf(f,'%s\n\\\\%%\n',konMac);
end

// zapisanie koncowej macierzy
ind_start=ind_koniec+1;
ind_koniec=l_kolumn;
mfprintf(f,'&\\left.%s\n',poczMac);
    wnetrze(f,macierz,l_wierszy,l_kolumn,prec1,prec2,konMac,ind_start,ind_koniec)
mfprintf(f,'%s\\right]\n',konMac);
endfunction




//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// zapis wnetrza macierzy
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function wnetrze(f,macierz,l_wierszy,l_kolumn,prec1,prec2,konMac,ind_start,ind_koniec)
licznikW=0;
for i=1:l_wierszy
   licznikW=licznikW+1;
   mfprintf(f,'\t');
   for j=ind_start:ind_koniec-1
      liczba=macierz(i,j);
      if liczba==0,
         mfprintf(f,'0 & ');
      else
         mfprintf(f,prec1,liczba);
      end
   end
   liczba=macierz(i,l_kolumn);

   // zapsz liczbe 0 jako 0 a nie jako 0.00000000000000000012
   if liczba==0,
      mfprintf(f,'0 ');
   else
      mfprintf(f,prec2,liczba);
   end

   if licznikW ~= l_wierszy,
       mfprintf(f,'\\\\\n');  // pisz '\\' na koncu wiersza
   else
       mfprintf(f,'\n');      // pisz 'nowa_linie' na koncu wiersza w ostatnim wierszu macierzy
   end
end
endfunction

function parseLaTeX(lrp, lrpPlotDataResult, templateFileName, outputFileName)
	START_DELIMETER="%%[";
	END_DELIMETER="]%%";

  requireType("lrp",lrp,"tlist","lrp_Sys_Cla");
  requireType("lrpPlotDataResult",lrpPlotDataResult,"tlist","lrp_Sys_Cla_Plt");
  requireType("templateFileName",templateFileName,"string")
  requireType("outputFileName",outputFileName,"string")
	// First, check whether template and output are not the same.
	//   Caution: This check will NOT catch every possible version
	if templateFileName==outputFileName then
		error('Template and output must be different files!');
		return;
	end

 [lrp,currentControllerNumber]=ltxSetController(lrp,1);

 [rootDir,fName,ext] = fileparts(outputFileName);

  [fdTemplate,err]=mopen(templateFileName,"r")
  if err~=0 then
  	error("Error opening template file:"+templateFileName);
  end
  [fdOutput,err]=mopen(outputFileName,"w")
  if err~=0 then
  	error("Error opening output file:"+outputFileName);
  end
	while ~meof(fdTemplate) 
  	str=mfscanf(1,fdTemplate,"%[^\n\r]s");
		if meof(fdTemplate) then
			break
		end
  	mgetstr(1,fdTemplate)
		if meof(fdTemplate) then
			break
		end
  	p=parseString(str);
		if ~isempty(p) then
			processedChars=1;
			for idx=1:(size(p,1)-1)
				if processedChars<=(p(idx,1)-1-length(START_DELIMETER)) then
					mfprintf(fdOutput,"%s",part(str,processedChars:(p(idx,1)-1-length(START_DELIMETER)))); // We need to skip the first char of the delimeter
				end				
	//disp('EXEC2:');
	//disp(part(str,p(idx,1):p(idx,2)));
				strToExecute=getStringToExecute(part(str,p(idx,1):p(idx,2)));
				execstr(strToExecute);
				
				processedChars=p(idx,2)+length(END_DELIMETER)+1
			end 
			// The last entry is different - there might be other characters
			//    after the last delimeter
			lastRow=size(p,1);
			mfprintf(fdOutput,"%s",part(str,processedChars:(p(lastRow,1)-1-length(START_DELIMETER))));
	//disp('EXEC:');
	//disp(part(str,p(lastRow,1):p(lastRow,2)))
			strToExecute=getStringToExecute(part(str,p(lastRow,1):p(lastRow,2)));
			execstr(strToExecute);
			
			mfprintf(fdOutput,"%s",part(str,(p(lastRow,2)+1+length(END_DELIMETER)):length(str)));
			mfprintf(fdOutput,"\n");
		else
			//No delimeters here - just plain LaTeX
			//disp(str)
			if ~isempty(str) then
				mfprintf(fdOutput,"%s",str)
			end				
			mfprintf(fdOutput,"\n")
		end
  end
  mclose(fdOutput);
	mclose(fdTemplate);
endfunction

function [idxVector]=parseString(str)
	posStart = strindex(str,[START_DELIMETER]);
	if isempty(posStart) then
		idxVector=[];
		return
	end
	posEnd = strindex(str,[END_DELIMETER]);
	if isempty(posEnd) then
		error("Unmatched delimeter:"""+START_DELIMETER+""" in string. Need closing:"""+END_DELIMETER+"""");
		idxVector=[];
		return	
	end
	idxVector=[posStart'+length(START_DELIMETER) posEnd'-1];
endfunction

function [lrp,currentControllerNumber]=ltxSetController(lrp,controllerNumber)
  requireType("lrp",lrp,"tlist","lrp_Sys_Cla");
  requireType('controllerNumber',controllerNumber,"integer");
  requireRange('controllerNumber',controllerNumber,1,length(lrp.controller));
//  currentControllerName=lrp.controller(controllerNumber).functionName;
  execstr('lrp=set'+lrp.controller(controllerNumber).functionName+'(lrp);');
  currentControllerNumber=controllerNumber;
endfunction;

function ltxFig(figureOptions, figureName, fdOutput)
  // figureOptions is optional

  // currentControllerName is global variable

  requireType('figureOptions',figureOptions,"string");
  
  [lhs,rhs]=argn();
  requireRange('Number of arguments',rhs,2,3);

  if rhs==2 then
     // NO OPTIONS
     //user called the function as ltxFig(figureOptions, figureName)
     //  we need to shift the variables
      requireType('fdOutput',figureName,"positive integer");
  else
      requireType('figureName',figureName,"string");
      requireType('fdOutput',fdOutput,"positive integer");
  end
  
  if rhs==2 then
     // NO OPTIONS
     //user called the function as ltxFig(figureOptions, figureName)
     //  we need to shift the variables
     fdOutput=figureName;
     figureName=figureOptions;
     figureOptions="";
     mfprintf(fdOutput,sprintf('\\includegraphics{%s/%s}',lrp.controller(currentControllerNumber).functionName,figureName))
  else
     mfprintf(fdOutput,sprintf('\\includegraphics[%s]{%s/%s}',figureOptions,lrp.controller(currentControllerNumber).functionName,figureName))
  end
endfunction;

function ltxCaption(figureName,fdOutput)
  // figureOptions is optional

  // currentControllerNumber is global variable

   requireType('figureName',figureName,"string");
   requireType('fdOutput',fdOutput,"positive integer");
   for plotNumber=1:length(lrpPlotDataResult.plotData)
       if lrpPlotDataResult.plotData(plotNumber).description.name==figureName then
          mfprintf(fdOutput,sprintf('\\caption{%s --- %s}',lrp.controller(currentControllerNumber).displayName, lrpPlotDataResult.plotData(plotNumber).description.title));
          return;
       end
   end
   error(sprintf('Unknown figure name: %s',figureName));
endfunction;

function ltxControllerName(fdOutput)
  // figureOptions is optional

  // currentControllerNumber is global variable

   requireType('fdOutput',fdOutput,"positive integer");

   mfprintf(fdOutput,sprintf('%s',lrp.controller(currentControllerNumber).displayName));
endfunction;

function str=getStringToExecute(str)
// Returns the string to execute in the Scilab workspace. Replaces
//   some commonly used functions with their short names: 
//
//  %%[ltxSetController(controllerNumber)]%% is replaced by
//  %%[[lrp,currentControllerNumber]=ltxSetController(lrp,controllerNumber)]%%
//
//  %%[ltxMatrix(MATRIX)]%% is replaced by (MATRIX is replaced by lrp.mat.MATRIX)
//  %%[ltxMatrix(lrp.mat.MATRIX,fdOutput)]%% 
//
//  %%[ltxMatrix(lrp.abc.MATRIX)]%% is replaced by (no changes to the MATRIX)
//  %%[ltxMatrix(lrp.abc.MATRIX,fdOutput)]%%

	if part(str,1:17)=='ltxSetController(' then
		strArg = part(str,(18:length(str)-1));
		if isempty(strindex(strArg,',')) then
			// One argument only --- form a complete command around it
			str='[lrp,currentControllerNumber]=ltxSetController(lrp,'+strArg+');'
		end
	else
		if part(str,1:10)=='ltxMatrix(' then
			strArg = part(str,(11:length(str)-1));
			if isempty(strindex(strArg,',')) then
				// One argument only --- form a complete command around it
				// Check, if we have any period characters
				if isempty(strindex(strArg,'.')) then
					// no - form a command
					str='ltxMatrix(lrp.mat.'+strArg+',fdOutput);';
				else
					// yes - do not add lrp.mat
					str='ltxMatrix('+strArg+',fdOutput);';
				end
			end		
		end  
	end
endfunction

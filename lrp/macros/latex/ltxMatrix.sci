function ltxMatrix(mtx, fileDescriptor)
	saveMat(fileDescriptor,mtx,2)
  //mfprintf(fileDescriptor,"%s","This is LRP!");  
endfunction

function saveMat(fid,mat,frac)

[nargout, nargin]=argn();

if (nargin==2) then
    frac=4;
end

[nrRows, nrCols]=size(mat)

//poczMac='\begin{bmatrix}';
//konMac='\end{bmatrix}';

prec1='% 2.' + sci2exp(frac,0) + 'g & ';
prec2='% 2.' + sci2exp(frac,0) + 'g ';

//if (nrRows+nrCols==2)
///   mfprintf(fid,'%s&='+prec2+'\n',name,mat);
//   return;
//end

//mfprintf(fid,'%s\n', poczMac);


countR=0;
for i=1:nrRows
   countR=countR+1;
   mfprintf(fid,'\t');
   for j=1:nrCols-1
      number=mat(i,j);
      if number==0,
         mfprintf(fid,'0 & ');
      else
         mfprintf(fid,prec1,number);
      end
      if modulo(j,8)==0, mfprintf(fid,'\n'); end
   end
   number=mat(i,nrCols);

   if number==0,
      mfprintf(fid,'0 ');
   else
      mfprintf(fid,prec2,number);
   end

   if countR ~= nrRows,
       mfprintf(fid,'\\\\\n');
   else
//       mfprintf(fid,'\n');
   end
end
//mfprintf(fid,'%s',konMac);

//mfprintf(fid,'\n');
endfunction


// LRP Toolkit for SciLab. This file is used to read the LRP model into the Tcl/Tk toolkit.
//
// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2008-10-10 15:19:00

function saveLaTeXFigure(win_num,filen,colored,orientation)
  [path,fname,extension]=fileparts(filen);
  filen=fullfile(path,fname);
  if ~isdef('colored') then
    colored=-1;
  end
  if ~isdef('orientation') then
    orientation='p';
  end

  pathCopy = pwd();
  cd(path);
  xs2eps(win_num,fname,colored,orientation);
  cd(pathCopy);
  if ~isdef('LRP_OPTIONS') then
    error('variable ""LRP_OPTIONS"" is undefined. Restart the Toolkit.');
    return;
  end
  if MSDOS==%T then
    printf("%s\n",fullfile(LRP_OPTIONS.latex_path,'epstopdf')+ ' ""' +filen+'.eps""');
    ccd=getcwd();
    cd(LRP_OPTIONS.latex_path);
    unix_w('epstopdf ""' +filen+'.eps""');
    cd(ccd);
  else
    unix_w(quoteStr(fullfile(LRP_OPTIONS.latex_path,'epstopdf'))+ ' ' +quoteStr(filen+'.eps'));
  end
endfunction


function createPDF(pathToTeXFile)
  requireType("pathToTeXFile",pathToTeXFile,"string");
  if LRP_OPTIONS.latex_path=="" then
     error("LaTeX support is disabled. Set LRP_OPTIONS.latex_path to enable");
     return;
  end
  [theDir, theFile, theExt]=fileparts(pathToTeXFile);
  workingDir=getcwd();
  cd(theDir);
  disp(fullfile(LRP_OPTIONS.latex_path,'pdflatex') + sprintf(' -interaction=nonstopmode --shell-escape ""%s"" -job-name=""%s.pdf""',pathToTeXFile,theFile));
  unix_s(fullfile(LRP_OPTIONS.latex_path,'pdflatex') + sprintf(' -interaction=nonstopmode --shell-escape ""%s""',pathToTeXFile));
  cd(workingDir);
endfunction

function showPDF(pathToPDFFile)
  //Opens the PDF file.
  // If the extension is different than .pdf it is changed back to .pdf
  //   Note: On Unix, as the size of the letters matters, (e.g. x.pdf <> x.PDF) 
  //     This function assumes ".pdf" (small letters). Such name is produced by createPDF.
  requireType("pathToPDFFile",pathToPDFFile,"string");
  if LRP_OPTIONS.latex_path=="" then
     error("LaTeX support is disabled. Set LRP_OPTIONS.latex_path to enable");
     return;
  end
  if (MSDOS) then
      run_pdf='start';
      com_batchmode="";
      run_options="";
      [theDir,theFile,theExt]=fileparts(pathToPDFFile);
      theExt='.pdf';
  else
     //check which program can I run PDF file
     open_PDF_with=['evince','xpdf','acroread','kpdf','kghostview','gpdf','gv'];
     run_pdf='';
     for i=1:size(open_PDF_with,2)
        results=unix_g('whereis '+open_PDF_with(i));
        [ind,which]=strindex(results, open_PDF_with(i));
        [ind,which2]=strindex(results, 'bin'); //check that is in binary tree, for example in: /usr/bin
        if (length(which)>=2) & (length(which2)>=1) then
           if open_PDF_with(i)=='xpdf' then
              run_options='-z width';
           end
           run_pdf=open_PDF_with(i);
           break;
        end
        com_batchmode=" &";
        [theDir,theFile,theExt]=fileparts(pathToPDFFile);
        theExt='.pdf';
     end
  end
   //call the pdfviwer
   if (run_pdf ~= '') then
      workingDir=getcwd();
      cd(theDir);
      printf('Trying to open PDF file:\n\t%s\n',pathToPDFFile);
      if run_pdf=='acroread' then
         printf('\tStarting ''acroread''. This may take a while.\n');
      end
      unix_s(run_pdf + ' ' + run_options + ' ' + theFile+theExt + com_batchmode);
      cd(workingDir);
   end
endfunction

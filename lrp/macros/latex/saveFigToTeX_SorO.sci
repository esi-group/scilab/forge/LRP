// parToLaTeXSaveFig=tlist(['parToLaTeXSaveFig';...
//  'nameOfLatexFile';'nameOfController';'wFigPlot'],...
//   nameOfLatexFile,  nameOfController,  wFigPSO);

function saveFigToTeX_SorO(f,lrp,parToLaTeXSaveFig,wFieldSO,howManyPlotSO)

[nargout, nargin]=argn();


if parToLaTeXSaveFig.nameOfController=='none' then
    error('Cannot use this controller: ' + parToLaTeXSaveFig.nameOfController + ' with this function: saveFigToTeX_SorO.');
end

[ind]=findLRPIndex(lrp,'controller',parToLaTeXSaveFig.nameOfController);
if ind==-1 then
    error('No such conntroller: '+ parToLaTeXSaveFig.nameOfController);
end

if (wFieldSO=='state') then 
    info1='x';
    info2='s'; 
elseif (wFieldSO=='output') 
    info1='y';  
    info2='o';
else
    error('Undefined field: '+wFieldSO);
end

howManyPlotSOtmp=length(parToLaTeXSaveFig.wFigPlot(wFieldSO));

if nargin==4 then
    howManyPlotSO=howManyPlotSOtmp;
else
    [w1,w2]=size(howManyPlotSO);
    if (w1+w2>2) then
        error('Variable ''howManyPlotSO'' should be a scalar.');
    end
    if (howManyPlotSO > howManyPlotSOtmp) | (howManyPlotSO < 1) then
        error('The number '+ wFieldSO +'s to plots is wrong. It have to in [1, '+ sci2exp(howManyPlotSOtmp,0) +'].')
    end
end


for i=1:2:howManyPlotSO
    mfprintf(f,'\n\\begin{figure}[h]\n\\centering\n');
    mfprintf(f,'\\begin{tabular}{cc}\n');
        
        NAME_FILE='./figures/'+parToLaTeXSaveFig.nameOfController+'/'+ ...   //given controller
            getNamePicture(parToLaTeXSaveFig.nameOfLatexFile,...
                parToLaTeXSaveFig.nameOfController, wFieldSO, ...
                parToLaTeXSaveFig.wFigPlot(wFieldSO)(i));
        mfprintf(f,'\t\\includegraphics[scale=0.35]{%s}\n&%%\n', NAME_FILE); 
        
        if (i+1 <= howManyPlotSO) then   
            NAME_FILE='./figures/'+parToLaTeXSaveFig.nameOfController+'/'+ ...   //given controller
                getNamePicture(parToLaTeXSaveFig.nameOfLatexFile,...
                    parToLaTeXSaveFig.nameOfController, wFieldSO, ...
                    parToLaTeXSaveFig.wFigPlot(wFieldSO)(i+1));
            mfprintf(f,'\t\\includegraphics[scale=0.35]{%s}\n',NAME_FILE);     
        else
            mfprintf(f,'\n');       
        end 
        
    mfprintf(f,'\\end{tabular}\n');
    
    if (i+1 <= howManyPlotSO) then 
        mfprintf(f,'\\caption{Simulation for process $%c_k^{%d}(p)$ and $%c_k^{%d}(p)$.}\n',...
            info1,parToLaTeXSaveFig.wFigPlot(wFieldSO)(i),info1,...
            parToLaTeXSaveFig.wFigPlot(wFieldSO)(i+1)); 
        mfprintf(f,'\\label{fig:so:%c:%d-%d}\n',...
            info2,parToLaTeXSaveFig.wFigPlot(wFieldSO)(i),...
            parToLaTeXSaveFig.wFigPlot(wFieldSO)(i+1));
    else
        mfprintf(f,'\\caption{Simulation for process $%c_k^{%d}(p)$.}\n',...
            info1,parToLaTeXSaveFig.wFigPlot(wFieldSO)(i)); 
        mfprintf(f,'\\label{fig:SorO:%c:%d}\n',info2,parToLaTeXSaveFig.wFigPlot(wFieldSO)(i));
    end 
    
    mfprintf(f,'\\end{figure}\n');
end
endfunction

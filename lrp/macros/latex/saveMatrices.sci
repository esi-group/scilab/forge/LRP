//saveMatrices - save matrices to file
//
// f=mopen('matrices.tex','w');
// A=rand(2,4);
// B=rand(6,2);
// matricesLst=tlist(['MatricesLst';...
//     'nameOfMat';...
//     'mat'],...
//     list(),list());

// matricesLst.nameOfMat=list('\widetilde{A}', '\widetilde{B}');
// matricesLst.mat=list(A, B);
// saveMat(f,matricesLst);
// mclose(f);


function saveMatrices(fileName,matricesLst,saveLst);
f=mopen(fileName,'w');
    saveMat(f,matricesLst,saveLst);
mclose(f);
endfunction

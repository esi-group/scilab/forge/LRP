// parToLaTeXSaveFig=tlist(['parToLaTeXSaveFig';...
//  'nameOfLatexFile';'nameOfController';'wFigPlot'],...
//   nameOfLatexFile,  nameOfController,  wFigPSO);



// // //create empty structure
// lrpFileListForLaTeX=tlist(['lrpFileListForLaTeX'; 'path'; 'name'; 'state'; 'output'],...
//  [], ...
//  [], ...
//  tlist(['state'; 'alongthepass'; 'passtopass'; 'threeD'],...
//      [],[],[]),...
//  tlist(['output'; 'alongthepass'; 'passtopass'; 'threeD'],...
//      [],[],[])...
// );



function saveFigToTeX_SO(f,lrp,parToLaTeXSaveFig,howManyPlotSO)

[nargout, nargin]=argn();


//if parToLaTeXSaveFig.nameOfController=='none' then
    //error('Cannot use this controller: ' + parToLaTeXSaveFig.nameOfController + ' with this function: saveFigToTeXSO.');
//end

[ind]=findLRPIndex(lrp,'controller',parToLaTeXSaveFig.nameOfController);
if ind==-1 then
    error('No such conntroller: '+ parToLaTeXSaveFig.nameOfController);
end


//nothing to plot
//if (isempty(parToLaTeXSaveFig.wFigPlot.state.threeD) | isempty(parToLaTeXSaveFig.wFigPlot.output.threeD)) then
    //disp('Empty')
//    return;
//end

//nos - number of states
nos=length(parToLaTeXSaveFig.wFigPlot.state.threeD);
//noo - number of outputs
noo=length(parToLaTeXSaveFig.wFigPlot.output.threeD);
howManyPlotSOtmp=[nos noo];


if nargin==3 then
    howManyPlotSO=howManyPlotSOtmp;
else
    if (howManyPlotSO(1) > nos) | (nos<1)
        error('The number states to plots is wrong. It have to in [1, '+ sci2exp(nos,0) +'].')
    end
    if (howManyPlotSO(2) > noo) | (noo<1)
        error('The number outputs to plots is wrong. It have to in [1, '+ sci2exp(noo,0) +'].')
    end
end


dirForFigLatex=basename(parToLaTeXSaveFig.wFigPlot.name);


//parToLaTeXSaveFig.wFigPlot.state.threeD
//parToLaTeXSaveFig.wFigPlot.output.threeD



direction='3D';
//first state ten output
for k=1:2
//    wField=parToLaTeXSaveFig.wFigPlot(1)(k+3);

    if (k==1) then
        info1='x';
        info2='s';
        variable='state';
    else
        info1='y';
        info2='o';
        variable='output';
    end

    for i=1:2:howManyPlotSO(k)
        mfprintf(f,'\n\\begin{figure}[!htb]\n\\centering\n');
        mfprintf(f,'\\begin{tabular}{cc}\n');

            desc=variable + '_' + sci2exp(parToLaTeXSaveFig.wFigPlot(variable).threeD(i),0) + '_' + direction;
            nameGF=strcat([dirForFigLatex '_fig_' parToLaTeXSaveFig.nameOfController  '_' desc]);
            NAME_FILE='./'+dirForFigLatex+'/'+parToLaTeXSaveFig.nameOfController+'/'+ nameGF;   //given controller
            mfprintf(f,'\t\\includegraphics[scale=0.4]{%s}\n&%%\n', NAME_FILE);

            if (i+1 <= howManyPlotSO(k)) then
                desc=variable + '_' + sci2exp(parToLaTeXSaveFig.wFigPlot(variable).threeD(i+1),0) + '_' + direction;
                nameGF=strcat([dirForFigLatex '_fig_' parToLaTeXSaveFig.nameOfController  '_' desc]);
                NAME_FILE='./'+dirForFigLatex+'/'+parToLaTeXSaveFig.nameOfController+'/'+ nameGF;   //given controller
                mfprintf(f,'\t\\includegraphics[scale=0.4]{%s}\n\\\\%%\n',NAME_FILE);
            else
                mfprintf(f,'\n');
            end

            if (i+1 <= howManyPlotSO(k)) then
                mfprintf(f,'\ta) Open loop process -- $%c_k^{%d}(p)$\n&%%\n',info1,parToLaTeXSaveFig.wFigPlot(variable).threeD(i));
                mfprintf(f,'\tb) Open loop process -- $%c_k^{%d}(p)$\n',info1,parToLaTeXSaveFig.wFigPlot(variable).threeD(i+1));
            end

        mfprintf(f,'\\end{tabular}\n');

        if (i+1 <= howManyPlotSO(k)) then
            //mfprintf(f,'\\caption{Simulation for process %s $%c_k^{%d}(p)$ and $%c_k^{%d}(p)$.}\n',...
            mfprintf(f,'\\caption{Simulation for process %ss.}\n', variable);
            mfprintf(f,'\\label{fig:so:%s:%c:%d-%d}\n',...
                parToLaTeXSaveFig.nameOfController,...
                info2,parToLaTeXSaveFig.wFigPlot(variable).threeD(i),...
                parToLaTeXSaveFig.wFigPlot(variable).threeD(i+1));
        else
            mfprintf(f,'\\caption{Simulation for proces %s $%c_k^{%d}(p)$.}\n',...
                variable,info1,parToLaTeXSaveFig.wFigPlot(variable).threeD(i));
            mfprintf(f,'\\label{fig:so:%s:%c:%d}\n',parToLaTeXSaveFig.nameOfController,...
                info2,parToLaTeXSaveFig.wFigPlot(variable).threeD(i));
        end

        mfprintf(f,'\\end{figure}\n');
    end
end
endfunction

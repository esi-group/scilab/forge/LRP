// parToLaTeXSaveFig=tlist(['parToLaTeXSaveFig';...
//  'nameOfLatexFile';'nameOfController';'wFigPlot'],...
//   nameOfLatexFile,  nameOfController,  wFigPSO);



// // //create empty structure
// lrpFileListForLaTeX=tlist(['lrpFileListForLaTeX'; 'path'; 'name'; 'state'; 'output'],...
//  [], ...
//  [], ...
//  tlist(['state'; 'alongthepass'; 'passtopass'; 'threeD'],...
//      [],[],[]),...
//  tlist(['output'; 'alongthepass'; 'passtopass'; 'threeD'],...
//      [],[],[])...
// );



function saveFigToTeX_SO_AaP(f,lrp,parToLaTeXSaveFig)

[nargout, nargin]=argn();

[ind]=findLRPIndex(lrp,'controller',parToLaTeXSaveFig.nameOfController);
if ind==-1 then
    error('No such conntroller: '+ parToLaTeXSaveFig.nameOfController);
end

// disp('1 saveFigToTeX_SO_AaP')
// //nothing to plot
// if isempty(parToLaTeXSaveFig.wFigPlot.state.alongthepass)  & ...
//    isempty(parToLaTeXSaveFig.wFigPlot.state.passtopass)    & ...
//    isempty(parToLaTeXSaveFig.wFigPlot.output.alongthepass) & ...
//    isempty(parToLaTeXSaveFig.wFigPlot.output.passtopass)   then
//     return;
// end
// disp('1 saveFigToTeX_SO_AaP')


dirForFigLatex=basename(parToLaTeXSaveFig.wFigPlot.name);


//first state then output
for so=1:2
    if (so==1) then
        variable='state';
        info1='x';
        info2='s';
    else
        variable='output';
        info1='y';
        info2='o';
    end

    //first alongthepass then passtopass
    for ap=1:2
        if (ap==1) then
            direction='alongthepass';
            info3='a';
            info4='along the pass';
        else
            direction='passtopass';
            info3='p';
            info4='pass to pass';
        end

        for i=1:length(parToLaTeXSaveFig.wFigPlot(variable)(direction))

            mfprintf(f,'\n\\begin{figure}[!htb]\n\t\\centering\n');

                desc=variable + '_' + sci2exp(parToLaTeXSaveFig.wFigPlot(variable)(direction)(i),0) + ...
                    '_' + direction + '_nrGF=' + sci2exp(i,0);
                nameGF=strcat([dirForFigLatex '_fig_' parToLaTeXSaveFig.nameOfController  '_' desc]);

                NAME_FILE='./'+dirForFigLatex+'/'+parToLaTeXSaveFig.nameOfController+'/'+ nameGF;   //given controller
                mfprintf(f,'\t\\includegraphics[scale=0.7]{%s}\n', NAME_FILE);

                mfprintf(f,'\\caption{Simulation %s for proces %s $%c_k^{%d}(p)$.}\n',...
                    info4,variable,info1,parToLaTeXSaveFig.wFigPlot(variable)(direction)(i));
                mfprintf(f,'\\label{fig:soap:%s:%c:%c:%d}\n',parToLaTeXSaveFig.nameOfController,...
                    info2,info3,parToLaTeXSaveFig.wFigPlot(variable)(direction)(i));

            mfprintf(f,'\\end{figure}\n');
        end
    end
end
endfunction

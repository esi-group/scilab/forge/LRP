//function [f]=latexHeader(title,author,nameOfLatexFile,namesDirUsedFig)
function [f]=latexHeader(title,author,nameOfLatexFile)

[f,err]=mopen(nameOfLatexFile,'w');
if (err~=0) then
       error('Creating new file for latex...');
end

//[path,fname,extension]=fileparts(nameOfLatexFile);

mfprintf(f,'\\documentclass[10pt,a4paper]{article}\n');
//%mfprintf(f,'\\usepackage[cp1250]{inputenc}\n');
//%mfprintf(f,'\\usepackage[OT4]{polski}\n');
mfprintf(f,'\\usepackage{anysize} \\marginsize{1.5cm}{1.5cm}{1.5cm}{1.5cm}\n');
mfprintf(f,'\\usepackage{cite}\n');
mfprintf(f,'\\usepackage{xcolor}\n');
mfprintf(f,'\\usepackage{graphicx}\n');
mfprintf(f,'\\usepackage{amsmath,amssymb,amsfonts,amsthm,amsxtra}\n');
mfprintf(f,'\\usepackage{hyperref}\n');
mfprintf(f,'\\usepackage[T1]{fontenc}\n');
//mfprintf(f,'\\usepackage{lastpage}\n');

mfprintf(f,'\\hypersetup{%%\n');
mfprintf(f,'\tpdftitle={%s},\n',title);
mfprintf(f,'\t%%pdfsubject={},\n');
mfprintf(f,'\tpdfauthor={Blazej Cichy, ISSI,\n');
mfprintf(f,'\tUniversity of Zielona Gora, Poland. e-mail: B.Cichy@issi.uz.zgora.pl},\n');
mfprintf(f,'\tpdfkeywords={LRP, LMI},\n');
mfprintf(f,'\tpdfpagemode={none},\n');
mfprintf(f,'\tpdfstartview={FitH},\n');
mfprintf(f,'\tcolorlinks={true},\n');
mfprintf(f,'\tlinkcolor={darkgray},\n');
mfprintf(f,'\tcitecolor={darkgray}\n}\n');


//mfprintf(f,'\\DeclareMathOperator{\\Sym}{{\\bf Sym}}\n');

mfprintf(f,'\\title{%s}\n',title);

mfprintf(f,'\\author{%s}\n',author);
mfprintf(f,'\\date{\\today}\n');

mfprintf(f,'\\DeclareMathOperator{\\Diag}{diag}\n');
mfprintf(f,'\\newcommand{\\diag}[1]{\\Diag\\left\\{#1\\right\\}}\n');


//mfprintf(f,'\\newcommand{\\dn}[1]{\\big(#1\\big)}\n');
//mfprintf(f,'\\newcommand{\\Dn}[1]{\\Big(#1\\Big)}\n');


mfprintf(f,'\\newtheorem{theorem}{Theorem}\n');
mfprintf(f,'\\newtheorem{corollary}{Corollary}\n');
mfprintf(f,'\\newtheorem{definition}{Definition}\n');
mfprintf(f,'\\newtheorem{proposition}{Proposition}\n');
mfprintf(f,'\\newtheorem{lemma}{Lemma}\n');
mfprintf(f,'\\newtheorem{remark}{Remark}\n');


// // // //grapics pathes
// // // mfprintf(f,'\\graphicspath{%%\n');
// // // for i=1:length(namesDirUsedFig)
// // //    mfprintf(f,'\t%s\n',namesDirUsedFig(i));
// // // end
// // // mfprintf(f,'}\n');


mfprintf(f,'\\addtocounter{MaxMatrixCols}{20} %%number columns in bmatrix envirnoment\n\n');

mfprintf(f,'\\begin{document}\n');
mfprintf(f,'\\maketitle\n');
endfunction

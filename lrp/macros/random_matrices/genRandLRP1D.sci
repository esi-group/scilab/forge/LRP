function [matricesLRP1D]=genRandLRP1D(n,m,r,boundaryValue,number_of_points)
//
// description:
// ============
// The function generates 1D equivalence matrices of LRP
//
// inputs:
// =======
// A,B,B0,C,D,D0 - matrices of a LRP model
// alpha(=number_of_points) - number of points on a given pass
//
// outputs:
// ========
// PHI, DELTA, THETA, GAMMA, SIGMA, PSI - 1D model matrices
//
// by Jaroslaw Gramacki, Technical University of Zielona Gora
// Date    : 03.1998
// Modified:

[nargout,nargin] = argn(0)

if (nargin~=2)
  error("Wrong number of input arguments.");
end

//create random matrices
[matricesLRP]=genRandLRP(n, m, r, boundaryValue);

//extract matrices from typed list (tlist)
[type_of_matrices,A,B,B0,C,D,D0]=matricesLRP(:)


// Galko's notation
// alpha = my_alpha
// my_alpha=number_of_points;

// ===================================================================
// Blocks in matrices :
// "dim" means dimension of one block within the whole matrix
// ===================================================================
// PhiDim      =size(D0);
// DeltaDim    =size(D);
// ThetaDim    =size(C);
// GammaDim    =size(B0);
// SigmaDim    =size(B);
// PsiDim      =size(A);

// ===================================================================
// Matrices dimensions :
// "size" means dimension of the whole matrix
// ===================================================================
// PhiSize     =PhiDim*alpha;
// DeltaSize   =DeltaDim*alpha;
// ThetaSize   =[ThetaDim(1)*alpha ThetaDim(2)];
// GammaSize   =GammaDim*alpha;
// SigmaSize   =SigmaDim*alpha;
// PsiSize     =[PsiDim(1)*alpha PsiDim(2)];



// extract matrices from typed list (tlist)
[type_of_matrices,A,B,B0,C,D,D0]=matricesLRP(:)
// we can also use following write
// matricesLRP.A <==> A
// matricesLRP.B <==> B
// etc.


//matrices 1D
Phi  =genMatrPhi(A, B0, C, D0, number_of_points);
Delta=genMatrDelta(A, B, C, D, number_of_points);
Theta=genMatrTheta(A, C, number_of_points);
Gamma=genMatrGamma(A, B0, number_of_points);
Sigma=genMatrSigma(A, B, number_of_points);
Psi  =genMatrPsi(A, number_of_points);

//create typed list (tlist)
matricesLRP1D=tlist(["lrp_Sys_Cla_Mat1D";"Phi";"Delta";"Theta";"Gamma";"Sigma";"Psi"],...
   Phi,Delta,Theta,Gamma,Sigma,Psi);
endfunction

function [u,parameters]=uZero(lrp,k,p, X, U, Y)
// CAUTION: 
// FOR DISPLAY: We can either have "LRP" argument of type "lrp_Sys_Cla" [classic LRP]
//   	or "LRP" can be equal to lrp.ext; the "lrp_Sys_Ext" tist.
// 		NOTE THAT THE ABOVE IS VALID FOR DISPLAY ONLY, WHEN THE NUMBER OF INPUT 
//		   ARGUMENTS EQUALS 0 OR 1
//
//  FOR CALCULATIONS: ALWAYS THE "FULL CLASSIC" LRP OF TYPE "lrp_Sys_Cla" IS GIVEN.   

if argn(2) <= 1 then
	u="0";
	parameters = list();
	return;
end 
u=zeros(lrp.dim.r,1);
parameters = lrp.ext.uParameters;
endfunction

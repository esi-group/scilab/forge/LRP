// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: Mon Aug 21 12:36:33 CEST 2006


// //-----------------------------------------------------
// //          Default property to plot 2D
// //-----------------------------------------------------
// // You can obtain it from the folowing code:
//
// xdel(winsid());
// x=1:10;
// y=rand(3,10);
// plot(x, y);
// h=scf(0);
//
// //from plot 1 - first row of y we have
//
// h.children.children.children(1)


// //------------------------------------------------------------------
// //PROPERTY_NAME,       DEFAULT_VALUE
// //------------------------------------------------------------------
// // "parent": Compound        ---> we omit this property
// // "children": []            ---> we omit this property
// // "visible"                 => "on"
// // "data" = matrix 10x2      ---> we omit the data value, of course you can change this parametr,
// //                                     but we do not perform any check on this operation
// // "closed"                  => "off"
// // "line_mode"               => "on"
// // "fill_mode"               => "off"
// // "line_style"              => 1
// // "thickness"               => 1
// // "arrow_size_factor"       => 1
// // "polyline_style"          => 1
// // "foreground"              => 5
// // "background"              => -2
// // "interp_color_vector"     => []
// // "interp_color_mode"       => "off"
// // "mark_mode"               => "off"
// // "mark_style"              => 0
// // "mark_size_unit"          => "point"
// // "mark_size"               => 0
// // "mark_foreground"         => -1
// // "mark_background"         => -2
// // "x_shift"                 => []
// // "y_shift"                 => []
// // "z_shift"                 => []
// // "bar_width"               => 0
// // "clip_state"              => "clipgrf"
// // "clip_box"                => []
// // "user_data"               => []

// //And a "color_map" is obtained from:
// h
// //or these values are obtained from
// h.color_map
//
//-----------------------------------------
// R                    G                  B
//-----------------------------------------
//     0.                  0.                  0.
//     0.                  0.                  1.
//     0.                  1.                  0.
//     0.                  1.                  1.
//     1.                  0.                  0.
//     1.                  0.                  1.
//     1.                  1.                  0.
//     1.                  1.                  1.
//     0.                  0.                  0.5647059
//     0.                  0.                  0.6901961
//     0.                  0.                  0.8156863
//     0.5294118    0.8078431    1.
//     0.                  0.5647059    0.
//     0.                  0.6901961    0.
//     0.                  0.8156863    0.
//     0.                  0.5647059    0.5647059
//     0.                  0.6901961    0.6901961
//     0.                  0.8156863    0.8156863
//     0.5647059    0.                  0.
//     0.6901961    0.                  0.
//     0.8156863    0.                  0.
//     0.5647059    0.                  0.5647059
//     0.6901961    0.                  0.6901961
//     0.8156863    0.                  0.8156863
//     0.5019608    0.1882353    0.
//     0.627451      0.2509804    0.
//     0.7529412    0.3764706    0.
//     1.                  0.5019608    0.5019608
//     1.                  0.627451      0.627451
//     1.                  0.7529412    0.7529412
//     1.                  0.8784314    0.8784314
//     1.                  0.8431373    0.
//     0.                  0.5                0.


// //property for:
// //x_label, y_label, z_label - only setting 'text' property, and 'font_size'
//
// //these can be obtained from:
// h.children.x_label  //smilarly the rest
//
// //and we have
// // Handle of type "Label" with properties:
// // =======================================
// // parent: Axes
// // visible = "on"
// // text = ""                          <-- we can setting only this value
// // foreground = -1
// // background = -2
// // fill_mode = "off"
// // font_style = 6
// // font_size = 1                      <-- we can setting only this value
// // font_angle = 0
// // auto_position = "on"
// // position = [5.5,-0.0827910]
// // auto_rotation = "on"

// -------------------------------------------------------------------
// THE REST OF PARAMETRS OF FIGURE
// -------------------------------------------------------------------
// you can set individually by returned handle to this graphic window
// for example a legend (see: help legend; or help legends)
// POKAZAC KONKRETNY PRZYKLAD
// -------------------------------------------------------------------


// -------------------------------------------------------------------
// BY THS FUNCTION YOU CAN SET THE FOLLOWING PROPERTIES:
// -------------------------------------------------------------------
// "visible"                 => "on"
// "data" = matrix 10x2      ---> we omit the data value, of course you can change this parametr,
//                                     but we do not perform any check on this operation
// "closed"                  => "off"
// "line_mode"               => "on"
// "fill_mode"               => "off"
// "line_style"              => 1
// "thickness"               => 1
// "arrow_size_factor"       => 1
// "polyline_style"          => 1
// "foreground"              => 5
// "background"              => -2
// "interp_color_vector"     => []
// "interp_color_mode"       => "off"
// "mark_mode"               => "off"
// "mark_style"              => 0
// "mark_size_unit"          => "point"
// "mark_size"               => 0
// "mark_foreground"         => -1
// "mark_background"         => -2
// "x_shift"                 => []
// "y_shift"                 => []
// "z_shift"                 => []
// "bar_width"               => 0
// "clip_state"              => "clipgrf"
// "clip_box"                => []
// "user_data"               => []
// ------------- COLOR MAP ----------------
// "color_map"               => an array [n rows by 3 cols] scaled fom 0 to 1
// ------------- X LABEL ----------------
// "text"                    => ""
// "font_size"               => 1
// ------------- Y LABEL ----------------
// "text"                    => ""
// "font_size"               => 1
// ------------- Z LABEL ----------------
// "text"                    => ""
// "font_size"               => 1



// //Example 1
//
// //Create 3 plots on one graphic window - with default values of plots
//
// x=1:10;
// y=rand(3,10);
// nrGraphicWindow=1;
// lrpPlot2D( x, y, nrGraphicWindow);


// //Example 2
//
// //Create 3 plots on one graphic window - 3 diffrent colors and line_style to 2
//
// x=1:10;
// y=rand(3,10);
// nrGraphicWindow=1;
// //and some optional arguments as:
// //          'propertyname_1', valueOfProperty_1,        'propertyname_2', valueOfProperty_2
// //
// //define 3 colors in RGB values
// color_map=[...
//     1 1 1; ...
//     0 0 0; ...
//     0.5 0.5 0.5 ...
// ];
// //and the setting the same line_style to all plots
// line_style=2;
// lrpPlot2D( x, y, nrGraphicWindow,  "color_map", color_map, "line_style", line_style);



// //Example 3
// //Create 3 plots on one graphic window
// x=1:10;
// y=rand(3,10);
// nrGraphicWindow=1;
// //and some optional arguments as:
// //          'propertyname_1', valueOfProperty_1,        'propertyname_2', valueOfProperty_2
// //
// //define 3 colors in RGB values - it can be only one matrix  [n-rows by 3-columns]
// color_map=[...
//     1 1 1; ...
//     0 0 0; ...
//     0.5 0.5 0.5 ...
// ];
// //and the 3 diffrent line_style for 3 plots on one graphic window,
// //      for plot 1 - line_style are 2
// //      for plot 2 - line_style are 5
// //      for plot 3 - line_style are 0 (default value)
// line_style=list(2, 5, 0);
// lrpPlot2D( x, y, nrGraphicWindow,  "color_map", color_map, "line_style", line_style);



// //Example 4
// //Create 3 plots on one graphic window
// x=1:10;
// y=rand(3,10);
// nrGraphicWindow=1;
// //and some optional arguments as:
// //          'propertyname_1', valueOfProperty_1,        'propertyname_2', valueOfProperty_2
// //
// //define 3 colors in RGB values - it can be only one matrix  [n-rows by 3-columns]
// color_map=[...
//     1 1 1; ...
//     0 0 0; ...
//     0.5 0.5 0.5 ...
// ];
// //and the line style for 3 plots on one graphic window,
// //      for plot 1 - line_style are 2
// //      for plot 2 - line_style are 5
// //      for plot 3 - line_style are 0 (default value)
// line_style=list(2, 5, 0);
// //and setting the same vale of thickness to all plots
// thickness=2;
// lrpPlot2D( x, y, nrGraphicWindow,  "color_map", color_map, "line_style", line_style, "thickness", thickness);





// //Example 5
// //Create 4 plots on one graphic window
// x=1:10;
// y=rand(4,10);
// nrGraphicWindow=1;

// [lrpPlotOptions2D]=createPlotOptions2DStub();

// lrpPlotOptions2D.thickness=2;
// lrpPlotOptions2D.foreground=list("red", [221 23 34], [0.1 0.1 0.1] , 3);

// lrpPlot2D( x, y, nrGraphicWindow, lrpPlotOptions2D);
//// a teraz nalezyprzetworzyc wartosci z kolorow funkcja:
// //         result=getScaledRGB(rgbVector)
// //         dla lrpPlotOptions2D.foreground=list("red", [221 23 34], [0.1 0.1 0.1] , 3);
// //      czyli wartosci:
// //           [221 23 34], [0.1 0.1 0.1] na -> getScaledRGB([0.1 0.1 0.1]) i otrzymam [26 26 26]






function [handleToGraphicWindow]=lrpPlot2D( x, y, nrGraphicWindow,  varargin)

// the first 3 arguments are required, the rest are optional
// x, y, nrGraphicWindow == 3
NUMBER_OF_REQUIRED_VAL=3;

// ==================== Arguments check =====================
//check types
requireType("x", x, ["integer vector"]);
      requireType("x", x, ["row vector"]);

requireType("y", y, ["2D matrix" "real vector", "real"]);

requireType("nrGraphicWindow", nrGraphicWindow, ["integer"]);
      requireRange("nrGraphicWindow", nrGraphicWindow, 0, %inf);

// ==================== Main ================================
[nargout, nargin]=argn();


if nargin == 3 then
    USE_DEFAULT_PLOT=%T;
else
    USE_DEFAULT_PLOT=%F;
end

//each plot are in column - so we need size of column
howManyPlots=size(y,2);

//get default values for property
[plotOptionsDefault]=createPlotOptions2DStub();
sizePlotOptionsDefault=lstsize(plotOptionsDefault);
nameOfProperty=plotOptionsDefault(1);

//is list of values?
isList=%F;

//define types as constant
//tlist
TYPE_T_LIST=16;



//==============================================================================
if nargin == 4 then
//==============================================================================

//obtained default structure
    lrpPlotOptions=varargin(1);
    requireType("lrpPlotOptions", lrpPlotOptions, "tlist", "lrp_Plt_Options2D");

    //check size of properties
    s=lstsize(lrpPlotOptions);
    if s ~= sizePlotOptionsDefault then
        printf("\nThe number of properties should be exactly: %d\n", sizePlotOptionsDefault);
        printf("\tbut currently is: %d\n", s);
        printf("Solution: \n\tCall function: ''[plotOptionsDefault]=createPlotOptions2DStub();''\n", s);
        printf("\tand then set appropriate values in variable ''plotOptionsDefault''.\n", s);
        error("Incorect number of properties ''lrp_Plt_Options2D''.");
    end

    propertyName=list();
    propertyValue=list();
    for i=2:sizePlotOptionsDefault
        propertyName($+1)=lrpPlotOptions(1)(i)
        propertyValue($+1)=lrpPlotOptions(i);
    end


//==============================================================================
elseif nargin >= 5
//==============================================================================
// some properties are only obtained

    //name_of_property_1 , value_1, name_of_property_2 , value_2, ... , name_of_property_n , value_n.
    if pmodulo(nargin - NUMBER_OF_REQUIRED_VAL, 2) ~= 0 then
        printf("***************************************************************\n\n");
        printf("The last property should have a value.\nCheck the numers of arguments.\n\n");
        printf("***************************************************************\n");
        error("Invalid name.");
    end

    ile=lstsize(varargin);
    propertyName=list();
    propertyValue=list();
    for i=1:2:ile
        propertyName($+1)=varargin(i);
        propertyValue($+1)=varargin(i+1);
    end
end
//#end#:  elseif nargin >= 5



//CHECKING NAMES e.g. is name a proper name?
lcCheckProperName(propertyName, nameOfProperty);

//CHECKING SUBSTRUCTURES for:
//      x_label, y_label, z_label
valuesToCheck=["x_label", "y_label", "z_label"];
fieldsToCheck=["text"; "font_size"];

sizePropertyName=lstsize(propertyName);
sizeValuesToCheck=size(valuesToCheck,'c');
for i=1:sizeValuesToCheck
    for j=1:sizePropertyName
        if propertyName(j)==valuesToCheck(i) then
            if typeof(propertyValue(j)) == "lrp_Plt_Options2DL" then
                namesOfLabel=propertyValue(j)(1);
                propertyValuesOfLabel=list();
                //checking names
                for k=2:size(fieldsToCheck,'c');
                    //property name MUST be a string
                    requireType(valuesToCheck(i)+".propertyName("+sci2exp(k)+")", namesOfLabel(k), "string");
                    propertyValuesOfLabel($+1)=propertyValue(j)(k);
                end
                //checking values
                for k=2:size(fieldsToCheck,'c');
                    //property value MUST be a string
                    requireType(valuesToCheck(i)+".propertyValue("+sci2exp(k)+")", propertyValuesOfLabel(k), "string");
                    //property value MUST be a integer in range <0,6>
                    requireType(valuesToCheck(i)+".propertyValue("+sci2exp(k)+")",propertyValuesOfLabel(k),["integer"])
                    requireRange(valuesToCheck(i)+".propertyValue("+sci2exp(k)+")",propertyValuesOfLabel(k),0,6);
                end
            else
                error("Incorrect type for ''"+ propertyName(j) +"''. It must be tlist of ''lrp_Plt_Options2DL''." );
            end
        end
    end
end


//(re)write  values to plotOptionsDefault
ile=lstsize(propertyName);
for i=1:ile
    plotOptionsDefault(propertyName(i))=propertyValue(i);
end



//CHECKING VALUES for proper values
for index = 1 : sizePlotOptionsDefault
    name=nameOfProperty(index);
    value=plotOptionsDefault(name);
    if (typeof(value) ~= "list") & (type(value) ~= TYPE_T_LIST) then
    //single value
        //if not error here
            checkBoundaryValues(plotOptionsDefault, name, value);
        //then OK
    elseif typeof(value) == "list" then
    //list of values
        isList=%T;
        if lstsize(value) ~= howManyPlots then
            error("Incorect number of values for plots. It must be equal number of plots e.g.: "+num2str(howManyPlots));
        end
        for nrPlot=1:howManyPlots
            //if not error here
                checkBoundaryValues(plotOptionsDefault, name, value(nrPlot));
            //then OK
        end
    end
end
//         //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//         //first we check color map
//         //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//         if (propertyName == "color_map") & (typeof(propertyValue) == "constant") then
//             //it must be a matrix [n by 3] == RGB
//             [rows,cols]=size(propertyValue);
//             if size(propertyValue, "c") ~= 3 then
//                 printf("***************************************************************\n\n");
//                 printf("Wrong number of columns for property\n\n     ""%s"".\n\n", propertyName);
//                 printf("Check the number of columns.\n\n");
//                 printf("It should be: [N x 3],\nbut currently is: [%d x %d].\n\n", size(propertyValue));
//                 printf("***************************************************************\n");
//                 error("Wrong size of property.");
//             end

//             //check that all values are in range <0,1>
//             for i=1:rows
//                 propertyValue(i,:)=lcGetCorrectRGB(propertyValue(i,:))/255;
//             end

//             //else evry thing is ok
//             color_map=propertyValue;
//         end
//         //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>




//CLONE VALUES IF isList
//we have a list, so we must clone all values, that are not in a list
if isList then
    for index = 1 : sizePlotOptionsDefault
        name=nameOfProperty(index);
        if (typeof(lrpPlotOptions(name)) ~= "list") & (type(lrpPlotOptions(name)) ~= TYPE_T_LIST) then
            //clone value for all plots
            value=lrpPlotOptions(name);
            lrpPlotOptions(name)=list();
            for nrPlot=1:howManyPlots
                lrpPlotOptions(name)(nrPlot)=value;
            end
        end
    end
end







//             for indexPlot=1 : howManyPlots
//                 currentPropertyValue=propertyValue(indexPlot);

//                 //if not error here
//                     checkBoundaryValues(plotOptionsDEFAULT, propertyName, currentPropertyValue, color_map);
//                 //then assign this value here
//                 lrpPlotOptions(propertyName)(indexPlot) = currentPropertyValue;
//             end

// // //if "propertyValue" is not a list
//         else
//             lrpPlotOptions(propertyName) = list();

//             //if not error here
//                 checkBoundaryValues(plotOptionsDEFAULT, propertyName, propertyValue, color_map);
//             //then assign this value here
//             for indexPlot=1 : howManyPlots
//                 lrpPlotOptions(propertyName)(indexPlot) = propertyValue;
//             end
//         end
//-------------------------------------------------------
// end the rest of parameters to check---------
//-------------------------------------------------------







handleToGraphicWindow=scf(nrGraphicWindow);
return






//
// if length(x) ~=1 then
//
//    lcPlotD2Line(x, y, whichGraphWindow, lrpPlotOptions, USE_DEFAULT_PLOT);
//
// else
// //     if nargin == 3 then
// //         lrpPlotOptions=tlist(["lrpPlotOptions"; ...
// //             "visible";                     "closed";                       "line_mode";                  "fill_mode"; ...
// //             "line_style";                "thickness";                  "arrow_size_factor";      "polyline_style"; ...
// //             "foreground";             "backround";                 "interp_color_vector";    "interp_color_mode"; ...
// //             "mark_mode";            "mark_style";                 "mark_size_unit";           "mark_size"; ...
// //             "mark_foreground";    "mark_background";     "x_shift";                         "y_shift"; ...
// //             "z_shift";                      "bar_width";                  "clip_state";                   "clip_box"; ...
// //             "user_data" ...
// //           ], ...
// //                 "on" ,              "off",          "on",           "off", ...
// //                 1,                    1,              1,                1, ...
// //                 5,                    -2,             [],               "off", ...
// //                 "off",                0,              "point",       0, ...
// //                 -1,                   -2,             [],                [], ...
// //                 [],                     0,              "clipgrf",      [], ...
// //                 [] ...
// //         );
// //     end
//
//    lcPlotD2Circle(pointsRange, lrpAlongThePassPlot.passes, values, whichGraphWindow, lrpAlongThePassPlot.description.legend);
// end


handleToGraphicWindow=scf(nrGraphicWindow);
endfunction







//--------------------------------------------------------------------------------------------------------------
//  function checkBoundaryValues(plotOptionsDEFAULT, propertyName, propertyValue, color_map)
//--------------------------------------------------------------------------------------------------------------
function checkBoundaryValues(plotOptionsDEFAULT, propertyName, propertyValue, color_map)
//*** UNSAFE ***

//     plotOptionsDEFAULT=tlist([ "lrpPlotOptions"; PROPERTY ], ...
//             "on" ,              "off",          "on",           "off", ...
//             1,                    1,              1,                1, ...
//             5,                    -2,             [],               "off", ...
//             "off",                0,              "point",       0, ...
//             -1,                   -2,             [],                [], ...
//             [],                     0,              "clipgrf",      [], ...
//             [] ...
//     );

defaultValue=plotOptionsDEFAULT(propertyName);

// string
if typeof(defaultValue) == "string" then
    select defaultValue
        case "on" then
                allowedValue=["on"; "off"];
        case "off" then
                allowedValue=["on"; "off"];
        case "point" then
                allowedValue=["point"; "tabulated"];
        case "clipgrf" then
                allowedValue=["clipgrf"; "on"; "off"];
        else
                error("Not implemented");
    end
    allowedType="string";

// empty
elseif isempty(defaultValue) then
    requireType("propertyValue("+indexPlot+")", propertyValue, "vector");
    allowedType="vector";

//  integer value
elseif typeof(defaultValue) == "constant" then
    requireType("propertyValue("+indexPlot+")", propertyValue, "integer");
    allowedType="integer";

//uknown type
else
    error("Not implemented.");
end

// NOW WE CAN CHECKING the value
if typeof(propertyValue) == "list" then

    //check allowed values
    for indexPlot=1:lstsize(propertyValue)
        select allowedType
            case "string" then
                if or(propertyValue(indexPlot) == allowedValue) == %F then
                    printf("*****************************************************************\n");
                    printf("\nWrong value for property name:\n    ""%s"".\n\n", propertyName);
                    printf("It shuld be a string with one of posible value: ");
                    printf("%s\n", allowedValue);
                    printf("\n*****************************************************************\n");
                    error("Wrong value.");
                end

            //FOR VECTOR I COULD'T CHECK THESE VALUES, so I omitt it here and pass it to Scilab
            // *** UNSAFE ***
            case "vector" then
                    //nothing to do

            case "integer" then
                [minVal, maxVal]=lcGetMinMaxValueOfProperty(propertyName, color_map);
                requireRange(propertyName+"("+sci2exp(indexPlot,0)+")", propertyValue(indexPlot), minVal, maxVal);
            else
                error("Not implemented");
        end
    end
end
// END check allowed values

endfunction







//--------------------------------------------------------------------------------------------------------------
//  function [minVal, maxVal]=lcGetMinMaxValueOfProperty(propertyName, color_map)
//--------------------------------------------------------------------------------------------------------------
function [minVal, maxVal]=lcGetMinMaxValueOfProperty(propertyName, color_map)
// *** UNSAFE ***

//get PROPERTY
[PROPERTY, sizeProperty]=lcGetPROPERTY();

[nargout, nargin]=argn();

if nargin==1 then
    minValColor=-2;
    maxValColor=33;
else
    minValColor=-2;
    maxValColor=size(color_map, "r");
end

select propertyName
    case "polyline_style"           then  minVal=0;         maxVal=5;
    case "line_style"                 then   minVal=0;         maxVal=5;
    case "thickness"                 then   minVal=0;         maxVal=30;
    case "arrow_size_factor"    then   minVal= -%inf;  maxVal=%inf

    // h=scf(nrWindow);
    // maxVal=size(h.color_map,1)
    case "foreground"              then   minVal= minValColor;       maxVal=maxValColor;
    case "background"             then   minVal= minValColor;       maxVal=maxValColor;

    // *** CHECK THIS *** UNSAFE ***
    //here we do not check this value - it is impossible here to check it - so we assign -inf -inf
    //
    //case "interp_color_vector" then   minVal= -%inf;  maxVal= -%inf

    case "mark_style" then   minVal= 0;       maxVal=14;
    case "mark_size"  then   minVal= 0;       maxVal=30;

    // h=scf(nrWindow);
    // maxVal=size(h.color_map,1)
    case "mark_foreground"             then   minVal= minValColor;       maxVal=maxValColor;
    case "mark_background"             then   minVal= minValColor;       maxVal=maxValColor;
    else
        error("Not implemented.")
end
endfunction




//--------------------------------------------------------------------------------------------------------------
//  function [PROPERTY, sizeProperty]=lcGetPROPERTY()
//--------------------------------------------------------------------------------------------------------------
function [PROPERTY, sizeProperty]=lcGetPROPERTY()


//PROPERTY NAMES
PROPERTY=[ ...
    "visible";   "closed";  "line_mode"; "fill_mode"; ...
    "line_style"; "thickness"; "arrow_size_factor";  ...
    "polyline_style";"foreground"; "background";   ...
    "interp_color_vector"; "interp_color_mode";    ...
    "mark_mode"; "mark_style"; "mark_size_unit"; ...
    "mark_size"; "mark_foreground"; "mark_background"; ...
    "x_shift"; "y_shift"; "z_shift"; "bar_width"; "clip_state";   ...
    "clip_box"; "user_data"; ...
    "color_map"  ...
];



// ------------- X LABEL ----------------
// "text"                    => ""
// "font_size"               => 1
// ------------- Y LABEL ----------------
// "text"                    => ""
// "font_size"               => 1
// ------------- Z LABEL ----------------
// "text"                    => ""
// "font_size"               => 1


//DEFAULT VALUES FOR PROPERTIES
plotOptionsDEFAULT=tlist([ "lrpPlotOptions"; PROPERTY ], ...
        "on" ,              "off",          "on",           "off", ...
        1,                    1,              1,                1, ...
        5,                    -2,             [],               "off", ...
        "off",                0,              "point",       0, ...
        -1,                   -2,             [],                [], ...
        [],                     0,              "clipgrf",      [], ...
        [],   [] ...
);

//NUMBER OF PROPERTIES
sizeProperty=size(PROPERTY,1);
endfunction





























//
// function [propertyList]=lcCheckArgAndClone(strNameProperty, property, lenY)
// // *** UNSAFE ***
//
// if typeof(property) == "list" then
//   if lstsize(property) ~= lenY then
//       printf("**************lrpPlot2D::lcCheckArgAndClone*****************\n");
//       printf("It must be a list contains %d elements\nin property of plot %s\n.",lenY, strNameProperty);
//       printf("************************************************************************\n");
//       error("Wrong numer elements in a list of property.");
//   else
//     propertyList=property;
//     return
//   end
// end
//
// propertyList=list();
//
// //empty
// if isempty(property) then
//   for i=index : lenY
//     propertyList(index)=property;
//   end
//   return
// end
//
// if lenY ~= max(size(property,1))
//   //string
//   if typeof(property)=="string" then
//         for i=index : lenY
//           propertyList(index)=property;
//         end
//         return
//   end
//
//   //constant
//   if typeof(property)=="constant" then
//         for i=index : lenY
//           propertyList(index)=property;
//         end
//         return
//   end
//
// error("Wrong type of argument in function: lrpPlot2D::lcCheckArgAndClone");
// endfunction
//
//
// //==============================================================================
// //                                                                                                                                                          //
// //==============================================================================
// function lcPlotD2Line(x, y, nrGraphicsWindow, lrpPlotOptions, USE_DEFAULT_PLOT)
// // *** UNSAFE ***
//
//
//         plotOptionsDefault=tlist(["lrpPlotOptions"; ...
//             "visible";                     "closed";                       "line_mode";                  "fill_mode"; ...
//             "line_style";                "thickness";                  "arrow_size_factor";      "polyline_style"; ...
//             "foreground";             "backround";                 "interp_color_vector";    "interp_color_mode"; ...
//             "mark_mode";            "mark_style";                 "mark_size_unit";           "mark_size"; ...
//             "mark_foreground";    "mark_background";     "x_shift";                         "y_shift"; ...
//             "z_shift";                      "bar_width";                  "clip_state";                   "clip_box"; ...
//             "user_data" ...
//           ], ...
//                 "on" ,              "off",          "on",           "off", ...
//                 1,                    1,              1,                1, ...
//                 5,                    -2,             [],               "off", ...
//                 "off",                0,              "point",       0, ...
//                 -1,                   -2,             [],                [], ...
//                 [],                     0,              "clipgrf",      [], ...
//                 [] ...
//         );
//
//
// //Example of use
//
// // x=1:10;
// // whichPassesOrPoints=[2 4 3 6 4 3 1];
// // y=round(rand(length(x), length(whichPassesOrPoints))*20);
// // nrGraphicsWindow=1;
// // strLegned="point";
// // plotD2Line(x, whichPassesOrPoints, y, nrGraphicsWindow, strLegned)
//
//
//
// //============ main part ======================
//
// //each plot are in column - so we need size of column
// lenY=size(y,2);
//
// NUMBER_OF_COLORS=31;
//
// //create graphic window
// figHandle=scf(nrGraphicsWindow);
//
//  //clear graphic window
// clf(figHandle);
//
// figHandle.immediate_drawing="off";
// figHandle.visible="off";
//
// for index=1:lenY
//   plot(x, y(:, index));
// end
//
// //defaut options for plot are used
// if USE_DEFAULT_PLOT then
//       colorIndex=1;
//       for index=1:lenY
//             //because 8 == withe color and it is invisible so it is no useful for us
//             if colorIndex==8 then
//                   colorIndex=colorIndex+1;
//             end
//             figHandle.children.children(index).children.foreground=colorIndex;
//             colorIndex=pmodulo(colorIndex, NUMBER_OF_COLORS);
//             colorIndex=colorIndex+1;
//       end
//
// else
// //defaut options for plot are NOT used
//     PROPERTY=[ ...
//         "visible";   "closed";  "line_mode"; "fill_mode"; ...
//         "line_style"; "thickness"; "arrow_size_factor";  ...
//         "polyline_style";"foreground"; "background";   ...
//         "interp_color_vector"; "interp_color_mode";    ...
//         "mark_mode"; "mark_style"; "mark_size_unit"; ...
//         "mark_size"; "mark_foreground"; "mark_background"; ...
//         "x_shift"; "y_shift"; "z_shift"; "bar_width"; "clip_state";   ...
//         "clip_box"; "user_data"  ...
//     ];
//     sizeProperty=size(PROPERTY,1);
//
//     //clone
//     for indexName = 1 : sizeProperty
//         lrpPlotOptions(PROPERTY(indexName)) = lcCheckArgAndClone( ...
//                 PROPERTY(indexName), ...
//                 lrpPlotOptions(PROPERTY(indexName)), ...
//                 lenY ...
//         );
//     end
//
//     colorIndex=1;
//     for index=1:lenY
//         //because 8 == withe color and it is invisible so it is no useful for us
//         if colorIndex==8 then
//                 colorIndex=colorIndex+1;
//         end
//
//         for indexName = 1 : sizeProperty
//             figHandle.children.children.children(index)(PROPERTY(indexName)) = ...
//                             lrpPlotOptions(PROPERTY(indexName))(index);
//         end
//
//         figHandle.children.children(index).children.foreground=colorIndex;
//         colorIndex=pmodulo(colorIndex, NUMBER_OF_COLORS);
//         colorIndex=colorIndex+1;
//     end
//
//
//
// end
//
//
//
//
//
//
//
// //wider window, because line will be truncated on the uper or lower edge of the rectangular of the plot
// // if figHandle.children(1).data_bounds(2,2) > 0 then
// //    tmp_up_y=figHandle.children(1).data_bounds(2,2)*1.03;
// // else
// //    tmp_up_y=figHandle.children(1).data_bounds(2,2)*0.97;
// // end
// // if figHandle.children(1).data_bounds(1,2) > 0 then
// //    tmp_down_y=figHandle.children(1).data_bounds(1,2)*0.97;
// // else
// //    tmp_down_y=figHandle.children(1).data_bounds(1,2)*1.03;
// // end
// // figHandle.children(1).data_bounds(2,2)=tmp_up_y;
// // figHandle.children(1).data_bounds(1,2)=tmp_down_y;
//
// //this not always work  !!!!!!!!
// //h.children(FIUGURE).tight_limits="on";
//
// figHandle.immediate_drawing="on";
// figHandle.visible="on";
// endfunction
//
//
//
//
//
//
//
//
//
//
// //==============================================================================
// //                                                                                                                                                          //
// //==============================================================================
// function lcPlotD2Circle(x, whichPassesOrPoints, y, nrGraphicsWindow, strLegned)
//
// // //example
// // x=3;
// // whichPassesOrPoints=[3 2 5 4 7]
// // y=round(rand(1, length(whichPassesOrPoints))*20)
// // nrGraphicsWindow=1;
// // strLegned="point";
// // plotD2Circle(x, whichPassesOrPoints, y, nrGraphicsWindow, strLegned)
//
//
// //============ check arguments ======================
// [nargout nargin]=argn();
// if (nargin ~= 5) then
//   error("Function expected 5 inputs argument, but has got: "+ sci2exp(nargin,0));
// end
//
// // if (nargout ~= 0) then
// //   error("Function no expected outputs argument, but has got: "+ sci2exp(nargout,0));
// // end
//
// requireType("x", x, "integer");
// requireType("whichPassesOrPoints", whichPassesOrPoints, ["integer vector", "integer"]);
// requireType("nrGraphicsWindow", nrGraphicsWindow, "integer");
// requireType("strLegned", strLegned, "string");
//
// requireValue("strLegned", strLegned, list("pass", "point"));
//
// //============ main part ======================
// NUMBER_OF_COLORS=31;
//
// //create graphic window
// figHandle=scf(nrGraphicsWindow);
//
//  //clear graphic window
// clf(figHandle);
//
// figHandle.immediate_drawing="off";
// figHandle.visible="off";
//
//
// lenY=length(y);
// for index=1:lenY
//   plot(x, y(index));
// end
//
// //children==1  ==> legend
// LEGEND=1;
//
// //figHandle.children.margins=[left right up down]
// //children==2  ==> figure
// FIUGURE=2;
//
// //wider window, because points will be truncated to half points
// if figHandle.children(1).data_bounds(2,2) > 0 then
//    tmp_up_y=figHandle.children(1).data_bounds(2,2)*1.03;
// else
//    tmp_up_y=figHandle.children(1).data_bounds(2,2)*0.97;
// end
// if figHandle.children(1).data_bounds(1,2) > 0 then
//    tmp_down_y=figHandle.children(1).data_bounds(1,2)*0.97;
// else
//    tmp_down_y=figHandle.children(1).data_bounds(1,2)*1.03;
// end
// figHandle.children(1).data_bounds(2,2)=tmp_up_y;
// figHandle.children(1).data_bounds(1,2)=tmp_down_y;
//
// //wider window, because points will be truncated to half points
// //this property make something like this abowe in comments -- but not always work !!! so we stay with this what above
// //figHandle.children(1).tight_limits="off";
//
//
// colorIndex=1;
// for index=1:lenY
//    //because 8 == withe color and it is invisible so it is no useful for us
//    if colorIndex==8 then
//         colorIndex=colorIndex+1;
//    end
//    figHandle.children.children(index).children.line_mode="off";
//    figHandle.children.children(index).children.mark_mode="on";
//    figHandle.children.children(index).children.mark_size_unit="point";
//    figHandle.children.children(index).children.mark_size=7;
//    figHandle.children.children(index).children.mark_style=9;  //circle
//    figHandle.children.children(index).children.mark_foreground=-1;  //black
//    figHandle.children.children(index).children.mark_background=colorIndex;  //background circle
//    colorIndex=pmodulo(colorIndex, NUMBER_OF_COLORS);
//    colorIndex=colorIndex+1;
// end
//
// //legend
// if lenY <= 18 then
//    strLegned=strLegned+" "+string(whichPassesOrPoints');
//    legend(strLegned);
// else
// //else withoud legend
// // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// //without legend - only information about this
// // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//    if strLegned=="pass" then
//       strLegned="passes";
//    end
//    if strLegned=="point" then
//       strLegned="points";
//    end
//
//    strLegned=[string(length(whichPassesOrPoints))+" "+strLegned; "on this plot"];
//    //setting legends
//    legend(strLegned,1);
//
//     lenTextPolyline=size(figHandle.children(LEGEND).children.children,1) ;
//       disp(lenTextPolyline)
//     for index=1: lenTextPolyline
//        //turn off visiblity of line in legend
//        if figHandle.children(LEGEND).children.children(index).type=="Polyline" then
//           if index ~= lenTextPolyline then
//              figHandle.children(LEGEND).children.children(index).visible="off";
//           end
//        end
// //        //move the all in box of the legend to the right side of the plot
// //        figHandle.children(LEGEND).children.children(index).data(:,1)= ...
// //                                figHandle.children(LEGEND).children.children(index).data(:,1)+SHIFT_ON_OX;
//     end
//    //move only the box of legend to right side of the plot
//    // 1 2
//    // 4 3
//    //2:3 means: move the right side of the box to the left about -0.0015
//    //[1 4] means: move the left side of the box to the right about +0.035
//     figHandle.children(LEGEND).children.children($).data(2:3,1)=  ...
//                       figHandle.children(LEGEND).children.children($).data(2:3,1)-0.0015;
//     figHandle.children(LEGEND).children.children($).data([1 4],1)=  ...
//                       figHandle.children(LEGEND).children.children($).data([1 4],1)+0.035;
// end
//
// figHandle.immediate_drawing="on";
// figHandle.visible="on";
// endfunction






//OK
//values from 0 to 255
function result=lcGetCorrectRGB(rgbVector)

// 1. wartosc >1 + czesc ulamkowa = BLAD
// 2. wartosc <1 + czesc ulamkowa = OK
// 3. wartosc =1 BEZ ulamka
//     co najmniej jedna jedynka + 2x[0;1] - ostrzezenie + przyjecie, ze to nieskalowane, 0:255
//     jedna >1 - nieskalowane + sprawdzic wszystkie, np. [1, 2.5,1] - BLAD, [1, 0.2,1]= OK
//     jedna <1 - skalowane + sprawdzic wszystkie np. [1,0.2,11] - BLAD
// 4. wartosc =1 + czesc ulamkowa = BLAD
// 5. DOPUSCIC WARTOSCI ROWNE -%EPS!, WTEDY PRZYJAC ZERO: [0,1,0]-%eps - unikniecie ostrzezenia z 1.


// 1 - scaled RGB (0:1), real. As a special exception the minus %eps value is accepted
// 2 - unscaled RGB (0:255), integer
// 0 is either scaled or unscaled. This is not a problem, as scaled and unscaled value has the same meaning
// 0 - inconclusive - can either be a scaled or unscaled RGB, as it consists only of zeroes and ones
// -1 -wrong RGB
  isInteger=and(fix(rgbVector)==rgbVector);
  if isInteger == %F then
    // AT LEAST ONE OF THE VALUES IS NOT AN INTEGER
    if or(max(rgbVector)>1)
        result=-1;
        error(sprintf("Wrongly scaled RGB - value %d too big for range [-%%eps .. 1]",max(rgbVector)));
    else
        minValue=min(rgbVector);
        if minValue<-%eps
            result=-1;
            error(sprintf("Wrongly scaled RGB - value %d too small for range [-%%eps .. 1]",minValue));
        else
            result=fix(rgbVector*255);
            result(result>255)=255;
            result(result<0)=0;
        end
    end
  else
    //All the values are integer
    minValue=min(rgbVector);
    if minValue>=0
        maxValue=max(rgbVector);
        if maxValue==1 then
            //It is a proper RGB range, but we cannot tell whether it is scaled or unscaled
            // RESULT INCONCLUSIVE
            warn1=sprintf("Vector %s can represent either scaled (0..1) or unscaled (0..255) RGB value",sci2exp(rgbVector));
            warn2=        "   This value is assumed to be UNSCALED(0..255); almost black color.";
            warn3=        "   To suppress this warning use (rgbVector-%%eps) as argument to this function\n";
            warn4=        "   **AND WORK ON *SCALED* VALUES**.";
            warn=[warn1;warn2;warn3;warn4];
            warning(warn);
            result=rgbVector;
        else
            if maxValue<=255 then
                result=rgbVector;
            else
                result=-1; // Wrong maximum value
                error(sprintf("Wrongly scaled RGB - value %d too big for range [-%%eps .. 1]",max(rgbVector)));
            end
        end
    else
        result =-1; // Wrong minimum value
        error(sprintf("Wrongly scaled RGB - value %d too small for range [-%%eps .. 1]",minValue));
    end
  end

// if result == 0, then convert '-%eps' to '0' value
endfunction






// OK
function lcCheckProperName(propertyNamesList, lrpDefaultNames)

sizePropertyLstNames = lstsize(propertyNamesList);
sizeDefaultNames = size(lrpDefaultNamesLst,'r');

for j=1:sizePropertyLstNames

    //property name MUST be a string
    requireType("propertyName("+sci2exp(j)+")", propertyNamesList(j), "string");

    namePropertyOK=%F;
    for i=1:sizeDefaultNames
        if propertyNamesList(j) == lrpDefaultNames(i) then
            namePropertyOK=%T;
            break;
        end
    end
    if namePropertyOK == %F then
        indErr=j;
        break;
    end
end

if namePropertyOK == %F then
    printf("***************************************************************\n\n");
    printf("Invalid name of property ""%s"".\nCheck the name.\n\n", propertyNamesList(indErr));
    printf("***************************************************************\n");
    error("Invalid name.");
end
endfunction

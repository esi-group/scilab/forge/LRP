function []=%lrp_Sys_Cla_Plt_p(var)
// LRP Toolkit for Scilab. This function is used to display the plot data
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-03-12 00:15:00

printf('Plot data:\n');
printf('maxPoints: %d\n',var.maxPoints);
printf('maxPasses: %d\n\n',var.maxPasses);

for x=1:length(var.plotData)
	printf('\n var.plotData(%d)=',x);
	disp(var.plotData(x));
end

endfunction

function lrp=setGenerator(lrp,generatorName,generatorFunName,parameterList)
  requireType("lrp",lrp,"tlist","lrp_Sys_Cla");
  requireType("generatorName",generatorName,"string");
  requireType("generatorFunName",generatorFunName,"string");
	
	// generatorFunName is a string and we need a pointer
	if argn(2)==0
	   disp("USAGE : lrp=setGenerator(lrp,generatorName,generatorFunName,parameterList)")
	   disp(" generatorName can be one of:""u""");
	   
     return
  end
  generatorFun = eval(generatorFunName);
	if argn(2)==4
  	requireType("parameterList",parameterList,["list","empty list"]);
  else
  	[name,parameterList] = generatorFun();
	end
  select generatorName,
  	case "u" then lrp.ext.uGenerator = generatorFunName; lrp.ext.uParameters = parameterList;
		else
			error(sprintf("Unknown generator name %s.",generatorName));
	end; 
endfunction

function []=%lrp_Sys_Cla_p(var)
// LRP Toolkit for Scilab. This function is used to display the classic LRP tlist.
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-03-29 21:29:00

controllerName=lrp.controller(lrp.indController).displayName;
printf('********     Classic LRP     ********\n');
printf('** %s **\n',part(controllerName,1:max(31,length(controllerName))));
disp('Dimensions (.dim.+):');
disp(var.dim);
disp('');
disp('Matrices (.mat.+):');
disp(var.mat);
printf('==========================\n');
printf('Initial conditions (.ini.+):\n');
disp(var.ini);
printf('==========================\n');
printf('External forces (.ext.+):\n');
disp(var.ext);
endfunction

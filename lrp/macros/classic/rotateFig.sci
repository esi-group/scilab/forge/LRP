function rotateFig(nrGW,at)

hf=scf(nrGW);
hc=get(hf,'children');

[nargout,nargin]=argn();

if (nargin == 1) then
	//matlab
	//set(hc,'rotation_angles',[89.45,225]);
	set(hc,'rotation_angles',[89.45,300]);
else
	set(hc,'rotation_angles',at);
end

endfunction
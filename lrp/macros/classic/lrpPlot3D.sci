// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-04-15 00:05:00

// function lrpPlot3D(...
//    lrp3DPlot.variableName, ...  //string: what to plot: 'state' ('x')or 'output' ('y')
//    gridPoints, ...          //matrix grid on OX
//    gridPasses, ...          //matrix grid on OY
//    Z, ...                  //what has to be painted:  matrix 3 dimensional
//    lrp3DPlot.variableNumber, ...  //scalar - which number shuld be plot
//    lrp3DPlot.pointsRange,...  //range axis on OX: vector 2 elements [10,30] - [from, to]
//    lrp3DPlot.passesRange,...  //range axis on OX: vector 2 elements [10,30] - [from, to]
//    whichGraphWindow, ...  //scalar: number of graphics window
//    lrp, ... //system lrp
//    fontSize ... //font size on axes
// )



        // ============= 3D PLOT ===============
        // type lrp3DPlot = record
        //      variableName   : ['state','input','output'];
        //      variableNumber : integer;
        //      pointsRange    : vector[1x2] --- [min(V), max(V)];
        //      passesRange    : vector[1x2] --- [min(V), max(V)];
        //      //vertex=1     : integer;
        // end;

function lrpPlot3D(...
   lrp, lrp3DPlot, surface3D, whichGraphWindow, plotBlackOrColor, showInfoAboutPlot ...
)


// ==================== Constants ===========================
THREEDPLOT="lrp_Sys_Cla_P_3D";
STATE="state";
OUTPUT="output";
INPUT="input";
MIN_INDEX=1;
MAX_INDEX=2;
// ==================== Arguments check =====================

//check types
requireType("lrp", lrp, "tlist", "lrp_Sys_Cla");
requireType("lrp3DPlot", lrp3DPlot, "tlist", THREEDPLOT);
requireType("lrp3DPlot.variableName", lrp3DPlot.variableName, "string");
requireType("lrp3DPlot.variableNumber", lrp3DPlot.variableNumber, "positive integer");
requireType("lrp3DPlot.pointsRange", lrp3DPlot.pointsRange, ["integer vector", "integer"]);
requireType("lrp3DPlot.passesRange", lrp3DPlot.passesRange, ["integer vector", "integer"]);

requireType("plotBlackOrColor", plotBlackOrColor, "string");
requireValue("plotBlackOrColor", plotBlackOrColor, list("black", "color"));

//check values
lrp3DPlot.variableName=replaceValue(lrp3DPlot.variableName, list("state","x"), STATE);
lrp3DPlot.variableName=replaceValue(lrp3DPlot.variableName, list("output","y"), OUTPUT);
lrp3DPlot.variableName=replaceValue(lrp3DPlot.variableName, list("input","u"), INPUT);
requireValue("lrp3DPlot.variableName", lrp3DPlot.variableName, list(STATE,OUTPUT,INPUT));

select lrp3DPlot.variableName
		case STATE then
				requireRange("lrp3DPlot.variableNumber", lrp3DPlot.variableNumber, [1,lrp.dim.n]);
		case OUTPUT
				requireRange("lrp3DPlot.variableNumber", lrp3DPlot.variableNumber, [1,lrp.dim.m]);
		case INPUT
				requireRange("lrp3DPlot.variableNumber", lrp3DPlot.variableNumber, [1,lrp.dim.r]);
		else
				error(sprintf("Unknown field %s.",lrp3DPlot.variableName));
end

requireRange("lrp3DPlot.pointsRange", lrp3DPlot.pointsRange, [lrp.dim.pmin, lrp.dim.pmax]);
requireRange("lrp3DPlot.passesRange", lrp3DPlot.passesRange, [lrp.dim.kmin, lrp.dim.kmax]);


// Ensure we get the row vectors, not column ones; change (and report a warning) if necessary
lrp3DPlot.pointsRange=makeRowVector("lrp3DPlot.pointsRange", lrp3DPlot.pointsRange);
lrp3DPlot.passesRange=makeRowVector("lrp3DPlot.passesRange", lrp3DPlot.passesRange);

// ==================== Number of arguments check =====================
if argn(2)==5 then
	showInfoAboutPlot=%F;
else
	requireType("showInfoAboutPlot", showInfoAboutPlot, "boolean");
end
// ==================== Main ================================


//because points start from 0, but matrices are indexed from 1
//How much to add so that matrix index starts from 1
offsetForPoints= -lrp.dim.pmin+1;
offsetForPasses= -lrp.dim.kmin+1;

//lrp3DPlot.variableName
select lrp3DPlot.variableName
	case STATE  then
//    				stateOrOutputName="State";
//    				dimStateOrOutput=lrp.dim.n;
   				Z=surface3D.state;
	case OUTPUT then
// 					stateOrOutputName="Output";
//    				dimStateOrOutput=lrp.dim.m;
   				Z=surface3D.output;
	case INPUT then
// 					stateOrOutputName="Output";
//    				dimStateOrOutput=lrp.dim.m;
   				Z=surface3D.input;
	else
   			error("Unknown field: "+lrp3DPlot.variableName+".");
end

// The lrpPassToPassPlot.passesRange is [min,max] (for example [1,4]) but
//  we need the continuous range for passes i.e. [1,2,3,4] - hence
//  the need for convertion
// This is the real range, as shown on the legend of the plot
pointsRange=lrp3DPlot.pointsRange(MIN_INDEX) : lrp3DPlot.pointsRange(MAX_INDEX);
passesRange=lrp3DPlot.passesRange(MIN_INDEX) : lrp3DPlot.passesRange(MAX_INDEX);
// printf("points and passes range\n")
// disp(pointsRange)
// disp(passesRange)

// This will be used for indexing the matrix, so it MUST start from 1. Hence
//   we add the offset
pointsIndicesForMatrix=pointsRange + offsetForPoints;
passesIndicesForMatrix=passesRange + offsetForPasses;
// printf("points and passes indices\n")
// disp(pointsIndicesForMatrix)
// disp(passesIndicesForMatrix)


// gridPoints=surface3D.grid.X(pointsIndicesForMatrix, passesIndicesForMatrix);
// gridPasses=surface3D.grid.Y(pointsIndicesForMatrix, passesIndicesForMatrix);

gridPoints=surface3D.points(pointsIndicesForMatrix, passesIndicesForMatrix);
gridPasses=surface3D.passes(pointsIndicesForMatrix, passesIndicesForMatrix);
// printf("grid points and passes \n")
// disp(gridPoints)
// disp(gridPasses)

values=Z( ...
	pointsIndicesForMatrix , ...
  passesIndicesForMatrix , ...
  lrp3DPlot.variableNumber ...
);

// printf("values=Z(pointsIndicesForMatrix , passesIndicesForMatrix ,  lrp3DPlot.variableNumber) \n")
// disp(values)
// printf("Z(:,:,lrp3DPlot.variableNumber) \n")
// disp(Z(:,:,lrp3DPlot.variableNumber))

////create graphic window
//figHandle - handle to figure
figHandle=scf(whichGraphWindow);
set(figHandle,"immediate_drawing","off");
//drawlater();

//// or this way
//xset("window",whichGraphWindow);
//xset("font size",10)

//clear graphic window
clf(whichGraphWindow);

//xset("colormap",jetcolormap(64));
//xset("colormap", hotcolormap(32));
//xset("colormap", graycolormap(32));
xset("colormap", hsvcolormap(512));

//info
if showInfoAboutPlot then
   lrpShowPlotInfo(lrp3DPlot);
end

//if size is axactly == 1  ---> 1 point in 3D
if (and(size(gridPoints)==1)) then //& and(size(gridPasses)==1)) then
         //create vector of 2 elements where each element has this same value for example [3 3]
         gridPoints=ones(1,2)*gridPoints;
         gridPasses=ones(1,2)*gridPasses;
         values=ones(1,2)*values;

         //create matrix as [gridPoints; gridPoints] etc.
         gridPoints=gridPoints(ones(1,2),:);
         gridPasses=gridPasses(ones(1,2),:);
         values=values(ones(1,2),:);

         mesh(gridPasses, gridPoints, values);

         // setting circle
         figHandle.children.children.mark_mode="on";
         figHandle.children.children.mark_size=7;
         figHandle.children.children.mark_style=9;  //circle
         figHandle.children.children.mark_foreground=-1;  //black
         figHandle.children.children.mark_background=4;  //background circle

//=======================================================================
// rectangle in 3D
//=======================================================================
elseif ((and(size(gridPasses)==[2 2]))) then //& (and(size(gridPoints)==[2 2]))) then
            mesh([gridPasses; gridPasses'], [gridPoints; gridPoints'],  [values; values'])
      //COMMENT 2 - to rectangle in 3D
      //
      //this fails when the 3D matrix has 2 dimension - for example:
      //
      // [a,b]=ndgrid(10:11, 10:11)
      // c=[1 2; 3 4]
      //   a  =                     b  =                      c  =
      //     10.    10.               10.    11.                1.    2.
      //     11.    11.               10.    11.                3.    4.
      //mesh(a, b, c)
      //
      //Error displayed:
      //             plot3d(XX,YY,list(ZZ,CC))
      //                                       !--error 999
      //             Objplot3d: x vector is not monotonous
      //             Error 999 : in plot3d called by surf !--error 999


// line in 3D more than 3 points or passes
elseif (or(size(gridPoints)==1)) then // | or(size(gridPasses)==1)) then
//example:
// one of zize is == 1  <==> size(gridPoints) == [1 6]
//gridPoints=  [ 5 5 5 5 5 5 ]
//gridPasses=[ 3 4 5 6 7 8 ]
//values=       [ 3 4 1 7 2 9 ]

         if size(gridPoints,1) == 1  then
            //create matrix as:
            //   [...
            //        gridPoints ; ...        //see the  colon operator (;)
            //        gridPoints ...
            //    ]
            //etc.
            gridPoints=gridPoints(ones(1,2),:);
            gridPasses=gridPasses(ones(1,2), :);
            values=values(ones(1,2), :);
         else
            //create matrix as:
            //   [...
            //        gridPoints     gridPoints ...
            //   ]
            //etc.
            gridPoints=gridPoints(:,ones(1,2));
            gridPasses=gridPasses(:,ones(1,2));
            values=values(:,ones(1,2));
         end

         mesh(gridPasses, gridPoints, values);


   //COMMENT 1
            //
            //here is only one exception:
            //always wy use (poins, passes) but here we must change the order to (passes, points)
            //this is only for presentation plots, where on OX will be passes and on OY will by points
            //
            //NOTE:
            //if you want to change the order:
            //       points on OX
            //       passes on OY
            //then you shold change create the grid in function:  [surface3D]=lrpCalculateSurface3D(lrp, lrpPlotData)
            // from:        [pointsGrid, passesGrid]=ndgrid(pointsRange, passesRange);
            //     this produces matrix of size, for example  <1..3> x <1..7>
                                    // -->[pointsGrid, passesGrid]=ndgrid(1:3,1:7)
                                    //  pointsGrid  =                                               //  passesGrid  =
                                    //     1.    1.    1.    1.    1.    1.    1.                   //     1.    2.    3.    4.    5.    6.    7.
                                    //     2.    2.    2.    2.    2.    2.    2.                   //     1.    2.    3.    4.    5.    6.    7.
                                    //     3.    3.    3.    3.    3.    3.    3.                   //     1.    2.    3.    4.    5.    6.    7.
            //
            //to :             [passesGrid, pointsGrid]=ndgrid(passesRange, pointsRange );
            // but here this produces matrix of size, for example  <1..7> x <1..3>
                                    // -->[pointsGrid, passesGrid]=ndgrid(1:7,1:3)
                                    //  pointsGrid  =                           //  passesGrid  =
                                    //     1.    1.    1.                           //     1.    2.    3.
                                    //     2.    2.    2.                           //     1.    2.    3.
                                    //     3.    3.    3.                           //     1.    2.    3.
                                    //     4.    4.    4.                           //     1.    2.    3.
                                    //     5.    5.    5.                           //     1.    2.    3.
                                    //     6.    6.    6.                           //     1.    2.    3.
                                    //     7.    7.    7.                           //     1.    2.    3.
      //
      // OR SIMPLY CHANGE THE DESCRIPTION ON THE AXIS OX FROM passes  TO points
else
       //IF SIZE > 2 THEN EVERYTHING IS OK
                //(see above for the long "COMMENT 1" )
	          mesh(gridPasses, gridPoints,  values);

       //cut the plot precisely to the boundary of data plot
       // Error - this causes the plot to disappear....
       //figHandle.children.tight_limits="on"
       h.children.children.hiddencolor=70; 
end

//color or black point
if (plotBlackOrColor=="color") then
   figHandle.children.children.color_flag=1;
else
   figHandle.children.children.color_flag=0;
end

//nameOfFigure of window
nameOfFigure=strcat(["%d - " lrp3DPlot.description.title]);

//this is because title of figure is bounded to 79 characters
nameOfFigure=shortenString(nameOfFigure, 79-floor(log10(whichGraphWindow)+1)+1); //+1 because (%d) have 2 characters
set(figHandle,"figure_name",nameOfFigure);


xtitle(lrp3DPlot.description.title, lrp3DPlot.description.x_label, ...
                  lrp3DPlot.description.y_label, lrp3DPlot.description.z_label);


//maksimize the window
//set(figHandle,"figure_size",[9999999,9999999])

// rotate graphic
//set(ha,"rotation_angles",[35,238]);

set(figHandle,"immediate_drawing","on");
set(figHandle,"visible","on");
//drawnow();

endfunction

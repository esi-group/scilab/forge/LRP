function [u,parameters]=uConstant(lrp,k,p, X, U, Y)
// CAUTION: 
// FOR DISPLAY: We can either have "LRP" argument of type "lrp_Sys_Cla" [classic LRP]
//   	or "LRP" can be equal to lrp.ext; the "lrp_Sys_Ext" tist.
// 		NOTE THAT THE ABOVE IS VALID FOR DISPLAY ONLY, WHEN THE NUMBER OF INPUT 
//		   ARGUMENTS EQUALS 0 OR 1
//
//  FOR CALCULATIONS: ALWAYS THE "FULL CLASSIC" LRP OF TYPE "lrp_Sys_Cla" IS GIVEN.   
if argn(2) <=1 then
	// DISPLAY
	if argn(2)==0 then
		u="u(k,p)=1";
		parameters = list(1);
	else
		if typeof(lrp)=="lrp_Sys_Cla_Ext" then
			ext = lrp;
		else
			ext = lrp.ext;
		end;
		if type(ext.uParameters) ~=15 then
			warning(sprintf("Type of lrp.ext.uParameters needs to be list, is: %s.",typeof(ext.uParameters)));
			warning("Default values assumed.");
			ext.uParameters = list();
		end
		if length(ext.uParameters)==0 then
			parameters=list(1);
		else
			parameters = ext.uParameters;
		end
		u=sprintf("%f",parameters(1));
	end
	return;
end 
	// CALCULATION
	// By assumption the full LRP of type "lrp_Sys_Cla" is available from now on.
  u=ones(lrp.dim.r,1)*lrp.ext.uParameters(1);
  parameters = lrp.ext.uParameters;
endfunction


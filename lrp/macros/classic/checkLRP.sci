function [result]=checkLRP(lrp)

n=lrp.dim.n;
m=lrp.dim.m;
r=lrp.dim.r;

result=%f;
//check the sizes of matrices
//============================
requireMatrixSize("lrp.mat.A",lrp.mat.A,[n,n]);
requireMatrixSize("lrp.mat.B",lrp.mat.B,[n,r]);
requireMatrixSize("lrp.mat.B0",lrp.mat.B0,[n,m]);
requireMatrixSize("lrp.mat.C",lrp.mat.C,[m,n]);
requireMatrixSize("lrp.mat.D",lrp.mat.D,[m,r]);
requireMatrixSize("lrp.mat.D0",lrp.mat.D0,[m,m]);

requireMatrixSize("lrp.controller(1).A",lrp.controller(1).A,[n,n]);
requireMatrixSize("lrp.controller(1).B",lrp.controller(1).B,[n,r]);
requireMatrixSize("lrp.controller(1).B0",lrp.controller(1).B0,[n,m]);
requireMatrixSize("lrp.controller(1).C",lrp.controller(1).C,[m,n]);
requireMatrixSize("lrp.controller(1).D",lrp.controller(1).D,[m,r]);
requireMatrixSize("lrp.controller(1).D0",lrp.controller(1).D0,[m,m]);

requireMatrixSize("lrp.ini.x0",lrp.ini.x0,[n,lrp.dim.beta]);
requireMatrixSize("lrp.ini.y0",lrp.ini.y0,[m,lrp.dim.alpha]);

if lrp.dim.kmax<lrp.dim.kmin then
   error('lrp.dim.kmax='+sci2exp(lrp.dim.kmax)+' must be greater than lrp.dim.kmin='+sci2exp(lrp.dim.kmin));
end
if lrp.dim.pmax<lrp.dim.pmin then
   error('lrp.dim.pmax='+sci2exp(lrp.dim.pmax)+' must be greater than lrp.dim.pmin='+sci2exp(lrp.dim.pmin));
end
requireRange("lrp.indController",lrp.indController,1,lstsize(lrp.controller));

requireValue("lrp.dim.beta",lrp.dim.beta,list(lrp.dim.kmax-lrp.dim.kmin+1));
requireValue("lrp.dim.alpha",lrp.dim.alpha,list(lrp.dim.pmax-lrp.dim.pmin+1));

result=%t;
endfunction

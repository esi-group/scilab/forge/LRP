function lrpShowPlotInfo(lrpPlotData, whichGraphWindow)

// ==================== Constants ===========================
ALONGTHEPASS="lrp_Sys_Cla_P_AP";
PASSTOPASS="lrp_Sys_Cla_P_PP";
THREEDPLOT="lrp_Sys_Cla_P_3D";

// ==================== Arguments check =====================
requireType("lrpPlotData",lrpPlotData,"tlist",[ALONGTHEPASS, PASSTOPASS, THREEDPLOT]);

// ==================== Main ================================
  select typeof(lrpPlotData)
         case ALONGTHEPASS then
              printf("typeof=%s\n", ALONGTHEPASS);
              printf("  variableName   : "+lrpPlotData.variableName+"\n");
              printf("  variableNumber : "+int2str(lrpPlotData.variableNumber)+"\n");
              printf("  passes         : "+int2str(lrpPlotData.passes)+"\n");
              printf("  pointsRange    : "+int2str(lrpPlotData.pointsRange)+"\n");
              printf("Graphics window  : "+int2str(whichGraphWindow)+"\n\n");
         case PASSTOPASS then
              printf("typeof=%s\n", PASSTOPASS);
              printf("  variableName   : "+lrpPlotData.variableName+"\n");
              printf("  variableNumber : "+int2str(lrpPlotData.variableNumber)+"\n");
              printf("  points         : " + int2str(lrpPlotData.points)+"\n");
              printf("  passesRange    : "+int2str(lrpPlotData.passesRange)+"\n");
              printf("Graphics window  : "+int2str(whichGraphWindow)+"\n\n");
         case THREEDPLOT then
              printf("typeof=%s\n", THREEDPLOT);
              printf("  variableName   : "+lrpPlotData.variableName+"\n");
              printf("  variableNumber : "+int2str(lrpPlotData.variableNumber)+"\n");
              printf("  pointsRange    : "+int2str(lrpPlotData.pointsRange)+"\n");
              printf("  passesRange    : "+int2str(lrpPlotData.passesRange)+"\n");
              printf("Graphics window  : "+int2str(whichGraphWindow)+"\n\n");
         else
             error("Unknown plot type "+ typeof(lrpPlotData)+".");
  end
endfunction

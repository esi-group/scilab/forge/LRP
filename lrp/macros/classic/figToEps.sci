//win_id - only 1 id to graphic window,
//                      for example: 'win_id=0; hw=scf(win_id);'
//
//name_of_graphic_file - name of the graphic file WITHOUT extension !!!!!
//
//fig_color - colored figure or black---> 1, colored;
//                                                          0, black
//
//orientation - optional character, with possible values 'p' (portrait) or 'l' (landscape). The default value is 'p'.
//
function [win_id]=figToEps(win_id, name_of_graphic_file, fig_color, orientation)

if argn(2)==2 then
   fig_color=1;
   orientation="p";
elseif argn(2)==3 then
   orientation="p";
end

//create eps file -- we obtain - after this operation - file with extension:
//                                                                             name_of_graphic_file+'.eps'  !!!
xs2eps(win_id, name_of_graphic_file, fig_color, orientation);

//create PDF grapic from EPS file
if (unix("epstopdf "+name_of_graphic_file+".eps") ~= 0) then
   error("Creating PDF error from file: "+name_of_graphic_file+".eps");
end

//incrase number of graphic window
win_id=win_id+1;
endfunction

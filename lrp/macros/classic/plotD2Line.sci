//Example of use

// xRange=1:10;
// whichPassesOrPoints=[2 4 3 6 4 3 1];
// y=round(rand(length(xRange), length(whichPassesOrPoints))*20);
// nrGraphicsWindow=1;
// strLegned="point";
// plotD2Line(xRange, whichPassesOrPoints, y, nrGraphicsWindow, strLegned)

function plotD2Line(xRange, whichPassesOrPoints, y, nrGraphicsWindow, strLegned)

//============ check arguments ======================
[nargout nargin]=argn();
if (nargin ~= 5) then
  error("Function expected 5 inputs argument, but has got: "+ sci2exp(nargin,0));
end

// if (nargout ~= 0) then
//   error("Function no expected outputs argument, but has got: "+ sci2exp(nargout,0));
// end

requireType("xRange", xRange, "integer vector");
requireType("whichPassesOrPoints", whichPassesOrPoints, ["integer vector", "integer"]);
requireType("nrGraphicsWindow", nrGraphicsWindow, "integer");
requireType("strLegned", strLegned, "string");

requireValue("strLegned", strLegned, list("pass", "point"));


//============ main part ======================

//each plot are in column - so we need size of column
lenY=size(y,2);

NUMBER_OF_COLORS=31;

figHandle=scf(nrGraphicsWindow);
 //clear graphic window
clf(figHandle);

figHandle.immediate_drawing="off";
figHandle.visible="off";

for index=1:lenY
  plot(xRange, y(:, index));
end
//
colorIndex=1;
for index=1:lenY
   //because 8 == withe color and it is invisible so it is no useful for us
   if colorIndex==8 then
        colorIndex=colorIndex+1;
   end
   figHandle.children.children(index).children.foreground=colorIndex;
   colorIndex=pmodulo(colorIndex, NUMBER_OF_COLORS);
   colorIndex=colorIndex+1;
end


//wider window, because line will be truncated on the uper or lower edge of the rectangular of the plot
// if figHandle.children(1).data_bounds(2,2) > 0 then
//    tmp_up_y=figHandle.children(1).data_bounds(2,2)*1.03;
// else
//    tmp_up_y=figHandle.children(1).data_bounds(2,2)*0.97;
// end
// if figHandle.children(1).data_bounds(1,2) > 0 then
//    tmp_down_y=figHandle.children(1).data_bounds(1,2)*0.97;
// else
//    tmp_down_y=figHandle.children(1).data_bounds(1,2)*1.03;
// end
// figHandle.children(1).data_bounds(2,2)=tmp_up_y;
// figHandle.children(1).data_bounds(1,2)=tmp_down_y;

//this not always work  !!!!!!!!
//h.children(FIUGURE).tight_limits="on";


//children==1  ==> legend
LEGEND=1;

//figHandle.children.margins=[left right up down]
//children==2  ==> figure
FIUGURE=2;
figHandle.children.margins=[0.125 0.2 0.125 0.125]

COMPOUND=find(figHandle.children(LEGEND).children.type=="Compound");
if isempty(COMPOUND) then
   disp(figHandle.children(LEGEND).children.type);
   error('Unable to find the ''Compound'' property. Use some other Scilab version');
   return;
end
//legend
if lenY <= 18 then
   strLegned=strLegned+" "+string(whichPassesOrPoints');
   //shift box of the legend
   SHIFT_ON_OX=0.13;

   //setting legends
   legend(strLegned,1);

   lenTextPolyline=size(figHandle.children(LEGEND).children(COMPOUND).children,1) ;
   for index=1: lenTextPolyline
      //move the all in box of the legend to the right side of the plot
      figHandle.children(LEGEND).children(COMPOUND).children(index).data(:,1)=  ...
                              figHandle.children(LEGEND).children(COMPOUND).children(index).data(:,1)+SHIFT_ON_OX;
//       if figHandle.children(LEGEND).children.children(index).type=="Polyline" then
//          if index~=lenTextPolyline then
//             figHandle.children(LEGEND).children.children(index).data(2,1)= ...
//                            figHandle.children(LEGEND).children.children(index).data(2,1)-0.02;
//          end
//       end

//       if figHandle.children(LEGEND).children.children(index).type=="Text" then
//             figHandle.children(LEGEND).children.children(index).data(1,1)= ...
//                            figHandle.children(LEGEND).children.children(index).data(1,1)-0.02;
//       end
   end

   //move the box of legend to right side of the plot
//    figHandle.children(LEGEND).children.children($).data(2:3,1)=  ...
//                      figHandle.children(LEGEND).children.children($).data(2:3,1)-0.0015;
//
else
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//without legend - only information about this
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   if strLegned=="pass" then
      strLegned="passes";
   end
   if strLegned=="point" then
      strLegned="points";
   end
   strLegned=[string(length(whichPassesOrPoints))+" "+strLegned; "on this plot"];
   //shift box of the legend
   SHIFT_ON_OX=0.125;

   //setting legends
   legend(strLegned,1);

   lenTextPolyline=size(figHandle.children(LEGEND).children.children,1) ;
   for index=1: lenTextPolyline
      //turn off visiblity of line in legend
      if figHandle.children(LEGEND).children.children(index).type=="Polyline" then
         if index~=lenTextPolyline then
            figHandle.children(LEGEND).children.children(index).visible="off";
         end
      end
      //move the all in box of the legend to the right side of the plot
      figHandle.children(LEGEND).children.children(index).data(:,1)= ...
                              figHandle.children(LEGEND).children.children(index).data(:,1)+SHIFT_ON_OX;
   end
   //move only the box of legend to right side of the plot
   // 1 2
   // 4 3
   //2:3 means: move the right side of the box to the left about -0.0015
   //[1 4] means: move the left side of the box to the right about +0.035
   figHandle.children(LEGEND).children.children($).data(2:3,1)=  ...
                     figHandle.children(LEGEND).children.children($).data(2:3,1)-0.0015;
   figHandle.children(LEGEND).children.children($).data([1 4],1)=  ...
                     figHandle.children(LEGEND).children.children($).data([1 4],1)+0.035;

   //exactly axes to size on OX and OY
end
//see above !
//ADJUST BOX OF THE LEGEND TO SIZE OF AXES OF THE PLOT
//the boundary are the margins of figure
// cornners of the legend box are in this order
//1     2
//4     3
//1:2 -> mens up corners
//3:4 -> mens down corners
// see that these values are <0 ( see sign - )
//   figHandle.children(LEGEND).children.children($).data(1:2,2) = - figHandle.children(FIUGURE).margins(3);
// -1 means the boundary of axes [0 0 1 1]
//   figHandle.children(LEGEND).children.children($).data(3:4,2) =   figHandle.children(FIUGURE).margins(4) - 1;

//h.children(FIUGURE).tight_limits="on";
figHandle.immediate_drawing="on";
figHandle.visible="on";
//h.children(FIUGURE).tight_limits="on";
endfunction

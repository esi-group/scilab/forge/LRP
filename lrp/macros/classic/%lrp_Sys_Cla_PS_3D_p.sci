function %lrp_Sys_Cla_PS_3D_p(var)
disp('Classic LRP 3D surface');
disp('Passes:');
disp(var(2));
disp('Points:');
disp(var(3));
disp('State:');
disp(var(4));
disp('Output:');
disp(var(5));
endfunction

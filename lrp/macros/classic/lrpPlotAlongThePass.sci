// Author Blazej Cichy
// e-mail: b.cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-04-15 00:05:00

// function lrpPlot2D(...
//    whatToPlot, ...  //string: what to plot: "state" or "output"
//    grid_X, ...          //matrix grid on OX
//    grid_Y, ...          //matrix grid on OY
//    X, ...                  //what  is going to be painted
//    lrpAlongThePassPlot.variableNumber, ...  //scalar - which number shuld be plot
//    whichGraphWindow, ...  //scalar: number of grapchic window
//    lrp,... //system lrp
//    rangeOnOX,...  //range axis on OX: vector 2 elements [10,30] - [from, to]
//    rangeOnOY,...  //range axis on OX: vector 2 elements [10,30] - [from, to]
// )


// function lrpPlotAlongThePass(...
//    lrpAlongThePassPlot.variableName, ...  //string: what to plot: "state" ("x") or "output" ("y")
//    directionStr, ... //string: "alongthepass" ("horizontal") or "passtopass" ("vertical")
//    M,...                   //what has to be painted:  matrix 3 dimensional
//    lrpAlongThePassPlot.variableNumber, ...  //scalar - which number shuld be plot
//    lrpAlongThePassPlot.passes,...    //which profiles
//    lrpAlongThePassPlot.pointsRange, ...   //range axis on OX or OY: vector 2 elements [10,30] -> [from, to]
//    whichGraphWindow, ...  //scalar: number of grapchic window
//    lrp... //system lrp
// )

function lrpPlotAlongThePass(...
   lrp, lrpAlongThePassPlot, surface3D, whichGraphWindow , showInfoAboutPlot...
)


// ==================== Constants ===========================
ALONGTHEPASS="lrp_Sys_Cla_P_AP";
STATE="state";
OUTPUT="output"
INPUT="input"
MIN_INDEX=1;
MAX_INDEX=2;
PLOT_DESCRIPTION="lrp_Plt_Description";

// ==================== Arguments check =====================
//check types
requireType("lrp", lrp, "tlist", "lrp_Sys_Cla");
requireType("lrpAlongThePassPlot", lrpAlongThePassPlot, "tlist", ALONGTHEPASS);
requireType("lrpAlongThePassPlot.description", lrpAlongThePassPlot.description, "tlist", PLOT_DESCRIPTION);
requireType("lrpAlongThePassPlot.variableName", lrpAlongThePassPlot.variableName, "string");
// States, inputs and outputs are numbered from 1, hence "positive integer"
requireType("lrpAlongThePassPlot.variableNumber", lrpAlongThePassPlot.variableNumber, "positive integer");
requireType("lrpAlongThePassPlot.passes", lrpAlongThePassPlot.passes, ["integer vector", "integer"]);
requireType("lrpAlongThePassPlot.pointsRange", lrpAlongThePassPlot.pointsRange, ["two element integer vector"]);

//check values
lrpAlongThePassPlot.variableName=replaceValue(lrpAlongThePassPlot.variableName, list("state","x"), STATE);
lrpAlongThePassPlot.variableName=replaceValue(lrpAlongThePassPlot.variableName, list("output","y"), OUTPUT);
lrpAlongThePassPlot.variableName=replaceValue(lrpAlongThePassPlot.variableName, list("input","u"), INPUT);
requireValue("lrpAlongThePassPlot.variableName", lrpAlongThePassPlot.variableName, list(STATE,OUTPUT,INPUT));

select lrpAlongThePassPlot.variableName
   case STATE then
	      requireRange("lrpAlongThePassPlot.variableNumber", lrpAlongThePassPlot.variableNumber, [1, lrp.dim.n]);
   case OUTPUT then
	      requireRange("lrpAlongThePassPlot.variableNumber", lrpAlongThePassPlot.variableNumber, [1, lrp.dim.m]);
   case INPUT then
	      requireRange("lrpAlongThePassPlot.variableNumber", lrpAlongThePassPlot.variableNumber, [1, lrp.dim.r]);
   else
        error('Not implemented')
end

requireRange("lrpAlongThePassPlot.passes", lrpAlongThePassPlot.passes, [lrp.dim.kmin, lrp.dim.kmax]);
requireRange("lrpAlongThePassPlot.pointsRange", lrpAlongThePassPlot.pointsRange, [lrp.dim.pmin, lrp.dim.pmax]);

// Ensure we get the row vectors, not column ones; change (and report a warning) if necessary
lrpAlongThePassPlot.passes=makeRowVector("lrpAlongThePassPlot.passes",lrpAlongThePassPlot.passes);
lrpAlongThePassPlot.pointsRange=makeRowVector("lrpAlongThePassPlot.pointsRange",lrpAlongThePassPlot.pointsRange);

// ==================== Number of arguments check =====================
if argn(2)==4 then
	showInfoAboutPlot=%F;
else
	requireType("showInfoAboutPlot", showInfoAboutPlot, "boolean");
end
// ==================== Main ================================

//because points start from 0, but matrices are indexed from 1
//How much to add so that matrix index starts from 1
offsetForPoints= -lrp.dim.pmin+1;
offsetForPasses= -lrp.dim.kmin+1;

//description=" - along the pass profile";

select lrpAlongThePassPlot.variableName
	case STATE then
   		M=surface3D.state;
	case OUTPUT then
   		M=surface3D.output;
	case OUTPUT then
   		M=surface3D.input;
	else
  	 error("Unknown field: "+lrpAlongThePassPlot.variableName+".");
end


// The lrpAlongThePassPlot.pointsRange is [min,max] (for example [1,4]) but
//  we need the continuous range for points i.e. [1,2,3,4] - hence
//  the need for convertion
// This is the real range, as shown on the legend of the plot
pointsRange=lrpAlongThePassPlot.pointsRange(MIN_INDEX) : lrpAlongThePassPlot.pointsRange(MAX_INDEX);
// This will be used for indexing the matrix, so it MUST start from 1. Hence
//   we add the offset
pointsIndicesForMatrix=pointsRange + offsetForPoints;
passesIndicesForMatrix=lrpAlongThePassPlot.passes + offsetForPasses;

values=M( ...
   pointsIndicesForMatrix , ...
   passesIndicesForMatrix , ...
   lrpAlongThePassPlot.variableNumber ...
);

//create graphic window
figHandle=scf(whichGraphWindow);
//clear graphic window
//clf(whichGraphWindow);

figHandle.immediate_drawing="off";
figHandle.visible="off";

//info
if showInfoAboutPlot then
  lrpShowPlotInfo(lrpAlongThePassPlot);
end

if length(pointsRange) ~=1 then
   plotD2Line(pointsRange, lrpAlongThePassPlot.passes, values, whichGraphWindow, lrpAlongThePassPlot.description.legend);
else
   plotD2Circle(pointsRange, lrpAlongThePassPlot.passes, values, whichGraphWindow, lrpAlongThePassPlot.description.legend);
end

//nameOfFigure of window
nameOfFigure=strcat(["%d: " lrpAlongThePassPlot.description.title " " sci2exp(lrpAlongThePassPlot.passes,0)]);

//this is because title of figure is bounded to 79 characters
nameOfFigure=shortenString(nameOfFigure, 79-floor(log10(whichGraphWindow)+1)+1); //+1 because (%d) have 2 characters
set(figHandle,"figure_name",nameOfFigure);

xtitle(lrpAlongThePassPlot.description.title, lrpAlongThePassPlot.description.x_label, ...
                  lrpAlongThePassPlot.description.y_label, lrpAlongThePassPlot.description.z_label);


figHandle.immediate_drawing="on";
figHandle.visible="on";
endfunction

function []=%lrp_Plt_Options2D_p(var)
// LRP Toolkit for Scilab. This function is used to display the 2D plot options
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-05 15:12:00

//constant
COLORS_TO_SHOW=8;  // How many colors in the color_map to show - set not to clutter
                    // the screen so much. You may change it if you want.

printf("LRP 2D Plot options:\n\n");
printf("  arrow_size_factor=%d\n", var.arrow_size_factor);
printf("  background=%d\n", var.background);
printf("  bar_width=%d\n", var.bar_width);
printf("  clip_box=%s\n", sci2exp(var.clip_box));
printf("  clip_state=""%s""\n", var.clip_state);
printf("  closed=""%s""\n", var.closed);
if (isempty(var.color_map)) then
   printf("  color_map=[]\n");
else
   printf("  var.color_map=\n");
   printf("      +======+========================================+\n");
   printf("      |index |\tR\t      G\t\t   B          |\n");
   printf("      +======+========================================+\n");
   for index=1:min(size(var.color_map,1),COLORS_TO_SHOW)
       printf("      |%5d | %5.6f\t  %5.6f\t%5.6f      |\n",index,var.color_map(index,1),var.color_map(index,2),var.color_map(index,3));
   end
   printf("      +======+========================================+\n");
   if size(var.color_map,1)>COLORS_TO_SHOW then
      printf("    ***** CAUTION: ONLY FIRST "+sci2exp(COLORS_TO_SHOW)+" OUT OF "+sci2exp(size(var.color_map,1))+" COLORS ARE SHOWN *****. \n      Use >>disp(variable_name.color_map);<< to see all.\n\n");
   end;
end
printf("  fill_mode=""%s""\n", var.fill_mode);
printf("  foreground=%d\n", var.foreground);
printf("  interp_color_mode=""%s""\n", var.interp_color_mode);
if (isempty(var.interp_color_vector)) then
   printf("  interp_color_vector=%s\n", sci2exp(var.interp_color_vector));
else
   printf("  interp_color_vector=");
   disp(var.interp_color_vector);
   printf("\n");
end
printf("  line_mode=""%s""\n", var.line_mode);
printf("  line_style=%d\n", var.line_style);
printf("  mark_background=%d\n", var.mark_background);
printf("  mark_foreground=%d\n", var.mark_foreground);
printf("  mark_mode=""%s""\n", var.mark_mode);
printf("  mark_size_unit=""%s""\n", var.mark_size_unit);
printf("  mark_size=%d\n", var.mark_size);
printf("  mark_style=%d\n", var.mark_style);
printf("  polyline_style=%d\n", var.polyline_style);
printf("  thickness=%d\n", var.thickness);
printf("  user_data=%s\n", sci2exp(var.user_data));
printf("  visible=""%s""\n", var.visible);
printf("  x_shift=%s\n", sci2exp(var.x_shift));
printf("  y_shift=%s\n", sci2exp(var.y_shift));
printf("  z_shift=%s\n", sci2exp(var.z_shift));

// printf('initial state points  x0=');
// if var.x0<>STUB then
//    disp(var.x0);
// else
//    printf(' *** ??? *** \n');
// end
// printf('initial output pass   y0=');
// if var.y0<>STUB then
//    disp(var.y0);
// else
//    printf(' *** ??? *** \n');
// end
endfunction

// matricesLRP - matrices --> matricesLRP=tlist(A,B,B0,C,D,D0)
// numberOfPoints - number of points on a pass
// numberOfPasses - number of passes

// The boundary conditions:
//  x0=0
//  y0 = yref

// [lrp]=standardByILCToLRP(A,B,C,K1,K2,yref,alpha,beta)

function [lrp]=standardToLRPByILC(A,B,C,K1,K2,yref,alpha,beta);
//sizes of matrices
[rA, cA]=size(A);
if (rA~=cA) then
   printf("***********************************************************************\n");
   printf("Sizes of matrix ""A"" are diffrent. It shold be equal [n by n].\n");
   printf("Current size is: [%d, %d]\n", rA, cA);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""A""");
end

[rB ,cB]=size(B);
if (rA ~= rB) then
   printf("***********************************************************************\n");
   printf("Sizes of matrix ""B"" are diffrent from ""A"".\n");
   printf("Size of matrix ""B"" is: [%d, %d]\n",rB, cB);
   printf("Size of matrix ""A"" is: [%d, %d]\n",rA, cA);
   printf("Correct size of matrix ""B"" should be: [%d, %d]\n",rA, cB);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""B""");
end

[rC ,cC]=size(C);
if (cA ~= cC) then
   printf("***********************************************************************\n");
   printf("Sizes of matrix ""C"" are diffrent from:\n\n    ""A"" or ""B0"".\n\n");
   printf("Size of matrix ""C"" is: [%d, %d]\n",rC, cC);
   printf("Size of matrix ""A"" is: [%d, %d]\n",rA, cA);
   printf("Correct size of matrix ""C"" should be: [%d, %d]\n",rC, cA);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""C""");
end

//=================== main part ===================================

//======================== size of system =====================
// [rB, cB]=size(B);
// [rD]=size(D0, 1);

n = rB;  //number of states
r = cB;  //number of inputs
m = rC; //number of outputs

[rK1 ,cK1]=size(K1);
if (r ~= rK1) | (n ~= cK1) then
   printf("***********************************************************************\n");
   printf("Size of matrix ""K1"" is wrong:\n\n");
   printf("Size of matrix ""B"" is: [%d, %d]\n",rB, cB);
   printf("Correct size of matrix ""K1"" should be: [%d, %d]\n",r, n);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""K1""");
end

[rK2 ,cK2]=size(K2);
if (r ~= rK2) | (m ~= cK2) then
   printf("***********************************************************************\n");
   printf("Size of matrix ""K2"" is wrong:\n\n");
   printf("Size of matrix ""B"" is: [%d, %d]\n",rB, cB);
   printf("Correct size of matrix ""K2"" should be: [%d, %d]\n",r,m);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""K2""");
end
[ryref ,cyref]=size(yref);
if (m ~= ryref) | (alpha ~= cyref) then
   printf("***********************************************************************\n");
   printf("Size of matrix ""yref"" is wrong:\n\n");
   printf("Size of matrix ""B"" is: [%d, %d]\n",rB, cB);
   printf("                   beta: %d\n",beta);   
   printf("Correct size of matrix ""yref"" should be: [%d, %d]\n",m,beta);
   printf("***********************************************************************\n");
   error("Mismatch sizes in matrix ""beta""");
end

lrp=createLRPModel(A+B*K1, B, B*K2, -C*(A+B*K1), zeros(m,r), (eye()-C*B*K2),alpha,beta, zeros(n,beta), yref);

endfunction

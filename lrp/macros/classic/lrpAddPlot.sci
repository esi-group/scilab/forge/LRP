//lrp - our system
//gso - grid on the X and Y, and values of our system on the X and Y
//directory - where the files of plots have to be
//directionStr - "alongthepass",  "horizontal"
//         or "passtopass", "vertical"
//variableStr - "x", "state" or "y", "output", "u", "input" or "all"
//         "all" means list("x","y","u").
//variableVal - row vector states, inputs or outputs, e.g. [1 2 3 4]
//adqValToVar - list of row vectors which contains number of points or passes:
//                 e.g list([1,2,3],[4,5,6,7,8,9],[1,2])
// The plots are generated as follows:
//   For each element of variableVal plot a corresponding
//     points/passes contained in adqValToVar.
//
//   Example:
//   To create two "along the pass" plots of state ("x") variables containing
//    plot 1: state 3, passes 1, 3 and 7, all points 
//    plot 2: state 3, pass 11, points from 3 to 19 (i.e. [3,4,5, ... 19])
//    plot 1: state 7, passes 1, 3 and 7, all points 
//    plot 2: state 7, pass 11, points from 3 to 19 (i.e. [3,4,5, ... 19])
//   set:
//     variableVal=[3,7] // For each state 
//     adqValToVar=list([1,3,4],[11])  // two plots will be created
//     variableStr="state"
//     directionStr="alongthepass"// i.e. for all (or range of) points on a given pass
//     adqValToVarRange=list("all",[3,19])
//
//  [lrpPlotData]=lrpAddPlot(lrp, directionStr, variableStr, ...
//             variableVal, adqValToVar, adqValToVarRange, lrpPlotData);
//
//will add two state (see: variableStr) plots along the pass (see: directionStr)
//       - for state #3 plot for passes 1, 3 and 4, for all points. See
//              variableVal(1), adqValToVar(1), adqValToVarRange(1)
//       - for state #7 plot for pass 11, for points 3 upto 19 (including 3
//              and 19). See variableVal(2), adqValToVar(2), adqValToVarRange(2)
//
//   For the plot's axes description see "lrp_Plt_Description" structure defined in this file
//
//  CAUTION:
// adqValToVar and adqValToVarRange are converted to a list.
//
// EXAMPLE:
//   plot=lrpAddPlot(lrp,'system','3D','output',[1],list('all'),list('all'))
function [lrpPlotDataResult]=lrpAddPlot(...
   lrp, ...               //system
	 name, ...              //name (can be empty)
   directionStr, ...      // "alongthepass", "passtopass", "3d"
   variableStr, ...       // "state", "output","input","x","y","u" or "all" or a list 
   ...                    //    containing any of those strings
   variableVal, ...       // vector of state, output or input, e.g. [2,1,3,6]
   adqValToVar, ...       // list of row vectors adequate to "variableVal" --- points in 3D
   ...                    //    OR list("All")
   adqValToVarRange,...   // list of row vectors adequate to "variableVal" --- passes in 3D
   ...                    //    OR list("All")
   lrpPlotData ...        // [optional] previous value of the lrpPlotData structure
)
//===================================== Constants =======================
ALONGTHEPASS="alongthepass";
PASSTOPASS="passtopass";
THREEDIMENSIONAL="3d";
STATE="state";
OUTPUT="output";
INPUT="input";
ALL="all";
//=======================================================================

[nargout, nargin]=argn();

if (nargin < 7) | (nargin > 8) then
    error("Incorrect number of arguments.");
end
//===================================== Requirements =======================
requireType("lrp", lrp, "tlist", "lrp_Sys_Cla");

requireType("name",name,["string","empty string"]);

requireType("directionStr", directionStr, "string");
                            // NOTE THAT THE NAME MIGHT BE SPECIFIED IN CAPITAL LETTERS ETC.
                            // Hence it is required to convert this name into some common ground.
  directionStr=replaceValue(directionStr, list(ALONGTHEPASS, "horizontal"), ALONGTHEPASS);
  directionStr=replaceValue(directionStr, list(PASSTOPASS, "vertical"), PASSTOPASS);
  directionStr=replaceValue(directionStr, list(THREEDIMENSIONAL, "3d", "threed", "threedimensional", "3dplot"), THREEDIMENSIONAL);   
  requireValue("directionStr", directionStr, list(ALONGTHEPASS, PASSTOPASS, THREEDIMENSIONAL));

if type(variableStr)==10 then
// it is a string - let's replace "ALL" with list("x","y","u")
   if strcmpi(variableStr,ALL)==0
      variableStr = list("x","y","u");      
   end
end
if ~exists('lrpPlotData')
   lrpPlotData=[];
end
if type(variableStr)==15 then
  // it is a list so iterate it
  [lrpPlotDataResult]=lrpAddPlot(...
     lrp, ...               //system
  	 name, ...              //name (can be empty)
     directionStr, ...      // "alongthepass", "passtopass", "3d"
     variableStr(1), ...       // "state", "output","input","x","y","u" or "all" or a list 
     ...                       //    containing any of those strings
     variableVal, ...       // vector of state, output or input, e.g. [2,1,3,6]
     adqValToVar, ...       // list of row vectors adequate to "variableVal" --- points in 3D
     ...                    //    OR list("All")
     adqValToVarRange,...   // list of row vectors adequate to "variableVal" --- passes in 3D
     ...                    //    OR list("All")
     lrpPlotData ...        // [optional] previous value of the lrpPlotData structure
  );
  for x=2:lstsize(variableStr)
    [lrpPlotDataResult]=lrpAddPlot(...
       lrp, ...               //system
    	 name, ...              //name (can be empty)
       directionStr, ...      // "alongthepass", "passtopass", "3d"
       variableStr(x), ...       // "state", "output","input","x","y","u" or "all" or a list 
       ...                       //    containing any of those strings
       variableVal, ...       // vector of state, output or input, e.g. [2,1,3,6]
       adqValToVar, ...       // list of row vectors adequate to "variableVal" --- points in 3D
       ...                    //    OR list("All")
       adqValToVarRange,...   // list of row vectors adequate to "variableVal" --- passes in 3D
       ...                    //    OR list("All")
       lrpPlotDataResult ...        // [optional] previous value of the lrpPlotData structure
    );
  end
  // The recurrence has done all the work so we exit now
  return;
end
requireType("variableStr",variableStr,"string");
  variableStr=replaceValue(variableStr, list("state","x"), STATE);
  variableStr=replaceValue(variableStr, list("output","y"), OUTPUT);
	variableStr=replaceValue(variableStr, list("input","u"), INPUT);    
  requireValue("variableStr", variableStr, list(STATE,OUTPUT,INPUT));

requireType("variableVal",variableVal,"vector");
	select variableStr, 
		case STATE then requireRange("variableVal",variableVal,[1,lrp.dim.n]);
		case OUTPUT then requireRange("variableVal",variableVal,[1,lrp.dim.m]);
		case INPUT then requireRange("variableVal",variableVal,[1,lrp.dim.r]);
	else
		error(sprintf('WRONG VALUE OF variableVal: %s',variableVal));
  end
if ((nargin==7) | (isempty(lrpPlotData))) then
   //no lrpPlotData argument
   lrpPlotData=createPlotDataStub();
else
    //We have the lrpPlotData argument, check
     if lcIsEmpty(lrpPlotData) then
        lrpPlotData=createPlotDataStub();
     else
         requireType("lrpPlotData",lrpPlotData,"tlist","lrp_Sys_Cla_Plt")
     end
end;
if isVector(adqValToVar) then
   adqValToVar=list(adqValToVar);
end
requireType("adqValToVar",adqValToVar,"list");

select directionStr
case ALONGTHEPASS then
   // We need the continuous RANGE (i.e. [0,1,2,3]) and not the [max,min]
   adqValToVar=replaceValue(adqValToVar, list("all"), [lrp.dim.kmin : lrp.dim.kmax]);
   requireRange("adqValToVar",adqValToVar,[lrp.dim.kmin,lrp.dim.kmax]);

   // We simply need the RANGE (i.e. [0, 3]) and not the continuous one [0,1,2,3]
   adqValToVarRange=parseListOfVectors("adqValToVarRange",adqValToVarRange,[lrp.dim.pmin,lrp.dim.pmax])
case PASSTOPASS
     // We need the continuous RANGE (i.e. [0,1,2,3]) and not the [max,min]
   adqValToVar=replaceValue(adqValToVar, list("all"), [lrp.dim.pmin : lrp.dim.pmax]);
   requireRange("adqValToVar",adqValToVar,[lrp.dim.pmin,lrp.dim.pmax])

   // We simply need the RANGE (i.e. [0, 3]) and not the continuous one [0,1,2,3]
   adqValToVarRange=parseListOfVectors("adqValToVarRange",adqValToVarRange,[lrp.dim.kmin,lrp.dim.kmax])
case THREEDIMENSIONAL then
   // For 3D we do not deal with discontinuous shapes.
   // We simply need the RANGE (i.e. [0, 3]) and not the continuous one [0,1,2,3]
   adqValToVar=parseListOfVectors("adqValToVar",adqValToVar,[lrp.dim.pmin,lrp.dim.pmax])

   // We simply need the RANGE (i.e. [0, 3]) and not the continuous one [0,1,2,3]
   adqValToVarRange=parseListOfVectors("adqValToVarRange",adqValToVarRange,[lrp.dim.kmin,lrp.dim.kmax])
else
    error("Not implemented");
end
//Now all the "all" values have been replaced, we can make sure that
//  adqValToVarRange is a list.
if isVector(adqValToVarRange) then
   adqValToVarRange=list(adqValToVarRange);
end
//requireType("adqValToVarRange",adqValToVarRange,["vector","list","string"]);
if isVector(variableVal) then
   if length(variableVal)>1
      lrpPlotDataResult = lrpAddPlot(...
         lrp, ...               //system
      	 name, ...              //name (can be empty)
         directionStr, ...      // "alongthepass", "passtopass", "3d"
         variableStr, ...       // "state", "output" or "input"
         variableVal(1), ...       // vector of state, output or input, e.g. [2,1,3,6]
         adqValToVar, ...       // list of row vectors adequate to "variableVal" --- points in 3D
         ...                    //    OR list("All")
         adqValToVarRange,...   // list of row vectors adequate to "variableVal" --- passes in 3D
         ...                    //    OR list("All")
         lrpPlotData ...        // [optional] previous value of the lrpPlotData structure
      )
      for x=2:length(variableVal)
        lrpPlotDataResult = lrpAddPlot(...
         lrp, ...               //system
      	 name, ...              //name (can be empty)
         directionStr, ...      // "alongthepass", "passtopass", "3d"
         variableStr, ...       // "state", "output" or "input"
         variableVal(x), ...       // vector of state, output or input, e.g. [2,1,3,6]
         adqValToVar, ...       // list of row vectors adequate to "variableVal" --- points in 3D
         ...                    //    OR list("All")
         adqValToVarRange,...   // list of row vectors adequate to "variableVal" --- passes in 3D
         ...                    //    OR list("All")
         lrpPlotDataResult ...        // [optional] previous value of the lrpPlotData structure
      )
      end 
      return;     
   end
end
//check lengths of arguments
//we require that   length(variableVal)==4,      //variableVal=[1, 5, 1, 3]
//will be equal to:
// lengths:  adqValToVar and adqValToVarRange
lenVariableVal=length(variableVal);
lenAdqValToVar=lstsize(adqValToVar);
lenAdqValToVarRange=lstsize(adqValToVarRange);

if ((lenVariableVal ~= lenAdqValToVar) | (lenAdqValToVar ~= lenAdqValToVarRange)) then
	printf("*******************************************************\n")
	printf("Length of ''variableVal'' must be equal to\n");
	printf("      lengths of ''adqValToVar'' and ''adqValToVarRange''.\n");
	printf("          length(variableVal)=%d\n", lenVariableVal);
	printf("          length(adqValToVar)=%d\n", lenAdqValToVar);
	printf("          length(adqValToVarRange)=%d\n", lenAdqValToVarRange);
	printf("*******************************************************\n")
	error("Check arguments'' lengths.")
end



//======================== Operational part ====================================

descriptionPlot=tlist(["lrp_Plt_Description"; ...
   "title"; "x_label"; "y_label"; "z_label"; "legend"; "name" ...
], ...
"STUB", "STUB", "STUB", "STUB", "STUB", "STUB" ...
);


// After modifying this part, modify also lcVariableName - see the bottom of this file
select variableStr
   case STATE then
         stateOrOutputName="State";
         dimStateOrOutput=lrp.dim.n;
   case OUTPUT then
         stateOrOutputName="Output";
         dimStateOrOutput=lrp.dim.m;
   case INPUT then
         stateOrOutputName="Input";
         dimStateOrOutput=lrp.dim.r;
   else
    error("Unknown field: "+lrpAlongThePassPlot.variableName+".");
end


select directionStr
  case ALONGTHEPASS then
									// ============= ALONG THE PASS ===============
									// type lrpAlongThePassPlot = record
									//      variableName   : ["state","input","output"];
									//      variableNumber : integer;
									//      passes         : vector[1xn];
									//      pointsRange    : vector[1x2];
									//      //vertex=1       : integer;
									// end;
									for plotNumber = 1:length(variableVal)
                              if dimStateOrOutput==1
                                descriptionPlot.title=stateOrOutputName + " - along the pass profile";
                              else
                                descriptionPlot.title=stateOrOutputName +" "+string(variableVal(plotNumber))+ ...
                                                                 " of "+string(dimStateOrOutput) + " - along the pass profile";
                              end
                              descriptionPlot.x_label="points";
                              descriptionPlot.y_label="";
                              descriptionPlot.z_label="";
                              descriptionPlot.legend="pass";
							                descriptionPlot.name=name;
                                 myPlot=tlist(...
                                       ["lrp_Sys_Cla_P_AP"; ...
                                                "variableName"; ...
                                                "variableNumber"; ...
                                                "passes"; ...
                                                "pointsRange"; ...
                                                "description" ...
                                          ], ...
												      variableStr, ...
												      variableVal(plotNumber), ...
												      makeRowVector("adqValToVar("+int2str(plotNumber)+")", adqValToVar(plotNumber)), ...
												      makeRowVector("adqValToVarRange("+int2str(plotNumber)+")", adqValToVarRange(plotNumber)), ...
                              descriptionPlot ...
											);
										   // Update the boundary information - Don't forget that adqValToVarRange is a a vector
											lrpPlotData.maxPoints = max(lrpPlotData.maxPoints,max(adqValToVarRange(plotNumber)));
											lrpPlotData.maxPasses = max(lrpPlotData.maxPasses,max(adqValToVar(plotNumber)));
											// Add myPlot to lrpPlotData
											lrpPlotData.plotData($+1) = myPlot;
									end
   case PASSTOPASS then
									// ============= PASS TO PASS ===============
									// type lrpPassToPassPlot = record
									//      variableName   : ["state","input","output"];
									//      variableNumber : integer;
									//      points         : vector[1xn];
									//      passesRange    : vector[1x2];
									//      //vertex=1     : integer;
									// end;
								for plotNumber = 1:length(variableVal)
                              if dimStateOrOutput==1
                                 descriptionPlot.title=stateOrOutputName + " - pass to pass profile";
                              else
                                  descriptionPlot.title=stateOrOutputName +" "+string(variableVal(plotNumber))+ ...
                                " of "+string(dimStateOrOutput) + " - pass to pass profile";
                              end
                              descriptionPlot.x_label="passes";
                              descriptionPlot.y_label="";
                              descriptionPlot.z_label="";
                              descriptionPlot.legend="point";
							                descriptionPlot.name=name;
							                
										myPlot=tlist(["lrp_Sys_Cla_P_PP"; ...
                                             "variableName"; ...
                                              "variableNumber"; ...
                                              "points"; ...
                                              "passesRange"; ...
                                              "description" ...
                                    ], ...
												variableStr, ...
												variableVal(plotNumber), ...
												makeRowVector("adqValToVar("+int2str(plotNumber)+")",adqValToVar(plotNumber)), ...
												makeRowVector("adqValToVarRange("+int2str(plotNumber)+")",adqValToVarRange(plotNumber)), ...
                                    descriptionPlot ...
										);
										// Update the boundary information - Don't forget that adqValToVarRange is a a vector
										lrpPlotData.maxPoints = max(lrpPlotData.maxPoints,max(adqValToVar(plotNumber)));
										lrpPlotData.maxPasses = max(lrpPlotData.maxPasses,max(adqValToVarRange(plotNumber)));
										// Add myPlot to lrpPlotData
										lrpPlotData.plotData($+1) = myPlot;
								end
   case THREEDIMENSIONAL then
										// ============= 3D PLOT ===============
										// type lrp3DPlot = record
										//      variableName   : ["state","input","output"];
										//      variableNumber : integer;
										//      pointsRange    : vector[1x2] --- [min(V), max(V)];
										//      passesRange    : vector[1x2] --- [min(V), max(V)];
										//      //vertex=1     : integer;
										// end;

									for plotNumber = 1:length(variableVal)
                              if dimStateOrOutput==1
                                 descriptionPlot.title=stateOrOutputName;
                              else
                                  descriptionPlot.title=stateOrOutputName+": "+string(variableVal(plotNumber))+" of "+string(dimStateOrOutput);
                              end
                              descriptionPlot.x_label="passes";
                              descriptionPlot.y_label="points";
                              if dimStateOrOutput==1
                                descriptionPlot.z_label=lcVariableName(stateOrOutputName)+"(k,p)";
                              else
                                descriptionPlot.z_label=lcVariableName(stateOrOutputName)+"_"+string(variableVal(plotNumber))+"(k,p)";
                              end
                              descriptionPlot.legend="";
                              descriptionPlot.name=name;

                  // Here we convert the (dis)continuous vector adqValToVar(plotNumber)
                  //   into a proper range of [min,max] - see getMinMaxVector below
											myPlot=tlist(["lrp_Sys_Cla_P_3D"; ...
                                                  "variableName"; ...
                                                   "variableNumber"; ...
                                                   "pointsRange"; ...
                                                    "passesRange"; ...
                                                    "description" ...
                                       ], ...
													variableStr, ...
													variableVal(plotNumber), ...
													getMinMaxVector("adqValToVar("+int2str(plotNumber)+")",adqValToVar(plotNumber)), ...
													getMinMaxVector("adqValToVarRange("+int2str(plotNumber)+")",adqValToVarRange(plotNumber)), ...
                                       descriptionPlot ...
											);
											// Update the boundary information - Don't forget that adqValToVarRange is a a vector
											lrpPlotData.maxPoints = max(lrpPlotData.maxPoints,max(adqValToVar(plotNumber)));
											lrpPlotData.maxPasses = max(lrpPlotData.maxPasses,max(adqValToVarRange(plotNumber)));
											// Add myPlot to lrpPlotData
											lrpPlotData.plotData($+1) = myPlot;
									end
   else
       error("Not implemented");
end //of Select

lrpPlotDataResult=lrpPlotData;
endfunction






//================================ inline functions =====================
function result=lcIsEmpty(variable)
    if or(type(variable)==[16, 17]) then
       //Tlist or Mlist by definition cannot be empty
       result=%F
    else
        result=isempty(variable);
    end
endfunction
//---------------------------------------------------------------------------------------------------------
function result=requireMinMaxRange(nameTwoElementVector, twoElementVector)
// **UNSAFE**
   if twoElementVector(1)>twoElementVector(2) then
      printf("\n********** lrpAddPlot::requireMinMaxRange **********************\n\n");
      printf("Wrong range in " + nameTwoElementVector+". Min>Max\n\n");
      printf("  Required value: ["+int2str(twoElementVector(2))+", "+int2str(twoElementVector(1))+"]\n\n");
      printf("  Current value of " + nameTwoElementVector+":\n");
      disp(twoElementVector);
      printf("\n**************************************************************\n");
      error("Wrong range");
   end;
   result=%T;
endfunction
//---------------------------------------------------------------------------------------------------------
function result=getMinMaxVector(vectorName, vector)
// **UNSAFE**

    vector=makeRowVector(vectorName, vector);
    result=[min(vector), max(vector)];
    select length(vector)
           case 1 then
                // Single number is converted without warning
                // This case cannot be removed, otherwise we would jump to
                //   the else section and display a warning, we do not want.
           case 2 then
                // We got the range - check it
                requireMinMaxRange(vectorName, vector);
           else
               warning(' '+vectorName+' has been converted to a range of '+int2str(result));
    end
endfunction
//---------------------------------------------------------------------------------------------------------

function result=parseListOfVectors(aListName, aList, vecRange)
  // Replaces each occurence of "all" by vecRange
  // Checks the range for all elements in aList
  // Returns a modified, parsed list
  // CAUTION - *UNSAFE* DOES NOT CHECK ARGUMENT TYPES
   aList=replaceValue(aList,list("all"),vecRange);
   for index=1:lstsize(aList)
       requireType(aListName,aList(index),"integer vector");
       requireRange(aListName,aList(index),vecRange)
       aList(index)=getMinMaxVector(aListName+"("+int2str(index)+")",aList(index));
   end
   result=aList;
endfunction

function result=lcVariableName(stateOrOutputName)
   // Changes the long name into a short variable name
   //   Note that the checks are case sensitive!
   if stateOrOutputName=="State"
     result="x";
     return result;
   end
   if stateOrOutputName=="Output"
     result="y";
     return result;
   end
   if stateOrOutputName=="Input"
     result="u";
     return result;
   end
   error('CRITICAL ERROR in lrpAddPlot/lcVariableName(): Unknown field: '+stateOrOutputName);
endfunction

//checking sizes of matrices 
//matrices 'A','B','B0' must be sizes of 'n' by 'n','r','m' respectively
//matrices 'C','D','D0' must be sizes of 'm' by 'n','r','m' respectively

function [info]=checkSizesOfMatrices(A,B,B0,C,D,D0)
info=%F;
//checking sizes of matrices 
//============================
[n,x]=size(A);
if (n~=x) then
	error('Incorrect size of matrix ''A''.');
end	

[y,r]=size(B);
if (n~=y) then
	error('Incorrect size of matrix ''B''.');
end	

[z,m]=size(B0)
if (n~=z) then
	error('Incorrect size of matrix ''B0''.');
end	

[mm,nn]=size(C);
if (m~=mm) | (n~=nn) then
	error('Incorrect size of matrix ''C''.');
end	

[mmx,rrx]=size(D);
if (m~=mmx) | (r~=rrx) then
	error('Incorrect size of matrix ''D''.');
end	

[mmy,mmz]=size(D0);
if (m~=mmy) | (m~=mmz) then
	error('Incorrect size of matrix ''D0''.');
end	

//evrything is OK
info=%T;
endfunction

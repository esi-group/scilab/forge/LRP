function []=%lrp_Sys_Cla_Ext_p(var)
// LRP Toolkit for Scilab. This function is used to display the external forces data
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-03-12 00:15:00

fun = eval(var.uGenerator);
[name, params] = fun(var);
requireType("generating function default parameters",params,["list","empty list"]);
requireType("generating function default description",name,"string");
if length(params)==0 then
	printf('Input U generated as  : u(k,p)=%s. No parameters specified.',name);
else
	printf('Input U generated as  : u(k,p)=%s\n',name);
	// No parameters are displayed by default
// 	printf('Parameters     : \n');
// 	for x=1:length(params)
// 		printf('ext.uParameters(%d) = ',x);
// 		if type(params(x))==1 then
// 			// This is a number, we can write it in a more compact way
// 			printf('%d\n',params(x));
// 		else
// 			disp(params(x));
// 		end
// 	end
end
endfunction

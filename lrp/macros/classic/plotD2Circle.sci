// //example
// x=3;
// whichPassesOrPoints=[3 2 5 4 7]
// y=round(rand(1, length(whichPassesOrPoints))*20)
// nrGraphicsWindow=1;
// strLegned="point";
// plotD2Circle(x, whichPassesOrPoints, y, nrGraphicsWindow, strLegned)

function plotD2Circle(x, whichPassesOrPoints, y, nrGraphicsWindow, strLegned)

//============ check arguments ======================
[nargout nargin]=argn();
if (nargin ~= 5) then
  error("Function expected 5 inputs argument, but has got: "+ sci2exp(nargin,0));
end

// if (nargout ~= 0) then
//   error("Function no expected outputs argument, but has got: "+ sci2exp(nargout,0));
// end

requireType("x", x, "integer");
requireType("whichPassesOrPoints", whichPassesOrPoints, ["integer vector", "integer"]);
requireType("nrGraphicsWindow", nrGraphicsWindow, "integer");
requireType("strLegned", strLegned, "string");

requireValue("strLegned", strLegned, list("pass", "point"));

//============ main part ======================
NUMBER_OF_COLORS=31;

figHandle=scf(nrGraphicsWindow);
//clear graphics window
clf();
figHandle.immediate_drawing="off";
figHandle.visible="off";


lenY=length(y);
for index=1:lenY
  plot(x, y(index));
end

//children==1  ==> legend
LEGEND=1;

//figHandle.children.margins=[left right up down]
//children==2  ==> figure
FIUGURE=2;

//wider window, because points will be truncated to half points
if figHandle.children(1).data_bounds(2,2) > 0 then
   tmp_up_y=figHandle.children(1).data_bounds(2,2)*1.03;
else
   tmp_up_y=figHandle.children(1).data_bounds(2,2)*0.97;
end
if figHandle.children(1).data_bounds(1,2) > 0 then
   tmp_down_y=figHandle.children(1).data_bounds(1,2)*0.97;
else
   tmp_down_y=figHandle.children(1).data_bounds(1,2)*1.03;
end
figHandle.children(1).data_bounds(2,2)=tmp_up_y;
figHandle.children(1).data_bounds(1,2)=tmp_down_y;

//wider window, because points will be truncated to half points
//this property make something like this abowe in comments -- but not always work !!! so we stay with this what above
//figHandle.children(1).tight_limits="off";


colorIndex=1;
for index=1:lenY
   //because 8 == withe color and it is invisible so it is no useful for us
   if colorIndex==8 then
        colorIndex=colorIndex+1;
   end
   figHandle.children.children(index).children.line_mode="off";
   figHandle.children.children(index).children.mark_mode="on";
   figHandle.children.children(index).children.mark_size_unit="point";
   figHandle.children.children(index).children.mark_size=7;
   figHandle.children.children(index).children.mark_style=9;  //circle
   figHandle.children.children(index).children.mark_foreground=-1;  //black
   figHandle.children.children(index).children.mark_background=colorIndex;  //background circle
   colorIndex=pmodulo(colorIndex, NUMBER_OF_COLORS);
   colorIndex=colorIndex+1;
end

//legend
if lenY <= 18 then
   strLegned=strLegned+" "+string(whichPassesOrPoints');
   legend(strLegned);
else
//else withoud legend
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//without legend - only information about this
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   if strLegned=="pass" then
      strLegned="passes";
   end
   if strLegned=="point" then
      strLegned="points";
   end

   strLegned=[string(length(whichPassesOrPoints))+" "+strLegned; "on this plot"];
   //setting legends
   legend(strLegned,1);

    lenTextPolyline=size(figHandle.children(LEGEND).children.children,1) ;
      disp(lenTextPolyline)
    for index=1: lenTextPolyline
       //turn off visiblity of line in legend
       if figHandle.children(LEGEND).children.children(index).type=="Polyline" then
          if index ~= lenTextPolyline then
             figHandle.children(LEGEND).children.children(index).visible="off";
          end
       end
//        //move the all in box of the legend to the right side of the plot
//        figHandle.children(LEGEND).children.children(index).data(:,1)= ...
//                                figHandle.children(LEGEND).children.children(index).data(:,1)+SHIFT_ON_OX;
    end
   //move only the box of legend to right side of the plot
   // 1 2
   // 4 3
   //2:3 means: move the right side of the box to the left about -0.0015
   //[1 4] means: move the left side of the box to the right about +0.035
    figHandle.children(LEGEND).children.children($).data(2:3,1)=  ...
                      figHandle.children(LEGEND).children.children($).data(2:3,1)-0.0015;
    figHandle.children(LEGEND).children.children($).data([1 4],1)=  ...
                      figHandle.children(LEGEND).children.children($).data([1 4],1)+0.035;
end

figHandle.immediate_drawing="on";
figHandle.visible="on";
endfunction

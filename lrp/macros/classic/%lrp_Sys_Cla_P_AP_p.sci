function []=%lrp_Sys_Cla_P_AP_p(var)
// LRP Toolkit for Scilab. This function is used to display the along the pass plot data
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-03-12 00:15:00

printf('                          Along the pass plot\n');
printf('variableName  : %s\n',var.variableName);
printf('variableNumber: %d\n',var.variableNumber);
printf('passes        : %d\n',var.passes);
printf('pointsRange   : %s\n',sci2exp(var.pointsRange));
printf('description   : ');
disp(var.description);

endfunction

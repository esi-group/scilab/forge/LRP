function []=%lrp_Sys_Cla_Ini_p(var)
// LRP Toolkit for Scilab. This function is used to display the initial conditions of the classic LRP
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-05 15:12:00

//constant
STUB="STUB";

printf('initial state points  x_k(0)=');
if var.x0<>STUB then
   disp(var.x0);
else
   printf(' *** ??? *** \n');
end
printf('initial output pass   y_0(p)=');
if var.y0<>STUB then
   disp(var.y0);
else
   printf(' *** ??? *** \n');
end
endfunction

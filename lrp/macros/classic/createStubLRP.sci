// This function creates an empty LRP system - ready to be filled with real-world data.
// Author Lukasz Hladowski
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2007-04-26 09:46:00 

//lrp=createStubLRP()
function [lrp]=createStubLRP()

//constant
STUB="STUB";

//matrices
A=STUB;
B=STUB;
B0=STUB;
C=STUB;
D=STUB;
D0=STUB;

//sizes
n=STUB;
r=STUB;
m=STUB;

//points and passes
numberOfPoints=STUB;
numberOfPasses=STUB;


//bonduary condition
x0=STUB;
//initial pass
y0=STUB;


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//setting typed list for controller "none"
CONTROLLER_NONE=tlist(...
["lrp_Sys_Cla_C_non";...
   "functionName"; ...
   "displayName"; ...
	 "solutionExists"; ...       //solution exists (always true)
   "A";"B";"B0";"C";"D";"D0" ...  ///name of matrices;
],...
    "none", ...
    "open loop system", ...
		%T, ... //always true (a priori)
    A, B, B0, C, D, D0 ...
);
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


//range on axis
//points 0 <= p <= alpha - 1
pmin=STUB;
pmax=STUB;;   // such long as number of points minus one:  alpha-1

//passes 0 <= k <= beta
kmin=STUB;
kmax=STUB;   // such long as number of passes:  beta


//dimensions
tl_dim=tlist(...
["lrp_Sys_Cla_Dim"; ...  //name of this list - dimensions of classic lrp
   "n"; ...  //number of states
   "r"; ...  //number of inputs
   "m"; ... //number of outputs
   "alpha"; ... //number of points
   "beta";...     //number of passes
   "pmin"; ... //range on axis on points; each point starts from "pmin" and ends at "pmax", including "pmin" and "pmax"
   "pmax"; ...//range on axis on points;     pmin <= p <= pmax
   "kmin"; ... //range on axis on passes; each pass starts from "kmin" and ends at "kmax", including "kmin" and "kmax"
   "kmax" ...//range on axis on passes    kmin <= k <= kmax
],...
   n,r,m,...
   numberOfPoints,...
   numberOfPasses,...
   pmin, pmax, ...
   kmin, kmax ...
);

tl_mat=tlist(...
["lrp_Sys_Cla_Mat"; ...  //matrices of classic LRP
   "A";"B";"B0";...  //name of matrices
   "C";"D";"D0" ...
],...
   A, B, B0,...
   C, D, D0 ...
);

tl_boundary=tlist(...
["lrp_Sys_Cla_Ini"; ...  //boundary conditions
   "x0"; "y0" ...
],...
   x0, y0 ...
);

tl_external=tlist(...
["lrp_Sys_Cla_Ext"; ...  //External forces - currently inputs; can include disturbances etc. 
   "uGenerator"; "uParameters" ...
],...
   "uZero", list() ... // The parameters will be calculated after type initialisation 
);
fun = eval(tl_external.uGenerator);
[name,params] = fun();
tl_external.uParameters = params;

//create lrp
lrp=tlist(...
["lrp_Sys_Cla"; ...
   "type"; ...
   "dim"; ...
   "mat"; ...
   "ini"; ... //boundary conditions
   "ext"; ... //external forces   
   "controller"; ...  //list of controllers
   "indController" ...  //index which controller is current
], ...
   0, ...            //type of LRP model; 0 -means classic LRP model
   tl_dim, ...
   tl_mat, ...
   tl_boundary, ...
   tl_external, ...   
   list(CONTROLLER_NONE),... //controller none - the first controller
   1 ...                     //indController - which controller is used from above list. 1==no controller
);
endfunction

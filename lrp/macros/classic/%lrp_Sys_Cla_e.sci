function result=%lrp_Sys_Cla_e(field,lrp)
// LRP Toolkit for SciLab. Shows an error if a particular field of the LRP structure 
//   does not exist  
//
// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2009-12-02 08:44:00

if field=='latex' then
	disp('LATEX!!!');
	//disp(lrp);
	result='LATEX!!!';
else
  if field=='check' then
  	//disp(lrp);
    result=isGoodTList(lrp,'arg',%t);
    disp(result);
  else
    allowedFields=lrp(1);
    allowedFieldNames = strcat(sci2exp(allowedFields(2:$)));
    printf("\nAllowed fields are %s\n",allowedFieldNames);
    warning(sprintf('Invalid field name ""%s""\n See also file %s in directory classic\n\n',field,'%lrp_Sys_Cla_e.sci'));
  	error('Unknown field:'+field+'.');
  end
end
endfunction

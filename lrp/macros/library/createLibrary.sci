//global LRP_OPTIONS
//lrp_space
lrp_space=" ";

//OS
if (MSDOS) then
    lrp_OS='w';
else
    lrp_OS='u';
end

//load pathes to folders which contains functions in the library
try
    exec(fullfile(LRP_OPTIONS.path, "/macros/library/paths.sce"));
catch
    printf("***************************************************************\n");
    printf("lrp/macros/library/createLibrary.sci:\n");
    printf("Error calling \n    %s\n",fullfile(LRP_OPTIONS.path, "/macros/library/paths.sce"));
    printf("Scilab reports the following error:\n");
    printf("    ''%s''\n",lasterror(%f));    
    printf("***************************************************************\n");
    error(lasterror());
    return;    
end

lrp_howManyLibraries=size(lrpLibrary.folder,1);

//create full path to library
for lrp_i=1:lrp_howManyLibraries
  lrpLibrary.folder(lrp_i)=pathconvert(fullfile(LRP_OPTIONS.path, lrpLibrary.folder(lrp_i)),%t,%f,lrp_OS);
end

//clear if there are old copies of functions in RAM of these library
try
    exec(fullfile(LRP_OPTIONS.path, "/macros/library/cleanFunFromRam.sci"));
catch
    printf("***************************************************************\n");
    printf("lrp/macros/library/createLibrary.sci:\n");
    printf("Error calling \n    %s\n",fullfile(LRP_OPTIONS.path, "/macros/library/cleanFunFromRam.sci"));
    printf("Scilab reports the following error:\n");
    printf("    ''%s''\n",lasterror(%f));    
    printf("***************************************************************\n");
    error(lasterror());
    return;    
end

//delete current library
printf("Deleting old LRP compiled library... ")
if (MSDOS) then
    lrp_com_if="if exist ";
    lrp_com_delete=" del ";
    for lrp_i=1:lrp_howManyLibraries
        lrp_fol=lrpLibrary.folder(lrp_i);
        lrp_files_bin=""""+lrp_fol+"*.bin"+"""";
        lrp_file_lib=""""+lrp_fol+"lib"+"""";
        lrp_file_names=""""+lrp_fol+"names"+"""";
        unix(lrp_com_if + lrp_files_bin + lrp_com_delete + lrp_files_bin);
        unix(lrp_com_if + lrp_file_names + lrp_com_delete + lrp_file_names);
        unix(lrp_com_if + lrp_file_lib + lrp_com_delete + lrp_file_lib);
        //this does not work on Windows ME
        //unix(lrp_com_delete + lrp_space + lrp_files_bin + lrp_space + lrp_file_lib + lrp_space + lrp_file_names);
    end
else
    lrp_com_delete="rm -f ";
    for lrp_i=1:lrp_howManyLibraries

        lrp_fol=lrpLibrary.folder(lrp_i);

        lrp_files_bin=lrp_fol + "*.bin";
        //subtitute for every lrp_space this 2 chars (without ") backslash+lrp_space --> "\ "
        lrp_files_bin=strsubst(lrp_files_bin, lrp_space ,"\ ");
        lrp_files_bin=lrp_files_bin+lrp_space;

        lrp_file_lib=""""+lrp_fol+"lib"+""""+lrp_space;
        lrp_file_names=""""+lrp_fol+"names"+""""+lrp_space;

        unix(lrp_com_delete + lrp_files_bin + lrp_file_lib + lrp_file_names + " 2> /dev/null");
    end
end
printf("done.\n\n");


//create library
printf("Creating new LRP library...\n\n");

ncl=lines(); // Save the current pager options
lines(0); // Remove the [more] prompt

for lrp_i=1:lrp_howManyLibraries
    printf("%s, %s\n", lrpLibrary.name(lrp_i), lrpLibrary.folder(lrp_i));
    //genlib(listNameOfLibrary(lrp_i)(1), listNameOfLibrary(lrp_i)(2),%f,%t);
    genlib(lrpLibrary.name(lrp_i), lrpLibrary.folder(lrp_i));
    // Check if we have actually created anything
    if isempty(fileinfo(fullfile(lrpLibrary.folder(lrp_i),'lib')))
       // NOPE - this library has NOT been created! This shouldn't have happened so we issue a critical error
        printf("\n***************************************************************\n");
        printf("lrp/macros/library/createLibrary.sci:\n");
        printf("Library ''%s'' has NOT been created in directory\n",lrpLibrary.name(lrp_i));
        printf("    ''%s''\n",lrpLibrary.folder(lrp_i));        
        if isempty(fileinfo(lrpLibrary.folder(lrp_i)))
          printf("This directory DOES NOT exist! Check file ''lrp/macros/library/pathes.sci''\n");
        else
          lrp_fol = dir(lrpLibrary.folder(lrp_i)); // I want to reuse this name to avoid creating another in non-function
          if isempty(lrp_fol(2)) // lrp_fol(1) always contains "!dir  name  date  bytes  isdir  !". 
             printf("This directory exists but is empty! If you want to remove a library, edit ''lrp/macros/library/pathes.sci''\n");
          else
             printf("This directory contains files!\n");
          end
        end
        printf("Also check if directories in ''lrp/etc/config'' are properly set.\n");   
        printf("***************************************************************\n");
        error('Library NOT created!');
        return;    
    end
end

lines(ncl(2),ncl(1)); // Set the previous pager options

clear lrp_fol lrp_OS lrp_i lrp_space lrp_howManyLibraries lrp_com_if lrp_com_delete
clear lrp_file_lib lrp_files_bin lrp_file_names lrpLibrary

printf("\nAll LRP libraries created. Press ENTER.\n");

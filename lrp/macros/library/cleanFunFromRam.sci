//clear if there are old copies of functions in RAM of these library
for lrp_i=1:size(lrpLibrary.folder,1)
    [lrp_fd,lrp_err]=mopen(fullfile(lrpLibrary.folder(lrp_i),"names"),"r");
    if (lrp_err~=0) then
        continue;
    end

    //clear name of library
    clear(lrpLibrary.name(lrp_i));

    //clear functions of above library
    while meof(lrp_fd)==0
        lrp_fun_to_clear=mgetl(lrp_fd,1);
        if ~isempty(lrp_fun_to_clear) then
            clear(lrp_fun_to_clear);
        end
    end
    mclose(lrp_fd);
end

clear lrp_fun_to_clear lrp_fd lrp_err lrp_i

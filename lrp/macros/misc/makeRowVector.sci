//---------------------------------------------------------------------------------------------------------
function result=makeRowVector(vectorName, vector)
   // if we have a column vector (at least 2 element one)
   requireType("vectorName", vectorName,"string");
   requireType(vectorName, vector,"vector");
   if (size(vector,2)==1) & (size(vector,1)>1) then
      // make it a row one
      warning(" makeRowVector: List element "+vectorName+" was converted to row vector.");
      result=vector';
   else
       result=vector;
   end
endfunction

function result=isVector(variable)
//check variableVal
t=type(variable);
// 1 : real or complex constant matrix.
// 5 : sparse matrix.
// 8 : matrix of integers stored on 1 2 or 4 bytes
// 10 : string
if ( and( t ~= [1 5 8 10] ) ) then
   result=%F;  //it is no vector
   return;
end
if (min(size(variable))==1) & (ndims(variable)==2) then
  result=%T;
else
  result=%F;
end
endfunction

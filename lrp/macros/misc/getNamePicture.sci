//nameOfLatexFile -- name of latex file
//description -- some text for example "state" or "output" or something else
//value - which state for example

function [name]=getNamePicture(nameOfLatexFile,name_of_controller, description, value)
[nargout,nargin]=argn();
[path,fname,extension]=fileparts(nameOfLatexFile);

if nargin > 3
    str_value=sci2exp(value,0);
    name=strcat([fname "_fig_" name_of_controller  "_"  description "_" str_value ]);
else
    name=strcat([fname "_fig_" name_of_controller  "_" description]);
end
endfunction
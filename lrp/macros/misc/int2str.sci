//converts a number to string
// **** UNSAFE **** - no type check is performed
function str=int2str(anInt)
	 if argn(2)~=1 then
	 	  error("Wrong number of arguments to function int2str. Expected: 1, got: "+sci2exp(argn(2),0)+".");
   	 	str="";
  	 	return;
	 end
	 str=sci2exp(anInt,0);
endfunction

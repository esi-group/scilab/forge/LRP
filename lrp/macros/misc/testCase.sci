function result=testCase(expectedResult, strFunName, varargin);
// Note that the values below are selected in a way that it is highly unlike
//   return value of any function. Change them if your functions by 
//   coincidence return those values.
// COPY AND PASTE THOSE LINES AS NEEDED IN YOUR TEST CASES

ERROR_STRING="** ERROR **";    // Error is expected
NO_RESULT="ERR* 54290127598221040596024312840%nan *ERR"; // No result is expected
NO_ARGUMENTS="ERR* %nan22343994991579761355498794699 *ERR"; // Do NOT add parenthesis after function.

// ==================== END OF CONSTANTS ============================



// DO NOT MODIFY THIS LINE! USE THE CONSTANT ABOVE! 
//    THIS IS *NOT* NEEDED IN ANY TEST FILE.
NO_ARGUMENTS=""""+NO_ARGUMENTS+"""";
// END: DO NOT MODIFY THIS LINE!

//requireType("expectedResult",expectedResult,"string");
requireType("strFunName",strFunName,"string");

result=NO_RESULT;

strVarargin=sci2exp(varargin);

//We need only the arguments. As varargin is always given as a list, such list needs to be cut out.
// Hence the "6" in: [6:length(strVarargin)-1]
// see here
strVarargin=part(strVarargin,[6:length(strVarargin)-1]);
printf("\n-------------------------------------------------------\n");
if strVarargin==NO_ARGUMENTS then
	if expectedResult==ERROR_STRING then
	   printf("Trying erroneous: %s;\n",strFunName);
	else
	   printf("Trying valid    : %s;\n",strFunName);
	end;
	errNumber=execstr("result="+strFunName+";","errcatch","n");	
else
	if expectedResult==ERROR_STRING then
	   printf("Trying erroneous: %s(%s);\n",strFunName, strVarargin);
	else
	   printf("Trying valid    : %s(%s);\n",strFunName, strVarargin);
	end;
	errNumber=execstr("result="+strFunName+"("+strVarargin+");","errcatch","n");
end

if type(result)~=10 then
   //if we do not have a string, convert it. Otherwise, leave as is.
   //result=sci2exp(result,0);
end

if expectedResult==ERROR_STRING then
   // Error was expected
   if errNumber==0 then
      printf("\n\n");
      printf("*****************TEST CASE FAILED**********************\n");
      printf("* ERROR WAS EXPECTED, BUT NO ERROR HAS BEEN GENERATED *\n");
      printf("*******************************************************\n\n");
		if strVarargin==NO_ARGUMENTS then
      printf("  Command was: result=%s;\n\n",strFunName);
    else
      printf("  Command was: result=%s(%s);\n\n",strFunName, strVarargin);
    end
      printf("  Expected result=** ERROR **.\n");
      if result==NO_RESULT then
         printf("           result=** NO RESULT **.\n\n");
      else
         printf("           result=>>%s<<.\n\n",sci2exp(result,0));
      end
      printf("*******************************************************\n");
      printf("*        END OF TEST CASE ERROR                       *\n")
      printf("*******************************************************\n\n");
      error('TEST FAILED!');
   else
       printf(" ... PASSED. error code=%d.\n",errNumber);
   end
else
    // No error expected
   if errNumber==0 then
       if expectedResult==result then
          printf(" ... PASSED. no error.\n");
       else
        printf("\n\n");
        printf("*****************TEST CASE FAILED**********************\n");
        printf("*   EXPECTED RESULT DIFFERENT THAN FUNCTION OUTPUT    *\n");
        printf("*******************************************************\n\n");
			if strVarargin==NO_ARGUMENTS then
        printf("  Command was: result=%s;\n\n",strFunName);
      else
      	printf("  Command was: result=%s(%s);\n\n",strFunName, strVarargin);
      end
        if expectedResult==NO_RESULT then
					 printf("  Expected result=** NO RESULT **.\n");
				else
					 printf("  Expected result=>>%s<<.\n", sci2exp(expectedResult,0));
				end
        if result==NO_RESULT then
           printf("           result=** NO RESULT **.\n\n");
        else
           printf("           result=>>%s<<.\n\n",sci2exp(result,0));
        end
        printf("*******************************************************\n");
        printf("*        END OF TEST CASE ERROR                       *\n")
        printf("*******************************************************\n\n");
        error('TEST FAILED!');
       end;
   else
      printf("\n\n");
      printf("*****************TEST CASE FAILED**********************\n");
      printf("* VALID ARGUMENTS USED, BUT FUNCTION GENERATED ERROR  *\n");
      printf("*******************************************************\n\n");
		if strVarargin==NO_ARGUMENTS then
      printf("  Command was: result=%s;\n\n",strFunName);
    else
    	printf("  Command was: result=%s(%s);\n\n",strFunName, strVarargin);
    end
        if expectedResult==NO_RESULT then
					 printf("  Expected result=** NO RESULT **.\n");
				else
					 printf("  Expected result=>>%s<<.\n", sci2exp(expectedResult,0));
				end
      if result==NO_RESULT then
         printf("           result=** NO RESULT **.\n\n");
      else
         printf("           result=>>%s<<.\n\n",sci2exp(result,0));
      end
      printf("*******************************************************\n");
      printf("*        END OF TEST CASE ERROR                       *\n")
      printf("*******************************************************\n\n");
      error('TEST FAILED!');
   end
end
endfunction

function result=shortenString(str, requiredLength)
//function result=shortenString(str, requiredLength)
//  Returns a string shortened upto 'requiredLength' characters
//  The minimal allowed length is 3.
//  If given string is shorter than (or equal to ) 'requiredLength', this function returns it
//  unmodified.
//  If it is longer, the resulting string is cut and an ellipsis ('...') is appended to the end.
//  The resulting string will be exactly 'requiredLength' characters in length (i.e.
//  length(result)=requiredLength

if argn(2)~=2 then
	error("Wrong number of arguments to function. Required: 2, got "+int2str(argn(2))+".");
end

requireType("str", str, "string");
requireType("requiredLength", requiredLength, "positive integer");
requireRange("requiredLength",requiredLength,3,%inf);



if length(str)<=requiredLength then
	result=str;
else
	result=part(str, [1 : requiredLength-3])+"...";
end
endfunction
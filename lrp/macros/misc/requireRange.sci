// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
//Last revised: 2006-07-07 15:00:00
function result=requireRange(strVariableName, variable, valMin, valMax);
// result=requireRange(strVariableName, variable, valMin, valMax)
// valMin can either be:
//    - a 2 element vector=[min,max] (then valMax cannot be specified)
//    - a scalar; then valMax is also required and must be a scalar too.

   result=%f; //Set the default value
   select argn(2)
        case 3 then
              requireType("valMin",valMin,"vector");
              if and(size(valMin(:))~=[2,1]) then
                 error("Wrong size of valMin. Expected a scalar or 2 element vector");
              else
                  valMax=max(valMin);
                  valMin=min(valMin);
              end
        case 4 then
             requireType("valMin",valMin,"real");
             requireType("valMax",valMax,"real");
        else
            error("Wrong number of arguments. Expected 3 or 4.");
   end
   requireType("strVariableName",strVariableName,"string");

	if typeof(variable)=="string" then
         printf("************** requireRange :: Wrong arguments ***********************\n");
         printf("The 2nd argument of requireRange function must be an\n")
         printf("immediate variable name, NOT a string. \n");
         printf("\nCurrent value of the 2nd argument: ''%s''\n\n",variable);
         printf("Do NOT enclose the variable name in apostrophes!\n");
         printf("**********************************************************************\n");
         error("Invalid arguments");
  end

   if typeof(variable)=="constant"
      len=max(size(variable(:)))
   else
      len=length(variable)
   end

   result=%T;
   for index=1:len
       if typeof(variable(index))~="string" then
          //scalar, vector or matrix
					if type(variable)==1 then
								if or(variable(index)>valMax) then
										if len>1 then
											//error(strVariableName+"("+int2str(index)+") contains too big value for range ["+int2str(valMin)+".."+int2str(valMax)+"]");
											msg="contains too big value for range ["+int2str(valMin)+".."+int2str(valMax)+"]";
											showMessage(strVariableName+"("+int2str(index)+")", variable(index), msg);
										else
												//error(strVariableName+" is too big for range ["+int2str(valMin)+".."+int2str(valMax)+"]");
												msg="is too big for range ["+int2str(valMin)+".."+int2str(valMax)+"]";
												showMessage(strVariableName, variable, msg);
										end
								end
								if or(variable(index)<valMin) then
                		if len>1 then
				             		//error(strVariableName+"("+int2str(index)+") contains too small value for range ["+int2str(valMin)+".."+int2str(valMax)+"]");
												msg="contains too small value for range ["+int2str(valMin)+".."+int2str(valMax)+"]";
												showMessage(strVariableName+"("+int2str(index)+")", variable(index), msg);
                		else
												msg="is too small for range ["+int2str(valMin)+".."+int2str(valMax)+"]";
												showMessage(strVariableName, variable, msg);
                    		//error(strVariableName+" is too small for range ["+int2str(valMin)+".."+int2str(valMax)+"]");
                		end
             		end
          else
              result=result & requireRange(strVariableName+"("+int2str(index)+")",variable(index), valMin, valMax);
          end
       else
								error("It must not contain a string.");
			end
   end //end for
endfunction


function showMessage(strVariableName, variable, msg)
printf("************** requireRange :: showMessage() ***********************\n");
printf("Variable ''%s'' has following value",strVariableName);
disp(variable);
printf("It %s\n", msg);
printf("********************************************************************\n");
error("Check arguments.")
endfunction

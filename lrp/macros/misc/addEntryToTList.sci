// lst - typed list
// name_of_entry - to add to typed list
// value_of_entry - to add


function [lst]=addEntryToTList(lst,name_of_entry, value_of_entry)

[lhs,rhs]=argn()

if (rhs~=3) then
    error("Wrong number of parameters. The number must be three.");
end

if (lhs>1) then
    error("Wrong number of parameters. The number must be one.");
end

//if typed list
if (type(lst)~=16) then
	error("The first parameter must be typed list.");
end

//size of column must be 1
if (size(lst(1),2) ~= 1) then
    error("Wrong typed list. The first entry must be vector (row) x 1 (column).");
end

//string
if (type(name_of_entry)~=10) then
	error("Wrong type of second parameter. It must be one string.");
end

[r,c]=size(name_of_entry);
if (r~=1 | c~=1) then
	error("The string must be only one - not matrix of strings.");
end

//checking that entry 'name_of_entry' is present or not in 'lst'
if (isThisEntry(lst,name_of_entry)) then
    error("This entry already exists.");
else //not present
	lst(1)($+1)=name_of_entry;
	lst($+1)=value_of_entry;
end

endfunction

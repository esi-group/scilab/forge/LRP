// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
//Last revised: 2006-07-07 15:00:00
function result=requireValue(strVariableName, variable, listRequiredValues);
// function requireValue(strVariableName, variable, listRequiredValues);

   //===================================================
  result=%f;
  if argn(2)~=3 then
     error("Wrong number of arguments to requireValue. Expecting 3.");
  end
  requireType("strVariableName",strVariableName,["string"]);
  requireType("listRequiredValues",listRequiredValues,["list"]);
  if ~(length(listRequiredValues)==1 & isempty(listRequiredValues(1))) then
     // Time to check the values
     valueIsOk=%F;
     for index=1:lstsize(listRequiredValues)
         if variable==listRequiredValues(index) then
            valueIsOk=%T;
            break;
         end
     end
     if valueIsOk==%F then
        printf("******************************************\n");
        printf("Wrong value of "+strVariableName+". Allowed are: \n");
				if lstsize(listRequiredValues)==1 then
        	disp(listRequiredValues(1));
        else
        	disp(listRequiredValues);
        end
				if type(variable)==1 then 
        	if and(size(variable)<10) then
						printf("Current value of "+strVariableName+":\n");
						disp(variable);
					end
				else
					if type(variable)==10 then
						printf("Current value of "+strVariableName+":\n");
						disp(variable);		
					end			
        end
        error("Wrong value");
        result=%f;
     end
   end;
   //disp("Value is OK!");
   result=%t;
endfunction

// lst - typed list - we search in the first entry strings lst(1) the
//                    'entry' for example 'x0' in 'lrp'
// entry - searched entry

// info - information - if entry is then %T else %F
// nofe - number of founded entries
// poz_r - pozition in rows
// poz_c - pozition in columns

////example
//// Find position 'x0' in 'lrp' model
////=====================================
// [info, nofe, poz_r, poz_c]=isThisEntry(lrp, 'x0')
//
//or in matrix of character strings
//=====================================
// str=['w'; 'a'; 'c']
// str2=[str str]
// [info, nofe, poz_r, poz_c]=isThisEntry(str2, 'a')

function [info, nofe, poz_r, poz_c]=isThisEntry(lst, entry)

lhs=argn(1)
rhs=argn(2)

if (rhs~=2) then
    error("Wrong number of parameters. The number must be two.");
end

//if typed list
if (type(lst)==16) then
    //size of column must be 1
    if (size(lst(1),2) ~= 1) then
        error("Wrong typed list. The first entry must be vector (row) x 1 (column).");
    end

    //the first entry of typed list
    matrix_of_strings=lst(1);

//matrix of character strings
elseif (type(lst)==10) then
        //here is matrix
        matrix_of_strings=lst;
else
    error("Wrong type of the first argument. It must be ''typed list'' or ''matrix of character strings''.");
end


if (lhs==1) then
    info=or(matrix_of_strings == entry);
    return;
end

[poz_r,poz_c]=find(matrix_of_strings == entry);

if (isempty(poz_r)) then
    info=%F;
else
    info=%T;
    nofe=length(poz_r);
end
endfunction

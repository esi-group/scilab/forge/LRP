// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
//Last revised: 2006-07-10 23:38:00
//
// function requireType(strVariableName, variable, vecRequiredTypeNames, optional vecRequiredListNames);
//
// Checkes, if a variable is of a specified type. If type of variable matches the requred type
//  this function does nothing. If it does not - it displays an error
//  message (via Scilab's 'error' function).
//
// PARAMETERS:
//  strVariableName - (string) - name of variable; for display purposes
//  variable - (any type) the variable to be checked
//  vecRequiredTypeNames - (vector of strings) - the allowed types vector
//  vecRequiredListNames - [OPTIONAL] (vector of strings) - additional type information for TLIST / MLIST
//       This information is obtained by running the 'typeof(variable)'  command.
//       Note that in Scilab the TLIST/MLIST are of TWO types:
//                      - general (tlist / mlist)
//                      - user named type (as set in the first argument to the tlist/mlist).
//       To allow all cases, two arguments to requireType function are needed:
//                      - vecRequiredTypeNames (for specifing a general type, like ['tlist])
//                      - vecRequiredListNames (for specifing the user type, like ['myOwnTLIST']).
//       The argument vecRequiredListNames is optional and can be used only, if vecRequiredTypeNames
//           contains 'tlist' or 'mlist'. For other types it is meaningless.
//
//  EXAMPLE:
//  To require the variable myVar to be an integer, use
//        requireType('myVar',myVar,['integer']);
//  To require the variable myVar to be an integer OR string, use
//        requireType('myVar',myVar,['integer','string']);
//  To require the variable myVar to be a tlist (any tlist will fit))
//        requireType('myVar',myVar,['tlist']);
//  To require the variable myVar to be a tlist of type MyTLIST (created by tlist(['MyTLIST'; ....]))
//        requireType('myVar',myVar,['tlist'],['MyTLIST']);

function result=requireType(strVariableName, variable, vecRequiredTypeNames, varargin);
// function requireType(strVariableName, variable, vecRequiredTypeNames, optional vecRequiredListNames);

   //===================================================
  select argn(2)
  case 4 then
       vecRequiredListNames=varargin(1);
  case 3 then
       vecRequiredListNames=[];
  else
      error("Wrong number of parameters to function requireType. Expected 3 or 4, got: "+int2str(argn(2)));
  end;

	// First, we check the required argument of vecRequiredTypeNames
  if lcIsStringVector(vecRequiredTypeNames)==%F then
     error("Wrong argument: vecRequiredTypeNames must be a vector of strings.");
  end
  //Each element of vecRequiredTypeNames must be a string
  for index=1:size(vecRequiredTypeNames,2)
      if (lcIsString(vecRequiredTypeNames(index))~=%T) then
          printf(sprintf("Wrong argument: vecRequiredTypeNames(%d) must be a string. Got: ", index));
          disp(vecRequiredTypeNames(index));
					error("Wrong argument.");
       end
  end;

	// Then, we check the OPTIONAL argument of vecRequiredListNames
	if lcIsEmpty(vecRequiredListNames)==%F then
		// We are given a nonempty vecRequiredListNames. Let's check it!
	  if lcIsStringVector(vecRequiredListNames)==%F then
	     error("Wrong argument: vecRequiredListNames must be a vector of strings or an empty vector ([])");
	  end
	  for index=1:size(vecRequiredListNames,2)
	      if (lcIsString(vecRequiredListNames(index))~=%T) then
	          error(sprintf("Wrong argument: vecRequiredListNames(%d) must be a string.", index));
	       end
	  end;
  end


   typeIsOk=%F;
   for index=1:size(vecRequiredTypeNames,2)
		select vecRequiredTypeNames(index)
           case "string" then // a single string = string scalar
               if (typeIsOk==%F & lcIsString(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "string vector" then
               if (typeIsOk==%F & lcIsStringVector(variable)) then
                  typeIsOk=%T;
                  continue;
               end
					 case "integer" then
               if (typeIsOk==%F & lcIsIntegerScalar(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "vector" then
               if (typeIsOk==%F & lcIsVector(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "row vector" then
               if (typeIsOk==%F & lcIsRowVector(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "column vector" then
               if (typeIsOk==%F & lcIsColumnVector(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "real" then
               if (typeIsOk==%F & lcIsRealScalar(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "real vector" then
               if (typeIsOk==%F & lcIsRealVector(variable)) then
                  typeIsOk=%T;
                  continue;
               end
					 case "tlist" then
               if (typeIsOk==%F & lcIsTlist(variable)) then
                  typeIsOk=%T;
                  continue;
               end
					 case "list" then
               if (typeIsOk==%F & lcIsList(variable)) then
                  typeIsOk=%T;
                  continue;
               end
					 case "mlist" then
               if (typeIsOk==%F & lcIsMlist(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "positive integer" then
               if (typeIsOk==%F & lcIsPositiveInteger(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "integer vector" then
                if (typeIsOk==%F & lcIsIntegerVector(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "2D matrix" then
                if (typeIsOk==%F & lcIs2DMatrix(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "nD matrix" then
								// n >= 3	, for 2D matrix use "2D matrix" for generic matrix use "matrix"
                if (typeIsOk==%F & lcIsNDMatrix(variable)) then
                  typeIsOk=%T;
                  continue;
               end
					 case "empty tlist" then
               if (typeIsOk==%F & lcIsEmptyTlist(variable)) then
                  typeIsOk=%T;
                  continue;
               end
					 case "empty list" then
               if (typeIsOk==%F & lcIsEmptyList(variable)) then
                  typeIsOk=%T;
                  continue;
               end
					 case "empty mlist" then
               if (typeIsOk==%F & lcIsEmptyMlist(variable)) then
                  typeIsOk=%T;
                  continue;
               end
					 case "empty string" then
               if (typeIsOk==%F & lcIsEmptyString(variable)) then
                  typeIsOk=%T;
                  continue;
               end
					 case "any string vector" then
               // Vector of strings, possibly empty
							 if (typeIsOk==%F & lcIsAnyStringVector(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case 'two element integer vector' then
               if (typeIsOk==%F & lcIsTwoElementVector(variable) & lcIsIntegerVector(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case 'empty' then
               if (typeIsOk==%F & lcIsEmpty(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "empty string" then
               if (typeIsOk==%F & lcIsEmptyString(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "boolean" then
               if (typeIsOk==%F & lcIsBoolean(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "boolean vector" then
               if (typeIsOk==%F & lcIsBooleanVector(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "user function" then
                //compiled function
                if lcIsUserFunction(variable) then
                  typeIsOk=%T;
                  break;
               end
           case "scilab function" then
                //compiled function
                if lcIsScilabFunction(variable) then
                  typeIsOk=%T;
                  break;
               end
           case "range vector" then
                // A range in a form of [a,b], a<=b
                if lcIsRangeVector(variable) then
                  typeIsOk=%T;
                  break;
               end
           case "positive integer vector" then
                if (typeIsOk==%F & lcIsPositiveIntegerVector(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           case "CoolMatrix" then
                if (typeIsOk==%F & lcIsCoolMatrix(variable)) then
                  typeIsOk=%T;
                  continue;
               end
           else
              // We have checked everything and have not found a proper type name.
              //  This means that the user has passed a name we do not know how to check.
              //  We will therefore issue an error
							printf("\n");
							printf("************************************************\n");
							printf("*    WRONG ARGUMENT TO FUNCTION requireType!   *\n");
							printf("************************************************\n");
							printf("I do not know, how to check for:""'+vecRequiredTypeNames(index)+'""\n");
							printf("\n");
							printf("  Offending argument: vecRequiredTypeNames("+int2str(index)+")\n");
							printf("\n");
							printf("Given arguments were:\n");
							printf("\n strVariableName=""%s""\n",strVariableName);
							printf(" variable=");
              if lcIsScilabFunction(variable) then
                 printf('***SCILAB COMPILED FUNCTION***\n')
              else
							   disp(variable);
							end
							printf(' vecRequiredTypeNames=%s\n',sci2exp(vecRequiredTypeNames,0));
							printf(' vecRequiredListNames=%s\n',sci2exp(vecRequiredListNames,0));
							typeIsOk=%F; //The type cannot be OK if we do not know, how to check
							printf('************************************************\n');
							printf('*      END OF WRONG ARGUMENTS INFORMATION      *\n');
							printf('************************************************\n');
							error('Unknown type name');
              result=%F;
							return;
        end
   end
   if typeIsOk==%F then
      printf("\n**************************************************\n");
      printf("Error in requiredType:\n")
      printf("\n  "+strVariableName+" must be one of:\n\n" + sci2exp(vecRequiredTypeNames,0)+"\n");
      printf("\n  Currently it is:\n\n");
      printf(sci2exp(lcGetTypeName(variable),0));
      printf("\n  Current value of variable "+strVariableName+":\n");
      disp(variable);
      printf("\n**************************************************\n");
      error("Wrong type");
      result=%F;
      return;
   end
   //disp('Type is OK!');
   // Numerical type is OK now. Time to check the type names if we know them

   // Now things get complicated:
   // We will check the user type of tlist or mlist.
   //  By assumption if variable is not tlist or mlist then it has a proper user type
	 if length(vecRequiredListNames)>0 then
		 typeNameIsOk=%F;
		 weHaveTlistOrMlist=%F;
     for index=1:size(vecRequiredListNames,2)
			 if lcIsTlist(variable)==%T | lcIsMlist(variable)==%T
			 	  weHaveTlistOrMlist=%T;
	       if typeof(variable)==vecRequiredListNames(index) then
	          typeNameIsOk=%T;
	          break;
	       end
	      end
     end
     if (weHaveTlistOrMlist==%F) then
     	// Hack: We have not found any tlist or mlist, so we assume that
			//  we have a proper user type
     	typeNameIsOk=%T;
		 else
			 //There is at least one tlist, so we need to check the user type
			 if typeNameIsOk==%F then
          printf("\n**************************************************\n");
          printf("Error in requiredType:\n")
      printf("\n  "+strVariableName+" must be a tlist or mlist of type:\n\n%s\n", strcat(vecRequiredListNames,", "));
      printf("\n  Currently it is a tlist or mlist of type: %s\n",typeof(variable));
      printf("\n  (in other words: I got a TLIST or MLIST, which is OK, \n   but I need it to be of other subtype - see help file)\n");
      printf("\n**************************************************\n");
      error("Wrong type");
	     end
	    end
   else
       typeNameIsOk=%T;
   end
   //disp('TypeName is OK!');
   result=%T;
endfunction

function result=lcGetTypeName(variable)
	// Returns a list of all possible type names that a given variable matches.
	// Caution! For Tlist or Mlist, an additional information is returned: the user type
	//  Consult a help file before blindly using this information. You can do it by writing
	//  a Scilab command:
	//    help requireType;

	result=[];
	if lcIsString(variable)==%T then
		result=[result,"string"];
	end
	if lcIsStringVector(variable)==%T then
		result=[result,"string vector"];
	end
	if lcIsIntegerScalar(variable)==%T then
		result=[result,"integer"];
	end
	if lcIsVector(variable)==%T then
		result=[result,"vector"];
	end
	if lcIsRowVector(variable)==%T then
		result=[result,"row vector"];
	end
	if lcIsColumnVector(variable)==%T then
		result=[result,"column vector"];
	end
	if lcIsRealScalar(variable)==%T then
		result=[result,"real"];
	end
	if lcIsRealVector(variable)==%T then
		result=[result,"real vector"];
	end
	if lcIsPositiveInteger(variable)==%T then
		result=[result,"positive integer"];
	end
	if lcIsIntegerVector(variable)==%T then
		result=[result,"integer vector"];
	end
	if lcIsPositiveIntegerVector(variable)==%T then
		result=[result,"positive integer vector"];
	end
	if lcIsList(variable)==%T then
		result=[result,"list"];
	end
	if lcIsTlist(variable)==%T then
		result=[result,"tlist   (user type:" + typeof(variable)+")"];
	end
	if lcIsMlist(variable)==%T then
		result=[result,"mlist   (user type:" + typeof(variable)+")"];
	end
	if lcIs2DMatrix(variable)==%T then
		result=[result,"2D matrix"];
	end
	if lcIsNDMatrix(variable)==%T then
		result=[result,"nD matrix"]; // n>=3. For 2D use "2D matrix"
	end
	if (lcIsTwoElementVector(variable)) & (lcIsIntegerVector(variable)) then
		result=[result,"two element integer vector"];
	end
	if (lcIsEmpty(variable)==%T) then
		result=[result,"empty"];
	end
	if (lcIsEmptyList(variable)==%T) then
		result=[result,"empty list"];
	end
	if (lcIsEmptyMlist(variable)==%T) then
		result=[result,"empty mlist"];
	end
	if (lcIsEmptyTlist(variable)==%T) then
		result=[result,"empty tlist"];
	end
	if (lcIsEmptyString(variable)==%T) then
		result=[result,"empty string"];
	end
	if (lcIsAnyStringVector(variable)==%T) then
		result=[result,"any string vector"];
	end
	if (lcIsBoolean(variable)==%T) then
		result=[result,"boolean"];
	end
	if (lcIsBooleanVector(variable)==%T) then
		result=[result,"boolean vector"];
	end
	if (lcIsUserFunction(variable)==%T) then
		result=[result,"user function"];
	end
	if (lcIsScilabFunction(variable)==%T) then
		result=[result,"scilab function"];
	end
	if (lcIsRangeVector(variable)==%T) then
		result=[result,"range vector"];
	end
	if (lcIsCoolMatrix(variable)==%T) then
		result=[result,"CoolMatrix"];
	end
	if isempty(result) then
		result=["***UNKNOWN*** Type#"+int2str(type(variable))+", typeof:"+typeof(variable)];
	end
endfunction


// =========================== THE CHECKING FUNCTIONS ============================

// CAUTION: When modifying any of those functions or writing your own, REMEMBER that
//  the checking function (e.g. lcIsMY_TYPE) MUST check its argument (i.e. be prepared.for any stupid argument it might obtain)
//   you MUST NOT assume that your function has been given anything sensible. We propose using something like
//   function result=lcIsMY_TYPE(variable)
//      if (type(variable)==TYPE_NUMBER_OF_REQUESTED_VARIABLE) then
//               result=RESULT_OF_ALL_THE_CHECKS(variable);
//      else
//              result=%F; // The type we got is wrong for us
//     end
//    endfunction


	function result=lcIs2DMatrix(variable)
		if type(variable)==1 then
			if and(size(variable)~=[0,0])
				// We do not allow empty matrices ([])
				result=%T;
			else
				result=%F;
			end
		else
			result=%F;
		end
	endfunction

	function result=lcIsNDMatrix(variable)
		// by assumption n>=3; for standard 2D matrix returns false
		// to check for 2D matrix use lcIs2DMatrix(variable)
		// note that the Scilab handles 2D and nD (n>=3) matrices differently,
		// that is the main reason we write this function
		if (type(variable)==17) & (typeof(variable)=="hypermat")  then
			result=%T;
		else
			result=%F;
		end
	endfunction

   function result=lcIsBoolean(variable)
       if type(variable)==4 then
          if max(size(variable))==1 then
             result=%T;
          else
              result=%F;
          end
       else
           result=%F;
       end
   endfunction
   function result=lcIsBooleanVector(variable)
       if type(variable)==4 then
          if lcIsVector(variable) then
             result=%T;
          else
              result=%F;
          end
       else
           result=%F;
       end
   endfunction
   function result=lcIsInteger(variable)
			if type(variable)==1 then
					if int(variable)==variable then
							result=%T;
					else
							result=%F;
					end
			else
					 result=%F;
			end
   endfunction
   function result=lcIsReal(variable)
				if type(variable)==1 then
						if lcIsScalar(variable) then
							result=%T;
						else
							result=%F;
						end
				else
						result=%F;
				end
   endfunction
   function result=lcIsRealVector(variable)
				if type(variable)==1 then
					if lcIsVector(variable)==%T then
						result=%T;
					else
						result=%F
					end
				else
						result=%F;
				end
   endfunction
   function result=lcIsBooleanScalar(variable)
 				if type(variable)==4 then
						if (lcIsScalar(variable)) then
							result=%T;
					else
							result=%F;
					end
			else
				result=%F;
		end
   endfunction
   function result=lcIsRealScalar(variable)
       if (lcIsReal(variable)) & (lcIsScalar(variable)) then
          result=%T;
       else
          result=%F;
       end
   endfunction

   function result=lcIsScalar(variable)
		// Scalar is either:
		//   a number (type 1)
		//   a string (type 10)
		if (type(variable)==1) | (type(variable)==10) then
					if and(size(variable)==[1,1]) then
							result=%T;
					else
							result=%F;
					end
		else
					result=%F;
		end
   endfunction

   function result=lcIsIntegerScalar(variable)
       if (lcIsInteger(variable)) & (lcIsScalar(variable)) then
          result=%T;
       else
           result=%F;
       end
   endfunction

   function result=lcIsVector(variable)
   		if type(variable)==1 | type(variable)==10 | type(variable)==4 then
						if (min(size(variable))==1) & (ndims(variable)==2) then
								result=%T;
						else
								result=%F;
						end
	   else
       result=%F;
	   end
   endfunction

   function result=lcIsRowVector(variable)
   		if type(variable)==1 | type(variable)==10 | type(variable)==4 then
						if (size(variable,1)==1) & (ndims(variable)==2) then
								result=%T;
						else
								result=%F;
						end
	   else
       result=%F;
	   end
   endfunction

   function result=lcIsColumnVector(variable)
   		if type(variable)==1 | type(variable)==10 | type(variable)==4 then
						if (size(variable,1)==1) & (ndims(variable)==2) then
								result=%T;
						else
								result=%F;
						end
	   else
       result=%F;
	   end
   endfunction

	 function result=lcIsEmpty(variable)
		if (type(variable)==1) | (type(variable)==10) then
		 	if max(size(variable))==0 then
		 			result=%T;
		 		else
		 			result=%F;
			 end
		else
			result=%F;
		end
	 endfunction

	 function result=lcIsEmptyList(variable)
			if type(variable)==15 then
				//we have a list here
				if lstsize(variable)==0 then
					// This list is empty
					result=%T;
				else
					result=%F;
				end;
			else
				result=%F;
			end
	 endfunction

	 function result=lcIsEmptyTlist(variable)
			if type(variable)==16 then
				//we have a tlist here
				// A tlist is empty if it does not contain any data.
				// The size of tlist is always>=1 as the first elements
				//   are the field names
				if lstsize(variable)<=1 then
					result=%T; // This Tlist is empty
				else
					result=%F;
				end;
			else
				result=%F;
			end
	 endfunction

	 function result=lcIsEmptyMlist(variable)
			if type(variable)==17 then
				//we have a mlist here
				// A mlist is empty if it does not contain any data.
				// The size of mlist is always>=1 as the first elements
				//   are the field names
				if lstsize(variable)<=1 then
					result=%T;// This Tlist is empty
				else
					result=%F;
				end;
			else
				result=%F;
			end
	 endfunction

   function result=lcIsVectorOrEmpty(variable)
	   if (type(variable)==1) | (type(variable)==10) then
					if lcIsEmpty(variable) | lcIsVector(variable) then
							result=%T;
					else
							result=%F;
					end
     else
         result=%F;
	  end
   endfunction

   function result=lcIsIntegerVector(variable)
       if lcIsInteger(variable) & lcIsVector(variable) then
          result=%T;
       else
          result=%F;
       end
   endfunction

   function result=lcIsPositiveIntegerVector(variable)
       if lcIsInteger(variable) & lcIsPositive(variable) & lcIsVector(variable) then
          result=%T;
       else
          result=%F;
       end
   endfunction

   function result=lcIsPositive(variable)
      if type(variable)==1 then
				if and(variable>0) then
						result=%T;
				else
						result=%F;
				end
  	  end
   endfunction

   function result=lcIsPositiveInteger(variable)
       if (lcIsIntegerScalar(variable)) & (lcIsPositive(variable)) then
          result=%T;
       else
           result=%F;
       end
   endfunction

   function result=lcIsTwoElementVector(variable)
       if lcIsVector(variable)
          // we can use length() as it is a vector
           if length(variable)==2
              result=%T;
           else
               // not a TWO ELEMENT vector
               result=%F;
           end
       else
           // not a vector at all so cannot be "TwoElementVector"
           result=%F;
       end
   endfunction

	function result=lcIsString(variable)
		// A nonempty string
		if (type(variable)==10) then
			if (lcIsScalar(variable)==%T) then
				if length(variable)>0 then
					result=%T;
				else
					result=%F;
				end
			else
				result=%F;
			end
		else
			result=%F;
		end
	endfunction

	function result=lcIsEmptyString(variable)
		// An empty string
		if (type(variable)==10) then
			if (lcIsScalar(variable)==%T) then
				if length(variable)==0 then
					result=%T;
				else
					result=%F;
				end
			else
				result=%F;
			end
		else
			result=%F;
		end
	endfunction


  function result=lcIsStringVector(variable)
		if type(variable)==10 then
			if lcIsVector(variable)==%T then
					for index=1:max(size(variable))
						if lcIsString(variable(index))==%F then
							result=%F;
							return;
						end
					end
					result=%T;
			else
				result=%F;
			end;
		else
			result=%F;
		end
	endfunction

  function result=lcIsAnyStringVector(variable)
		if type(variable)==10 then
			if lcIsVector(variable)==%T then
				result=%T;
			else
				result=%F;
			end;
		else
			result=%F;
		end
	endfunction

	function result=lcIsList(variable)
		if type(variable)==15 then
			if lstsize(variable)>0 then
				//We allow non-empty lists only
				result=%T;
			else
				result=%F;
			end
		else
			result=%F;
		end
	endfunction

	function result=lcIsTlist(variable)
		if type(variable)==16 then
			if lstsize(variable)>1 then
				//We allow non-empty tlists only
				// Note that tlists always have at least one element (user type name
				//  and field definition) - that is why we check against one, NOT zero.
				result=%T;
			else
				result=%F;
			end;
		else
			result=%F;
		end
	endfunction

	function result=lcIsMlist(variable)
		if type(variable)==17 then
			if lstsize(variable)>1 then
				//We allow non-empty mlists only
				// Note that mlists always have at least one element (user type name
				//  and field definition) - that is why we check against one, NOT zero.
				//
				result=%T;
			else
				result=%F;
			end
		else
			result=%F;
		end
	endfunction

  function result=lcIsUserFunction(variable)
     //compiled function
     if type(variable) == 13 then
           result=%T;
     else
           result=%F;
     end
  endfunction

  function result=lcIsScilabFunction(variable)
     //compiled function
     if type(variable) == 130 then
           result=%T;
     else
           result=%F;
     end
  endfunction

  function result=lcIsRangeVector(variable)
     //compiled function
     if (lcIsRowVector(variable) == %T)  & (length(variable)==2) & (variable(1)<=variable(2)) then
					 result=%T;
     else
           result=%F;
     end
  endfunction

  function result=lcIsCoolMatrix(variable)
     //CoolMatrix
     if (typeof(variable) == "mtx") then
					 result=%T;
     else
           result=%F;
     end
  endfunction

// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
//Last revised: 2006-09-05 18:01:00
function result=requireMatrixSize(strMatrixName, aMatrix, sizeVector);
   requireType("sizeVector",sizeVector,"vector");
    if and(size(sizeVector(:))~=[2,1]) then
       error("Wrong size of sizeVector. Expected 2x1 row vector");
    end
   requireType("strMatrixName",strMatrixName,"string");
   requireType("aMatrix",aMatrix,"2D matrix");

   s=size(aMatrix);
   if or(s~=sizeVector) then
      result=%f; 
      error('Incorrect size of matrix '+strMatrixName+'. Is: '+sci2exp(s)+', should be:'+sci2exp(sizeVector));
   else
       result=%t;
   end
endfunction

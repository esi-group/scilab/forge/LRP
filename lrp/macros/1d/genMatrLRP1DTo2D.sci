// list_matrices - {     (typed list) matrices 1D --> 'matrices_LRP_1D'
//                 { or
//                 {     (typed list) LRP         --> 'lrp_model'
//
// number_of_points - on a given pass (required only when the 1-st argument is
//                    used as 'matrices_LRP_1D')
//



function [matricesLRP]=genMatrLRP1DTo2D(list_matrices, number_of_points)


// ===================================================================
// Blocks in matrices :
// "dim" means dimension of one block within the whole matrix
// ===================================================================
// PhiDim      =size(D0);
// DeltaDim    =size(D);
// ThetaDim    =size(C);
// GammaDim    =size(B0);
// SigmaDim    =size(B);
// PsiDim      =size(A);

// ===================================================================
// Matrices dimensions :
// "size" means dimension of the whole matrix
// ===================================================================
// PhiSize     =PhiDim*alpha;
// DeltaSize   =DeltaDim*alpha;
// ThetaSize   =[ThetaDim(1)*alpha ThetaDim(2)];
// GammaSize   =GammaDim*alpha;
// SigmaSize   =SigmaDim*alpha;
// PsiSize     =[PsiDim(1)*alpha PsiDim(2)];


// typed list (tlist)
type_of_list=list_matrices(1)(1);

//checking of teh typed list and then take an action
if (type_of_list=='matrices_LRP_1D') then
    m1d=list_matrices;
elseif (type_of_list=='lrp_model') then
    info=0;
    for i=2:lstsize(list_matrices)
       if (list_matrices(1)(i)=='m1d') then
         info=1;
       end
    end
    if (info==0) then
        error('LRP model has not set the sublist ''m1d''.')
    end
    m1d=list_matrices.m1d;
    number_of_points=list_matrices.alpha;
else
    error('Wrong typed list')
end


if (isempty(m1d.Phi)) then
    error('The matrix ''Phi'' is empty.');
end
if (isempty(m1d.Delta)) then
    error('The matrix ''Delta'' is empty.');
end
if (isempty(m1d.Theta)) then
    error('The matrix ''Theta'' is empty.');
end
if (isempty(m1d.Gamma)) then
    error('The matrix ''Gamma'' is empty.');
end
if (isempty(m1d.Sigma)) then
    error('The matrix ''Sigma'' is empty.');
end
if (isempty(m1d.Psi)) then
    error('The matrix ''Psi'' is empty.');
end



PhiSize=size(m1d.Phi);
PhiDim=PhiSize ./ number_of_points;
D0=m1d.Phi(1:PhiDim(1),1:PhiDim(2));

DeltaSize=size(m1d.Delta);
DeltaDim=DeltaSize ./ number_of_points;
D=m1d.Delta(1:DeltaDim(1),1:DeltaDim(2));

ThetaSize=size(m1d.Theta);
ThetaDim=ThetaSize(1) / number_of_points;    //exception of Dim
C=m1d.Theta(1:ThetaDim,:);

GammaSize=size(m1d.Gamma);
GammaDim=GammaSize ./ number_of_points;
B0=m1d.Gamma(1:GammaDim(1),1:GammaDim(2));

SigmaSize=size(m1d.Sigma);
SigmaDim=SigmaSize ./ number_of_points;
B=m1d.Sigma(1:SigmaDim(1),1:SigmaDim(2));

PsiSize=size(m1d.Psi);
PsiDim=PsiSize(1) / number_of_points;    //exception of Dim
A=m1d.Psi(1:PsiDim,:);

matricesLRP=tlist(['matrices_LRP';'A';'B';'B0';'C';'D';'D0'],A,B,B0,C,D,D0);

endfunction

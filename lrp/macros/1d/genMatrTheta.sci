function Theta=genMatrTheta(A, C, my_alpha)

//alpha = my_alpha

// ===================================================================
//  matrix THETA
// ===================================================================
//  | C             | 
//  | C*A           |
//  |  .            |
//  |  .            |
//  |  .            |
//  | C*A^(alpha-1) |

Theta = C;
for i=1:my_alpha-1
    Theta = [C; Theta*A];
end
endfunction

//example
// A=...
// [   0.32  -0.78;...
//   -0.83  -0.64];
//
// number_of_points_on_a_pass=5;
//
// Psi=genMatrPsi(A, number_of_points_on_a_pass)
//
// then you obtain
//
// Psi=...
// [   0.32  -0.78;...
//   -0.83  -0.64;...
//   0.7498  0.2496;...
//   0.2656   1.057;...
//   0.03277 -0.7446;...
//  -0.7923 -0.8836;...
//   0.6285   0.451;...
//   0.4799   1.184;...
//  -0.1732 -0.7789;...
//  -0.8288 -1.132];



function Psi=genMatrPsi(A, my_alpha)

//alpha == my_alpha == number_of_points_on_a_pass

// ===================================================================
//  matrix Psi
// ===================================================================
//  | A                     | 
//  | A^2                   |
//  |  .                    |
//  |  .                    |
//  |  .                    |
//  | A^alpha               |

Psi = A;
for i=1:my_alpha-1
    Psi = [A; Psi*A];
end
endfunction

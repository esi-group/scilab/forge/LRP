//lrp - model of lrp - typed list


function [lrp]=make1D(lrp)

[lhs,rhs]=argn();

//checking inputs
if (rhs ~= 1) then 
	error('Wrong number of inputs parameters - it must be one.');
end

//checking outputs
if (lhs>1) then
	error('Wrong number of outputs parameter. It must be only one.');
end;


//make typed list of matrices A,B,B0,C,D,D0
[matricesLRP]=makeMatricesLRP(lrp.A,lrp.B,lrp.B0,lrp.C,lrp.D,lrp.D0);


[matricesLRP_1D]=genMatrLRP1D(matricesLRP,lrp.alpha);

//insert matrices 1D in LRP model
//*******************************

//insert substructure m1d (-> Model 1D) into 'lrp'
//=================================================
lrp=insertM1DToLrp(lrp, matricesLRP_1D)

endfunction

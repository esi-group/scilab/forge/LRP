function [lrp]=boundCond_x1(lrp)
  requireType("lrp",lrp,"tlist","lrp_Sys_Cla");

// Sets the X0 to 1.
//bonduary condition
lrp.ini.x0=ones(lrp.dim.n, lrp.dim.beta);
endfunction

function [lrp]=boundCond_xsin(lrp,T,k)
  requireType("lrp",lrp,"tlist","lrp_Sys_Cla");
  if ~isdef('T') then
     T=1;
  end
  requireType("T",T,"real");
  if ~isdef('k') then
     k=1;
  end
  requireType("k",k,"real");  
  
  s=k.*sin(T*((0:lrp.dim.beta).*2*%pi)./((lrp.dim.beta))); // There must be beta+1 values
  lrp.ini.x0=s(ones(1,lrp.dim.n),1:$);
endfunction

#!/bin/sh
# Files to will be removed

find . -type f -name "*.*~" -exec rm {} \;
find . -type f -name "*.htm" -exec rm {} \;
find . -type f -name "*.html" -exec rm {} \;

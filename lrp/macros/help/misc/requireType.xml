<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<!DOCTYPE MAN SYSTEM "file://../manrev.dtd">
<MAN>
  <LANGUAGE>eng</LANGUAGE>
  <TITLE>requireType</TITLE>
  <TYPE>Scilab Function - part of the LRP Toolkit  </TYPE>
  <DATE>8-Jul-2006</DATE>
  <SHORT_DESCRIPTION name="requireType">  checks, if a variable is of a specified type</SHORT_DESCRIPTION>

  <CALLING_SEQUENCE>
  <CALLING_SEQUENCE_ITEM>result = requireType(strVariableName,variable,vecRequireTypeNames)</CALLING_SEQUENCE_ITEM>
  <CALLING_SEQUENCE_ITEM>result = requireType(strVariableName,variable,vecRequireTypeNames,vecRequiredListNames)</CALLING_SEQUENCE_ITEM>
  </CALLING_SEQUENCE>

  <PARAM>
  <PARAM_INDENT>

    <PARAM_ITEM>
    <PARAM_NAME>strVariableName</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : a character string. The name of variable, used for displaying errors.
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>

    <PARAM_ITEM>
    <PARAM_NAME>variable</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : the variable of any type that needs to be checked.
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>

    <PARAM_ITEM>
    <PARAM_NAME>vecRequiredTypeNames</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : vector of character strings listing all allowed types for <BD>variable</BD>.
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>

    <PARAM_ITEM>
    <PARAM_NAME>vecRequiredListNames</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : <EM>optional</EM> vector of character strings listing the
             second type information (see below).
             </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>

    <PARAM_ITEM>
    <PARAM_NAME>result</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : boolean value, always true (%T) as this produces error message
             if variable is not of the required type.
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>
  </PARAM_INDENT>
  </PARAM>
  <DESCRIPTION>
     <DESCRIPTION_INDENT>
     <DESCRIPTION_ITEM>
     <P>
      Checks, if a variable is of a specified type. If type of variable matches the requred type
  this function does nothing. If it does not - it displays an error
  message (via Scilab's <LINK> error</LINK> function).
     </P>
        <P>
      The function requires that the type of the variable is <EM>any of</EM> the given types
            i.e. if only one type matches the function returns successfully. If you need to
            enforce that the <BD>variable</BD> is of two types at the same type, use this function twice.
        </P>
        <P><BD>Working with tlist or mlist types.</BD>
        </P>
        <P>
        Additional care needs to be taken when working with
            <EM> tlist</EM> and <EM> mlist</EM> variables. Those two Scilab datatypes
            have <EM>two</EM> types:
     </P>
      <ITEMIZE label="Types of lists">
                <ITEM label="general type"><PP>either <TT>tlist</TT> or <TT>mlist</TT>
                                If you need to check only the <EM>general type</EM>, add its name to
                the <BD>vecRequiredTypeNames</BD> vector as <TT>'tlist'</TT> or <TT>'mlist'</TT>.
                </PP></ITEM>
                <ITEM label="user type"><PP>given as the first element of
                                                                the argument for functions <EM> tlist</EM> and <EM> mlist</EM>.
                                                                Note that this check is much more specific, as
                                                                all the variables for which the <EM>user type</EM>
                                                                can be defined must have a <EM>general type</EM>
                                                                (If you declare a <LINK>tlist</LINK>
                                                                of type <EM>MY_TYPE</EM> it will have the <EM>general type</EM>
                                                                of <EM>tlist</EM> and <EM>user type</EM> of <EM>MY_TYPE</EM>.) </PP>

                                                                <PP>If you need to check for this type you need first to
                                                                check the <EM>general type</EM>, by adding its name to the
                                                                <BD>vecRequiredTypeNames</BD> vector. The name of the
                                                                <EM>user type</EM> is given as the optional
                <BD>vecRequiredListNames</BD> parameter. </PP><PP>Obviously <BD>vecRequiredListNames</BD>
                is used for <LINK> tlist</LINK> or <LINK> mlist</LINK> only. It is meaningless
                for any other type and should not be given for such case.
                </PP></ITEM>
                </ITEMIZE>
</DESCRIPTION_ITEM>
     <DESCRIPTION_ITEM>
         <P>
            <BD>Known types</BD>
         </P>
         <P>
         You may safely check the variable of any imaginable type against anything.
         If the check is successful, <BD>requireType</BD> function returns true (<TT>%T</TT>).
         If it is unsuccessful, the function will generate an error.
         This function recognizes the following type names - only those names can be
         used in the <BD>vecRequiredTypeNames</BD> argument.
     </P>
         <ITEMIZE label="Type names">
           <ITEM label="string"><SP> A single, nonempty string. To include an empty string
                                                                 use <TT>["string","empty string"]</TT>.</SP></ITEM>
           <ITEM label="empty string"><SP> A single, empty string. An empty string is a
                                                                            string of length zero, i.e
                                                                            <TT>length(</TT><BD>variable</BD><TT>)>0</TT>.</SP></ITEM>
           <ITEM label="string vector"><SP> A vector of nonempty strings - no string in the vector can
                                                                                be of zero length.
                                                                                Note that a single <TT>"string"</TT> is a one element vector
                                                                                To allow empty strings in your vector, use
                                                                                <TT>"any string vector"</TT>. Note that it is
                                                                                not necessary to include a <TT>"string vector"</TT>
                                                                                in such case.</SP></ITEM>
           <ITEM label="any string vector"><SP> A vector of strings. Some or all of those strings
                                                                                    can be empty. If you want to disallow empty strings in
                                                                                     the vector, use <TT>"string vector"</TT>.
                                                                                     <TT>"any string vector"</TT> includes
                                                                                     <TT>"string vector"</TT>.
                                                                                     </SP></ITEM>
           <ITEM label="vector"><SP> A nonempty vector of numbers, booleans or strings.</SP></ITEM>
           <ITEM label="row vector"><SP> A nonempty row (<TT>[1xn]</TT>) vector of numbers, booleans or strings.</SP></ITEM>
           <ITEM label="column vector"><SP> A nonempty column (<TT>[nx1]</TT>) vector of numbers, booleans or strings.</SP></ITEM>
           <ITEM label="integer"><SP> A single integer.</SP></ITEM>
                     <ITEM label="positive integer"><SP> A single integer greater than zero (<BD>variable</BD> must be >0).</SP></ITEM>
                     <ITEM label="integer vector"><SP> A nonempty vector consisting of integer values only. If you
                                                                                want to also allow an empty vector, use
                                                                                 <TT>["integer vector","empty"]</TT>.</SP></ITEM>
                     <ITEM label="positive integer vector"><SP> A nonempty vector consisting of integer values that are greater than zero only. If you
                                                                                want to also allow an empty vector, use
                                                                                 <TT>["positive integer vector","empty"]</TT>.</SP></ITEM>
                     <ITEM label="two element integer vector"><SP> A two element vector of integers. Particulary
                                                                                                              useful when you want to enforce a range ([min,max])
                                                           See also <BD> requireRange</BD> LRP Toolkit's function.
                                                             </SP></ITEM>
           <ITEM label="real"><SP> A single real number. Includes <TT>"integer"</TT>.</SP></ITEM>
           <ITEM label="real vector"><SP> A vector of real numbers. Includes a <TT>"real"</TT>.</SP></ITEM>
           <ITEM label="boolean"><SP> A single <TT>boolean</TT> type value.</SP></ITEM>
           <ITEM label="boolean vector"><SP> A nonempty vector of <TT>boolean</TT> type values.</SP></ITEM>

           <ITEM label="tlist"><SP> A tlist. Read the description above if you want to work with those.</SP></ITEM>
           <ITEM label="list"><SP> A nonempty list. To check for any list (including empty ones)
                                                    use <TT>["list","empty list"]</TT> types.</SP></ITEM>
           <ITEM label="empty list"><SP> An empty list that does not contain any elements.
                                                            To check for any list (including nonempty ones)
                                                        use <TT>["list","empty list"]</TT> types.</SP></ITEM>
           <ITEM label="mlist"><SP> A nonempty mlist. A mlist is empty if it has only field definition
                                                      and no data has been entered. For such mlist <TT>lstsize(</TT><BD>variable</BD><TT>)</TT>=1.
                                                   Read the description above if
                                                   you want to work with those. </SP></ITEM>
           <ITEM label="empty mlist"><SP> An empty mlist. Read the description above if
                                                                        you want to work with those.
                                                                      To check for any mlist (including nonempty ones)
                                                                  use <TT>["mlist","empty mlist"]</TT> types.</SP></ITEM>
           <ITEM label="tlist"><SP> A nonempty tlist. A tlist is empty if it has only field definition
                                                      and no data has been entered. For such tlist
                                                      <TT>lstsize(</TT><BD>variable</BD><TT>)</TT>=1.
                                                   Read the description above if you want to work with those. </SP></ITEM>
                 <ITEM label="empty tlist"><SP> An empty <TT>tlist</TT>. Read the description above if you want to work with those.
                                                                            To check for any <TT>tlist</TT> (including nonempty ones)
                                                                        use ["tlist","empty tlist"] types.</SP></ITEM>
           <ITEM label="2D matrix"><SP> A classic matrix, vector or a single number. CAUTION: does NOT
                                                                        accept multidimensional matrices ("hypermat"s).
                                                                         Will not accept the <BD>variable</BD> if <TT>ndims(</TT><BD>variable</BD><TT>)>2</TT>. For such
                                                                        cases use <TT>"nD matrix"</TT>.
                                                                     </SP></ITEM>
           <ITEM label="nD matrix"><SP> <EM>(n>=3)</EM> A multidimensional matrix ("hypermat").
                                                                    CAUTION: will NOT accept 2D matrices, vectors
                                                                    or single numbers. It covers only the
                                                                    cases for which <TT>ndims(</TT><BD>variable</BD><TT>)>2</TT>.
                                                                    For classic matrices uses <TT>"2D matrix"</TT>. For
                                                                    vectors use <TT>"vector"</TT>.
                                                                     </SP></ITEM>
           <ITEM label="empty"><SP> An empty vector. Will not accept <TT>list</TT>, <TT>mlist</TT> or
                                                            <TT>tlist</TT>. For such cases use: <TT>"empty list"</TT>, <TT>"empty mlist"</TT>
                                                              or <TT>"empty tlist"</TT> respectively.
                                                             </SP></ITEM>
           <ITEM label="user function"><SP> A name of the user defined function. Internally can be viewed as a pointer to the user function.
                                          To allow using also the Scilab function, use <TT>["user function", "scilab function]"</TT>.
                                          </SP></ITEM>
           <ITEM label="scilab function"><SP> A name of the Scilab defined function. Internally can be viewed as a pointer to the Scilab function.
                                          To allow using also the user function, use <TT>["user function", "scilab function]"</TT>.
                                          Note that Scilab is not capitalised in the type name (it is <TT>"scilab function"</TT>, NOT <TT>"Scilab function"</TT>.)
           </SP></ITEM>
           <ITEM label="range vector"><SP> A valid range, i.e. a two-element row vector, which 2nd element is greater or equal to the first.
           </SP></ITEM>
           <ITEM label="CoolMatrix"><SP> A CoolMatrix matrix (mlist, subtype name "mtx").
           </SP></ITEM>
           </ITEMIZE>
         <P>
            <BD>Important note</BD>
         </P>
         <P>
                Pay attention to character sizes - you must use exactly the type name given above.
                This means that "string" will work, but "String", "STRING" or "StRinG" will *NOT*.
                If <BD>vecRequiredListNames</BD> will contain ANY argument that is NOT on the above list,
                this function will generate a "unknown type name" error.
         </P>
     </DESCRIPTION_ITEM>
     </DESCRIPTION_INDENT>
  </DESCRIPTION>
    <SECTION label="Safety warning">
    <P>
      This function gives an error message if the requirements given above are not met.
      In such case it returns a false boolean value but such value will be available
      only when the function is called from inside a try ... catch block or via the Scilab function
      <LINK> execstr</LINK>  with the 'errcatch' flag set.
    </P>
    </SECTION>
 <SECTION label="Examples">
<!-- ======================================================================= -->
    <P><BD>
       Example 1.
    </BD></P><P>
       A simple type check.
    </P><VERBATIM>

        str="this is a string";
        requireType("str",str,["string"]);
    </VERBATIM>
    <P><EM> Result: </EM>
            CHECK PASSED, str IS a non-empty string.
    </P>
<!-- ======================================================================= -->
    <P><BD>
       Example 2.
    </BD></P><P>
       A simple type check - this time failed.
    </P><VERBATIM>

        strEmpty="";
        requireType("strEmpty",strEmpty,["string"]);
    </VERBATIM>
    <P><EM> Result: </EM>
          CHECK FAILED, <BD> strEmpty</BD> is an empty string, such strings are NOT
          permitted, see <EM>Known types</EM> section above.
    </P>
<!-- ======================================================================= -->
    <P><BD>
       Example 3.
    </BD></P><P>
       Two allowed types.
    </P><VERBATIM>

        strEmpty="";
        requireType("strEmpty",strEmpty,["string","empty string"]);
    </VERBATIM>
    <P><EM> Result: </EM>
      CHECK PASSED, <BD> strEmpty</BD> must be either:
   </P>
    <ITEMIZE label="possible choices">
       <ITEM> <PP> a non-empty string, </PP></ITEM>
       <ITEM> <PP> an empty string. </PP></ITEM>
    </ITEMIZE>
  <P>
 Since <BD> strEmpty</BD> is an empty string, the check is passed.
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 4.
    </BD></P><P>
        A simple failed check.
    </P><VERBATIM>

         str="A sample string";
         requireType("str",str,["empty string"]);
    </VERBATIM>
    <P><EM> Result: </EM>
     CHECK FAILED, <BD> str</BD> must be an empty string.
     Since <BD> str</BD> is a non-empty string, the check has failed.
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 5.
    </BD></P><P>
        A check for totally different type.
    </P><VERBATIM>

              str="A sample string";
              requireType("str",str,["integer"]);
    </VERBATIM>
    <P><EM> Result: </EM>
       CHECK FAILED, str IS *NOT* an integer.
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 6.
    </BD></P><P>
         Mixing different types.
    </P><VERBATIM>

        str="A sample string";
        requireType("str",str,["integer","string"]);
    </VERBATIM>
    <P><EM> Result: </EM>
    CHECK PASSED, str must be either:</P>
    <ITEMIZE label="possible choices">
       <ITEM> <PP> an integer, </PP></ITEM>
       <ITEM> <PP> a non-empty string. </PP></ITEM>
    </ITEMIZE>
    <P>
    Since str is a string, the check is passed.
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 7.
    </BD></P><P>
       A simple <BD> tlist</BD> check.
    </P><VERBATIM>

        myTlist = tlist(["MY_TYPE";"field"],"Test");
        requireType("myTlist",myTlist,["tlist"]);
    </VERBATIM>
    <P><EM> Result: </EM>
      CHECK PASSED, myTlist IS a tlist (of any user type).
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 8.
    </BD></P><P>
        A simple <BD> tlist</BD> check with an additional user type constrain.
    </P><VERBATIM>

        myTlist = tlist(["MY_TYPE";"field"],"Test");
        requireType("myTlist",myTlist,["tlist"],["MY_TYPE"]);
    </VERBATIM>
    <P><EM> Result: </EM>
            CHECK PASSED, myTlist is a tlist, of user type <TT>MY_TYPE</TT> .
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 9.
    </BD></P><P>
        A simple <BD> tlist</BD> check with an additional user type constrain.
    </P><VERBATIM>

        myTlist = tlist(["MY_TYPE";"field"],"Test");
        requireType("myTlist",myTlist,["tlist"],["OTHER_TYPE"]);
    </VERBATIM>
    <P><EM> Result: </EM>
            CHECK FAILED, myTlist is a tlist, but the user type does not fit.
            <BD> myTlist</BD> is of type <TT>MY_TYPE</TT> but the function
            requires a tlist of type <TT> OTHER_TYPE</TT>.
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 10.
    </BD></P><P>
         Combining multiple checks - require a row vector of integer values.
    </P>
    <P> In this scenerio, user requires a row vector that contains only integer
    values. Such constrain does not exist in the <BD> requireType</BD> function,
    but can be specified as conjunction (ligical AND) of two basic types:
    </P>
    <ITEMIZE label="possible choices">
       <ITEM> <PP> an integer vector, </PP></ITEM>
       <ITEM> <PP> a row vector. </PP></ITEM>
    </ITEMIZE>
    <VERBATIM>

       rowVector=[1,2,3,4];
       requireType("rowVector",rowVector,["integer vector"]);
       requireType("rowVector",rowVector,["row vector"]);
    </VERBATIM>
    <P><EM> Result: </EM>
       CHECK PASSED.
 First, we check if it is an integer vector (row or column one; does
    not matter for now)...
 Then we enforce it to be a row vector. We are not concerned with it being
   an INTEGER vector, as we have already checked it.
    </P>
    <P>
       This simple trick can be used for combining other requirements as well.
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 11
    </BD></P><P>
       Potential pitfall of Example 10.
    </P>
    <P>
       It is easy to confuse the "combining multiple checks" of Example 10 with
       doing it with a single command. The result is obviously very different.
    </P><VERBATIM>

       rowVector=[1,2,3,4];
       requireType("rowVector",rowVector,["integer vector","row vector"]);
    </VERBATIM>
    <P><EM> Result: </EM>
    CHECK PASSED. This time of a totally different reason: This instruction
    checks if <BD> rowVector</BD> is ANY OF </P>
    <ITEMIZE label="possible choices">
       <ITEM> <PP> an integer vector, </PP></ITEM>
       <ITEM> <PP> a row vector. </PP></ITEM>
    </ITEMIZE>
    <P>
    This means that if we declare the <BD> rowVector </BD> variable like
    </P>
    <VERBATIM>

       rowVector=["Some", "vector", "of", "strings"];
    </VERBATIM>
    <P>
      it will be passed as well, as it IS a row vector, after all.
    </P>
</SECTION>
   <SECTION label="Common mistakes">
  <P>
     Pay attention to cases. The names of the types must be entered EXACTLY as
     written in this help. You cannot use e.g.
  </P>
  <VERBATIM>

      myTlist = tlist(["MY_TYPE";"field"],"Test");
      requireType("myTlist",myTlist,["TLIST"]);
  </VERBATIM>
  <P>
     as type <TT> TLIST</TT> does not exist - it has been probably confused with
     <TT> tlist</TT>. Note that <TT> tlist</TT> type name is very different to
     <TT> TLIST</TT>, <TT> TliST</TT> or <TT> Tlist</TT>.
  </P>
  <P><EM> Result: </EM> **WRONG CODE**</P>
  <P>
    The programmer probably intended something like
  </P>
  <VERBATIM>

      myTlist = tlist(["MY_TYPE";"field"],"Test");
      requireType("myTlist",myTlist,["tlist"]);
  </VERBATIM>
  <P>
   (note the case in "tlist"). This a a proper way of calling
    this function.
   </P>
  <P><EM> Result: </EM> CHECK PASSED.</P>
  </SECTION>
  <SECTION label="Related functions">
  <ITEMIZE label="Related functions list">
    <ITEM label="requireValue"> <PP> - the <LINK> requireValue</LINK> function is used
          when you need to enforce a variable to consist solenly of specified discrete values</PP></ITEM>
    <ITEM label="requireRange"> <PP> - the <LINK> requireRange</LINK> function is used when you need to
    enforce all variable elements to be in specified range</PP></ITEM>
  </ITEMIZE>
  </SECTION>
  <SEE_ALSO>
    <SEE_ALSO_ITEM> <LINK> try</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> execstr</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> error</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> type</LINK> </SEE_ALSO_ITEM>
      <SEE_ALSO_ITEM> <LINK> typeof</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> tlist</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> mlist</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> requireValue</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> requireRange</LINK> </SEE_ALSO_ITEM>
  </SEE_ALSO>

  <AUTHORS>
    <AUTHORS_ITEM label='Lukasz Hladowski'>
    ISSI, UZ, Poland. L.Hladowski@issi.uz.zgora.pl
    </AUTHORS_ITEM>
  </AUTHORS>
</MAN>

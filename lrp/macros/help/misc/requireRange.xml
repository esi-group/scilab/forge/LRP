<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<!DOCTYPE MAN SYSTEM "file://../manrev.dtd">
<MAN>
  <LANGUAGE>eng</LANGUAGE>
  <TITLE>requireRange</TITLE>
  <TYPE>Scilab Function  </TYPE>
  <DATE>12-Jul-2006</DATE>
  <SHORT_DESCRIPTION name="requireRange">  checks, if all elements of a  variable are within required range</SHORT_DESCRIPTION>

  <CALLING_SEQUENCE>
  <CALLING_SEQUENCE_ITEM>result = requireRange(strVariableName,variable,valMin,valMax)</CALLING_SEQUENCE_ITEM>
  <CALLING_SEQUENCE_ITEM>result = requireRange(strVariableName,variable,valMin)</CALLING_SEQUENCE_ITEM>
  </CALLING_SEQUENCE>

  <PARAM>
  <PARAM_INDENT>

    <PARAM_ITEM>
    <PARAM_NAME>strVariableName</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : a character string. The name of variable, used for displaying errors
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>

    <PARAM_ITEM>
    <PARAM_NAME>variable</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : the variable of any type that needs to be checked
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>

    <PARAM_ITEM>
    <PARAM_NAME>valMin</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : real scalar, minimum allowed value
       </SP>
       <SP>
       : two element vector of real values - minimum and maximum allowed values
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>

    <PARAM_ITEM>
    <PARAM_NAME>valMax</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : real scalar, maximum allowed value. You can give this argument only, if <BD> valMin</BD> is also a scalar.
       If <BD> valMin</BD> is a vector, this argument must be omitted, see the calling sequence above
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>

    <PARAM_ITEM>
    <PARAM_NAME>result</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : true (<TT>%t</TT>)if all the <BD> variable</BD> elements are within a specified range,
       false (<TT>%f</TT>) otherwise. Moreover if the result is false, the function generates an error message
       via the Scilab's <LINK> error</LINK> message
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>
  </PARAM_INDENT>
  </PARAM>
  <DESCRIPTION>
     <DESCRIPTION_INDENT>
     <DESCRIPTION_ITEM>
     <P>
      Checks, if all elements of <BD> variable</BD> are within specified bounds.
      The bounds are specified either as
      </P>
      <ITEMIZE label="Bounds specification">
      <ITEM label="valMin vector"> <PP>- this two element vector of real values
            (i.e. <TT> valMin=[minValue,maxValue]</TT>])contains the upper and lower bounds.
      The first element of <BD> valMin</BD> vector is the lower bound, the second - the upper bound.</PP></ITEM>
      <ITEM label="valMin and valMax arguments"><PP> - the valMin and valMax arguments are each a single real number.
      The value of <BD> valMin</BD> is a lower bound, <BD> valMax</BD> is an upper bound </PP></ITEM>
      </ITEMIZE>
      <P>
      If it is true, then
  this function does nothing and returns a true (<TT>%t</TT>) value.
  If any element of <BD> variable</BD> contains anything that is not an element of
  <BD> listRequiredValues</BD> - this function returns false (<TT>%f</TT>) and displays an error
  message (via Scilab's <LINK> error</LINK> function).
     </P>
     </DESCRIPTION_ITEM>
     </DESCRIPTION_INDENT>
  </DESCRIPTION>
    <SECTION label="Safety warning">
    <P>
        This function gives an error message if the requirements given as parameters are not met.
      In such case it returns a false boolean value but such value will be available
      only when the function is called from inside a try ... catch block or via the Scilab function
      execstr  with the 'errcatch' flag set.
    </P>
    </SECTION>
<SECTION label="Example">
<!-- ======================================================================= -->
    <P><BD>
        Example 1.
    </BD></P><P>
    A simple range test.
    </P><VERBATIM>

     val=10;
     requireRange("val",val,5,20);
    </VERBATIM>
    <P><EM> Result: </EM>
      PASSED - val&gt;=5 and val&lt;=20
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 2.
    </BD></P><P>
    The same as Example 1, but using the vector notation.
    </P><VERBATIM>

     val=10;
     requireRange("val",val,[5,20]);
    </VERBATIM>
    <P><EM> Result: </EM>
       PASSED - val&gt;=5 and val&lt;=20
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 3.
    </BD></P><P>
    Require that val&gt;=5.
    </P><VERBATIM>

     val=10;
     requireRange("val",val,5,%inf);
    </VERBATIM>
    <P><EM> Result: </EM>
       PASSED, as val&gt;=5. Note that the 2nd test (val&lt;=%inf) is always true as no
       variable can have value greater than infinity.
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 4.
    </BD></P><P>
     Require that val&lt;7.
    </P><VERBATIM>

     val=10;
     requireRange("val",val,[-%inf,7]);
    </VERBATIM>
    <P><EM> Result: </EM>
      FAILED, as the requirement that val&lt;=7 is NOT fulfilled. Note that the 1st test (val&gt;=-%inf) is always true as no
      variable can have value lesser than minus infinity. This test would have passed if
      <BD> val</BD> had been equal to e.g. 5.
    </P>
<!-- ======================================================================= -->
    <P><BD>
        Example 5.
    </BD></P><P>
      Require that val&lt;7 - the same as Example 4, only using the double argument notation.
    </P><VERBATIM>

     requireRange("val",val,-%inf,7);
    </VERBATIM>
    <P><EM> Result: </EM>
    FAILED, This is exactly the same as Example 4, only using the double argument notation.
    </P>
</SECTION>
  <SECTION label="Related functions">
  <ITEMIZE label="Related functions list">
    <ITEM label="requireType"> <PP> - the <LINK> requireType</LINK> function is used
          when you need to enforce a variable to be of particular type</PP></ITEM>
    <ITEM label="requireValue"> <PP> - the <LINK> requireValue</LINK> function is used
          when you need to enforce a variable to consist solenly of specified discrete values</PP></ITEM>
  </ITEMIZE>
  </SECTION>
  <SEE_ALSO>
    <SEE_ALSO_ITEM> <LINK> error</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> try</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> execstr</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> requireType</LINK> </SEE_ALSO_ITEM>
    <SEE_ALSO_ITEM> <LINK> requireValue</LINK> </SEE_ALSO_ITEM>
  </SEE_ALSO>

  <AUTHORS>
    <AUTHORS_ITEM label='Lukasz Hladowski'>
    ISSI, UZ, Poland, e-mail: l.hladowski@issi.uz.zgora.pl
    </AUTHORS_ITEM>
  </AUTHORS>
</MAN>

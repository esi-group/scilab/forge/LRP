<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<!DOCTYPE MAN SYSTEM "file://../manrev.dtd">
<MAN>
  <LANGUAGE>eng</LANGUAGE>
  <TITLE>genRandLRP</TITLE>
  <TYPE>Scilab Function  </TYPE>
  <DATE>06-Feb-2007</DATE>
  <SHORT_DESCRIPTION name="genRandLRP">  Generates a random LRP system</SHORT_DESCRIPTION>

  <CALLING_SEQUENCE>
  <CALLING_SEQUENCE_ITEM>lrp = genRandLRP()</CALLING_SEQUENCE_ITEM>
	<CALLING_SEQUENCE_ITEM>lrp = genRandLRP(n, m, r, numberOfPasses, numberOfPoints, rangeForValues)</CALLING_SEQUENCE_ITEM>  
  </CALLING_SEQUENCE>

  <PARAM>
  <PARAM_INDENT>

    <PARAM_ITEM>
    <PARAM_NAME>n</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : number of states
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>
    <PARAM_ITEM>
    <PARAM_NAME>m</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : number of outputs
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>
    <PARAM_ITEM>
    <PARAM_NAME>r</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : number of inputs
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>
    <PARAM_ITEM>
    <PARAM_NAME>numberOfPasses</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : number of passes to simulate, often called 'beta'
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>
    <PARAM_ITEM>
    <PARAM_NAME>numberOfPoints</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : the pass length, often called 'alpha'
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>
    <PARAM_ITEM>
    <PARAM_NAME>rangeForValues</PARAM_NAME>
    <PARAM_DESCRIPTION>
       <SP>
       : the range of random values for all matrices
       </SP>
    </PARAM_DESCRIPTION>
    </PARAM_ITEM>

  </PARAM_INDENT>
  </PARAM>
  <DESCRIPTION>
     <DESCRIPTION_INDENT>
     <DESCRIPTION_ITEM>
     <P>
			This function is used to create a random, example LRP structure. 
			It is useful for checking the other functions quickly.
		  </P>
			<P>
			This function creates the random LRP structure, which size is given as arguments to 
			this function. Note that the boundary conditions are set as follows:</P>
			<ITEMIZE label="boundary conditions">
				<ITEM label="x(k,0)"><PP> equal to zero for all passes k</PP></ITEM>
			  <ITEM label="y(0,p)"><PP> equal to one, for all points p</PP></ITEM>				
			</ITEMIZE><P>
			</P> 
      </DESCRIPTION_ITEM>
     <DESCRIPTION_ITEM>
         <P>
            <BD>Calling</BD>
         </P>
         <P>
         	This function can be called in two ways:
         </P><ITEMIZE label="calling">
				   <ITEM label="without arguments">
					 	 <PP>
					   	If the function is created without arguments, it behaves as if it has
					   	been called with the following arguments:
					   		<VERBATIM>
								 n=2; // 2 states
								m=3; // 3 outputs
								r=4; // 4 inputs
								numberOfPasses=20; // beta =20 passes
								numberOfPoints=10; // alpha=10 points
								rangeForValues=[-1 1]; // All matrices from -1 to 1
								</VERBATIM>
					 	 </PP>
					 </ITEM>
				   <ITEM label="with four arguments"><PP>
					 	This method allows to specify the dimensions for the
					 	LRP structure.
					 </PP></ITEM>
         </ITEMIZE>
		 </DESCRIPTION_ITEM>
      </DESCRIPTION_INDENT>
  </DESCRIPTION>
<SECTION label="Example">
<!-- ======================================================================= -->
    <P><BD>
        Example 1 - calling without arguments.
    </BD></P><P>
    A working example illustrating the use of the createRandLRP function without arguments.
    First, it creates the random LRP structure, then it shows the X0 boundary condition.
    Note that the resulting lrp.mat.* matrices will be different each time
    this function is called.
    </P><VERBATIM>
			lrp=genRandLRP();
			disp(lrp.ini.x0);
    </VERBATIM>
    <P><EM> Result: </EM>
       A valid lrp structure is created and then the following 
			 10-element vector of ones will be printed on screen:
			 <VERBATIM>
			    [1 1 1 1 1 1 1 1 1 1]
			 </VERBATIM>
    </P>
    <P><BD>
        Example 2 - calling with arguments.
    </BD></P><P>
    A working example illustrating the use of the createRandLRP function with arguments.
    The structure will have 1 state, input and output, 10 passes and 5 points on each pass.
		All lrp.mat.* matrices will be random from 0 to 7. 
    </P><VERBATIM>
			lrp=genRandLRP(1, 1, 1, 10, 5, [0 7]);
    </VERBATIM>
    <P><EM> Result: </EM>
       A valid LRP structure. 
    </P>
</SECTION>
  <SECTION label="Related functions">
  <ITEMIZE label="Related functions list">
    <ITEM label="checkLRP"> <PP> - the <LINK> checkLRP</LINK> function is used
          to check the LRP structure for inconsistency.</PP></ITEM>
  </ITEMIZE>
  </SECTION>
  <SEE_ALSO>
    <SEE_ALSO_ITEM> <LINK> checkLRP</LINK> </SEE_ALSO_ITEM>
  </SEE_ALSO>

  <AUTHORS>
    <AUTHORS_ITEM label='Lukasz Hladowski'>
    ISSI, UZ, Poland, e-mail: l.hladowski@issi.uz.zgora.pl
    </AUTHORS_ITEM>
  </AUTHORS>
</MAN>

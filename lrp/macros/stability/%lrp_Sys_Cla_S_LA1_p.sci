// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 21:00:00
// 
// Display function for the asymptotic stability
function []=%lrp_Sys_Cla_S_LA1_p(var)
printf('  Asymptotic stability, calculated by the LMI\n');
select var.solutionExists
	case 1 then 
		printf('System is asymptotically stable (as calculated via '+var.functionName+')\n');
		printf('Result:\n');
		printf('Q=');
		disp(var.Q);
	case 2 then
		printf('Test +'+var.functionName+' is inconclusive; system is probably asymptotically unstable\n');
	else
		warning(sprintf('\nvar.solutionExists=%d not appliciable for this stability test\n See function []=%%lrp_Sys_Cla_S_LA1_p(var)\n\n',var.solutionExists));
		error('Wrong value')
end
endfunction

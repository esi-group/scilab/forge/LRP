// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 21:00:00
// 
// Display function for the practical stability
function []=%lrp_Sys_Cla_S_P1_p(var)
printf('  Practical stability, calculated by the spectral value test\n');
select var.solutionExists
	case 0 then
		printf('System is UNSTABLE in the practical sense (as calculated via '+var.functionName+')\n');
		printf('Eigenvalues of the A matrix:\n');
		disp(var.eigenValuesA);
		printf('Eigenvalues of the D0 matrix:\n');
		disp(var.eigenValuesD0);
		if var.lambdaMaxA>1 then
			isAStable="UNSTABLE";
		else
			isAStable="stable";
		end
		if var.lambdaMaxD0>1 then
			isD0Stable="UNSTABLE";
		else
			isD0Stable="stable";
		end
		printf('Maximum absolute value of eigenvalues of  A:%f\t%s\n',var.lambdaMaxA,isAStable);
  	printf('Maximum absolute value of eigenvalues of D0:%f\t%s\n',var.lambdaMaxD0,isD0Stable);		
	case 1 then 
		printf('System is stable in the practical sense (as calculated via '+var.functionName+')\n');
		printf('Eigenvalues of the A matrix:\n');
		disp(var.eigenValuesA);
		printf('Eigenvalues of the D0 matrix:\n');
		disp(var.eigenValuesD0);
		if var.lambdaMaxA>1 then
			isAStable="UNSTABLE";
		else
			isAStable="stable";
		end
		if (var.lambdaMaxD0>1) then
			isD0Stable="UNSTABLE";
		else
			//CAUTION - DO NOT REMOVE THE EMPTY LINE BELOW - IT *IS* IMPORTANT!
			//UWAGA! - NIE USUWAĆ PUSTEJ LINII PONIŻEJ - ONA *JEST* WAŻNA!
			isD0Stable="stable";

		end
		printf('Maximum absolute value of eigenvalues of  A:%f\t%s\n',var.lambdaMaxA,isAStable);
  	printf('Maximum absolute value of eigenvalues of D0:%f\t%s\n',var.lambdaMaxD0,isD0Stable);		
	else
		warning(sprintf('\nvar.solutionExists=%d not appliciable for this stability test\n See function []=%%lrp_Sys_Cla_S_P1_p(var)\n\n',var.solutionExists));
		error('Wrong value')
end
endfunction

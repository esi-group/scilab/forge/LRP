// Author Lukasz Hladowski
// e-mail: L.Hladowski@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 21:00:00
// 
// Display function for the stability along the pass
function []=%lrp_Sys_Cla_S_LP1_p(var)
printf('  Practical stability, calculated by the LMI\n');
select var.solutionExists
	case 1 then 
		printf('System is stable in the practical sense (as calculated via '+var.functionName+')\n');
		printf('Results:\n');
		printf('P=');
		disp(var.P);
		printf('Q=');
		disp(var.Q);
	case 2 then
		printf('Test +'+var.functionName+' is inconclusive; system is probably unstable in the practical sense\n');
	else
		warning(sprintf('\nvar.solutionExists=%d not appliciable for this stability test\n See function []=%%lrp_Sys_Cla_S_L1_p(var)\n\n',var.solutionExists));
		error('Wrong value')
end
endfunction

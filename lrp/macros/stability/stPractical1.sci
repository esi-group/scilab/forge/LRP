// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 21:00:00 by L.Hladowski
// 
// ======================================================================
// Stabillity - practical 1
// ======================================================================
//
// r(A) < 1  and  r(D_0) < 1
//
// where:
// 	r(.) = \max { | \lambda |: \lambda \in \Lambda(.) }
//
// ====================================================================== 
function [lrp]=stPractical1(lrp)
TCL_SetVar("LRP_Help_Status","**** WORKING ****");
ncl=lines(); // Save the current pager options
lines(0); // Remove the [more] prompt

[ind,last]=findLRPIndex(lrp,"stability","stPractical1");
//if is not this field
if ind == -1 then
   //if is not 'stability' and 'indStability' field in lrp(1)
   if last == -2 then
      [lrp]=addStabilityToLRP(lrp); //this set lrp.indStability to 1
   else
      lrp.indStability=last+1;
   end
else
    lrp.indStability=ind;
end

eigenValuesA=spec(lrp.mat.A);
lambdaMaxA= max(abs(eigenValuesA));
info1= lambdaMaxA < 1;


eigenValuesD0=spec(lrp.mat.D0);
lambdaMaxD0= max(abs(eigenValuesD0));
info2= lambdaMaxD0 < 1;

TCL_SetVar("LRP_Help_Status","Ready");

if (info1 & info2) then
	//save results to lrp.stability(indStability)
    lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_P1";...
				 "functionName"; ...
         "solutionExists"; ...
         "eigenValuesA"; "lambdaMaxA"; ...
         "eigenValuesD0"; "lambdaMaxD0"],...
         "stPractical1", ...
         1, ... //solution exists - true
         eigenValuesA, lambdaMaxA,...
         eigenValuesD0, lambdaMaxD0);

else
	//save results to lrp.stability(indStability)
	//save results to lrp.stability(indStability)
    lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_P1";...
				 "functionName"; ...
         "solutionExists"; ...
         "eigenValuesA"; "lambdaMaxA"; ...
         "eigenValuesD0"; "lambdaMaxD0"],...
         "stPractical1", ...
				 0, ... //Unstable
         eigenValuesA, lambdaMaxA,...
         eigenValuesD0, lambdaMaxD0);
end
lines(ncl(2),ncl(1)); // Load the previous pager options
endfunction

// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 21:27:00 by L.Hladowski
// 
// ======================================================================
// Stabillity - asymptotic 1
// ======================================================================
// Author:  K. Galkowski, E. Rogers, S. Xu, J. Lam, D. H. Ownes,
//          B. Sulikowski, W. Paszke
// Article: LMI based stability analisis and robust controller design
//          for discrete linear repetetive processes
//
// pp. 6, eq. (18)
//
// P=P' > 0
// Q=Q' > 0
// [Ad1'*P*Ad1+Q-P,      Ad1'*P*Ad2]
// [Ad2'*P*Ad1,          Ad2'*P*Ad2 - Q] < 0
//
// ======================================================================
function [lrp]=stAlongThePassLMI1(lrp)
TCL_SetVar("LRP_Help_Status","**** WORKING ****");
// Prepare the screen for the LONG LMI output
ncl=lines(); // Save the current pager options
lines(0); // Remove the [more] prompt
[ind,last]=findLRPIndex(lrp,"stability","stAlongThePassLMI1");
//if is not this field
if ind == -1 then
   //if is not 'stability' and 'indStability' field in lrp(1)
   if last == -2 then
   	disp(typeof(lrp));
      [lrp]=addStabilityToLRP(lrp); //this set lrp.indStability to 1
   else
      lrp.indStability=last+1;
   end
else
    lrp.indStability=ind;
end
disp('ok');
//prepare data
Ad1=[lrp.mat.A lrp.mat.B0; zeros(lrp.mat.C) zeros(lrp.mat.D0)];
Ad2=[zeros(lrp.mat.A) zeros(lrp.mat.B0); lrp.mat.C lrp.mat.D0];

//initial values of variables
Pi=zeros(Ad1);
Qi=zeros(Ad1);
XListIn=list(Pi,Qi);

options=lmiSolverOptions();
errcatch(-1,"continue");
[XListOut]=lmisolver(XListIn,stAlongThePassLMI1_eval,options);
TCL_SetVar("LRP_Help_Status","Ready");
if (iserror()) then

   //save results to lrp.mat.Controller(indController)
   lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_L1";...
      "functionName";"solutionExists"], "stAlongThePassLMI1", 2);    //unknown result - test inconclusive

   errclear();
else
    [P,Q]=XListOut(:);

    //save results to lrp.mat.Controller(indController)
    lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_L1";...
				 "functionName"; ...
         "solutionExists"; ...
         "P";"Q"],...
         "stAlongThePassLMI1", ...
				 1, ... //solution exists - true
         P,Q);
end
lines(ncl(2),ncl(1)); // Load the previous pager options
endfunction


//function for LMI
function [LME,LMI,OBJ]=stAlongThePassLMI1_eval(XListIn)
[P,Q]=XListIn(:);

OBJ=[];

LME=list(...
    P-P',...
    Q-Q'...
);

LMI=list(...
    -[Ad1'*P*Ad1+Q-P,  Ad1'*P*Ad2; ...
      Ad2'*P*Ad1,      Ad2'*P*Ad2 - Q]-eye(),...
    P-eye(),...
    Q-eye()...
);
endfunction

// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 21:27:00 by L.Hladowski
//
// ======================================================================
// Stabillity - practical LMI 1
// ======================================================================
//
// P=P' > 0
// Q=Q' > 0
// A'*P*A - P < 0
// D0'*Q*D0 - Q < 0
//
// ======================================================================
function [lrp]=stPracticalLMI1(lrp)
TCL_SetVar("LRP_Help_Status","**** WORKING ****");
// Prepare the screen for the LONG LMI output
ncl=lines(); // Save the current pager options
lines(0); // Remove the [more] prompt

[ind,last]=findLRPIndex(lrp,"stability","stPracticalLMI1");
//if is not this field
if ind == -1 then
   //if is not 'stability' and 'indStability' field in lrp(1)
   if last == -2 then
      [lrp]=addStabilityToLRP(lrp); //this set lrp.indStability to 1
   else
      lrp.indStability=last+1;
   end
else
    lrp.indStability=ind;
end

//initial values of variables
Pi=zeros(lrp.mat.A);
Qi=zeros(lrp.mat.D0);
XListIn=list(Pi,Qi);

options=lmiSolverOptions();
errcatch(-1,"continue");
[XListOut]=lmisolver(XListIn,stPracticalLMI1_eval,options);
TCL_SetVar("LRP_Help_Status","Ready");

if (iserror()) then

   //save results to lrp.controller(indController)
   lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_LP1";...
			"functionName"; ...
     	"stPracticalLMI1", ...	
		  "solutionExists"], 2);    //test inconclusive

   errclear();
else
    [P,Q]=XListOut(:);

    //save results to lrp.controller(indController)
    lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_LP1";...
				 "functionName"; ...
         "solutionExists"; ...
         "P";"Q"],...
     		 "stPracticalLMI1", ...
         1, ... //solution exists - true
         P,Q);
end
lines(ncl(2),ncl(1)); // Load the previous pager options
endfunction


//function for LMI
function [LME,LMI,OBJ]=stPracticalLMI1_eval(XListIn)
[P,Q]=XListIn(:);

OBJ=[];

LME=list(...
    P-P',...
    Q-Q'...
);

LMI=list(...
    -[lrp.mat.A'*P*lrp.mat.A - P]-eye(),...
    -[lrp.mat.D0'*Q*lrp.mat.D0 - Q]-eye(),...
    P-eye(),...
    Q-eye()...
);
endfunction

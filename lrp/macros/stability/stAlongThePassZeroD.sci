// Author Blazej Cichy
// e-mail: B.Cichy@issi.uz.zgora.pl,
//
// Institute of Control & Computation Engineering,
// Faculty of Electrical Engineering, Computer Science and Telecommunications,
// University of Zielona Gora,
// POLAND
//
// Last revised: 2006-09-16 21:27:00 by L.Hladowski
// 
// ======================================================================
// Stabillity along the pass for D = 0
// ======================================================================
function [lrp]=stAlongThePassZeroD(lrp)
TCL_SetVar("LRP_Help_Status","**** WORKING ****");
// Prepare the screen for the LONG LMI output
ncl=lines(); // Save the current pager options
lines(0); // Remove the [more] prompt
[ind,last]=findLRPIndex(lrp,"stability","stAlongThePassZeroD");
//if is not this field
if ind == -1 then
   //if is not 'stability' and 'indStability' field in lrp(1)
   if last == -2 then
   	disp(typeof(lrp));
      [lrp]=addStabilityToLRP(lrp); //this set lrp.indStability to 1
   else
      lrp.indStability=last+1;
   end
else
    lrp.indStability=ind;
end
if and(lrp.mat.D==0)
	// D = 0 - we can use this test
	disp('ok');
else
	disp('TEST **CANNOT** BE USED - lrp.mat.D <> 0');
   lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_LZD";...
      "functionName";"solutionExists"], "stAlongThePassZeroD", 2);    //unknown result - test inconclusive
end

//prepare data
G = lrp.mat.A + lrp.mat.B0*(eye()-lrp.mat.D0)^(-1)*lrp.mat.C;
eigG = spec(G);
printf('module of the eigenvalues (must be <1)\n');
disp(abs(eigG))
if 	or(abs(eigG)>=1) then
	stable = 0;
else
	stable = 1;
end
//initial values of variables

TCL_SetVar("LRP_Help_Status","Ready");
//save results to lrp.mat.Controller(indController)
lrp.stability(lrp.indStability)=tlist(["lrp_Sys_Cla_S_LZD";...
		 "functionName"; ...
     "solutionExists"; ...
     "G";"eigG"],...
     "stAlongThePassZeroD", ...
		 stable, ... //solution exists - true
     G,eigG);
lines(ncl(2),ncl(1)); // Load the previous pager options
endfunction

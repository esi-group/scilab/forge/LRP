function [lrp]=addStabilityToLRP(lrp)
if isThisEntry(lrp(1), "stability") == %F then
   [lrp]=addEntryToTList(lrp,"stability", list());
end
if isThisEntry(lrp(1), "indStability") == %F then
   [lrp]=addEntryToTList(lrp,"indStability", 1); //set on 1 always !
end
endfunction
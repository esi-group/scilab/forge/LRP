//info == 1 --> contain
//info == 0 --> no contain
//else error
function [info]=lrpContainsStability(lrp)

if isThisEntry(lrp(1), "stability") == %F then
   info1=0; //not found
else
   info1=1; //found
end

if isThisEntry(lrp(1), "indStability") == %F then
   info2=0; //not found
else
   info2=1; //found
end

//final check
if (info1==0) & (info2==0) then
   info=0; //not found
elseif (info1==1) & (info2==1)
   info=1; //found
else
   printf("Both fields: >>stability<< and >>indStability<< are required in ''lrp''.\n");
   error("One field is missing in ''lrp''.");
end
endfunction


==============================================================
LRP Toolikt for the Linear Repetitive Processes
Written by L. Hladowski, B. Cichy, K. Galkowski, B. Sulikowski
Copyright (c)2005 by the Institute of Control and Computation Engineering,
                         University of Zielona Gora, Poland.
==============================================================

To run the Toolkit, select "Run LRP" in the
"Start->Programs->LRP Toolkit 0.1 for (ScilabVersion)"
menu. This will run the Scilab environment and additionally add a new menu entry,
entitled "LRP" (as the last right menu).

To run the GUI for the Toolkit, select "LRP->Start GUI".

To create a new system using the GUI, select
"Create a new system". Note that the matrices need to be entered in the Scilab format.
For example, to enter the matrix of
 1 2
 3 4
 5 6
enter:
[1 2; 3 4; 5 6]
At the "Enter the system matrices" page, to simplify entering a matrix,
click on the "..." button. To generate a random matrix,
click the "rand" button. To automatically set all the matrices to random values, click
the "Randomize all" button at the bottom of the screen.

After creating the LRP, click Plots button to create various plots.

To exit the GUI, click "Exit" on the main page.
